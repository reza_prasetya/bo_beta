var tiles1 = [
    {
        name : "Registrasi",
        thumbnail : "../public/images/icon/registrasi.png",
        content : "Registrasi",
        url : "/",
        size : "4x2",
        theme : "softblue",
        link : "registrasi",
        target: "_self" 
    }, {
        name : "Deposit",
        thumbnail : "../public/images/icon/deposit.png",
        content : "Deposit",
        url : "/",
        size : "4x2",
        theme : "darkblue",
        link : "deposit",
        target: "_self"
    }, {
        name : "Sign_Out",
        thumbnail : "../public/images/icon/signout.png",
        content : "Sign Out",
        url : "/",
        size : "2x2",
        theme : "darkred",
        link : "authenticate",
        target: "_self"
    }
];

Metro.HTML.addContainer({
    size: "full",
    widgets: tiles1
});
