var tiles1 = [
	{
		name : "Verifikasi",
		thumbnail : "images/icon/cash-register.png",
		content : "Data Verifikasi",
		url : "/",
		size : "4x2",
		theme : "softblue",
		link : "verifikasi.html",
		target: "_self"	
	}, {
		name : "Sign_Out",
		thumbnail : "images/icon/signout.png",
		content : "Sign Out",
		url : "/",
		size : "2x2",
		theme : "darkred",
		link : "../index.html",
		target: "_self"
	}
];
Metro.HTML.addContainer({
	size : "full",
	widgets : tiles1
});
