var tiles1 = [
	{
		name : "User",
		thumbnail : "images/icon/retur.png",
		content : "Registrasi",
		url : "/",
		size : "4x2",
		theme : "softblue",
		link : "registrasi.html",
		target: "_self"	
	}, {
		name : "Troubleshooting",
		thumbnail : "images/icon/cash-register.png",
		content : "Deposit",
		url : "/",
		size : "4x2",
		theme : "darkblue",
		link : "userguide/index.html",
		target: "_blank"
	// }, {
	// 	name : "Penjualan",
	// 	thumbnail : "images/icon/cash-register.png",
	// 	content : "Penjualan",
	// 	url : "/",
	// 	size : "4x2",
	// 	theme : "green",
	// 	link : "penjualan.html",
	// 	target: "_self"
	// }, {
	// 	name : "Retur",
	// 	thumbnail : "images/icon/retur.png",
	// 	content : "Retur",
	// 	url : "/",
	// 	size : "4x2",
	// 	theme : "blue",
	// 	link : "retur-jual.html",
	// 	target: "_self"
	}, {
		name : "Sign_Out",
		thumbnail : "images/icon/signout.png",
		content : "Sign Out",
		url : "/",
		size : "2x2",
		theme : "darkred",
		link : "../index.html",
		target: "_self"
	}
];
Metro.HTML.addContainer({
	size : "full",
	widgets : tiles1
});
