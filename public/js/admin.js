function makeRandomTile(a) {
    return {
        name: "tile_000" + a,
        thumbnail: "../public/images/help.png",
        content: "Unknown",
        url: "pages/blank.php",
        size: "2x2",
        theme: "darkmagenta",
        target: "_self"
    }
}
var tiles1 = [
	{
		name : "Stakeholder",
		thumbnail: "../public/../public/images/icon/helpdesk.png",
		content : "Stakeholder",
		url : "/",
		size : "4x2",
		theme : "magenta",
		link : "stakeholder",
		target: "_self"	
	}, {
		name : "Override Deposit",
		thumbnail: "../public/../public/images/icon/sync.png",
		content : "Override Deposit",
		url : "/",
		size : "4x2",
		theme : "darkmagenta",
		link : "override_list",
		target: "_self"	
	}, {
		name : "Detail Data",
		thumbnail: "../public/../public/images/icon/userguide.png",
		content : "Detail Data",
		url : "/",
		size : "4x2",
		theme : "softblue",
		link : "detail_data",
		target: "_self"	
	}, {
		name : "Kebijakan",
		thumbnail: "../public/../public/images/icon/pengaturan.png",
		content : "Kebijakan",
		url : "/",
		size : "4x2",
		theme : "softgreen",
		link : "kebijakan",
		target: "_self"	
	}, {
		name : "Sign_Out",
		thumbnail: "../public/../public/images/icon/signout.png",
		content : "Sign Out",
		url : "/",
		size : "2x2",
		theme : "darkred",
		link : "../index",
		target: "_self"
	}
];
Metro.HTML.addContainer({
	size : "full",
	widgets : tiles1
});
