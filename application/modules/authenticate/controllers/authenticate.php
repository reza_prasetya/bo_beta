<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authenticate extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->module = 'authenticate';
		// $this->module_cashier = 'cashier';
		// $this->module_accountant = 'accounting';
		// $this->module_head = 'head';
		// $this->module_technician = 'technician';
		// $this->module_gudang = 'gudang';
		// $this->module_master = 'master';
		// $this->module_laporan = 'laporan';
		$this->cname = 'authenticate';
		$this->load->model('Mdl_login');
		// echo $this->config->item('name');exit;
	}

	public function index()
	{
		$data['cname'] = $this->module;
		$data['title'] = "Authentication";
		$data['content'] = $this->load->view($this->module.'/login',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function login()
	{
		// print_r('test');
		$param = $this->input->post();
		$exist = $this->Mdl_login->login($param);
		// print_r($param);exit;
		if (!is_null($exist)) {
			$module = json_decode($exist[0]->module);
			$this->session->set_userdata('basmalahsession',$exist);
			$redirect = $module[0];
		}else{
			$redirect = $this->cname;
		}
		// print_r($redirect);exit;
		redirect($redirect);
	}
}

/* End of file signin.php */
/* Location: ./application/modules/cashier/controllers/signin.php */