<?php //$user = $this->session->userdata('basmalahsession'); ?>
<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block" style="min-height:650px;">
		<!-- Start Content -->
		<div class="row">
			<div class="col-sm-12">
				<div class="row" style='margin-bottom:10px;'>
					<div class="col-sm-3">
						<button class="btn btn-block btn-lg btn-primary" onClick="browseButton()"><i class="icon-reorder"></i> HOME</button>
					</div>
					<div class="col-sm-3">
						<button class="btn btn-block btn-lg btn-primary" onClick="searchButton()"><i class="icon-search"></i> SEARCH</button>
					</div>
					<div class="col-sm-3">
						<button class="btn btn-block btn-lg btn-primary" onClick="scanButton()"><i class="icon-barcode"></i> SCAN</button>
					</div>
					<div class="col-sm-3">
						<button class="btn btn-block btn-lg btn-primary" data-toggle="modal" href="#modal-tutup"><i class="icon-money"></i> TUTUP KASIR</button>
					</div>
				</div>
				
				<div id="selling-div">
				<div class="block" style="padding-bottom:20px;">
					<div class="row">
						<div class="col-sm-4" style="margin-bottom:8px;">
							<blockquote style="margin: 0px 0px 5px;">
								<p><i class="icon-laptop dataNote">&nbsp;Workstation 1</i></p>
							</blockquote>
						</div>
						<div class="col-sm-4" style="margin-bottom:8px;">
							<blockquote style="margin: 0px 0px 5px;">
								<p><i class="icon-file-text dataInvoice">&nbsp;<span id="Date"></span>,&nbsp;<span id="hours"></span>:<span id="min"></span>:<span id="sec"></span></i></p>
							</blockquote>
						</div>
						<div class="col-sm-4" style="margin-bottom:8px;">
							<blockquote style="margin: 0px 0px 5px;">
								<p><i class="icon-user"></i>&nbsp;Frontliner 1<?php //echo $user[0]->nama; ?></p>
							</blockquote>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-8">
							<div class="block" id="trans_referral">
					
								<div class="row">
									<div class="block-section">
										<div class="input-group input-group-lg">
											<input id="id_referral" name="id_referral" class="form-control" placeholder="INPUT REFERRAL!" type="text">
											<div class="input-group-btn">
												<button class="btn btn-default"><i class="icon-barcode icon-fixed-width"></i></button>
											</div>
										</div>
									</div>
									<div class="ms-message-list list-group referral-list">
										<a href="javascript:void(0)" class="list-group-item" >
											<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
											<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Nama Referral</h4>
										</a>
										<a href="javascript:void(0)" class="list-group-item" >
											<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
											<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Nama Referral</h4>
										</a>
										<a href="javascript:void(0)" class="list-group-item" >
											<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
											<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Nama Referral</h4>
										</a>
										<a href="javascript:void(0)" class="list-group-item" >
											<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
											<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Nama Referral</h4>
										</a>
										<a href="javascript:void(0)" class="list-group-item" >
											<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
											<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Nama Referral</h4>
										</a>
										<a href="javascript:void(0)" class="list-group-item" >
											<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
											<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Nama Referral</h4>
										</a>
										<a href="javascript:void(0)" class="list-group-item" >
											<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
											<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Nama Referral</h4>
										</a>
										<a href="javascript:void(0)" class="list-group-item" >
											<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
											<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Nama Referral</h4>
										</a>
										<a href="javascript:void(0)" class="list-group-item" >
											<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
											<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Nama Referral</h4>
										</a>
										<a href="javascript:void(0)" class="list-group-item" >
											<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
											<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Nama Referral</h4>
										</a>
									</div>
								</div>
								<br/>
								<div class="row"  style="margin-bottom: 20px;">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-block btn-lg btn-success" onClick="nextPendataan()">SAVE & NEXT</button>
									</div>
								</div>
							</div>
							<div class="block" id="trans_pendataan">
								<div class="block">
						          <form id="form-validation" action="dashboard.html" method="post" class="form-horizontal" style="margin-left:10%;">
						          	<div class="form-group">
						              <label class="col-md-2 control-label" for="val_referral">Referral</label>
						              <div class="col-md-10">
						                <div class="input-group">
						                  <input type="text" id="val_referral" name="val_referral" class="form-control">
						                  <span class="input-group-addon"><i class="icon-user icon-fixed-width"></i></span>
						                </div>
						              </div>
						            </div>
						            <div class="form-group">
						              <label class="col-md-2 control-label" for="val_no_ktp">NIK</label>
						              <div class="col-md-3">
						                <div class="input-group">
						                  <input type="text" id="val_no_ktp" name="val_no_ktp" class="form-control" />
						                  <span class="input-group-addon"><i class="icon-pencil icon-fixed-width"></i></span>
						                </div>
						              </div>
						              <div class="col-md-3">
						                <div class="input-group">
						                  <input type="text" id="val_no_ktp2" name="val_no_ktp2" class="form-control" />
						                  <span class="input-group-addon"><i class="icon-pencil icon-fixed-width"></i></span>
						                </div>
						              </div>
						              <div class="col-md-3">
						                <div class="input-group">
						                  <input type="text" id="val_no_ktp3" name="val_no_ktp3" class="form-control" />
						                  <span class="input-group-addon"><i class="icon-pencil icon-fixed-width"></i></span>
						                </div>
						              </div>
						            </div>
						            
						            <div class="form-group">
						              <label class="col-md-2 control-label" for="val_nama">Nama</label>
						              <div class="col-md-10">
						                <div class="input-group">
						                  <input type="text" id="val_nama" name="val_nama" class="form-control">
						                  <span class="input-group-addon"><i class="icon-user icon-fixed-width"></i></span>
						                </div>
						              </div>
						            </div>
						            <div class="form-group">
						              <label class="col-md-2 control-label" for="val_jns_kl">Jenis Kelamin</label>
						              <div class="col-md-10">
						                <div class="input-group">
						                  <input type="text" id="val_jns_kl" name="val_jns_kl" class="form-control">
						                  <span class="input-group-addon"><i class="icon-ok-circle icon-fixed-width"></i></span>
						                </div>
						              </div>
						            </div>
						            <div class="form-group">
						              <label class="col-md-2 control-label">Tanggal Lahir</label>
						              <div class="col-md-6">
						                <input type="text" id="example-datepicker2" name="example-datepicker2" class="form-control input-datepicker text-center" data-date-format="dd/mm/yy" placeholder="dd/mm/yy">
						              </div>
						            </div>
						            <div class="form-group">
						              <label class="col-md-2 control-label" for="val_brth_plc">Tempat Lahir</label>
						              <div class="col-md-10">
						                <div class="input-group">
						                  <input type="text" id="val_brth_plc" name="val_brth_plc" class="form-control">
						                  <span class="input-group-addon"><i class="icon-spinner icon-fixed-width"></i></span>
						                </div>
						              </div>
						            </div>
						            <div class="form-group">
						              <label class="col-md-2 control-label" for="val_email">Email</label>
						              <div class="col-md-10">
						                <div class="input-group">
						                  <input type="text" id="val_email" name="val_email" class="form-control">
						                  <span class="input-group-addon"><i class="icon-envelope icon-fixed-width"></i></span>
						                </div>
						              </div>
						            </div>
						            <div class="form-group">
						              <label class="col-md-2 control-label" for="val_kd_pos">Kode Pos</label>
						              <div class="col-md-10">
						                <div class="input-group">
						                  <input type="text" id="val_kd_pos" name="val_kd_pos" class="form-control">
						                  <span class="input-group-addon"><i class="icon-inbox icon-fixed-width"></i></span>
						                </div>
						              </div>
						            </div>
						            <div class="form-group">
						              <label class="col-md-2 control-label" for="val_asal">Alamat Asal</label>
						              <div class="col-md-10">
						                <div class="input-group">
						                  <input type="text" id="val_asal" name="val_asal" class="form-control">
						                  <span class="input-group-addon"><i class="icon-globe icon-fixed-width"></i></span>
						                </div>
						              </div>
						            </div>
						            <div class="form-group">
						              <label class="col-md-2 control-label" for="val_skr">Alamat Sekarang</label>
						              <div class="col-md-10">
						                <div class="input-group">
						                  <input type="text" id="val_skr" name="val_skr" class="form-control">
						                  <span class="input-group-addon"><i class="icon-home icon-fixed-width"></i></span>
						                </div>
						              </div>
						            </div>
						            <div class="form-group">
						              <label class="col-md-2 control-label" for="val_nama">No Telp</label>
						              <div class="col-md-10">
						                <div class="input-group">
						                  <input type="text" id="val_telp" name="val_telp" class="form-control">
						                  <span class="input-group-addon"><i class="icon-phone icon-fixed-width"></i></span>
						                </div>
						              </div>
						            </div>
						            <div class="form-group">
						              <label class="col-md-2 control-label">Yakin ?</label>
						              <div class="col-md-10">
						                <div class="checkbox">
						                  <label for="val_terms">
						                    <input type="checkbox" id="val_terms" name="val_terms" value="1"> Saya yakin
						                  </label>
						                </div>
						              </div>
						            </div>
						            <br />
						          </form>
						        </div>
								<!-- <div class="row"  style="margin-bottom: 10px;">
									<div class="col-sm-6">
										<button class="btn btn-block btn-lg btn-danger" onclick="cancel()">VOID</button>
									</div>
									<div class="col-sm-6">
										<button class="btn btn-block btn-lg btn-primary" onclick="suspend()">SUSPEND</button>
									</div>
								</div> -->
								<div class="row"  style="margin-bottom: 20px;">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-block btn-lg btn-success" onClick="nextDeposit()">SAVE & NEXT</button>
									</div>
								</div>
							</div>
							<div class="block" id="trans_suspend">
								<div class="table-responsive">
									<table id="general-table" class="table table-stripped">
										<thead>
											<tr>
												<th class="text-center">#</th>
												<th>Invoice</th>
												<th>Produk</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody class="dataSuspend">
											<tr>
											  	<td colspan="8" align="center" style="font-size:14px;font-weight:bold;">No Data Available</td>
											</tr>
											<!-- <tr>
												<td>1</td>
												<td>Marimas Buah</td>
												<td>2</td>
												<td>5000</td>
												<td>10%</td>
												<td>10000</td>
									            <td id="r1c1" data-id="1" data-nama="Edit Marimas Buah 1" align="center">
									            	<div class="btn-group btn-group-xs">
														<a class="btn btn-default" title="" data-toggle="tooltip" href="javascript:void(0)" data-original-title="Edit">
															<i class="icon-edit"></i>
															Ubah Qty
														</a>
													</div>
												</td>
									            <td id="r1c2" data-id="1" data-nama="Del Marimas Buah 1" align="center">
													<div class="btn-group btn-group-xs">
														<a class="btn btn-default" title="" data-toggle="tooltip" href="javascript:void(0)" data-original-title="Delete">
															<i class="icon-remove"></i>
															Hapus
														</a>
													</div>
									            </td>
									        </tr> -->
										</tbody>
									</table>
								</div>
								
							</div>

							<div class="block full" style="margin-top:10px;" id="trans_deposit">
								<div class="row">
									
								</div>
								<div class="row">
									<div class="col-sm-12 pull-right">
										<div class="row" style="margin-bottom: 10px;">
											<div class="form-horizontal">
												<div class="form-group">
													<div class="col-md-12">
														
													</div>
												</div>
												<div class="form-group" style="display:none;">
													<div class="col-md-12">
														<input id="dumpTotal" name="dumpTotal" class="form-control" placeholder="Bayar Sejumlah" type="text" style="height:45px;">
													</div>
												</div>
												<div class="form-group" style="display:none;">
													<div class="col-md-12">
														<input id="total" name="total" class="form-control" placeholder="Bayar Sejumlah" type="text" style="height:45px;">
													</div>
												</div>
												<div class="form-group id_pelanggan" style="display:none;">
													<div class="col-md-12">
														<input id="pelanggan_id" readonly name="pelanggan_id" class="form-control" placeholder="ID Pelanggan" type="text" style="height:45px;">
													</div>
												</div>
												<div class="form-group nama_pelanggan" style="display:none;">
													<div class="col-md-12">
														<input id="pelanggan_nama" readonly name="pelanggan_nama" class="form-control" placeholder="Nama Pelanggan" type="text" style="height:45px;">
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
														<select id="cara" name="cara" class="form-control" size="1" style="height:45px;">
															<!-- <option value="0">Pilih Cara Pembayaran</option> -->
															<option value="2">Cash</option>
															<option value="3">Giro</option>
															<option value="3">Voucher</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-12">
														<input id="bayar" name="bayar" class="form-control" placeholder="Bayar Sejumlah" type="text" style="height:45px;">
													</div>
												</div>
												<div class="form-group" style="display:none;">
													<div class="col-md-12">
														<input id="kembali" name="kembali" class="form-control" placeholder="Kembali Sejumlah" type="text" style="height:45px;">
													</div>
												</div>
											</div>
										</div>
										<div class="row" style="margin-bottom: 10px;">
											<div class="col-sm-12">
												<button onclick="bayar();" class="btn btn-block btn-lg btn-primary">Bayar</button>
											</div>
											
										</div>
									</div>
								</div>
							</div>

						</div>

						<div class="col-sm-4" style="float:right;">
							<div class="row">
								<div class="col-md-12" style="border: 2px solid #EEE;">
									<h4>Referral</h4>
									<h2 class="text-success"><span class="viReferral">-</span></h2>
								</div>
								<br/>
								<div class="col-md-12" style="border: 2px solid #EEE;">
									<h4>Pairing</h4>
									<h2 class="text-success"><span class="viPairing">-</span></h2>
								</div>
								<br/>
								<div class="col-sm-12">
									<div class="alert alert-success">
										<h4 style="margin-top:10px;">
											<strong>Administration Fee : </strong>
											<br/>
											<span class="pull-right">Rp.&nbsp;<span class="viAdministration"></span>,-</span>
										</h4>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="alert alert-success">
										<h4 style="margin-top:10px;">
											<strong>Deposit : </strong>
											<br/>
											<span class="pull-right">Rp.&nbsp;<span class="viDeposit"></span>,-</span>
										</h4>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="alert alert-success">
										<h4 style="margin-top:10px;">
											<strong>Completion : </strong>
											<br/>
											<span class="pull-right">Rp.&nbsp;<span class="viCompletion"></span>,-</span>
										</h4>
									</div>
								</div>
							</div>

							<!-- <div class="row">
								<div class="col-sm-12">
									<button class="btn btn-block btn-lg btn-default" onClick="browseButton()"><i class="icon-copy"></i> TOTAL TRANSAKSI &nbsp;(<span class="total_transaksi">0</span>)</button>
								</div>
								<div class="col-sm-12">
									<button class="btn btn-block btn-lg btn-default" onClick="list_void()"><i class="icon-copy"></i> TOTAL VOID &nbsp;(<span class="total_void">0</span>)</button>
								</div>
								<div class="col-sm-12">
									<button class="btn btn-block btn-lg btn-default" onClick="list_suspend()"><i class="icon-copy"></i> TOTAL SUSPEND &nbsp;(<span class="total_suspend">0</span>)</button>
								</div>
							</div> -->
						</div>
					</div>
				</div>
				
				</div>

				
				<div class="block full" style="margin-top:10px;" id="browse-div">
					<div class="row">
						<div class="col-md-4" style="margin-top: -30px;">
							<h4 class="sub-header">CATEGORIES</h4>
							<div class="ms-message-list list-group">
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
							</div>
						</div>
						<div class="col-md-8" style="margin-top: -30px;">
							<h4 class="sub-header">LIST ITEMS</h4>
							<div class="ms-message-list list-group">
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
									<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="block full" style="margin-top:10px;" id="search-div">
					<div class="row">
						<div class="block-section">
							<span id="notif"></span>
							<form id="form-searchproduk" method="post">
								<div class="input-group input-group-lg">
									<div class="input-group-btn">
										<select id="category" name="category" class="form-control" size="1" style="width: 200px; height: 45px; font-size: 17px; background-color: #D8D8D8;">
											<option value="all">All Category</option>
											<?php foreach ($category as $key => $value): ?>
												<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
											<?php endforeach ?>
										</select>
									</div>
									<input id="keyword" name="keyword"  class="form-control" placeholder="Search Keyword.." type="text">
									<div class="input-group-btn">
										<button type="submit" class="btn btn-default"><i class="icon-search icon-fixed-width"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-top: -30px;">
							<h4 class="sub-header">SEARCH RESULT</h4>
							<div class="ms-message-list list-group" id="list_produk">
								<?php foreach ($barang as $key => $value): ?>
									<a href="javascript:void(0)" class="list-group-item" >
										<img src="<?php echo base_url(); ?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
										<h4 class="list-group-item-heading pull-right"><strong><?php echo $value->price1; ?></strong></h4>
										<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;"><?php echo $value->name; ?></h4>
									</a>	
								<?php endforeach ?>
								
							</div>
						</div>
					</div>
				</div>
				<div class="block full" style="margin-top:10px;" id="scan-div">
					<div class="row">
						<div class="block-section">
							
							<div class="input-group input-group-lg">
									<input id="barcode_produk" name="barcode_produk" class="form-control" placeholder="Barcode.." type="text">
									<div class="input-group-btn">
										<button type="submit" class="btn btn-default"><i class="icon-search icon-fixed-width"></i></button>
									</div>
							</div>
							
						</div>
					</div>
					<div class="row" style="border-top:1px dotted black;">
						<div class="list-group" id="list_scanproduk">
							
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- End Content -->
	</div>
	<footer class="clearfix">
		<div class="pull-right">
			Design and Developed by <a target="_blank" href="http://www.utomosakti.com" target="_blank">CV. Utomo Sakti</a>
		</div>
		<div class="pull-left">
			<span id="year-copy"></span> &copy; <a target="_blank" href="http://www.basmalah.com" target="_blank">Basmallah</a>
		</div>
	</footer>
</div>

<div id="modal-regular" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Buka Kasir</h4>
            </div>
            <div class="modal-body">
					<div class="row" style="margin-bottom: 10px;">
						<div class="form-horizontal">
							<div class="form-group" style="display:none;">
								<div class="col-md-12">
									<input id="invoice" name="invoice" class="form-control buka_kasir" placeholder="Cashdraw" type="text" style="height:45px;">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input id="awal" name="awal" class="form-control awal" placeholder="Kas Awal" type="text" style="height:45px;">
								</div>
							</div>
						</div>
					</div>
				</div>
            <div class="modal-footer">
            		<div class="row" style="margin-bottom: 10px;">
						<div class="col-sm-12">
							<button onclick="open_cash();" class="btn btn-block btn-lg btn-primary">Buka Kasir</button>
						</div>
					</div>
            </div>
        </div>
    </div>

</div>

<div id="modal-cekmember" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form id = "formcekmember" method="post">
        		<div class="modal-header">
	                <h4 class="modal-title">Member</h4>
	            </div>
	            <div class="modal-body">
						<div class="row" style="margin-bottom: 10px;">
							<div class="form-horizontal">
								<div class="form-group">
									<div class="col-md-12">
										<input id="code_member" name="code_member" class="form-control" placeholder="ID Member" type="text" style="height:45px;">
									</div>
								</div>
							</div>
						</div>
					</div>
	            <div class="modal-footer">
	            		<div class="row" style="margin-bottom: 10px;">
							<div class="col-sm-6">
								<a onClick="cekmember()" class="btn btn-block btn-lg btn-primary">Cek Member</a>
							</div>
							<div class="col-sm-6">
								<a onClick="asReguler()" class="btn btn-block btn-lg btn-default" data-dismiss="modal">Log as Reguler</a>
							</div>
						</div>
	            </div>
        	</form>
            
        </div>
    </div>

</div>


<div id="modal-add" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Transaction</h4>
            </div>
            <div class="modal-body">
					<div class="row" style="margin-bottom: 10px;">
						<div class="form-horizontal">
							<div class="form-group" style="display:none;">
								<div class="col-md-12">
									<input id="invoice" name="invoice" class="form-control invoice" placeholder="Invoice" type="text" style="height:45px;">
								</div>
							</div>
							<div class="form-group" style="display:none;">
								<div class="col-md-12">
									<input id="kas" name="kas" class="form-control kas" placeholder="Cashdraw" type="text" style="height:45px;">
								</div>
							</div>
							<div class="form-group" style="display:none;">
								<div class="col-md-12">
									<input id="barcode" name="barcode" class="form-control barcode" placeholder="Barcode" type="text" style="height:45px;">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input id="qty" name="qty" class="form-control qty" placeholder="Qty" type="text" style="height:45px;">
								</div>
							</div>
						</div>
					</div>
				</div>
            <div class="modal-footer">
            		<div class="row" style="margin-bottom: 10px;">
						<div class="col-sm-12">
							<button id="tambahkan_produk" data-aksi="kurang" onclick="add_transaction();" class="btn btn-block btn-lg btn-primary">Tambahkan</button>
						</div>
					</div>
            </div>
        </div>
    </div>

</div>

<div id="modal-ubah" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Transaction</h4>
            </div>
            <div class="modal-body">
					<div class="row" style="margin-bottom: 10px;">
						<div class="form-horizontal">
							<div class="form-group" style="display:none;">
								<div class="col-md-12">
									<input id="ident" name="ident" class="form-control ident" placeholder="Ident" type="text" style="height:45px;">
								</div>
							</div>
							<div class="form-group" style="display:none;">
								<div class="col-md-12">
									<input id="price" name="price" readonly class="form-control price" placeholder="Price" type="text" style="height:45px;">
								</div>
							</div>
							<div class="form-group" style="display:none;">
								<div class="col-md-12">
									<input id="discount" name="discount" class="form-control discount" placeholder="Discount" type="text" style="height:45px;">
								</div>
							</div>
							<div class="form-group" style="display:none;">
								<div class="col-md-12">
									<input id="qty" name="qty" readonly class="form-control qty" placeholder="Qty" type="text" style="height:45px;">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input id="qty_edit" name="qty_edit" class="form-control qty_edit" placeholder="Qty" type="text" style="height:45px;">
								</div>
							</div>
						</div>
					</div>
				</div>
            <div class="modal-footer">
            		<div class="row" style="margin-bottom: 10px;">
						<div class="col-sm-12">
							<button onclick="edit_transaction();" class="btn btn-block btn-lg btn-primary">Ubah</button>
						</div>
					</div>
            </div>
        </div>
    </div>

</div>

<div id="modal-tutup" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
                <h4 class="modal-title">Tutup Kasir</h4>
            </div>
            <div class="modal-body">
					<div class="row" style="margin-bottom: 10px;">
						<div class="form-horizontal">
							<div class="form-group">
								<div class="col-md-12">
									<input id="tutup_kasir" name="search-term" class="form-control tutup_kasir" placeholder="Kas Akhir" type="text" style="height:45px;">
								</div>
							</div>
						</div>
					</div>
				</div>
            <div class="modal-footer">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-sm-12">
						<button onclick="tutupKasir();" class="btn btn-block btn-lg btn-primary">Tutup Kasir</button>
					</div>
				</div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">

function proses_suspend_back(id){
	var idsuspen = document.getElementById(id).getAttribute('invoice');
	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url($this->cname).'/suspen_back?invoice=' ?>"+idsuspen,
		success: function(data)
		{
			alert(data);
			$('#trans_suspend').hide();
			$('#trans_penjualan').show();
			$('#id_product').focus();
		}
	});
}

$(document).ready(function($) {
	$('#keyword').keyup(function(){
		var key = $('#keyword').val();
		 $.ajax({
				type: "POST",
				url: "<?php echo base_url($this->cname).'/search_produk' ?>",
				data: $('#form-searchproduk').serialize(),
				success: function(data)
				{
					$('#list_produk').html(data);
				}
			});
	});
	$('#barcode_produk').on('change',function(){
		var key = $('#barcode_produk').val();
		 $.ajax({
				type: "GET",
				url: "<?php echo base_url($this->cname).'/search_scanproduk?barcode_produk=' ?>"+key,
				success: function(data)
				{
					$('#list_scanproduk').html(data);
					$('#barcode_produk').val('');
					$('#barcode_produk').focus();
				}
			});
		return false;
	});
	$('#id_product').on('change',function(){
		var product = $('#id_product').val();
		var kas = $.trim($(".dataNote").html());
		var inv = $.trim($(".dataInvoice").html());

		$(".invoice").val(inv);
		$(".kas").val(kas);
		$(".barcode").val(product);
		$.ajax({
			url: "<?php echo base_url($this->cname).'/getprodukfromstok?barcode_produk=' ?>"+product,
			success: function(data)
			{
				if (data == 'kosong') {
					alert('Barang belum tersedia');
				}else{
					var jml = parseInt(data);
					if (jml > 0) {
						$("#tambahkan_produk").data("aksi", "tambah");
						$('#modal-add').modal('show');
						$(".qty").val('');
						$("#qty").focus();
					}else if(jml <= 0){
						alert('Stok barang habis');
					}	
				}
			}
		});
		
		$('#id_product').val('');
		return false;
	}); 

	$('.pelanggan').on('change',function(){
		var pelanggan = $('#pelanggan').val();

		if(pelanggan){
			var url = "<?php echo base_url($this->cname); ?>/add_pelanggan";
			var form_data = {
				pelanggan: $.trim(pelanggan),
				is_ajax: 'open'
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(data)
				{
					pesan = data.split("|");
					status=pesan[0];

					if(status == 0){
						alert('Data Pelanggan tersebut tidak ada dalam database!');
					}else if(status == 1){
						id = pesan[1];
						nama = pesan[2];
						$('#pelanggan_id').val(id);
						$('#pelanggan_nama').val(nama);
						$('#pelanggan').val('');
						$('#cara').focus();
					}
				}
			});
		}else{
			$(".chosenValidate").show();
			$('#pelanggan').val('');
			$('#pelanggan').focus();
		}
	}); 

	$("#bayar").keyup(function() {
        var total = $('#total').val();
        var bayar = $('#bayar').val();
        if (bayar == '') {
            $('.dataBayar').html('0');
        }
        $('.dataBayar').html(bayar);
        $(".dataBayar").mask("000.000.000.000.000", {
            reverse: true
        });
        var kembalian = bayar - total;
        if (kembalian < 0) {
            $('#kembali').val('0');
            $('.dataKembali').html('0');
        } else {
            $('#kembali').val(kembalian);
            $('.dataKembali').html(kembalian);
            $(".dataKembali").mask("000.000.000.000.000", {
                reverse: true
            });
        }
    });

    $("#diskon").keyup(function() {
        var total = $('#dumpTotal').val();
        var percent = $(this).val();
        if (percent) {
            diskon = parseInt(percent) / 100;
            potongan = parseInt(total) * diskon;
            price = parseInt(total) - parseInt(potongan);
            $('#total').val(price);
            $('.dataTotal').html(price);
            $(".dataTotal").mask("000.000.000.000.000", {
                reverse: true
            });
        } else {
            $('#total').val(total);
            $('.dataTotal').html(total);
            $(".dataTotal").mask("000.000.000.000.000", {
                reverse: true
            });
        }

        var total = $('#total').val();
        var bayar = $('#bayar').val();
        if (bayar == '') {
            $('.dataBayar').html('0');
        }
        $('.dataBayar').html(bayar);
        $(".dataBayar").mask("000.000.000.000.000", {
            reverse: true
        });
        var kembalian = bayar - total;
        if (kembalian < 0) {
            $('#kembali').val('0');
            $('.dataKembali').html('0');
        } else {
            $('#kembali').val(kembalian);
            $('.dataKembali').html(kembalian);
            $(".dataKembali").mask("000.000.000.000.000", {
                reverse: true
            });
        }

    });

	// $('#modal-regular').modal({ backdrop: 'static', keyboard: false }); 
});

function asReguler() {
	$.ajax({
		url: "<?php echo base_url($this->cname).'/cek_member_reguler' ?>",
		success: function(msg)
		{
			$('#modal-cekmember').modal('hide');
		}
	});
	$("#id_product").focus();
}

function cekmember(){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url($this->cname).'/cek_member' ?>",
		data: $('#formcekmember').serialize(),
		success: function(msg)
		{
			alert(msg);
			if(msg == 'Member Success'){
				$('#modal-cekmember').modal('hide');
			}
		}
	});
}

function checking(){
	var url = "<?php echo base_url($this->cname); ?>/cashdraw_check";

	$.ajax({
		url: "<?php echo base_url($this->cname); ?>/ceksession_member",
		success: function(msg)
		{
			var member = msg;

			var form_data = {
				is_ajax: 'kas'
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(pesan)
				{
					//alert(pesan);
					data = pesan.split("|");
					dataValid=data[0];
					dataStatus=data[1];
					dataCash=data[2];
					get_total_suspend($.trim(dataCash));
					get_total_transaction($.trim(dataCash));
					if(dataValid == 1){
						
						$('#modal-regular').modal('hide');
						$(".dataNote").html(dataCash);
						if (member == '') {
							$('#code_member').focus();
							$('#modal-cekmember').modal('show');
							// $('#code_member').val('');
							
						}
						$("#id_product").focus();
						if(dataStatus == 'temporary'){
							var cash = $(".dataNote").html();
							var urlTmp = "<?php echo base_url($this->cname); ?>/temporary_check";
							var form_dataTmp = {
								cash : $.trim(dataCash),
								is_ajax: 'temporary'
							};
							$.ajax({
								type: "POST",
								url: urlTmp,
								data: form_dataTmp,
								success: function(msg)
								{
									data = msg.split("|");
									dataValid2=data[0];
									dataInvoice2=data[1];
									

									if(dataValid2==0){
										$("#id_product").focus();
										var urlInv = "<?php echo base_url($this->cname); ?>/invoice_request";
										var urlDataSuspend = "<?php echo base_url($this->cname); ?>/get_suspend_data";
										var form_dataInv = {
											kas: $.trim(dataCash),
											is_ajax: 'request'
										};
										var form_dataListSuspend = {
				                            kas: $.trim(dataCash),
				                            is_ajax: 'suspended'
				                        };
										$.ajax({
											type: "POST",
											url: urlInv,
											data: form_dataInv,
											success: function(inv)
											{
												data = inv.split("|");
												$(".dataInvoice").html(data[0]);
											}
										});

										$.ajax({
				                            type: "POST",
				                            url: urlDataSuspend,
				                            data: form_dataListSuspend,
				                            success: function(temp) {
				                            	$(".dataSuspend").html(temp);
				                            }
				                        });
									}else if(dataValid2==1){
										//alert(msg);
										$("#id_product").focus();
										$(".dataInvoice").html(dataInvoice2);
				                        var urlDataTmp = "<?php echo base_url($this->cname); ?>/get_temp_data";
				                        var urlDataSuspend = "<?php echo base_url($this->cname); ?>/get_suspend_data";
				                        var form_dataListTmp = {
				                            kas: $.trim(dataCash),
				                            inv: $.trim(dataInvoice2),
				                            is_ajax: 'temporary'
				                        };
				                        var form_dataListSuspend = {
				                            kas: $.trim(dataCash),
				                            is_ajax: 'suspended'
				                        };
				                        $.ajax({
				                            type: "POST",
				                            url: urlDataTmp,
				                            data: form_dataListTmp,
				                            success: function(temp) {
				                            	//alert(temp);
				                                var data = temp.split("|");
				                                $(".dataSelling").html(data[0]);
				                                $(".totalSelling").html(data[1]);
				                                $(".totalDiskon").html(data[2]);
				                                $(".dataTotal").html(data[1]);
				                                $("#total").val(data[1]);
				                                $("#dumpTotal").val(data[1]);
				                                $(".dataTotal").html(data[1]);
				                                $(".totalSelling").mask("000.000.000.000.000", {
				                                    reverse: true
				                                });
				                                $(".dataTotal").mask("000.000.000.000.000", {
				                                    reverse: true
				                                });
				                            }
				                        });
										
										$.ajax({
				                            type: "POST",
				                            url: urlDataSuspend,
				                            data: form_dataListSuspend,
				                            success: function(temp) {
				                            	$(".dataSuspend").html(temp);
				                            }
				                        });
										
				                        
									}else{
										alert('Software Error! Harap segera hubungi Admin atau Teknisi Outlet!');
									}
								}
							});
						}else if(dataStatus == 'closed'){
							 new Messi('Kas anda masih belum di validasi oleh pihak management, sehingga anda tidak bisa melakukan transaksi, harap tunggu hingga kas anda di validasi.', {
						        title: 'Information',
						        modal: true,
						        titleClass: 'info',
						        buttons: [{
						            id: 0,
						            label: 'Ya',
						            val: 'Y'
						        }],
						        callback: function(val) {
						            var confirm = val;
						            if (confirm == 'Y') {
						            	setTimeout(function() {window.location = "dashboard"},500);
						            }
						        }
						    })
						}else if(dataStatus == 'valid'){
							new Messi('Kas anda sudah di validasi oleh pihak management, tetapi anda tidak bisa melakukan transaksi dengan akun tersebut di hari yang sama.', {
						        title: 'Information',
						        modal: true,
						        titleClass: 'info',
						        buttons: [{
						            id: 0,
						            label: 'Ya',
						            val: 'Y'
						        }],
						        callback: function(val) {
						            var confirm = val;
						            if (confirm == 'Y') {
						            	setTimeout(function() {window.location = "dashboard"},500);
						            }
						        }
						    })
						}
					}
					else if(dataValid == 0){
						$('#modal-regular').modal('show');
						 
						$(".buka_kasir").focus();
						var urlCont = "<?php echo base_url($this->cname); ?>/cashdraw_request";
						var form_dataCont = {
							is_ajax: 'request'
						};
						$.ajax({
							type: "POST",
							url: urlCont,
							data: form_dataCont,
							success: function(msg)
							{
								//alert(msg);
								data = msg.split("|");
								$(".dataNote").html(data[0]);
								// $(".statTemp").html(data[1]);
								$("#invoice").val(data[0]);
							}
						});

					}
				}
			});
		}
	});
	
	setTimeout('checking()', 10000);
}

function get_total_transaction(kase){
	var url = "<?php echo base_url($this->cname); ?>/total_transaction";
	var form_data = {
		kas: kase,
		is_ajax: 'kas'
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(data)
		{
			//alert(pesan);
			$(".total_transaksi").html(data);
		}
	})
	
}

function get_total_suspend(datacash){
	var url = "<?php echo base_url($this->cname); ?>/total_suspend";
	var form_data = {
		kas: datacash,
		is_ajax: 'kas'
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(data)
		{
			$(".total_suspend").html(data);
		}
	})
	
}

function get_total_cancel(){
	var url = "<?php echo base_url($this->cname); ?>/total_cancel";
	var form_data = {
		is_ajax: 'kas'
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(data)
		{
			$(".total_void").html(data);
		}
	})
	setTimeout('get_total_cancel()', 10000);
}

	$(window).load(function() {
	// 	checking();
	// 	get_total_cancel();
	});

	function open_cash(){
		var cash = $("#invoice").val();
		var awal = $('#awal').val();
		
		if(cash && awal){
			var url = "<?php echo base_url($this->cname); ?>/open_cash";
			var form_data = {
				inv: $.trim(cash),
				awal: $.trim(awal),
				is_ajax: 'open'
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(data)
				{
					//alert(data);
					if (data==1){
						$('#modal-regular').modal('hide');
					}
					else if(data==2){
						addData();
					}
					else if(data==0){
						addGagal();
					}
					else{
						inpoServer();
					}
				}
			});
		}else{
			$(".chosenValidate").show();
		}
		return false;
	}

	function del(ident){
		var url = "<?php echo base_url($this->cname); ?>/delete_temp";
	    var form_data = {
	        ident: ident,
	        is_ajax: 'delete'
	    };
	    $.ajax({
            type: "POST",
            url: url,
            data: form_data,
            success: function(data) {
                if (data == 1) {
                    delSukses();
                    checking();
                } else if (data == 0) {
                    delGagal();
                } else {
                    inpoServer();
                }
            }
        });
        

	    // new Messi('Apakah Anda yakin akan menghapus data tersebut?', {
	    //     title: 'Information',
	    //     modal: true,
	    //     titleClass: 'info',
	    //     buttons: [{
	    //         id: 0,
	    //         label: 'Ya',
	    //         val: 'Y'
	    //     }, {
	    //         id: 1,
	    //         label: 'Tidak',
	    //         val: 'N'
	    //     }],
	    //     callback: function(val) {
	    //         var confirm = val;
	    //         if (confirm == 'Y') {
	    //             $.ajax({
	    //                 type: "POST",
	    //                 url: url,
	    //                 data: form_data,
	    //                 success: function(data) {
	    //                     if (data == 1) {
	    //                         delSukses();
	    //                         checking();
	    //                     } else if (data == 0) {
	    //                         delGagal();
	    //                     } else {
	    //                         inpoServer();
	    //                     }
	    //                 }
	    //             })
	    //         } else if (confirm == 'N') {

	    //         }
	    //     }
	    // })
	    return false;
	}

	function add_transaction(){
		var invoice = $(".invoice").val();
		var cash = $('.kas').val();
		var barcode = $('.barcode').val();
		var qty = $('.qty').val();
		
		if(invoice && cash && barcode && qty){
			var urlcek = "<?php echo base_url($this->cname); ?>/cek_saldo_produk";
			var form_datacek = {
				qty: $.trim(qty),
				barcode: $.trim(barcode),
			};

			$.ajax({
				type: "POST",
				url: urlcek,
				data: form_datacek,
				success: function(data)
				{
					// alert(data);
					if (data == 'ada') {
						var url = "<?php echo base_url($this->cname); ?>/add_transaction";
						var form_data = {
							inv: $.trim(invoice),
							cash: $.trim(cash),
							barcode: $.trim(barcode),
							qty: $.trim(qty),
							is_ajax: 'open'
						};
						$.ajax({
							type: "POST",
							url: url,
							data: form_data,
							success: function(data)
							{
								//alert(data);
								if (data==1){
									$('#modal-add').modal('hide');
									$("#id_product").focus();
									checking();
								}
								else if(data==2){
									// addData();
									$('#modal-add').modal('hide');
									$("#id_product").focus();
								}
								else if(data==0){
									addGagal();
									$('#modal-add').modal('hide');
									$("#id_product").focus();
								}
								else{
									inpoServer();
									$('#modal-add').modal('hide');
									$("#id_product").focus();
								}
							}
						});
					}else if(data == 'tidak cukup'){
						alert('Produk yang ada di dalam rak tidak cukup, silahkan ulangi kembali!');
						$('#modal-add').modal('hide');
						$("#id_product").focus();
					}
				}
			});
			
		}else{
			$(".chosenValidate").show();
		}
		return false;
	}

	function editqty(ident){
		var id = ident;
		
		if(id){
			var url = "<?php echo base_url($this->cname); ?>/view_detail_temp";
			var form_data = {
				ident: $.trim(id),
				is_ajax: 'open'
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(data)
				{
					req = data.split("|");
					$(".ident").val(id);
					$(".price").val(req[1]);
					$(".discount").val(req[2]);
					$(".qty").val(req[0]);
					$('#modal-ubah').modal('show');
					$('.qty_edit').focus();
				}
			});
		}else{
			$(".chosenValidate").show();
		}
		return false;
	}



	function edit_transaction(){
		var kas = $.trim($(".dataNote").html());
		var inv = $.trim($(".dataInvoice").html());
		var ident = $('.ident').val();
		var price = $('.price').val();
		var discount = $('.discount').val();
		if(discount){
			var discount = $('.discount').val();
		}else{
			var discount = 0;
		}
		var qty = $('.qty').val();
		var qty_edit = $('.qty_edit').val();

		var subtotal = parseInt(qty_edit) * parseInt(price);
		var diskon = (parseInt(subtotal) * parseInt(discount)) / 100;
		var subdiskon = subtotal - diskon;

		// alert(inv+'|'+kas+'|'+price+'|'+discount+'|'+qty+'|'+qty_edit);
		
		if(inv && kas && price && qty && qty_edit){
			var url = "<?php echo base_url($this->cname); ?>/edit_transaction";
			var form_data = {
				ident: $.trim(ident),
				qty: $.trim(qty_edit),
				diskon: $.trim(diskon),
				subtotal: $.trim(subtotal),
				subdiskon: $.trim(subdiskon),
				is_ajax: 'open'
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(data)
				{
					if (data==1){
						$('#modal-ubah').modal('hide');
						$("#id_product").focus();
						editSukses();
	                    checking();
					}
					else if(data==2){
						editData();
					}
					else if(data==0){
						editGagal();
					}
					else{
						inpoServer();
					}
				}
			});
		}else{
			$(".chosenValidate").show();
		}
		return false;
	}

	function bayar() {
        var cara = $('#cara').val();
        var pelanggan = $('#pelanggan_id').val();
        if(pelanggan){
        	id_cust = $('#pelanggan_id').val();
        	name_cust = $('#pelanggan_nama').val();
        }else{
        	id_cust = '1';
        	name_cust = 'Reguler';
        }
        var total = $('#total').val();
        var diskon = $('#diskon').val();
        var bayar = $('#bayar').val();
        var kembali = $('#kembali').val();
        var kas = $('.dataNote').html();
        var nota = $('.dataInvoice').html();
        var url = "<?php echo base_url($this->cname); ?>/payment_transaction";

        var form_data = {
            kas: $.trim(kas),
            invoice: $.trim(nota),
            cara: $.trim(cara),
            id_cust: id_cust,
            name_cust: name_cust,
            total: $.trim(total),
            diskon: $.trim(diskon),
            bayar: $.trim(bayar),
            kembali: $.trim(kembali),
            is_ajax: 'finish'
        };
        cekbayar = parseInt(bayar);
        cektotal = parseInt(total);
        if (cara && total && bayar && kembali) {
            if (cekbayar >= cektotal) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form_data,
                    success: function(data) {
                        var msg = data.split("|");
                        var ret = msg[0];
                        var invoice = msg[1];
                        if (ret == 1) {
                            transSukses();
                            moveData(nota);
                            clearAll();
                        } else if (ret == 0) {
                            transGagal();
                            clearAll();
                        } else if (ret == 2) {
                            transData();
                            clearAll();
                        } else if (ret == 3) {
                            transData();
                            clearAll();
                            moveData(nota);
                        } else {
                            inpoServer();
                            clearAll();
                        }
                    }
                })
            } else {}
        } else {}
        return false;
    }

    function moveData(invoicenya) {

	    var inv = $(".dataInvoice").html();
	    var kas = $('.dataNote').html();
	    var url = "<?php echo base_url($this->cname); ?>/move_transaction";
	    var url_invoice = "<?php echo base_url($this->cname); ?>/print_invoice";
	    var form_data = {
	        invoice: $.trim(inv),
	        is_ajax: 'reset'
	    };
	    $.ajax({
	        type: "POST",
	        url: url,
	        data: form_data,
	        success: function(data) {
	            if(data == 1){
	                // open_window(url_invoice,400,500);
	                var form_data = {
				        invoice: $.trim(inv),
				        is_ajax: 'reset'
				    };
				    $.ajax({
				        type: "GET",
				        url: url_invoice,
				        data: "invoice="+invoicenya+"&kas="+kas,
				        success: function(data) {
				            if(data == 1){
				                
				            }
				            else if(data == 0){
				                
				            }
				        }
				    });
	                window.setTimeout(function(){window.location.reload();}, 1000);
	            }
	            else if(data == 0){
	                //open_window('module/invoice/invoice.php?log='+inv,400,500);
	                window.setTimeout(function(){window.location.reload();}, 1000);
	            }
	        }
	    });
	}

	function tutupKasir(){
			var kas = $(".dataNote").html();
			var end = $("#tutup_kasir").val();
			// alert(kas+'|'+end);
	    	var url = "<?php echo base_url($this->cname); ?>/tutupkasir";
	    	var url_invoice = "<?php echo base_url($this->cname); ?>/print_kas";;
			var form_data = {
		        kas: $.trim(kas),
		        end: $.trim(end),
		        is_ajax: 'reset'
		    };
			$.ajax({
	        	type: "POST",
		        url: url,
		        data: form_data,
		        success: function(data) {
		            // alert(data);
		            if(data==1){
		            	var form_data = {
					        kas: $.trim(kas),
		        			end: $.trim(end),
					        is_ajax: 'reset'
					    };
					    $.ajax({
					        type: "GET",
					        url: url_invoice,
					        data: "end="+end+"&kas="+kas,
					        success: function(data) {
					            if(data == 1){
					                
					            }
					            else if(data == 0){
					                
					            }
					        }
					    });
		            	setTimeout(function() {window.location = "dashboard"},500);
		            }else if(data==0){
		            	$.gritter.add({
					        time: '5000',
					        title: 'Failed!',
					        text: 'Kasir gagal ditutup, silahkan hubungi administrator atau teknisi outlet.'
					    });
		            }else{
		            	inpoServer();
		            }
		        }
		    });
			// setTimeout(function() {window.location = "dashboard"},500);
		}

	function suspend(){
		var cash = $(".dataInvoice").html();
		var urlTmp = "<?php echo base_url($this->cname); ?>/temp_count_check";
		var form_dataTmp = {
			invoice : $.trim(cash),
			is_ajax: 'temporary'
		};
		new Messi('Apakah Anda yakin akan menunda transaksi dengan kode : '+cash+' tersebut?', {
	        title: 'Information',
	        modal: true,
	        titleClass: 'info',
	        buttons: [{
	            id: 0,
	            label: 'Ya',
	            val: 'Y'
	        }, {
	            id: 1,
	            label: 'Tidak',
	            val: 'N'
	        }],
	        callback: function(val) {
	            var confirm = val;
	            if (confirm == 'Y') {
					$.ajax({
						type: "POST",
						url: urlTmp,
						data: form_dataTmp,
						success: function(msg)
						{
							if(msg==0){
								alert('Proses "Suspend" gagal, karena tidak ada data dalam transaksi dengan kode : '+cash);
							}else if(msg>=1){
								var inv = $(".dataInvoice").html();
								var url = "<?php echo base_url($this->cname); ?>/suspend_transaction";
							    var form_data = {
							        invoice: $.trim(inv),
							        is_ajax: 'reset'
							    };
							    $.ajax({
							        type: "POST",
							        url: url,
							        data: form_data,
							        success: function(data) {
							            if(data == 1){
							                window.setTimeout(function(){window.location.reload();}, 1000);
							            }
							            else if(data == 0){
							                window.setTimeout(function(){window.location.reload();}, 1000);
							            }
							        }
							    });
							}else{
								alert('Software Error! Harap segera hubungi Admin atau Teknisi Outlet!');
							}
						}
					});
	            } else if (confirm == 'N') {

	            }
	        }
	    })
	    return false;
	}

	function cancel(){
		var cash = $(".dataInvoice").html();
		var urlTmp = "<?php echo base_url($this->cname); ?>/temp_count_check";
		var form_dataTmp = {
			invoice : $.trim(cash),
			is_ajax: 'temporary'
		};
		new Messi('Apakah Anda yakin akan membatalkan transaksi dengan kode : '+cash+' tersebut?', {
	        title: 'Information',
	        modal: true,
	        titleClass: 'info',
	        buttons: [{
	            id: 0,
	            label: 'Ya',
	            val: 'Y'
	        }, {
	            id: 1,
	            label: 'Tidak',
	            val: 'N'
	        }],
	        callback: function(val) {
	            var confirm = val;
	            if (confirm == 'Y') {
					$.ajax({
						type: "POST",
						url: urlTmp,
						data: form_dataTmp,
						success: function(msg)
						{
							if(msg==0){
								alert('Proses "Void" gagal, karena tidak ada data dalam transaksi dengan kode : '+cash);
							}else if(msg>=1){
							    var inv = $(".dataInvoice").html();
								var url = "<?php echo base_url($this->cname); ?>/cancel_transaction";
							    var form_data = {
							        invoice: $.trim(inv),
							        is_ajax: 'reset'
							    };
							    $.ajax({
							        type: "POST",
							        url: url,
							        data: form_data,
							        success: function(data) {
							            if(data == 1){
							                window.setTimeout(function(){window.location.reload();}, 1000);
							            }
							            else if(data == 0){
							                window.setTimeout(function(){window.location.reload();}, 1000);
							            }
							        }
							    });
							}else{
								alert('Software Error! Harap segera hubungi Admin atau Teknisi Outlet!');
							}
						}
					});
				} else if (confirm == 'N') {

	            }
	        }
	    })
	    return false;
	}

	function clearText(){

	}
	$('#trans_suspend').hide();
	
	function list_suspend(){
		$('#trans_suspend').slideDown();
		$('#trans_penjualan').slideUp();
		$('#payment-div').slideUp();
	}

	function addSukses() {
	    $.gritter.add({
	        time: '5000',
	        title: 'Success!',
	        text: 'Data yang Anda Masukkan berhasil disimpan!'
	    });
	}

	function addGagal() {
	    $.gritter.add({
	        time: '5000',
	        title: 'Failed!',
	        text: 'Data yang Anda Masukkan gagal disimpan, silahkan dicek secara manual inputan anda.'
	    });
	}

	function addData() {
	    $.gritter.add({
	        time: '5000',
	        title: 'Failed!',
	        text: 'Data yang Anda Masukkan gagal disimpan, karena data sudah ada.'
	    });
	}

	function editSukses() {
	    $.gritter.add({
	        time: '5000',
	        title: 'Success!',
	        text: 'Data yang Anda Masukkan berhasil diganti!'
	    });
	}

	function editGagal() {
	    $.gritter.add({
	        time: '5000',
	        title: 'Failed!',
	        text: 'Data yang Anda Masukkan gagal diganti, silahkan dicek secara manual inputan anda.'
	    });
	}

	function editData() {
	    $.gritter.add({
	        time: '5000',
	        title: 'Failed!',
	        text: 'Data yang Anda Masukkan gagal diganti, karena data sudah ada di dalam database.'
	    });
	}

    function transSukses() {
        $.gritter.add({
            time: '5000',
            title: 'Success!',
            text: 'Data Transaksi berhasil disimpan!'
        });
    }

    function transGagal() {
        $.gritter.add({
            time: '5000',
            title: 'Failed!',
            text: 'Data Transaksi gagal disimpan, silahkan ulangi lagi!'
        });
    }

    function transData() {
        $.gritter.add({
            time: '5000',
            title: 'Failed!',
            text: 'Data Transaksi gagal disimpan, silahkan ulangi lagi!'
        });
    }

	function inpoServer() {
	    $.gritter.add({
	        time: '5000',
	        title: 'Timeout!',
	        text: 'Server error, connection timeout!'
	    });
	}

	 function delSukses() {
        $.gritter.add({
            time: '5000',
            title: 'Success!',
            text: 'Data berhasil dihapus!'
        });
    }

    function delGagal() {
        $.gritter.add({
            time: '5000',
            title: 'Failed!',
            text: 'Data gagal dihapus!'
        });
    }


</script>
<script>
$(function(){
	//$('#modal-regular').modal('show');
	//$("#awal").focus();
	//$(".buka_kasir").focus();
	$("#modal-compose").on("shown.bs.modal",function(){$(".modal-select-chosen").chosen({width:"100%"})}),$(".ms-message-list").slimScroll({height:610,color:"#000000",size:"3px",touchScrollStep:750});var e=$(".ms-categories");$("a",e).click(function(){$("li",e).removeClass("active"),$(this).parent().addClass("active"),$(this).data("cat")});begin();});

		function toggleButton(){
			if($('.hide-key').is(':visible')){
				$('.hide-key').hide();
			} else {
				$('.hide-key').show();
			}
		}
		nextReferral();
		function nextDeposit(){
			$('#trans_referral').slideUp();
			$('#trans_pendataan').slideUp();
			$('#trans_deposit').slideDown();
		}
		function nextPendataan(){
			$('#trans_referral').slideUp();
			$('#trans_deposit').slideUp();
			$('#trans_pendataan').slideDown();
		}
		function nextReferral(){
			$('#trans_deposit').slideUp();
			$('#trans_pendataan').slideUp();
			$('#trans_referral').slideDown();
		}
		function scanButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideDown();
			$('#payment-div').slideUp();
			$('#selling-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#sync-div').slideUp();
			$('#barcode_produk').focus();
			$('#barcode_produk').val('');
		}
		function searchButton(){
			$('#head-div').slideUp();
			$('#browse-div').slideUp();
			$('#search-div').slideDown();
			$('#scan-div').slideUp();
			$('#payment-div').slideUp();
			$('#selling-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#sync-div').slideUp();
			$("#search_sale").focus();
			$(".search_sale").focus();
		}
		function begin(){
			$('#browse-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#payment-div').slideUp();
			$('.hide-key').slideUp();
			$('#selling-div').slideDown();
			$('#sync-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
		}
		function syncButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#tambah-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#sync-div').slideDown();
			$('#selling-div').slideUp();
			generate();
		}
		function importButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#tambah-div').slideUp();
			$('#import-div').slideDown();
			$('#export-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#sync-div').slideUp();
			$('#selling-div').slideUp();
		}
		function exportButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#tambah-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideDown();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#sync-div').slideUp();
			$('#selling-div').slideUp();
		}

      $(document).on('keydown', function(e){
        var key = e.which;
        if(e.ctrlKey && key == 66){ //CTRL+B
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+P!');
            browseButton();
            return false;
        }else if(e.ctrlKey && key == 83){ //CTRL+S
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+R!');
            searchButton();
            return false;
        }else if(e.ctrlKey && key == 67){ //CTRL+C
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+H!');
            scanButton();
            return false;
        }else if(e.ctrlKey && key == 80){ //CTRL+P
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            paymentButton();
            return false;
        }else if(e.ctrlKey && key == 84){ //CTRL+T
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            $('#modal-tutup').modal('show');
			$("#tutup_kasir").focus();
			$(".tutup_kasir").focus();
            return false;
        }else if(e.ctrlKey && key == 87 ){ //CTRL+W
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+W!');
            suspend();
            $('.btnbox.btn').focus();
            return false;
        }else if(e.ctrlKey && key == 86){ //CTRL+V
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+V!');
            cancel();
            $('.btnbox.btn').focus();
            return false;
        }else if(e.ctrlKey && key == 89){ //CTRL+Y
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+Y!');
            list_suspend();
            return false;
        }else if(key == 82 ){ //R
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('R!');
            asReguler();
            return false;
        }else if(key == 77  ){ //M
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('M!');
            cekmember();
            return false;
        }else if(key == 27){
        	setTimeout(function() {window.location = "dashboard"},500);
        }else{
        	return true;
        }
      });
      $(document).on('keyup', function(e){
          var key = e.which;
        if(e.ctrlKey && key == 66){ //CTRL+B
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+P!');
            browseButton();
            return false;
        }else if(e.ctrlKey && key == 83){ //CTRL+S
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+R!');
            searchButton();
            return false;
        }else if(e.ctrlKey && key == 67){ //CTRL+C
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+H!');
            scanButton();
            return false;
        }else if(e.ctrlKey && key == 80){ //CTRL+P
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            paymentButton();
            return false;
        }else if(e.ctrlKey && key == 86){ //CTRL+V
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+V!');
            cancel();
            $('.btnbox.btn').focus();
            return false;
        }else if(e.ctrlKey && key == 89){ //CTRL+Y
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+Y!');
            list_suspend();
            return false;
        }else if(key == 82 ){ //R
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('R!');
            asReguler();
            return false;
        }else if(key == 77  ){ //M
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('M!');
            cekmember();
            return false;
        }else if(key == 27){
        	setTimeout(function() {window.location = "dashboard"},500);
        }else{
        	return true;
        }
      });
      $(document).on('keypress', function(e){
          var key = e.which;
        if(e.ctrlKey && key == 66){ //CTRL+B
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+P!');
            browseButton();
            return false;
        }else if(e.ctrlKey && key == 83){ //CTRL+S
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+R!');
            searchButton();
            return false;
        }else if(e.ctrlKey && key == 67){ //CTRL+C
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+H!');
            scanButton();
            return false;
        }else if(e.ctrlKey && key == 80){ //CTRL+P
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            paymentButton();
            return false;
        }else if(e.ctrlKey && key == 86){ //CTRL+V
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+V!');
            cancel();
            $('.btnbox.btn').focus();
            return false;
        }else if(e.ctrlKey && key == 89){ //CTRL+Y
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+Y!');
            list_suspend();
            return false;
        }else if(key == 82 ){ //R
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('R!');
            asReguler();
            return false;
        }else if(key == 77  ){ //M
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('M!');
            cekmember();
            return false;
        }else if(key == 27){
        	setTimeout(function() {window.location = "dashboard"},500);
        }else{
        	return true;
        }
      });
		


    </script>
	
	<script type="text/javascript">
		$(document).ready(function() {
		// Create two variable with the names of the months and days in an array
		var monthNames = [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ]; 
		var dayNames= ["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"]

		// Create a newDate() object
		var newDate = new Date();
		// Extract the current date from Date object
		newDate.setDate(newDate.getDate());
		// Output the day, date, month and year    
		$('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

		setInterval( function() {
			// Create a newDate() object and extract the seconds of the current time on the visitor's
			var seconds = new Date().getSeconds();
			// Add a leading zero to seconds value
			$("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
			},1000);
			
		setInterval( function() {
			// Create a newDate() object and extract the minutes of the current time on the visitor's
			var minutes = new Date().getMinutes();
			// Add a leading zero to the minutes value
			$("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
		    },1000);
			
		setInterval( function() {
			// Create a newDate() object and extract the hours of the current time on the visitor's
			var hours = new Date().getHours();
			// Add a leading zero to the hours value
			$("#hours").html(( hours < 10 ? "0" : "" ) + hours);
		    }, 1000);
			
		}); 
	</script>