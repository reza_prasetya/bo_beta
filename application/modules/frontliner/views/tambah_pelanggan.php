<div id="fx-container" class="fx-opaidentification_number">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/cari"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Cari</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Pendaftaran Pelanggan</p>
							</blockquote>
						</div>
					</div>
					<span id="flash_message"></span>
					<form id="addPelanggan" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode Pelanggan</span>
								<input type="hidden" id="id" name="id" class="form-control" placeholder="ID Pelanggan">
								<input type="hidden" id="no_urut" name="no_urut" class="form-control">
								<input readonly type="text" id="code" name="code" class="form-control" placeholder="Kode Pelanggan">
								<span class="input-group-addon"><i class="icon-user"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama</span>
								<input type="text" id="name" name="name" class="form-control" placeholder="Nama Pelanggan">
								<span class="input-group-addon"><i class="icon-user"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">No KTP</span>
								<input type="text" id="identification_number" name="identification_number" class="form-control" placeholder="Nomor KTP Pelanggan">
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Tanggal Lahir</span>
								<input type="text" id="born_date" name="born_date" class="form-control  input-datepicker" placeholder="Tanggal Lahir Pelanggan" data-date-format="yyyy-mm-dd">
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Alamat</span>
								<input type="text" id="address" name="address" class="form-control" placeholder="Alamat Pelanggan">
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">No Telp</span>
								<input type="text" id="phone" name="phone" class="form-control" placeholder="Nomor Telepon Pelanggan">
								<span class="input-group-addon"><i class="halfling-phone"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Email</span>
								<input type="email" id="email" name="email" class="form-control" placeholder="E-Mail Pelanggan">
								<span class="input-group-addon"><i class="halfling-phone"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Tanggal Registrasi</span>
								<input readonly type="text" id="registered" name="registered" class="form-control" placeholder="Tanggal Registrasi Pelanggan" value="<?php echo date('Y-m-d'); ?>">
								<span class="input-group-addon"><i class="icon-calendar"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Tanggal Expired</span>
								<input type="text" id="expired" name="expired" value="<?php echo date('Y-m-d',strtotime('+1 years')); ?>" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
								<span class="input-group-addon"><i class="icon-calendar"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Status</span>
								<select id="status" name="status" class="form-control">
									<option value="1">Aktif</option>
									<option value="0">Tidak Aktif</option>
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function begin(){
	var id = "<?php echo $this->uri->segment(4); ?>";
	// alert(id);
	if(id){
		//$("#begin-div").hide();
		//$("#tambah-div").show();
		$("#id").val(id);
		var url = "<?php echo base_url(); ?>master/pelanggan/detail_pelanggan";
		// alert('asas');
		var form_data = {
			id: id
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = msg.split('|');
				$("#code").val(data[0]);
				$("#name").val(data[1]);
				$("#registered").val(data[2]);
				$("#address").val(data[3]);
				$("#identification_number").val(data[4]);
				$("#born_date").val(data[5]);
				$("#phone").val(data[6]);
				$("#email").val(data[7]);
				$("#expired").val(data[8]);
				$("#status").val(data[9]);
				$('#status').trigger('chosen:updated');
				$("#no_urut").val(data[10]);
			}
		});
		return false;
	} else {
		//$("#kode_begin").focus();
		//$("#tambah-div").hide();
		var url = "<?php echo base_url(); ?>master/pelanggan/gen_code";
		// alert('asas');
		var form_data = {
			id: id
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = msg.split('|');
				$("#code").val(data[0]);
				$("#no_urut").val(data[1]);
			}
		});
		return false;
	}
}
$(document).ready(function(){
	begin();
	// $("#kode_begin").change(function(){
	// 	var kode = $("#kode_begin").val();
	// 	$("#code").val(kode);
	// 	$("#kode_begin").val('')
		//$("#begin-div").hide();
		//$("#tambah-div").show();
	//});
	$("#addPelanggan").submit(function(){
		// var id = $("#id").val();
		// var code = $("#code").val();
		// var name = $("#name").val();
		// var company = $("#company").val();
		// var address = $("#address").val();
		// var identification_number = $("#identification_number").val();
		// var born_date = $("#born_date").val();
		// var phone = $("#phone").val();
		// var email = $("#email").val();
		var url = "<?php echo base_url(); ?>master/pelanggan/tambah_pelanggan";
		// alert('asas');
		// var form_data = {
		// 	id: id,
		// 	code: code,
		// 	name: name,
		// 	company: company,
		// 	address: address,
		// 	identification_number: identification_number,
		// 	born_date: born_date,
		// 	phone: phone,
		// 	email: email,

		// };
		$.ajax({
			type: "POST",
			url: url,
			data: $('#addPelanggan').serialize(),
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				if(data[0]==1){
					clear();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	function clear(){
		$("#id").val('');
		$("#code").val('');
		$("#name").val('');
		$("#company").val('');
		$("#address").val('');
		$("#identification_number").val('');
		$("#born_date").val('');
		$("#phone").val('');
		$("#email").val('');
		$("#kode_begin").focus();
	}
});
</script>