<script type="text/javascript">
	
	
	
	function checking(){

			var tgl = $('.dataTgl').html();
			var user = $('.dataUser').html();
			var url = "<?php echo base_url($this->module);?>/penjualan/checking";
			var form_data = {
				tgl: $.trim(tgl),
				user: $.trim(user),
				is_ajax: 'kas'
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(pesan)
				{
					data = pesan.split("|");
					dataValid=data[0];
					dataStatus=data[1];

					if(dataValid == 1){
						alert('Silahkan melanjutkan transaksi');
						$('#modal-regular').modal('hide');
						if(dataStatus == 'temporary'){
							var tgl = $('.dataTgl').html();
							var urlTmp = "<?php echo base_url($this->module);?>/penjualan/checking";
							var form_dataTmp = {
								tgl: $.trim(tgl),
								is_ajax: 'temporary'
							};
							$.ajax({
								type: "POST",
								url: urlTmp,
								data: form_dataTmp,
								success: function(msg)
								{
									data = msg.split("|");
									$(".dataNote").html(' '+data[1]);
									$("#kas_penjualan").val(data[1]);
									$(".chosenWarn").show();
									$(".chosenNew").hide();
									$(".chosenAccept").hide();
									$(".finish").show();
									$(".open").hide();
									$(".kasInput").hide();
									$('#id_produk').focus();
								}
							});
						}else if(dataStatus == 'submitted'){
							$(".chosenNew").hide();
							$(".chosenAccept").hide();
							$(".dataNote").html('Pending');
							$(".open").hide();
							$(".kasInput").hide();
							$(".chosenSubmit").show();
						}else if(dataStatus == 'accepted'){
							$(".chosenNew").hide();
							$(".chosenSubmit").hide();
							$(".dataNote").html('Accepted');
							$(".open").hide();
							$(".kasInput").hide();
							$(".chosenAccept").show();
						}

						
					}
					else if(dataValid == 0){
						$('#modal-regular').modal('show');
						
						/*var urlCont = "module/transaction/selling/controller.php";
						var form_dataCont = {
							is_ajax: 'request'
						};
						$.ajax({
							type: "POST",
							url: urlCont,
							data: form_dataCont,
							success: function(msg)
							{
								data = msg.split("|");
								$(".dataNote").html(data[0]);
								$(".statTemp").html(data[1]);
								$("#invoice").val(data[0]);
							}
						});*/
					}
				}
			});
			return false;
		}
		
		

			
$(document).ready(function(){
	//stat();

	function stat(){
		var tgl = $('.dataTgl').html();
		var nota = $('.dataNote').html();
		var url = "module/transaction/selling/controller.php";
		var form_data = {
			tgl: $.trim(tgl),
			nota: $.trim(nota),
			is_ajax: 'stat'
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = msg.split("|");
				$("#statBar").html(data[0]);
				$("#statTemp").html(data[1]);
			}
		});
		return false;
	}
	
	$(".addForm").validate({
        rules: {
            awal: "required"
        },
        messages: {
			awal: "Harap isikan nominal kas awal, jika kosong isikan '0' !"
        }
    });
	
	$('#openCash').submit(function(){
		var inv = $('#invoice').val();
		var usid = $('#userid').val();
		var outlet = $('#outlet').val();
		var awal = $('#awal').val();
		var tgl = $('.dataTgl').html();
		
		if(awal && inv && tgl){
			if(outlet){
			var url = "module/transaction/selling/controller.php";
			var form_data = {
				inv: $.trim(inv),
				userid: $.trim(usid),
				outlet: $.trim(outlet),
				awal: $.trim(awal),
				tgl: $.trim(tgl),
				is_ajax: 'open'
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(data)
				{
					if (data==1){
						addSukses();
						clearAll();
						window.location= 'transaction/selling/create';
					}
					else if(data==2){
						addData();
					}
					else if(data==0){
						addGagal();
					}
					else{
						inpoServer();
					}
				}
			});
			}else{
				alert('Anda Belum diberi hak akses untuk mengelola kasir, silahkan hubungi admin');
			}
		}else{
			$(".chosenValidate").show();
		}
		return false;
	});
	
	function clearAll(){
		$("#invoice").val('');
		$("#userid").val('');
		$("#outlet").val('');
		$("#awal").val('');
	}
	
	function addSukses(){
		$.gritter.add({
			time: '5000',
			title: 'Success!',
			text: 'Data yang Anda Masukkan berhasil disimpan!'
		});
	}
	function addGagal(){
		$.gritter.add({
			time: '5000',
			title: 'Failed!',
			text: 'Data yang Anda Masukkan gagal disimpan, silahkan dicek secara manual inputan anda.'
		});
	}
	function addData(){
		$.gritter.add({
			time: '5000',
			title: 'Failed!',
			text: 'Data yang Anda Masukkan gagal disimpan, karena data sudah ada.'
		});
	}
	function inpoServer(){
		$.gritter.add({
			time: '5000',
			title: 'Timeout!',
			text: 'Server error, connection timeout!'
		});
	}
});

</script>
	