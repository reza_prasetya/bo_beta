<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class _datatable extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
	} 

	public function index()
	{
		redirect('frontliner');
	}
	
	public function pelanggan()
	{	
		$this->datatables->select('id,name,phone,address,city')
		->add_column('Actions', get_detail('$1'),'id')
        ->from('atombizz_customers');
        
        echo $this->datatables->generate();
	}

	public function pemasok()
	{	
		$this->datatables->select('id,name,phone,address,city')
		->add_column('Actions', get_detail('$1'),'id')
        ->from('atombizz_suppliers');
        
        echo $this->datatables->generate();
	}

	public function karyawan()
	{	
		$this->datatables->select('id,nama,jabatan,telp,alamat')
		->add_column('Actions', get_detail('$1'),'id')
        ->from('view_employee');
        
        echo $this->datatables->generate();
	}

	public function rak_gudang()
	{	
		$this->datatables->select('id,status,kode,nama')
		->add_column('Actions', get_detail('$1'),'id')
        ->from('atombizz_rack');
        
        echo $this->datatables->generate();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
