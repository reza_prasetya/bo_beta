<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->module = 'frontliner';
		$this->cname = 'frontliner/registrasi/';
		$this->load->model('Mdl_registrasi');
		$this->load->model('Mdl_checking');

		// $this->session->unset_userdata('membersession');
	}
	
	public function index()
	{
		// $access = strtolower($this->module.'.'.__class__.'.'.__function__);
		// $this->permission->check_permission($access);
		
		$data['cname'] = $this->module;
		$data['title'] = "Registrasi";
		// $data['barang'] = $this->Mdl_registrasi->get_barang();
		// $data['category'] = $this->Mdl_registrasi->get_category_product();
		$data['content'] = $this->load->view($this->module.'/registrasi',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function search_produk()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->get_product($param);
		$stmt = "";
		foreach ($hasil as $key => $value) {
			$stmt .= '<a href="javascript:void(0)" class="list-group-item" >
						<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
						<h4 class="list-group-item-heading pull-right"><strong>'.format_rupiah($value->price1).'</strong></h4>
						<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">'.$value->name.'</h4>
					</a>';
		}

		echo $stmt;
	}

	public function search_scanproduk()
	{
		$param = $this->input->get();
		$hasil = $this->Mdl_registrasi->get_product_scan($param);
		$stmt = "";
		if ($hasil != null) {
			foreach ($hasil as $key => $value) {
				$diskon = $value->discount == 0 ? '-' : $value->discount;
				$stmt .= '<a href="javascript:void(0)" class="list-group-item" style="">
								<h4 class="list-group-item-heading">
									<strong>'.$value->name.'</strong>
								</h4>
								<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-right" width="150px" height="150px" style="margin-right:35px;">
								<p class="list-group-item-text">
									<table id="general-table" class="table table-stripped" style="max-width:60%;">
										<tbody>
											<tr>
												<td>Harga</td>
												<td>'.format_rupiah($value->price1).'</td>
											</tr>
											<tr>
												<td>Diskon</td>
												<td>'.$diskon.'%</td>
											</tr>
											<tr>
												<td>Stock</td>
												<td>'.$value->saldo.'</td>
											</tr>
										</tbody>
									</table>
								</p>
								</a>';
			}
		}else{
			$stmt = "Data Tidak Tersedia";
		}
		
		echo $stmt; 
	}

	public function getprodukfromstok()
	{
		$code = $this->input->get('barcode_produk');
		$hasil = $this->Mdl_registrasi->getstok($code);
		if ($hasil != null) {
			echo @$hasil[0]->saldo;
		}
		else{
			echo 'kosong';
		}
	}

	public function ceksession_member()
	{
		$member = $this->session->userdata('membersession'); 

		if (is_array($member)) {
			$member = 'member';
		}else{
			$member = $member;
		}

		echo $member;
	}

	public function cek_member()
	{
		$param = $this->input->post();
		$cek = $this->Mdl_checking->cek_member($param);

		echo $cek;
	}

	public function cek_member_reguler()
	{
		$this->session->set_userdata('membersession', 'reguler');
	}

	public function cashdraw_check()
	{
		$hasil = $this->Mdl_checking->kas();
		echo $hasil;
	}

	public function invoice_check()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_checking->transaction($param);
		echo $hasil;
	}

	public function temporary_check()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_checking->temporary($param);
		echo $hasil;
	}

	public function temp_count_check()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->temp_count_check($param);
		echo $hasil;
	}

	public function cashdraw_request()
	{
		$hasil = $this->Mdl_registrasi->request_cashdraw();
		echo $hasil;
	}

	public function invoice_request()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->request_invoice($param);
		echo $hasil;
	}

	public function open_cash()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->open_cash($param);
		echo $hasil;
	}

	public function get_temp_data(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->temp_transaction($param);
		echo $hasil;
	}

	public function get_suspend_data(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->temp_suspended($param);
		echo $hasil;
	}

	public function add_transaction(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->add_transaction($param);
		echo $hasil;
	}

	public function view_detail_temp(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->view_detail_temp($param);
		echo $hasil;
	}

	public function delete_temp(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->delete_temp($param);
		echo $hasil;
	}

	public function edit_transaction(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->edit_transaction($param);
		echo $hasil;
	}

	public function add_pelanggan(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->add_pelanggan($param);
		echo $hasil;
	}

	public function payment_transaction(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->payment_transaction($param);
		echo $hasil;
	}

	public function tutupkasir()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->tutupkasir($param);
		echo $hasil;
	}

	public function cek_saldo_produk(){
		$param = $this->input->post();
		$qty = $param['qty'];
		$data = $this->Mdl_registrasi->cek_stok($param['barcode']);
		// echo $this->db->last_query();exit;
		if($qty<=$data->saldo){
			echo "ada";
		} else {
			echo "tidak cukup";
		}
	}

	public function print_invoice()
	{
		// $id = $this->input->get('invoice');
		// $data['hasil'] = $this->Mdl_registrasi->print_invoice($id);
		// echo $this->load->view($this->module.'/invoice',$data);

		$param = $this->input->get();
		$hasil = $this->Mdl_registrasi->print_invoice($param);
		
		/* text */  
		 $printTestText =  "            KOPONTEN SIDOGIRI          \n";
		 $printTestText .= "       SIDOGIRI KRATORN PASURUAN       \n";
		 $printTestText .= "      TOKO BASMALAH CAB. WONOREJO      \n";
		 $printTestText .= "    Jl. Raya Wonorejo / 0343-455655    \n";
		 $printTestText .= "\n";
		 $printTestText .= "Inv. ID : ".$hasil[0]->invoice."\n";
		 $printTestText .= "Date    : ".date('d M Y H:i:s')."\n";
		 $printTestText .= "----------------------------------------\n";
		 $printTestText .= "Item        Harga   Jml  Disc Subtotal\n";
		 $printTestText .= "----------------------------------------\n";

		 foreach ($hasil as $key => $value) {
		 	$produk = explode(' ', $value->product_name);
		 	$nama_produk = "";
		 	$a = 0;
		 	if (count($produk) > 2) {
		 		for ($i=0; $i < count($produk)-1; $i++) { 
		 			if ($i == 0) {
		 				$nama_produk .= @$produk[$a]." ".@$produk[$a+1]."\n";
		 			}else{
		 				$nama_produk .= @$produk[$a+1]." ".@$produk[$a+2]."\n";
		 			}
		 			
		 			$a++;
		 		}
		 		
		 	}else{
		 		$nama_produk = $value->product_name."\n";
		 	}
		 	$printTestText .= $nama_produk."          Rp.".$value->price.",-  ".$value->qty."    ".$value->discount."%  Rp.".$value->discount_sub.",-\n";
		 }

		 $printTestText .= "----------------------------------------\n";
		 $printTestText .= "	  Detail Pembayaran\n";
		 $printTestText .= "	  Total	: Rp. ".$hasil[0]->total.",-\n";
		 $printTestText .= "	  Bayar	: Rp. ".$hasil[0]->pay.",-\n";
		 $printTestText .= "	  Kembali: Rp. ".$hasil[0]->pay_back.",-\n";
	 	 // $printTestText .= "    Harga sudah termasuk PPN 10%\n";
		 $printTestText .= "----------------------------------------\n";
		 $printTestText .= "               Terima Kasih             \n";
		 $printTestText .= "        Barang yang sudah dibeli        \n";
		 $printTestText .= "    Tidak dapat ditukas/dikembalikan    \n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";

		 
		// /* tulis dan buka koneksi ke printer */    
		// $printer = printer_open("SP-POS76II");  
		// /* write the text to the print job */ 
		// printer_set_option($handle, PRINTER_MODE, "RAW"); 
		// printer_write($printer, $printTestText);   
		// /* close the connection */ 
		// printer_close($printer); 
		 $handle = printer_open("SP-POS76II");
			 printer_set_option($handle, PRINTER_MODE, "RAW");
			 printer_write($handle, $printTestText); 
			 printer_close($handle);
	}

	public function print_kas()
	{
		// $id = $this->input->get('invoice');
		// $data['hasil'] = $this->Mdl_registrasi->print_invoice($id);
		// echo $this->load->view($this->module.'/invoice',$data);

		$param = $this->input->get();
		$hasil = $this->Mdl_registrasi->print_kas($param);
		
		/* text */  
		 $printTestText =  "            KOPONTEN SIDOGIRI          \n";
		 $printTestText .= "       SIDOGIRI KRATORN PASURUAN       \n";
		 $printTestText .= "      ".$this->config->item['outlet_name']." CAB. ".$this->config->item['outlet_city']."      \n";
		 $printTestText .= "    ".$this->config->item['outlet_address']." / ".$this->config->item['outlet_telp']."    \n";
		 $printTestText .= "               Tutup Kasir              \n";
		 
		 $printTestText .= "----------------------------------------\n";
		 $printTestText .= "Date      : ".date('d M Y H:i:s')."\n";
		 $printTestText .= "Kas No    : ".$hasil[0]->cashdraw_no."\n";
		 $printTestText .= "Kasir     : ".$hasil[0]->nama."\n";
		 $printTestText .= "Kas Akhir : ".format_rupiah($hasil[0]->end_cash)."\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";
		 $printTestText .= "\n";

		 
		// /* tulis dan buka koneksi ke printer */    
		// $printer = printer_open("SP-POS76II");  
		// /* write the text to the print job */ 
		// printer_set_option($handle, PRINTER_MODE, "RAW"); 
		// printer_write($printer, $printTestText);   
		// /* close the connection */ 
		// printer_close($printer); 
		 $handle = printer_open("SP-POS76II");
			 printer_set_option($handle, PRINTER_MODE, "RAW");
			 printer_write($handle, $printTestText); 
			 printer_close($handle);
	}

	public function move_transaction(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->move_transaction($param);
		echo $hasil;
	}

	public function cashdraw_transaction(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->cashdraw_transaction($param);
		echo $hasil;
	}

	public function suspend_transaction(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->suspend_transaction($param);
		echo $hasil;
	}

	public function cancel_transaction(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->cancel_transaction($param);
		echo $hasil;
	}

	public function suspen_back(){
		$param = $this->input->get('invoice');
		$hasil = $this->Mdl_registrasi->suspen_back($param);
		echo $hasil;
	}

	public function total_transaction(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->total_transaksi($param);
		echo $hasil;
	}

	public function total_suspend(){
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->total_suspend($param);
		echo $hasil;
	}

	public function total_cancel(){
		$hasil = $this->Mdl_registrasi->total_cancel();
		echo $hasil;
	}

	public function get_list_penjualan($id='')
	{
		$data['list_penjualan'] = $this->Mdl_registrasi->get_list_penjualan($id);
		$view_data = $this->load->view($this->module.'/list_penjualan', $data);
		echo $view_data;
	}

	public function get_list_barang_penjualan()
	{
		$id = $this->input->get('cari_barang');
		
		if ($id!='') {
			$data['barang'] = $this->Mdl_registrasi->get_barang('name',$id);
		}else{
			$data['barang'] = $this->Mdl_registrasi->get_barang('name','');
		}
		
		$view_data = $this->load->view($this->module.'/list_barang_penjualan', $data);
		
		echo $view_data;
	}

	public function get_list_barang_barcode_penjualan()
	{
		$id = $this->input->get('search_barcode');
		
		if ($id!='') {
			$data['barang'] = $this->Mdl_registrasi->get_barang('code',$id);
		}else{
			$data['barang'] = $this->Mdl_registrasi->get_barang('code','');
		}
		
		$view_data = $this->load->view($this->module.'/list_barang_barcode_penjualan', $data);
		
		echo $view_data;
	}


	public function add_penjualan()
	{
		$param = $this->input->get();
		$this->Mdl_registrasi->add_penjualan($param);
	}

	public function edit_penjualan()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->edit_penjualan($param);

		if ($hasil>=0) {
			echo 'Sukses, Item pembelian berhasil diedit!';
		}else{
			echo 'Gagal, Item pembelian gagal diedit!';
		}
	}

	public function delete_penjualan()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->delete_penjualan($param);

		if ($hasil>=0) {
			echo 'Sukses, Item pembelian berhasil dihapus!';
		}else{
			echo 'Gagal, Item pembelian gagal dihapus!';
		}
	}

	public function get_total_penjualan($id='')
	{
		$total = $this->Mdl_registrasi->get_list_penjualan($id);
		$totalnya = 0;
		foreach ($total as $key => $value) {
			$totalnya += $value->subtotal;
		}

		echo format_rupiah($totalnya);
	}

	public function get_kembalian()
	{
		$jumlah_bayar = $this->input->get('jumlah_bayar');

		$total = $this->Mdl_registrasi->get_list_penjualan();
		$totalnya = 0;
		foreach ($total as $key => $value) {
			$totalnya += $value->subtotal;
		}

		$total_kembalian = $jumlah_bayar - $totalnya;

		echo format_rupiah($jumlah_bayar).'|'.format_rupiah($total_kembalian);

	}

	public function proses_pembayaran()
	{
		$param = $this->input->post();

		$hasil = $this->Mdl_registrasi->proses_pembayaran($param);

		if ($hasil>=0) {
			echo 'Sukses, Proses Pembelian Berhasil diproses!';
		}else{
			echo 'Gagal, Proses Pembelian Gagal diproses!';
		}
		exit;
	}

	public function proses_suspend()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_registrasi->proses_suspend($param);
		echo $hasil;
		
	}

	

}

/* End of file penjualan.php */
/* Location: ./application/modules/cashier/controllers/penjualan.php */