<div id="page-content" class="block full">
	<div class="block-header">
		<a href="#" class="header-title-link">
		<h1>
		<i class="icon-remove-circle animation-expandUp"></i>404 Error<br><small>Halaman tidak ditemukan, mohon klik back untuk kembali ke halaman sebelumnya.</small>
		</h1>
		</a>
	</div>
	<ul class="breadcrumb breadcrumb-top">
		<li><i class="icon-file-alt"></i></li>
		<li>Pages</li>
		<li><a href="#">404 Error</a></li>
	</ul>
	<div class="row gutter30 error-container animation-expandUp">
		<div class="col-xs-6 text-right">
			<i class="icon-remove text-danger"></i>
		</div>
		<div class="col-xs-6 text-left">
			<h1>404</h1>
			<small>PAGE NOT FOUND!</small>
		</div>
	</div>
</div>