<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->module = 'error';
		$this->cname = 'error';
		$this->load->model('Mdl_login');
		// echo 'ihir';
	}

	public function index()
	{
		$data['cname'] = $this->module;
		$data['title'] = "404";
		$data['content'] = $this->load->view($this->module.'/404',$data,TRUE);
		$this->load->view('/template', $data);
		
	}



	public function logout()
	{
		$this->session->unset_userdata('accountantsession');
		
		redirect($this->module);
	}

	public function genpass($value='')
	{
		echo paramEncrypt($value);
	}

	public function test()
	{
		$value = 'ihir';
		return $value;
	}
}

/* End of file signin.php */
/* Location: ./application/modules/cashier/controllers/signin.php */