<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penjualan extends BASMALAH_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->module = 'cashier';
		$this->cname = 'cashier/penjualan/';
		$this->load->model('Mdl_penjualan');
	}
	
	public function index()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$data['cname'] = $this->module;
		$data['title'] = "Penjualan";
		$data['barang'] = $this->Mdl_penjualan->get_barang();
		$data['content'] = $this->load->view($this->module.'/home',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function checking()
	{
		$getter=$_REQUEST['is_ajax'];
		$tgl=$_REQUEST['tgl'];
		$user = $this->session->userdata('basmalahsession');;
		$hasil = $this->Mdl_penjualan->checking($getter,$tgl,$user);

		echo $hasil;
	}

	public function buka_kasir()
	{
		$buka_kasir = $this->input->get('buka_kasir');

		$hasil = $this->Mdl_penjualan->buka_kasir($buka_kasir);

		if ($hasil>=0) {
			echo 'Sukses, Kasir Berhasil Dibuka!';
		}else{
			echo 'Gagal, Kasir Gagal Dibuka!';
		}
	}

	public function get_list_penjualan($id='')
	{
		$data['list_penjualan'] = $this->Mdl_penjualan->get_list_penjualan($id);

		$view_data = $this->load->view($this->module.'/list_penjualan', $data);
		
		echo $view_data;
	}

	public function get_list_barang_penjualan()
	{
		$id = $this->input->get('cari_barang');
		
		if ($id!='') {
			$data['barang'] = $this->Mdl_penjualan->get_barang('name',$id);
		}else{
			$data['barang'] = $this->Mdl_penjualan->get_barang('name','');
		}
		
		$view_data = $this->load->view($this->module.'/list_barang_penjualan', $data);
		
		echo $view_data;
	}

	public function get_list_barang_barcode_penjualan()
	{
		$id = $this->input->get('search_barcode');
		
		if ($id!='') {
			$data['barang'] = $this->Mdl_penjualan->get_barang('code',$id);
		}else{
			$data['barang'] = $this->Mdl_penjualan->get_barang('code','');
		}
		
		$view_data = $this->load->view($this->module.'/list_barang_barcode_penjualan', $data);
		
		echo $view_data;
	}


	public function add_penjualan()
	{
		$param = $this->input->get();
		$this->Mdl_penjualan->add_penjualan($param);
	}

	public function edit_penjualan()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_penjualan->edit_penjualan($param);

		if ($hasil>=0) {
			echo 'Sukses, Item pembelian berhasil diedit!';
		}else{
			echo 'Gagal, Item pembelian gagal diedit!';
		}
	}

	public function delete_penjualan()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_penjualan->delete_penjualan($param);

		if ($hasil>=0) {
			echo 'Sukses, Item pembelian berhasil dihapus!';
		}else{
			echo 'Gagal, Item pembelian gagal dihapus!';
		}
	}

	public function get_total_penjualan($id='')
	{
		$total = $this->Mdl_penjualan->get_list_penjualan($id);
		$totalnya = 0;
		foreach ($total as $key => $value) {
			$totalnya += $value->subtotal;
		}

		echo format_rupiah($totalnya);
	}

	public function get_kembalian()
	{
		$jumlah_bayar = $this->input->get('jumlah_bayar');

		$total = $this->Mdl_penjualan->get_list_penjualan();
		$totalnya = 0;
		foreach ($total as $key => $value) {
			$totalnya += $value->subtotal;
		}

		$total_kembalian = $jumlah_bayar - $totalnya;

		echo format_rupiah($jumlah_bayar).'|'.format_rupiah($total_kembalian);

	}

	public function proses_pembayaran()
	{
		$param = $this->input->post();

		$hasil = $this->Mdl_penjualan->proses_pembayaran($param);

		if ($hasil>=0) {
			echo 'Sukses, Proses Pembelian Berhasil diproses!';
		}else{
			echo 'Gagal, Proses Pembelian Gagal diproses!';
		}
		exit;
	}

	public function proses_suspend()
	{
		$hasil = $this->Mdl_penjualan->proses_suspend();

		if ($hasil>=0) {
			echo 'Sukses, Proses Pembelian Berhasil disuspend!';
		}else{
			echo 'Gagal, Proses Pembelian Gagal disuspend!';
		}
	}

	public function proses_void()
	{
		$hasil = $this->Mdl_penjualan->proses_void();

		if ($hasil>=0) {
			echo 'Sukses, Proses void berhasil dijalankan!';
		}else{
			echo 'Gagal, Proses void gagal dijalankan!';
		}
	}

	

}

/* End of file penjualan.php */
/* Location: ./application/modules/cashier/controllers/penjualan.php */