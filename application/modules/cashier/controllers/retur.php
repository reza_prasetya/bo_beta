<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Retur extends BASMALAH_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->module = 'cashier';
		$this->cname = 'cashier/retur/';
		$this->load->model('Mdl_retur');
	}
	
	public function index()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);
		
		$data['cname'] = $this->module;
		$data['title'] = "Retur";
		$data['barang'] = $this->Mdl_retur->get_barang();
		$data['content'] = $this->load->view($this->module.'/retur',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function checking()
	{
		$getter=$_REQUEST['is_ajax'];
		$tgl=$_REQUEST['tgl'];
		$user = $this->session->userdata('basmalahsession');;
		$hasil = $this->Mdl_retur->checking($getter,$tgl,$user);

		echo $hasil;
	}

	public function ceksession()
	{
		$nota = $this->session->userdata('notasession');
		if ($nota != null) {
			echo json_encode($nota[0]);
		}
		else{
			echo 'kosong';
		}
	}

	public function ceknota()
	{
		$nota = $this->input->post('nota');
		$hasil = $this->Mdl_retur->get_nota($nota);

		if ($hasil != null) {
			$this->session->set_userdata( 'notasession', $hasil );
			echo 'nota ada';
		}else{
			echo 'nota tidak ada';
		}
	}

	public function cekbarangretur()
	{
		$id_product = $this->input->post('id_product');
		$dataproduk = $this->Mdl_retur->cekbarangretur($id_product);
		if ($dataproduk != null) {
			$insert_retur_tmp = $this->Mdl_retur->insert_retur_tmp($dataproduk);
		}
		
	}

	public function get_list_retur()
	{
		$data['retur'] = $this->Mdl_retur->get_list_retur();
		$view_data = $this->load->view($this->module.'/list_retur', $data);
		
		echo $view_data;
	}

	public function proses_retur()
	{
		$nota = $this->input->post('nota');

		$hasil = $this->Mdl_retur->proses_retur($nota);

		if ($hasil >= 0) {
			echo 'Retur untuk invoice '.$nota.' berhasil dilakukan';
			$this->session->unset_userdata('notasession');
		}else{
			echo 'Retur untuk invoice '.$nota.' gagal dilakukan';
		}
	}

	public function get_list_penjualan($id='')
	{
		$data['list_penjualan'] = $this->Mdl_retur->get_list_penjualan($id);

		$view_data = $this->load->view($this->module.'/list_penjualan_retur', $data);
		
		echo $view_data;
	}

	public function get_list_barang_penjualan()
	{
		$id = $this->input->get('cari_barang');
		
		if ($id!='') {
			$data['barang'] = $this->Mdl_retur->get_barang('name',$id);
		}else{
			$data['barang'] = $this->Mdl_retur->get_barang('name','');
		}
		
		$view_data = $this->load->view($this->module.'/list_barang_penjualan', $data);
		
		echo $view_data;
	}

	public function get_list_barang_barcode_penjualan()
	{
		$id = $this->input->get('search_barcode');
		
		if ($id!='') {
			$data['barang'] = $this->Mdl_retur->get_barang('code',$id);
		}else{
			$data['barang'] = $this->Mdl_retur->get_barang('code','');
		}
		
		$view_data = $this->load->view($this->module.'/list_barang_barcode_penjualan', $data);
		
		echo $view_data;
	}


	public function add_penjualan()
	{
		$param = $this->input->get();
		$this->Mdl_retur->add_penjualan($param);
	}

	public function edit_penjualan()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_retur->edit_penjualan($param);

		if ($hasil>=0) {
			echo 'Sukses, Item pembelian berhasil diedit!';
		}else{
			echo 'Gagal, Item pembelian gagal diedit!';
		}
	}

	public function delete_penjualan()
	{
		$param = $this->input->post();
		$hasil = $this->Mdl_retur->delete_penjualan($param);

		if ($hasil>=0) {
			echo 'Sukses, Item pembelian berhasil dihapus!';
		}else{
			echo 'Gagal, Item pembelian gagal dihapus!';
		}
	}

	public function get_total_penjualan($id='')
	{
		$total = $this->Mdl_retur->get_list_penjualan($id);
		$totalnya = 0;
		foreach ($total as $key => $value) {
			$totalnya += $value->subtotal;
		}

		echo format_rupiah($totalnya);
	}

	public function get_kembalian()
	{
		$jumlah_bayar = $this->input->get('jumlah_bayar');

		$total = $this->Mdl_retur->get_list_penjualan();
		$totalnya = 0;
		foreach ($total as $key => $value) {
			$totalnya += $value->subtotal;
		}

		$total_kembalian = $jumlah_bayar - $totalnya;

		echo format_rupiah($jumlah_bayar).'|'.format_rupiah($total_kembalian);

	}

	public function proses_pembayaran()
	{
		$param = $this->input->post();

		$hasil = $this->Mdl_retur->proses_pembayaran($param);

		if ($hasil>=0) {
			echo 'Sukses, Proses Pembelian Berhasil diproses!';
		}else{
			echo 'Gagal, Proses Pembelian Gagal diproses!';
		}
		exit;
	}

	public function proses_suspend()
	{
		$hasil = $this->Mdl_retur->proses_suspend();

		if ($hasil>=0) {
			echo 'Sukses, Proses Pembelian Berhasil disuspend!';
		}else{
			echo 'Gagal, Proses Pembelian Gagal disuspend!';
		}
	}

	public function proses_void()
	{
		$hasil = $this->Mdl_retur->proses_void();

		if ($hasil>=0) {
			echo 'Sukses, Proses void berhasil dijalankan!';
		}else{
			echo 'Gagal, Proses void gagal dijalankan!';
		}
	}

}

/* End of file penjualan.php */
/* Location: ./application/modules/cashier/controllers/penjualan.php */