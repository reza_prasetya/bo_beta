<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signin extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->module = 'cashier';
		$this->cname = 'cashier/signin';
		$this->load->model('Mdl_login');
	}

	public function index()
	{
		$data['cname'] = $this->module;
		$data['title'] = "Penjualan";
		$data['content'] = $this->load->view($this->module.'/login',$data,TRUE);
		$this->load->view('/template', $data);
		
	}

	public function login()
	{
		$param = $this->input->post();
		
		$hasil = $this->Mdl_login->login($param);

		if ($hasil != null) {
			$this->session->set_userdata( 'basmalahsession', $hasil );
		}
		
		redirect($this->module);
	}

	public function logout()
	{
		$this->session->unset_userdata('basmalahsession');
		$this->session->unset_userdata('membersession');
		$this->session->unset_userdata('notasession');
					
		redirect($this->module);
	}

	public function genpass($value='')
	{
		echo paramEncrypt($value);
	}

	public function denpass($value='')
	{
		echo paramDecrypt($value);
	}
}

/* End of file signin.php */
/* Location: ./application/modules/cashier/controllers/signin.php */