<a href="javascript:void(0)" id="to-top"><i class="icon-angle-up"></i></a>
<script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main-1.03.js"></script>
<script>
$(function () {
	$("#modal-compose").on("shown.bs.modal", function () {
		$(".modal-select-chosen").chosen({
			width : "100%"
		})
	}),
	$(".ms-message-list").slimScroll({
		height : 610,
		color : "#000000",
		size : "3px",
		touchScrollStep : 750
	});
	var e = $(".ms-categories");
	$("a", e).click(function () {
		$("li", e).removeClass("active"),
		$(this).parent().addClass("active"),
		$(this).data("cat")
	});
	begin();
});
</script>
<script type="text/javascript">
	var col;
	function ChangeCol(){
		if(col == 1){
	colors = new Array('#4D3D98', '#0C8B44', '#88288F', '#D24827', '#00A0B1'); /* Colors of the Menu Items */
	body_color = "#1C1651"; /* Background Color of the whole Navigation */
	hover_color = "#352869"; /* Color of the border of the menu items for HOVER*/
	scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
	scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
	scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
	$("div.metro_nav").show();
	$("div.metro_na").hide();
		}else if(col == 2){
	colors = new Array('#4472B8', '#56439B', '#069547', '#B41E43', '#D74F2B');
	body_color = "#1D1D1D"; /* Background Color of the whole Navigation */
	hover_color = "#2D2D2D"; /* Color of the border of the menu items for HOVER*/
	scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
	scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
	scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
	$("div.metro_nav").show();
	$("div.metro_na").hide();
		}else if(col == 3){
			$("div.metro_nav").hide();
			$("div.metro_na").show();
		}
		setColors();
	}
</script>
<script type="text/javascript">
	function e(e, t) {
		return Math.floor(Math.random() * (t - e + 1)) + e
	}
	function toggleButton(){
		if($('.hide-key').is(':visible')){
			$('.hide-key').hide();
		} else {
			$('.hide-key').show();
		}
	}
	function generate(){
		var t = 0;
		$(".progress-bar", ".bars-container").each(function () {
			t = e(10, 100) + "%",
			$(this).css("width", t).html(t)
		});
	}
	function akun(){
		$('#awal-persediaan').slideUp();
		$('#awal-piutang').slideUp();
		$('#awal-hutang').slideUp();
		$('#awal-akun').slideDown();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function hutang(){
		$('#awal-persediaan').slideUp();
		$('#awal-piutang').slideUp();
		$('#awal-hutang').slideDown();
		$('#awal-akun').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function persediaan(){
		$('#awal-persediaan').slideDown();
		$('#awal-piutang').slideUp();
		$('#awal-hutang').slideUp();
		$('#awal-akun').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function piutang(){
		$('#awal-persediaan').slideUp();
		$('#awal-piutang').slideDown();
		$('#awal-hutang').slideUp();
		$('#awal-akun').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function importButton(){
		$('#awal-persediaan').slideUp();
		$('#awal-piutang').slideUp();
		$('#awal-hutang').slideUp();
		$('#awal-akun').slideUp();
		$('#import-div').slideDown();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function exportButton(){
		$('#awal-persediaan').slideUp();
		$('#awal-piutang').slideUp();
		$('#awal-hutang').slideUp();
		$('#awal-akun').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideDown();
		$('#sync-div').slideUp();
	}
	function syncButton(){
		$('#awal-persediaan').slideUp();
		$('#awal-piutang').slideUp();
		$('#awal-hutang').slideUp();
		$('#awal-akun').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideDown();
		generate();
	}
	function begin(){
		$('#awal-persediaan').slideUp();
		$('#awal-piutang').slideUp();
		$('#awal-hutang').slideUp();
		$('#awal-akun').slideDown();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
</script>