<a href="javascript:void(0)" id="to-top"><i class="icon-angle-up"></i></a>
<script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main-1.03.js"></script>
<script>
$(function () {
	$("#modal-compose").on("shown.bs.modal", function () {
		$(".modal-select-chosen").chosen({
			width : "100%"
		})
	}),
	$(".ms-message-list").slimScroll({
		height : 610,
		color : "#000000",
		size : "3px",
		touchScrollStep : 750
	});
	var e = $(".ms-categories");
	$("a", e).click(function () {
		$("li", e).removeClass("active"),
		$(this).parent().addClass("active"),
		$(this).data("cat")
	});
	jurnal();
});
</script>
<script type="text/javascript">
	var col;
	function ChangeCol(){
		if(col == 1){
	colors = new Array('#4D3D98', '#0C8B44', '#88288F', '#D24827', '#00A0B1'); /* Colors of the Menu Items */
	body_color = "#1C1651"; /* Background Color of the whole Navigation */
	hover_color = "#352869"; /* Color of the border of the menu items for HOVER*/
	scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
	scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
	scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
	$("div.metro_nav").show();
	$("div.metro_na").hide();
		}else if(col == 2){
	colors = new Array('#4472B8', '#56439B', '#069547', '#B41E43', '#D74F2B');
	body_color = "#1D1D1D"; /* Background Color of the whole Navigation */
	hover_color = "#2D2D2D"; /* Color of the border of the menu items for HOVER*/
	scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
	scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
	scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
	$("div.metro_nav").show();
	$("div.metro_na").hide();
		}else if(col == 3){
			$("div.metro_nav").hide();
			$("div.metro_na").show();
		}
		setColors();
	}
</script>
<script type="text/javascript">
	function e(e, t) {
		return Math.floor(Math.random() * (t - e + 1)) + e
	}
	function toggleButton(){
		if($('.hide-key').is(':visible')){
			$('.hide-key').hide();
		} else {
			$('.hide-key').show();
		}
	}
	function generate(){
		var t = 0;
		$(".progress-bar", ".bars-container").each(function () {
			t = e(10, 100) + "%",
			$(this).css("width", t).html(t)
		});
	}
	function transaksi(){
		$('#tambah-div').slideUp();
		$('#list-jurnal').slideUp();
		$('#transaksi-jurnal').slideDown();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function jurnal(){
		$('#tambah-div').slideDown();
		$('#list-jurnal').slideDown();
		$('#transaksi-jurnal').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function importButton(){
		$('#tambah-div').slideUp();
		$('#list-jurnal').slideUp();
		$('#transaksi-jurnal').slideUp();
		$('#import-div').slideDown();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function exportButton(){
		$('#tambah-div').slideUp();
		$('#list-jurnal').slideUp();
		$('#transaksi-jurnal').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideDown();
		$('#sync-div').slideUp();
	}
	function syncButton(){
		$('#tambah-div').slideUp();
		$('#list-jurnal').slideUp();
		$('#transaksi-jurnal').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideDown();
		generate();
	}
</script>
<script>
	function optAkun(){
		var url = "<?php echo base_url(); ?>accounting/_option/opt_akun_list";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$(".akun-list").html(msg);
				$('.akun-list').trigger("chosen:updated");
			}
		});
		return false;
	}
	function optTrans(){
		// var rekening =  $("#no_rekening").val();
	    var url = "<?php echo base_url(); ?>accounting/_nomor_urut/no_jurnal_umum";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split('|');
				$("#kode_bukti").val(data[0]);
				$("#no_bukti").val(data[1]);
				jurnal_list();
			}
		});
		return false;
	}
	function jurnal_list(){
		var kode = $('#no_bukti').val();
		var url = "<?php echo base_url(); ?>accounting/jurnal_umum/jurnal_list/";
		var form_data = {
			kode:kode
		};
		// alert(kode);
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$('#jurnal-list').html(msg);
			}
		});
		return false;
	}
	function batalkan(){
		var kode = $('#no_bukti').val();
		var url = "<?php echo base_url(); ?>accounting/jurnal_umum/batalkan/";
		var form_data = {
			kode:kode
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				if(msg==0){
					alert('Delete gagal');
				} else if(msg==1){
					clear();
					$('#kode_bukti').val('');
					$('#no_bukti').val('');
					$('#tgl_transaksi').val('');
					$('#ket_transaksi').val('');
					setTimeout(function() {location.reload();}, 1500);
				}
			}
		});
		return false;
	}
	function simpan(){
		var balance = $('#total-balance').html();
		if(balance!=0){
			alert('Balance harus 0.');
		} else {
			var kode = $("input[name=no_bukti]").val();
			var url = "<?php echo base_url(); ?>accounting/jurnal_umum/simpan/";
			var form_data = {
				kode:kode
			};
			// this bit needs to be loaded on every page where an ajax POST may happen
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					alert(msg);
					if(msg==0){
						alert('Delete gagal');
					} else if(msg==1){
						clear();
						$('#kode_bukti').val('');
						$('#no_bukti').val('');
						$('#tgl_transaksi').val('');
						$('#ket_transaksi').val('');
						setTimeout(function() {location.reload();}, 1500);
					}
				}
			});
			return false;
		}
	}
	function clear(){
		$('#akun').prop('selected', false);
    	$('#akun').trigger('liszt:updated');
    	$('#posisi').prop('selected', false);
    	$('#posisi').trigger('liszt:updated');

		// $('#akun').val('');
		// $('#posisi').val('');
		$('#nominal').val('');
		$('#faktur').val('');
	}
	$(document).ready(function() {
		optTrans();
		optAkun();
		jurnal_list();	
		$('#form-jurnal-umum').submit(function(){
			var kode = $("input[name=kode_bukti]").val();
			// var kode = $('#kode_bukti').val();
			var referensi = $('#no_bukti').val();
			var tgl = $('#tgl_transaksi').val();
			var keterangan = $('#ket_transaksi').val();
			var akun  = $('#akun').val();
			var posisi = $('#posisi').val();
			var nominal = $('#nominal').val();
			var faktur = $('#faktur').val();
			if(posisi=='debet'){
				var debet = nominal;
				var kredit = 0;
			} else if(posisi=='kredit'){
				var debet = 0;
				var kredit = nominal;
			}
			// alert(kode+' | '+referensi+' | '+tgl+' | '+keterangan+' | '+akun+' | '+posisi+' | '+nominal+' | '+faktur);
			var url = "<?php echo base_url(); ?>accounting/jurnal_umum/insert/";
			var form_data = {
				kode:kode,
				no_referensi:referensi,
				no_rekening:akun,
				tanggal:tgl,
				keterangan:keterangan,
				debit:debet,
				kredit:kredit,
				faktur:faktur
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					if(msg==0){
						alert('gagal');
					} else if(msg==1){
						jurnal_list();
						clear();
					} else {
						alert(msg);
					}
				}
			});
			return false;
		});	
	});
</script>