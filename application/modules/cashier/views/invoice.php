<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="<?php echo base_url().'public/invoice/'; ?>invoice.css" type="text/css" charset="utf-8" />
	<script type="text/javascript" src="<?php echo base_url().'public/invoice/'; ?>js/jquery.js"></script> 
	<script type="text/javascript" src="<?php echo base_url().'public/invoice/'; ?>js/jquery.tablesorter.js"></script> 
	  <script type="text/javascript">
	  $(document).ready(function() 
	      { 
	          $("#table").tablesorter({ 
	      
	      }); 
	      } 
	  ); 
	  </script>
	  
	<title>Invoice</title>

</head>

<body onload="window.print();">
 <!--  -->
<div id="page">
	
	<p class="company-address">
		BMS Sidogiri <br/>
		[alamat]<br/>
		[website]
		[contact]
	</p>	
		
	<p class="recipient-address">
	<strong>Invoice ID : <?php echo $hasil[0]->invoice; ?></strong><br />
	Operator : <?php echo $hasil[0]->user_name; ?><br />
	Tanggal : <?php echo date('d M Y'); ?><br />
	Jam : <?php echo date('H:i:s'); ?>
	</p>
	
	<table id="table" class="tablesorter" cellspacing="0"> 
	<thead> 
		<tr> 
			<th>Item</th>
			<th>Harga</th> 
			<th>Jml</th> 
			<th>Discount</th> 
			<th>Subtotal</th> 
		</tr> 
	</thead> 
	<tbody>
	<?php
		  foreach ($hasil as $key => $value) {
		  	?>
				<tr> 
					<td><?php echo $value->product_name; ?></td>
					<td><?php echo $value->price; ?></td> 
					<td><?php echo $value->qty; ?></td>
					<td><?php echo $value->discount.'%'; ?></td> 
					<td><?php echo $value->discount_sub; ?></td> 
				</tr>
			<?php
		  }
	?>
	</tbody>
	<tfoot>
			<tr> 
				<td colspan="4">Sub Total</td> 
				<td><?php echo $hasil[0]->total; ?></td> 
			</tr>

	</tfoot> 
	</table>
	<div class="total-due">
		<div class="total-heading"><p>Detail Pembayaran</p></div>
		<table id="table" class="tablesorter" cellspacing="0"> 
		<thead> 
			<tr> 
				<th></th>
				<th></th> 
				<th></th> 
				<th></th> 
			</tr> 
		</thead> 
		<tbody>
			<tr> 
				<td colspan="3">Bayar : </td><td><?php echo $hasil[0]->pay; ?></td>
			</tr>
			<tr> 
				<td colspan="3">Kembali : </td><td><?php echo $hasil[0]->pay_back; ?></td>
			</tr>
		</tbody>
		</table>
	</div>
	
	
	<hr />
	
	<div class="terms">
		Terima kasih atas kunjungan Anda.
	
</div>

</body>
</html>
