<?php $user = $this->session->userdata('basmalahsession'); ?>

<div id="modal-regular" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button class="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
	                <h4 class="modal-title">Buka Kasir</h4>
	            </div>
	            <div class="modal-body">
						<div class="row" style="margin-bottom: 10px;">
							<div class="form-horizontal">
								<div class="form-group">
									<div class="col-md-12">
										<input id="buka_kasir" name="search-term" class="form-control buka_kasir" placeholder="Kas Awal" type="text" style="height:45px;">
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-sm-12">
								<!-- <button onclick="alert('Sukses, Kasir Berhasil Dibuka!');$('#modal-regular').modal('hide');return false;" class="btn btn-block btn-lg btn-primary">Buka Kasir</button> -->
								<button id = "but_buka_kasir" class="btn btn-block btn-lg btn-primary">Buka Kasir</button>
							</div>
						</div>
					</div>
	            <div class="modal-footer">
	                <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
	            </div>
	        </div>
	    </div>

	</div>

	<div id="modal-tutup" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button class="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
	                <h4 class="modal-title">Tutup Kasir</h4>
	            </div>
	            <div class="modal-body">
						<div class="row" style="margin-bottom: 10px;">
							<div class="form-horizontal">
								<div class="form-group">
									<div class="col-md-12">
										<input id="tutup_kasir" name="search-term" class="form-control tutup_kasir" placeholder="Kas Akhir" type="text" style="height:45px;">
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-sm-12">
								<button onclick="alert('Sukses, Kasir Berhasil Ditutup!');$('#modal-tutup').modal('hide');tutupKasir();return false;" class="btn btn-block btn-lg btn-primary">Tutup Kasir</button>
							</div>
						</div>
					</div>
	            <div class="modal-footer">
	                <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
	            </div>
	        </div>
	    </div>

	</div>
<div id="fx-container" class="fx-opacity">
				<div id="page-content" class="block" style="min-height:650px;">
					<!-- Start Content -->
					<div class="row">
						<div class="col-sm-12">
							<div class="row" style='margin-bottom:10px;'>
								<div class="col-sm-3">
									<button class="btn btn-block btn-lg btn-primary" onClick="browseButton()"><i class="icon-reorder"></i> HOME</button>
								</div>
								<div class="col-sm-3">
									<button class="btn btn-block btn-lg btn-primary" onClick="searchButton()"><i class="icon-search"></i> SEARCH</button>
								</div>
								<div class="col-sm-3">
									<button class="btn btn-block btn-lg btn-primary" onClick="scanButton()"><i class="icon-barcode"></i> SCAN</button>
								</div>
								<div class="col-sm-3">
									<button class="btn btn-block btn-lg btn-primary" data-toggle="modal" href="#modal-tutup"><i class="icon-money"></i> TUTUP KASIR</button>
								</div>
							</div>
							<div class="row" style='margin-bottom:10px; display:none;'>
								<div class="col-sm-6">
									<button class="btn btn-block btn-lg btn-primary" onClick="exportButton()"><i class="icon-search"></i>EXPORT</button>
								</div>
								<div class="col-sm-6">
									<button class="btn btn-block btn-lg btn-primary" onClick="syncButton()"><i class="icon-reorder toggle-bars"></i> SYNC DATA</button>
								</div>
							</div>
							<div class="block full" style="margin-top:10px;" id="import-div">
								<div class="alert alert-info alert-dismissable">
									<h4><i class="icon-info-sign"></i> Info</h4> Silahkan Upload Data Produk dan Masukkan Keycode yang Anda Dapatkan melalui Kantor Pusat - <a href="javascript:void(0)" class="alert-link">File Berupa CSV</a>!
								</div>
								<form action="" method="post" enctype="multipart/form-data" class="form-horizontal" onSubmit="return false;">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-asterisk"></i></span>
											<input type="password" id="example-password3" name="example-password3" class="form-control">
											<span class="input-group-addon">Keycode</span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label" for="example-file-input">File input</label>
										<div class="col-md-5">
											<input type="file" id="example-file-input" name="example-file-input">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-5 col-md-offset-2">
											<button type="reset" class="btn btn-default"><i class="icon-remove"></i> Reset</button>
											<button type="submit" class="btn btn-primary"><i class="icon-arrow-right"></i> Submit</button>
										</div>
									</div>
								</form>
							</div>
							<div class="block full" style="margin-top:10px;" id="export-div">
								<div class="alert alert-info alert-dismissable">
									<h4><i class="icon-info-sign"></i> Info</h4> Silahkan Masukkan Keycode dan generate file CSV Produk - <a href="javascript:void(0)" class="alert-link">File Berupa CSV</a>!
								</div>
								<form action="" method="post" enctype="multipart/form-data" class="form-horizontal" onSubmit="return false;">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-asterisk"></i></span>
											<input type="password" id="example-password3" name="example-password3" class="form-control">
											<span class="input-group-addon">Keycode</span>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-5 col-md-offset-2">
											<button type="reset" class="btn btn-default"><i class="icon-remove"></i> Reset</button>
											<button type="submit" class="btn btn-primary"><i class="icon-arrow-right"></i> Generate and Sends</button>
										</div>
									</div>
								</form>
							</div>
							<div class="block full" style="margin-top:10px;" id="sync-div">
								<div class="alert alert-info alert-dismissable">
									<h4><i class="icon-info-sign"></i> Info</h4> Silahkan tunggu hingga proses sinkronisasi selesai (100%)
								</div>
								<div class="bars-container">
									<h4>Sedang dalam proses</h4>
									
									<div class="progress progress-striped active">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">80%</div>
									</div>
								</div>
							</div>
							<div class="block" id="selling-div">
								<div class="row">
									<div class="col-sm-9">
										<blockquote>
											<p><i class="icon-list"></i> PENJUALAN</p>
										</blockquote>
										<input type = "text" id = "id_produk" name = "id_produk" />
										<input type = "hidden" id = "kas_penjualan" name = "kas_penjualan" />
									</div>
									
									<div class="col-sm-3" align="right">
										<blockquote>
											<p><i class="icon-file-text"></i><i class="dataTgl"><?php echo date('Y-m-d'); ?></i></p>
										</blockquote>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<blockquote>
											<p><i class="icon-file-text"></i><i class="dataInvoice">&nbsp;BSM1/0123</i> </p>
										</blockquote>
									</div>
									<div class="col-sm-4">
										<blockquote>
											<p><i class="icon-laptop" class="dataNote"></i>&nbsp;CASHI/131</p>
										</blockquote>
									</div>

									<div class="col-sm-4">
										<blockquote>
											<p><i class="icon-user"></i><i class="dataUser">&nbsp;<?php echo $user[0]->nama; ?></i></p>
										</blockquote>
									</div>
								</div>
								<div class="table-responsive" id = "list_penjualan">
									
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="alert alert-success">
											<h4 style="margin-top:10px;">
												<strong>TAX : </strong>
												<span class="pull-right">Rp. -</span>
											</h4>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="alert alert-success">
											<h4 style="margin-top:10px;">
												<strong>DISC : </strong>
												<span class="pull-right">Rp. -</span>
											</h4>
										</div>
									</div>
								</div>
								<div class="alert alert-success">
									<h2 style="margin-top:10px;">
										<strong>Total : </strong>
										<span class="pull-right total_penjualan">Rp. 0</span>
									</h2>
								</div>
								<div class="row"  style="margin-bottom: 10px;">
									<div class="col-sm-6">
										<button class="btn btn-block btn-lg btn-danger" id = "but_void">VOID</button>
									</div>
									<div class="col-sm-6">
										<a class="btn btn-block btn-lg btn-primary" id = "but_suspend">SUSPEND</a>
									</div>
								</div>
								<div class="row"  style="margin-bottom: 20px;">
									<div class="col-sm-12">
										<button class="btn btn-block btn-lg btn-success" onClick="paymentButton()">PAY</button>
									</div>
								</div>
							</div>
							<div class="block full" style="margin-top:10px;" id="payment-div">
								<div class="row">
									
								</div>
								<div class="row">
									<div class="col-sm-6">
										<table class="table table-striped">
											<tbody>
												<tr>
													<td class="col-xs-12">
														<div class="row text-center">
															<div class="col-md-12">
																<h4>Total</h4>
																<h2 class="text-success pay_total_jual"></h2>
															</div>
															<br/>
															<div class="col-md-12">
																<h4>Dibayar</h4>
																<h2 class="text-success pay_dibayar">Rp. -</h2>
															</div>
															<br/>
															<div class="col-md-12">
																<h4>Kembalian</h4>
																<h2 class="text-warning pay_kembalian">Rp. -</h2>
															</div>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="col-sm-6 pull-right">
										<form id = "form_pembayaran">
											<div class="row" style="margin-bottom: 10px;">
												<div class="form-horizontal">
													
													<div class="form-group">
														<div class="col-md-12">
															<input type = 'hidden' name = 'pay_kas' id = 'pay_kas' />
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12">
															<select id="example-select selling_pay" name="example-select" class="form-control selling_pay" size="1" style="height:45px;">
																<option value="0">Pilih Pelanggan</option>
																<option value="2">Pak Joko</option>
																<option value="3">Pak Tejo</option>
																<option value="3">Bu Maimunah</option>
															</select>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12">
															<select id="example-select" name="example-select" class="form-control" size="1" style="height:45px;">
																<option value="0">Pilih Cara Pembayaran</option>
																<option value="2">Cash</option>
																<option value="3">Giro</option>
																<option value="3">Voucher</option>
															</select>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12">
															<input id="jumlah_bayar" name="search-term" class="form-control" placeholder="Bayar Sejumlah" type="text" style="height:45px;">
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12">
															<input id="search-term" name="search-term" class="form-control" placeholder="Diskon" type="text" style="height:45px;">
														</div>
													</div>
												</div>
											</div>
											<div class="row" style="margin-bottom: 10px;">
												<div class="col-sm-12">
													<a id = "but_bayar_penjualan" class="btn btn-block btn-lg btn-primary">Bayar</a>
												</div>
												
											</div>
										</form>
										
									</div>
								</div>
							</div>
							<div class="block full" style="margin-top:10px;" id="browse-div">
								<div class="row">
									<div class="col-md-4" style="margin-top: -30px;">
										<h4 class="sub-header">CATEGORIES</h4>
										<div class="ms-message-list list-group">
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
										</div>
									</div>
									<div class="col-md-8" style="margin-top: -30px;">
										<h4 class="sub-header">LIST ITEMS</h4>
										<div class="ms-message-list list-group">
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="block full" style="margin-top:10px;" id="search-div">
								<div class="row">
									<div class="block-section">
										<form id = "form_cari_barang" method="post">
											<div class="input-group input-group-lg">
												<!-- <div class="input-group-btn">
													<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="min-width:150px;">All Categories <span class="caret"></span></button>
													<ul class="dropdown-menu">
														<li class="active">
															<a href="javascript:void(0)"><i class="icon-ok"></i> All Categories</a>
														</li>
														<li class="divider"></li>
														<li><a href="javascript:void(0)">Users</a></li>
														<li><a href="javascript:void(0)">Projects</a></li>
														<li><a href="javascript:void(0)">Sites</a></li>
													</ul>
												</div> -->
												<input id="cari_barang" name="cari_barang" class="form-control cari_barang" placeholder="Search.." type="text">
												<div class="input-group-btn">
													<button type="submit" class="btn btn-default"><i class="icon-search icon-fixed-width"></i></button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12" style="margin-top: -30px;">
										<h4 class="sub-header">SEARCH RESULT</h4>
										<div class="ms-message-list list-group" id = "list_brg_penjualan">
											
										</div>
									</div>
								</div>
							</div>
							<div class="block full" style="margin-top:10px;" id="scan-div">
								<div class="row">
									<div class="block-section">
										
										<div class="input-group input-group-lg">
											<input id="search_barcode" name="search_barcode" class="form-control" type="text">
											<div class="input-group-btn">
											<button type="submit" class="btn btn-default"><i class="icon-barcode icon-fixed-width"></i></button>
										</div>
										</div>
										
									</div>
								</div>
								<div class="row" style="border-top:1px dotted black;">
									<div class="list-group" id = "list_brg_barcode">
										
									</div>
								</div>
							</div>

						</div>
					</div>
					<!-- End Content -->
				</div>
				<script>$(function(){$('#modal-regular').modal('show');
			$("#buka_kasir").focus();
			$(".buka_kasir").focus();$("#modal-compose").on("shown.bs.modal",function(){$(".modal-select-chosen").chosen({width:"100%"})}),$(".ms-message-list").slimScroll({height:610,color:"#000000",size:"3px",touchScrollStep:750});var e=$(".ms-categories");$("a",e).click(function(){$("li",e).removeClass("active"),$(this).parent().addClass("active"),$(this).data("cat")});begin();});</script>
	<script>

		function toggleButton(){
			if($('.hide-key').is(':visible')){
				$('.hide-key').hide();
			} else {
				$('.hide-key').show();
			}
		}
		function tutupKasir(){
			setTimeout(function() {window.location = "dashboard"},500);
		}
		function browseButton(){
			$('#head-div').slideUp();
			$('#browse-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#payment-div').slideUp();
			$('#selling-div').slideDown();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#sync-div').slideUp();
			$('#id_produk').focus();	
		}
		function paymentButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#payment-div').slideDown();
			$('#selling-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#sync-div').slideUp();
			$("#selling_pay").focus();
			$(".selling_pay").focus();
		}
		function scanButton(){
			
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideDown();
			$('#payment-div').slideUp();
			$('#selling-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#sync-div').slideUp();
			$('#search_barcode').focus();
		}
		function searchButton(){

			$('#head-div').slideUp();
			$('#browse-div').slideUp();
			$('#search-div').slideDown();
			$('#scan-div').slideUp();
			$('#payment-div').slideUp();
			$('#selling-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#sync-div').slideUp();
			$("#search_sale").focus();
			$(".search_sale").focus();
			
		}
		function begin(){
			$('#browse-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#payment-div').slideUp();
			$('.hide-key').slideUp();
			$('#selling-div').slideDown();
			$('#sync-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
		}
		function syncButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#tambah-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#sync-div').slideDown();
			$('#selling-div').slideUp();
			generate();
		}
		function importButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#tambah-div').slideUp();
			$('#import-div').slideDown();
			$('#export-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#sync-div').slideUp();
			$('#selling-div').slideUp();
		}
		function exportButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#tambah-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideDown();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#sync-div').slideUp();
			$('#selling-div').slideUp();
		}

      $(document).on('keydown', function(e){
        var key = e.which;
        if(e.ctrlKey && key == 66){ //CTRL+B
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+P!');
            browseButton();
            return false;
        }else if(e.ctrlKey && key == 83){ //CTRL+S
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+R!');
            searchButton();
            return false;
        }else if(e.ctrlKey && key == 67){ //CTRL+C
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+H!');
            scanButton();
            $('#search_barcode').focus();
            return false;
        }else if(e.ctrlKey && key == 80){ //CTRL+P
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            paymentButton();
            return false;
        }else if(e.ctrlKey && key == 84){ //CTRL+T
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            $('#modal-tutup').modal('show');
			$("#tutup_kasir").focus();
			$(".tutup_kasir").focus();
            return false;
        }else if(key == 27){
        	setTimeout(function() {window.location = "dashboard.html"},500);
        }else{
        	return true;
        }
      });
      $(document).on('keyup', function(e){
          var key = e.which;
        if(e.ctrlKey && key == 66){ //CTRL+B
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+P!');
            browseButton();
            return false;
        }else if(e.ctrlKey && key == 83){ //CTRL+S
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+R!');
            searchButton();
            return false;
        }else if(e.ctrlKey && key == 67){ //CTRL+C
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+H!');
            scanButton();
            return false;
        }else if(e.ctrlKey && key == 80){ //CTRL+P
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            paymentButton();
            return false;
        }else if(key == 27){
        	setTimeout(function() {window.location = "dashboard.html"},500);
        }else{
        	return true;
        }
      });
      $(document).on('keypress', function(e){
          var key = e.which;
        if(e.ctrlKey && key == 66){ //CTRL+B
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+P!');
            browseButton();
            return false;
        }else if(e.ctrlKey && key == 83){ //CTRL+S
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+R!');
            searchButton();
            return false;
        }else if(e.ctrlKey && key == 67){ //CTRL+C
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+H!');
            scanButton();
            return false;
        }else if(e.ctrlKey && key == 80){ //CTRL+P
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            paymentButton();
            return false;
        }else if(key == 27){
        	setTimeout(function() {window.location = "dashboard.html"},500);
        }else{
        	return true;
        }
      });
    </script>
	
	<script type="text/javascript">
		$(document).ready(function() {
		// Create two variable with the names of the months and days in an array
		var monthNames = [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ]; 
		var dayNames= ["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"]

		// Create a newDate() object
		var newDate = new Date();
		// Extract the current date from Date object
		newDate.setDate(newDate.getDate());
		// Output the day, date, month and year    
		$('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

		setInterval( function() {
			// Create a newDate() object and extract the seconds of the current time on the visitor's
			var seconds = new Date().getSeconds();
			// Add a leading zero to seconds value
			$("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
			},1000);
			
		setInterval( function() {
			// Create a newDate() object and extract the minutes of the current time on the visitor's
			var minutes = new Date().getMinutes();
			// Add a leading zero to the minutes value
			$("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
		    },1000);
			
		setInterval( function() {
			// Create a newDate() object and extract the hours of the current time on the visitor's
			var hours = new Date().getHours();
			// Add a leading zero to the hours value
			$("#hours").html(( hours < 10 ? "0" : "" ) + hours);
		    }, 1000);
			
		}); 
	</script>
	<?php echo $this->load->view('template/js/selling'); ?>
	<script type="text/javascript">
		checking();
		get_list_penjualan();

		function get_list_penjualan(){
			var url = "<?php echo base_url($this->cname); ?>/get_list_penjualan";

			$.ajax({
				url: url,
				success: function(msg)
				{
					$("#list_penjualan").html(msg);
				}
			});
		}

		get_list_barang_penjualan();

		function get_list_barang_penjualan(){
			var url = "<?php echo base_url($this->cname); ?>/get_list_barang_penjualan";

			$.ajax({
				url: url,
				success: function(msg)
				{
					$("#list_brg_penjualan").html(msg);
				}
			});
		}

		get_list_barang_barcode_penjualan();

		function get_list_barang_barcode_penjualan(){
			var url = "<?php echo base_url($this->cname); ?>/get_list_barang_barcode_penjualan";

			$.ajax({
				url: url,
				success: function(msg)
				{
					$("#list_brg_barcode").html(msg);
				}
			});
		}

		get_total_penjualan();

		function get_total_penjualan(){
			var url = "<?php echo base_url($this->cname); ?>/get_total_penjualan";

			$.ajax({
				url: url,
				success: function(msg)
				{
					$(".total_penjualan").html(msg);
					$('.pay_total_jual').html(msg);
				}
			});
			
		}

		$('#id_produk').on('change',function(){
			
			var id_produk = $('#id_produk').val();
			var kas_penjualan = $('#kas_penjualan').val();

			var url = "<?php echo base_url($this->cname); ?>/add_penjualan?id_produk="+id_produk+"&kas="+kas_penjualan;

			$.ajax({
				url: url,
				success: function(msg)
				{
					get_list_penjualan();
					get_total_penjualan();
					$('#id_produk').val('');
					$('#id_produk').focus();
				}
			});
		});

		$('#but_buka_kasir').on('click',function(){
			
			var buka_kasir = $('#buka_kasir').val();

			var url = "<?php echo base_url($this->cname); ?>/buka_kasir?buka_kasir="+buka_kasir;

			$.ajax({
				url: url,
				success: function(msg)
				{
					alert(msg);
					if (msg == 'Sukses, Kasir Berhasil Dibuka!') {
						$('#modal-regular').modal('hide');
						get_list_penjualan();
						get_total_penjualan();
						$('#id_produk').val('');
						$('#id_produk').focus();	
						checking();
					}
				}
			});
		});

		$('#cari_barang').on('keyup',function(){
			
			var cari_barang = $('#cari_barang').val();

			var url = "<?php echo base_url($this->cname); ?>/get_list_barang_penjualan?cari_barang="+cari_barang;

			$.ajax({
				url: url,
				success: function(msg)
				{
					$("#list_brg_penjualan").html(msg);
				}
			});
		});

		$('#jumlah_bayar').on('keyup',function(){
			
			var jumlah_bayar = $('#jumlah_bayar').val();

			var url = "<?php echo base_url($this->cname); ?>/get_kembalian?jumlah_bayar="+jumlah_bayar;

			$.ajax({
				url: url,
				success: function(msg)
				{
					var hasil = msg.split('|');
					$(".pay_dibayar").html(hasil[0]);
					$(".pay_kembalian").html(hasil[1]);
				}
			});
		});	


		var kas_penjualan = $('#kas_penjualan').val();

		$('#pay_kas').val(kas_penjualan);

		$('#but_bayar_penjualan').on('click',function(){
			
			var url = "<?php echo base_url($this->cname); ?>/proses_pembayaran";

			$.ajax({
				method:'POST',
				url: url,
				data: $('#form_pembayaran').serialize(),
				success: function(msg)
				{
					alert(msg);
					window.location = "<?php echo base_url($this->cname); ?>";
				}
			});
		});

		$('#but_suspend').on('click',function(){
			
			var url = "<?php echo base_url($this->cname); ?>/proses_suspend";

			$.ajax({
				url: url,
				success: function(msg)
				{
					alert(msg);
					get_list_penjualan();
					get_total_penjualan();
					$('#id_produk').val('');
					$('#id_produk').focus();	
				}
			});
		});

		$('#but_void').on('click',function(){
			
			var url = "<?php echo base_url($this->cname); ?>/proses_void";

			$.ajax({
				url: url,
				success: function(msg)
				{
					alert(msg);
					get_list_penjualan();
					get_total_penjualan();
					$('#id_produk').val('');
					$('#id_produk').focus();	
				}
			});
		});

		

		$('#search_barcode').val('');
		$('#search_barcode').focus();
		$('#search_barcode').on('change',function(){
			
			var search_barcode = $('#search_barcode').val();

			var url = "<?php echo base_url($this->cname); ?>/get_list_barang_barcode_penjualan?search_barcode="+search_barcode;

			$.ajax({
				url: url,
				success: function(msg)
				{
					$("#list_brg_barcode").html(msg);
					$('#search_barcode').val('');
				}
			});
		});

		 
		 
	</script>