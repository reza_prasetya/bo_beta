<table id="general-table" class="table table-stripped">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th>Product</th>
			<th>Qty</th>
			<th>Price</th>
			<th>Subtotal</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$no = 1;
			$total = 0;
			foreach ($retur as $key => $value) {
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $value->namabrg; ?></td>
				<td><?php echo $value->jml; ?></td>
				<td><?php echo format_rupiah($value->harga); ?></td>
				<td><?php echo format_rupiah($value->subtotal); ?></td>
			</tr>
			<?php
			$no++;
		} ?>
		
	</tbody>
</table>