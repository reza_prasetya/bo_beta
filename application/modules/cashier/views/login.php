<div id="login-container">
	<div id="page-content" class="block remove-margin animation-bigEntrance">
		<div class="block-header">
			<div class="header-section">
				<h1 class="text-center">Welcome to Basmalah<br><small><i class="icon-user"></i> Please Login</small></h1>
			</div>
		</div>
		<form action="<?php echo base_url($this->cname).'/login'; ?>" method = "post" >
			<div class="form-group">
				<input type="text" id="login-email" name="login-email" class="form-control input-lg" placeholder="Username">
			</div>
			<div class="form-group">
				<input type="password" id="login-password" name="login-password" class="form-control input-lg" placeholder="Password">
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-xs-8">
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-default disabled">Remember me?</button>
							<button type="button" class="btn btn-sm btn-default" data-toggle="button"><i class="icon-ok"></i></button>
						</div>
					</div>
					<div class="col-xs-4 text-right">
						<button type="submit" class="btn btn-sm btn-primary"><i class="icon-angle-right"></i> Login</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	// $(document).ready(function(){
	// 	$('#formLogin').submit(function(){
	// 		var uname = $("#login-email").val();
	// 		var upass = $("#login-password").val();
			
	// 		if(uname == 'kasir'){
	// 			setTimeout(function() {window.location = "<?php echo base_url($this->cname); ?>login"},1500);
	// 		}
	// 		else if(uname == 'gudang'){
	// 			setTimeout(function() {window.location = "./con-gudang/dashboard.html"},1500);
	// 		}
	// 		else if(uname == 'akuntan'){
	// 			setTimeout(function() {window.location = "./con-accounting/dashboard.html"},1500);
	// 		}
	// 		else if(uname == 'pusat'){
	// 			setTimeout(function() {window.location = "./con-pusat/dashboard.html"},1500);
	// 		}
	// 		else{
	// 			setTimeout(function() {window.location = "./index.html"},1500);
	// 		}
	// 		return false;
	// 	});
	// });
</script>