<div id="fx-container" class="fx-opacity">
				<div id="page-content" class="block" style="min-height:650px;">
					<!-- Start Content -->
					<div class="row">
						<div class="col-sm-12">
							<div class="row" style='margin-bottom:10px'>
								<div class="col-sm-4">
									<button class="btn btn-block btn-lg btn-primary" onClick="browseButton()"><i class="icon-reorder"></i> BROWSE</button>
								</div>
								<div class="col-sm-4">
									<button class="btn btn-block btn-lg btn-primary" onClick="searchButton()"><i class="icon-search"></i> SEARCH</button>
								</div>
								<div class="col-sm-4">
									<button class="btn btn-block btn-lg btn-primary" onClick="scanButton()"><i class="icon-barcode"></i> SCAN</button>
								</div>
							</div>
							<div class="row" style='margin-bottom:10px; display:none;'>
								<div class="col-sm-6">
									<button class="btn btn-block btn-lg btn-primary" onClick="exportButton()"><i class="icon-search"></i>EXPORT</button>
								</div>
								<div class="col-sm-6">
									<button class="btn btn-block btn-lg btn-primary" onClick="syncButton()"><i class="icon-reorder toggle-bars"></i> SYNC DATA</button>
								</div>
							</div>
							<div class="block full" style="margin-top:10px;" id="import-div">
								<div class="alert alert-info alert-dismissable">
									<h4><i class="icon-info-sign"></i> Info</h4> Silahkan Upload Data Produk dan Masukkan Keycode yang Anda Dapatkan melalui Kantor Pusat - <a href="javascript:void(0)" class="alert-link">File Berupa CSV</a>!
								</div>
								<form action="" method="post" enctype="multipart/form-data" class="form-horizontal" onSubmit="return false;">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-asterisk"></i></span>
											<input type="password" id="example-password3" name="example-password3" class="form-control">
											<span class="input-group-addon">Keycode</span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label" for="example-file-input">File input</label>
										<div class="col-md-5">
											<input type="file" id="example-file-input" name="example-file-input">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-5 col-md-offset-2">
											<button type="reset" class="btn btn-default"><i class="icon-remove"></i> Reset</button>
											<button type="submit" class="btn btn-primary"><i class="icon-arrow-right"></i> Submit</button>
										</div>
									</div>
								</form>
							</div>
							<div class="block full" style="margin-top:10px;" id="export-div">
								<div class="alert alert-info alert-dismissable">
									<h4><i class="icon-info-sign"></i> Info</h4> Silahkan Masukkan Keycode dan generate file CSV Retur - <a href="javascript:void(0)" class="alert-link">File Berupa CSV</a>!
								</div>
								<form action="" method="post" enctype="multipart/form-data" class="form-horizontal" onSubmit="return false;">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-asterisk"></i></span>
											<input type="password" id="example-password3" name="example-password3" class="form-control">
											<span class="input-group-addon">Keycode</span>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-5 col-md-offset-2">
											<button type="reset" class="btn btn-default"><i class="icon-remove"></i> Reset</button>
											<button type="submit" class="btn btn-primary"><i class="icon-arrow-right"></i> Generate and Sends</button>
										</div>
									</div>
								</form>
							</div>
							<div class="block full" style="margin-top:10px;" id="sync-div">
								<div class="alert alert-info alert-dismissable">
									<h4><i class="icon-info-sign"></i> Info</h4> Silahkan tunggu hingga proses sinkronisasi selesai (100%)
								</div>
								<div class="bars-container">
									<h4>Sedang dalam proses</h4>
									
									<div class="progress progress-striped active">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">80%</div>
									</div>
								</div>
							</div>
							<div class="block" id="selling-div">
								<div class="row">
									<div class="col-sm-12">
										<blockquote>
											<p><i class="icon-list"></i> RETUR PENJUALAN</p>
										</blockquote>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<blockquote>
											<p><i class="icon-file-text no_faktur"></i></p>
										</blockquote>
									</div>
									<div class="col-sm-4">
										<blockquote>
											<p><i class="icon-laptop no_kas"></i></p>
										</blockquote>
									</div>
									<?php $user = $this->session->userdata('basmalahsession'); ?>
									<div class="col-sm-4">
										<blockquote>
											<p><i class="icon-user"></i> <?php echo $user[0]->nama; ?></p>
										</blockquote>
									</div>
								</div>
								<div class="row">
									<div class="block-section">
										<div class="input-group input-group-lg">
											<input id="id_product" name="id_product" class="form-control" placeholder="Scan Barcode!" type="text">
											<div class="input-group-btn">
												<button class="btn btn-default"><i class="icon-barcode icon-fixed-width"></i></button>
											</div>
										</div>
									</div>
								</div>
								<div class="table-responsive" id = "list_penjualan">
									
								</div>
								<div class="row"  style="margin-bottom: 20px;">
									<div class="col-sm-12">
										<button class="btn btn-block btn-lg btn-success" id="retur_btn">RETUR</button>
									</div>
								</div>
							</div>
							<div class="block full" style="margin-top:10px;" id="payment-div">
								<div class="row">
									<table class="table table-striped">
										<tbody>
											<tr>
												<td class="col-xs-6">
													<div class="row text-center">
														<div class="col-md-6">
															<h4>Bayar</h4>
															<h2 class="text-success">Rp. 900.000.000,00</h2>
														</div>
														<div class="col-md-6">
															<h4>Kurang/Kembalian</h4>
															<h2 class="text-warning">Rp. 100.000.000,00</h2>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="row">
									<div class="col-sm-12 pull-right">
										<div class="row" style="margin-bottom: 10px;">
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-default" onClick="toggleButton()"><i class="icon-keyboard"></i></button>
											</div>
											<div class="col-sm-4">
											</div>
											<div class="col-sm-2 hide-key">
												<button class="btn btn-block btn-lg btn-primary">CASH</button>
											</div>
											<div class="col-sm-3 hide-key">
												<input id="search-term" name="search-term" class="form-control" placeholder="Search.." type="text" style="height:45px;">
											</div>
											<div class="col-sm-2 hide-key">
												<button class="btn btn-block btn-lg btn-primary">DELETE</button>
											</div>
										</div>
										<div class="row hide-key" style="margin-bottom: 10px;">
											<div class="col-sm-5">
											</div>
											<div class="col-sm-2">
												<button class="btn btn-block btn-lg btn-primary">VOUCHER</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">7</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">8</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">9</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">+</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">-</button>
											</div>
										</div>
										<div class="row hide-key" style="margin-bottom: 10px;">
											<div class="col-sm-5">
											</div>
											<div class="col-sm-2">
												<button class="btn btn-block btn-lg btn-primary">QUANTITY</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">4</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">5</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">6</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">*</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">/</button>
											</div>
										</div>
										<div class="row hide-key" style="margin-bottom: 10px;">
											<div class="col-sm-5">
											</div>
											<div class="col-sm-2">
												<button class="btn btn-block btn-lg btn-primary">PRICE</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">1</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">2</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">3</button>
											</div>
											<div class="col-sm-2">
												<button class="btn btn-block btn-lg btn-primary">&nbsp;</button>
											</div>
										</div>
										<div class="row hide-key" style="margin-bottom: 10px;">
											<div class="col-sm-5">
											</div>
											<div class="col-sm-2">
												<button class="btn btn-block btn-lg btn-primary">DISCOUNT</button>
											</div>
											<div class="col-sm-2">
												<button class="btn btn-block btn-lg btn-primary">0</button>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-block btn-lg btn-primary">.</button>
											</div>
											<div class="col-sm-2">
												<button class="btn btn-block btn-lg btn-primary">Enter</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="block full" style="margin-top:10px;" id="browse-div">
								<div class="row">
									<div class="col-md-4" style="margin-top: -30px;">
										<h4 class="sub-header">CATEGORIES</h4>
										<div class="ms-message-list list-group">
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
										</div>
									</div>
									<div class="col-md-8" style="margin-top: -30px;">
										<h4 class="sub-header">LIST ITEMS</h4>
										<div class="ms-message-list list-group">
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
											<a href="javascript:void(0)" class="list-group-item" >
												<img src="<?php echo base_url();?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
												<h4 class="list-group-item-heading pull-right"><strong>500.000</strong></h4>
												<h4 class="list-group-item-heading" style="padding-left:60px; min-height: 45px; max-width:70%;">Judul Produk Akan Ditaruh Disini</h4>
											</a>
										</div>
									</div>
								</div>
							</div>	
							
							<div class="block full" style="margin-top:10px;" id="search-div">
								<div class="row">
									<div class="block-section">
										<form id = "form_cari_barang" method="post">
											<div class="input-group input-group-lg">
												<!-- <div class="input-group-btn">
													<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="min-width:150px;">All Categories <span class="caret"></span></button>
													<ul class="dropdown-menu">
														<li class="active">
															<a href="javascript:void(0)"><i class="icon-ok"></i> All Categories</a>
														</li>
														<li class="divider"></li>
														<li><a href="javascript:void(0)">Users</a></li>
														<li><a href="javascript:void(0)">Projects</a></li>
														<li><a href="javascript:void(0)">Sites</a></li>
													</ul>
												</div> -->
												<input id="cari_barang" name="cari_barang" class="form-control cari_barang" placeholder="Search.." type="text">
												<div class="input-group-btn">
													<button type="submit" class="btn btn-default"><i class="icon-search icon-fixed-width"></i></button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12" style="margin-top: -30px;">
										<h4 class="sub-header">SEARCH RESULT</h4>
										<div class="ms-message-list list-group" id = "list_brg_penjualan">
											
										</div>
									</div>
								</div>
							</div>
							<div class="block full" style="margin-top:10px;" id="scan-div">
								<div class="row">
									<div class="block-section">
										
										<div class="input-group input-group-lg">
											<input id="search_barcode" name="search_barcode" class="form-control" type="text">
											<div class="input-group-btn">
											<button type="submit" class="btn btn-default"><i class="icon-barcode icon-fixed-width"></i></button>
										</div>
										</div>
										
									</div>
								</div>
								<div class="row" style="border-top:1px dotted black;">
									<div class="list-group" id = "list_brg_barcode">
										
									</div>
								</div>
							</div>

						</div>
					</div>
					<!-- End Content -->
				</div>

			<footer class="clearfix">
				<div class="pull-right">
					Design and Developed by <a target="_blank" href="http://www.utomosakti.com" target="_blank">CV. Utomo Sakti</a>
				</div>
				<div class="pull-left">
					<span id="year-copy"></span> &copy; <a target="_blank" href="http://www.basmallah.com" target="_blank">Basmallah</a>
				</div>
			</footer>
		</div>
	</div>
	<div id="modal-ceknota" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form id = "formcekmember" method="post">
        		<div class="modal-header">
	                <h4 class="modal-title">Nota</h4>
	            </div>
	            <div class="modal-body">
						<div class="row" style="margin-bottom: 10px;">
							<div class="form-horizontal">
								<div class="form-group">
									<div class="col-md-12">
										<input id="nota" name="nota" class="form-control" placeholder="Nota" type="text" style="height:45px;">
									</div>
								</div>
							</div>
						</div>
					</div>
	            <div class="modal-footer">
	            		<div class="row" style="margin-bottom: 10px;">
							<div class="col-sm-12">
								<a id="cekmember" class="btn btn-block btn-lg btn-primary">Cek Nota</a>
							</div>
						</div>
	            </div>
        	</form>
            
        </div>
    </div>

</div>
	<script>
		$(function(){$("#modal-compose").on("shown.bs.modal",function(){$(".modal-select-chosen").chosen({width:"100%"})}),$(".ms-message-list").slimScroll({height:610,color:"#000000",size:"3px",touchScrollStep:750});var e=$(".ms-categories");$("a",e).click(function(){$("li",e).removeClass("active"),$(this).parent().addClass("active"),$(this).data("cat")});begin();});
	</script>
	<script type="text/javascript">
		$('#id_produk').val('');
		$('#id_produk').focus();
		function toggleButton(){
			if($('.hide-key').is(':visible')){
				$('.hide-key').hide();
			} else {
				$('.hide-key').show();
			}
		}
		function browseButton(){
			$('#head-div').slideUp();
			$('#browse-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#payment-div').slideUp();
			$('#selling-div').slideDown();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#sync-div').slideUp();
			$('#id_produk').focus();	
		}
		function paymentButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#payment-div').slideDown();
			$('#selling-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#sync-div').slideUp();
		}
		function scanButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideDown();
			$('#payment-div').slideUp();
			$('#selling-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#sync-div').slideUp();
			$('#search_barcode').focus();
		}
		function searchButton(){
			$('#head-div').slideUp();
			$('#browse-div').slideUp();
			$('#search-div').slideDown();
			$('#scan-div').slideUp();
			$('#payment-div').slideUp();
			$('#selling-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#sync-div').slideUp();
			$("#search_sale").focus();
			$(".search_sale").focus();

		}
		function begin(){
			$('#browse-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#payment-div').slideUp();
			$('.hide-key').slideUp();
			$('#selling-div').slideDown();
			$('#sync-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
		}
		function syncButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#tambah-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#sync-div').slideDown();
			$('#selling-div').slideUp();
			generate();
		}
		function importButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#tambah-div').slideUp();
			$('#import-div').slideDown();
			$('#export-div').slideUp();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#sync-div').slideUp();
			$('#selling-div').slideUp();
		}
		function exportButton(){
			$('#head-div').slideDown();
			$('#browse-div').slideUp();
			$('#tambah-div').slideUp();
			$('#import-div').slideUp();
			$('#export-div').slideDown();
			$('#search-div').slideUp();
			$('#scan-div').slideUp();
			$('#sync-div').slideUp();
			$('#selling-div').slideUp();
		}

		/*$(document).on('keydown', function(e){
        var key = e.which;
        if(e.ctrlKey && key == 66){ //CTRL+B
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+P!');
            browseButton();
            return false;
        }else if(e.ctrlKey && key == 83){ //CTRL+S
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+R!');
            searchButton();
            return false;
        }else if(e.ctrlKey && key == 67){ //CTRL+C
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+H!');
            scanButton();
            return false;
        }else if(e.ctrlKey && key == 82){ //CTRL+R
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            paymentButton();
            return false;
        }else if(key == 27){
        	setTimeout(function() {window.location = "dashboard.html"},500);
        }else{
        	return true;
        }
      });
      $(document).on('keyup', function(e){
          var key = e.which;
        if(e.ctrlKey && key == 66){ //CTRL+B
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+P!');
            browseButton();
            return false;
        }else if(e.ctrlKey && key == 83){ //CTRL+S
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+R!');
            searchButton();
            return false;
        }else if(e.ctrlKey && key == 67){ //CTRL+C
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+H!');
            scanButton();
            return false;
        }else if(e.ctrlKey && key == 82){ //CTRL+R
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            paymentButton();
            return false;
        }else if(key == 27){
        	setTimeout(function() {window.location = "dashboard.html"},500);
        }else{
        	return true;
        }
      });
      $(document).on('keypress', function(e){
          var key = e.which;
        if(e.ctrlKey && key == 66){ //CTRL+B
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+P!');
            browseButton();
            return false;
        }else if(e.ctrlKey && key == 83){ //CTRL+S
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+R!');
            searchButton();
            return false;
        }else if(e.ctrlKey && key == 67){ //CTRL+C
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+H!');
            scanButton();
            return false;
        }else if(e.ctrlKey && key == 82){ //CTRL+R
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            console.log('Ctrl+U!');
            paymentButton();
            return false;
        }else if(key == 27){
        	setTimeout(function() {window.location = "dashboard.html"},500);
        }else{
        	return true;
        }
      });*/
	</script>
	
	<script type="text/javascript">

		checking();

		function checking(){
			var urlchek_session = "<?php echo base_url($this->cname).'/ceksession' ?>";
			$.ajax({
				url: urlchek_session,
				success: function(msg)
				{
					if (msg == 'kosong') {
						$('#modal-ceknota').show();
					}else{
						$('#modal-ceknota').hide();
						$('#id_product').val('');
						$('#id_product').focus();
						var data = jQuery.parseJSON(msg);

						$('.no_faktur').html(data.invoice_no);
						$('.no_kas').html(data.cashdraw_no);
					}
				}
			});

			get_list_retur ();
			
			setTimeout('checking()', 10000);
		}

		function get_list_retur () {
			var url = "<?php echo base_url($this->cname).'/get_list_retur' ?>";
			$.ajax({
				url: url,
				success: function(msg)
				{
					$('#list_penjualan').html(msg);
				}
			});
		}

		$('#cekmember').on('click', function(){
			var url = "<?php echo base_url($this->cname).'/ceknota' ?>";
			var form_data = {
				nota: $('#nota').val()
			};

			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(data)
				{
					alert(data);
					if (data == 'nota ada') {
						$('#modal-ceknota').hide();
						checking();
					}
				}
			})
		});

		$('#retur_btn').on('click', function(){
			var url = "<?php echo base_url($this->cname).'/proses_retur' ?>";
			var form_data = {
				nota: $('#nota').val()
			};

			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(data)
				{
					alert(data);
					checking();
				}
			})
		});

		$('#id_product').on('change', function(){
			var url = "<?php echo base_url($this->cname).'/cekbarangretur' ?>";
			var form_data = {
				id_product: $('#id_product').val()
			};

			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(data)
				{
					checking();
				}
			})
		});


		get_list_penjualan();

		function get_list_penjualan(){
			var url = "<?php echo base_url($this->cname); ?>/get_list_penjualan";

			$.ajax({
				url: url,
				success: function(msg)
				{
					$("#list_penjualan").html(msg);
				}
			});
		}

		get_list_barang_penjualan();

		function get_list_barang_penjualan(){
			var url = "<?php echo base_url($this->cname); ?>/get_list_barang_penjualan";

			$.ajax({
				url: url,
				success: function(msg)
				{
					$("#list_brg_penjualan").html(msg);
				}
			});
		}

		get_list_barang_barcode_penjualan();

		function get_list_barang_barcode_penjualan(){
			var url = "<?php echo base_url($this->cname); ?>/get_list_barang_barcode_penjualan";

			$.ajax({
				url: url,
				success: function(msg)
				{
					$("#list_brg_barcode").html(msg);
				}
			});
		}

		$('#id_produk').on('change',function(){
			
			var id_produk = $('#id_produk').val();
			
			var url = "<?php echo base_url($this->cname); ?>/add_penjualan?id_produk="+id_produk;

			$.ajax({
				url: url,
				success: function(msg)
				{
					get_list_penjualan();
					$('#id_produk').val('');
					$('#id_produk').focus();
				}
			});
		});

		$('#cari_barang').on('keyup',function(){
			
			var cari_barang = $('#cari_barang').val();

			var url = "<?php echo base_url($this->cname); ?>/get_list_barang_penjualan?cari_barang="+cari_barang;

			$.ajax({
				url: url,
				success: function(msg)
				{
					$("#list_brg_penjualan").html(msg);
				}
			});
		});

		$('#search_barcode').val('');
		$('#search_barcode').focus();
		$('#search_barcode').on('change',function(){
			
			var search_barcode = $('#search_barcode').val();

			var url = "<?php echo base_url($this->cname); ?>/get_list_barang_barcode_penjualan?search_barcode="+search_barcode;

			$.ajax({
				url: url,
				success: function(msg)
				{
					$("#list_brg_barcode").html(msg);
					$('#search_barcode').val('');
				}
			});
		});

	</script>