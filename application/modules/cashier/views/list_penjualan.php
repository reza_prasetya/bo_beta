<div id="edit_jual" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
    
    <div class="modal-dialog">
	    <div class="modal-content">
	        <div class="modal-header">
	            <button class="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
	            <h4 class="modal-title">Edit Pembelian</h4>
	        </div>
	        <div class="modal-body">
	        	<form id = "form_edit_penjualan">
	        		<div class="row" style="margin-bottom: 10px;">
						<div class="form-horizontal">
							<div class="form-group">
								<div class="col-md-12">
									<input id="quantity" name="quantity" class="form-control" placeholder="Quantity" type="text" style="height:45px;">
									<input id="id_jual" name="id_jual" class="form-control" placeholder="ID Penjualan" type="hidden" style="height:45px;">
									<input id="code" name="code" class="form-control" placeholder="ID Penjualan" type="hidden" style="height:45px;">
								</div>
							</div>
						</div>
					</div>
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-sm-12">
							<!-- <button onclick="alert('Sukses, Kasir Berhasil Dibuka!');$('#modal-regular').modal('hide');return false;" class="btn btn-block btn-lg btn-primary">Buka Kasir</button> -->
							<a id = "but_edit_produk" class="btn btn-block btn-lg btn-primary">Edit Produk</a>
						</div>
					</div>
	        	</form>
			</div>
	        <div class="modal-footer">
	            <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
	        </div>
	    </div>
	</div>
</div>
<div id="hapus_jual" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
    
    <div class="modal-dialog">
	    <div class="modal-content">
	        <div class="modal-header">
	            <button class="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
	            <h4 class="modal-title">Hapus Item Penjualan</h4>
	        </div>
	        <div class="modal-body">
	        	<form id = "form_hapus_penjualan">
	        		<div class="row" style="margin-bottom: 10px;">
						<div class="form-horizontal">
							<div class="form-group">
								<div class="col-md-12">
									<input id="delid_jual" name="delid_jual" class="form-control" placeholder="ID Penjualan" type="hidden" style="height:45px;">
								</div>
							</div>
						</div>
					</div>
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-sm-12">
							<!-- <button onclick="alert('Sukses, Kasir Berhasil Dibuka!');$('#modal-regular').modal('hide');return false;" class="btn btn-block btn-lg btn-primary">Buka Kasir</button> -->
							<a id = "but_del_produk" class="btn btn-block btn-lg btn-primary">Hapus Produk</a>
						</div>
					</div>
	        	</form>
			</div>
	        <div class="modal-footer">
	            <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
	        </div>
	    </div>
	</div>
</div>
<table id="general-table" class="table table-stripped">
	<thead>
		<tr>
			<th class="text-center">No.</th>
			<th>Product</th>
			<th>Qty</th>
			<th>Price</th>
			<th>Subtotal</th>
			<th class="text-center"></th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$no = 1;
			$total = 0;
			foreach ($list_penjualan as $key => $value) {
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $value->name; ?></td>
				<td><?php echo $value->jml; ?></td>
				<td><?php echo $value->price1; ?></td>
				<td><?php echo $value->subtotal; ?></td>
				<td>
					<div class="btn-group btn-group-xs">
						<a class="btn btn-default" onclick = "editpenjualan(<?php echo $value->id.','.$value->jml.','.$value->code; ?>);" data-toggle="modal" href="#edit_jual" data-original-title="Edit">
							<i class="icon-edit"></i>
						</a>
						<a class="btn btn-default" onclick = "hapuspenjualan(<?php echo $value->id; ?>);" title="" data-toggle="modal" href="#hapus_jual" data-original-title="Delete">
							<i class="icon-remove"></i>
						</a>
					</div>
				</td>
			</tr>
			<?php
			$total += $value->subtotal;
			$no++;
		} ?>
		
	</tbody>
</table>
<input type="hidden" id = "total_penjualannya" value = "<?php echo $total; ?>" >
<script type="text/javascript">
// $('#edit_penjualan').live('click',function(){
// 	var id = $(this).attr('idjual');
// 	var qty = $(this).attr('jml');
// 	var code = $(this).attr('code');

// 	$('#quantity').val(qty);
// 	$('#id_jual').val(id);
// 	$('#code').val(code);

// 	alert(id);

// });

function editpenjualan (id, qty, code ) {
	$('#quantity').val(qty);
	$('#id_jual').val(id);
	$('#code').val(code);
}

function hapuspenjualan (id) {
	
	$('#delid_jual').val(id);
}

$('#but_edit_produk').on('click',function(){
	var url = "<?php echo base_url($this->module);?>/penjualan/edit_penjualan";
	$.ajax({
			type: "POST",
			url: url,
			data: $('#form_edit_penjualan').serialize(),
			success: function(pesan)
			{
				alert(pesan);
				if (pesan == 'Sukses, Item pembelian berhasil diedit!') {
					$('#edit_jual').modal('hide');
					get_list_penjualan();
					get_total_penjualan();
					$('#id_produk').val('');
					$('#id_produk').focus();
				}
			}
	});
});

$('#but_del_produk').on('click',function(){
	var url = "<?php echo base_url($this->module);?>/penjualan/delete_penjualan";
	$.ajax({
			type: "POST",
			url: url,
			data: $('#form_hapus_penjualan').serialize(),
			success: function(pesan)
			{
				alert(pesan);
				if (pesan == 'Sukses, Item pembelian berhasil dihapus!') {
					$('#hapus_jual').modal('hide');
					get_list_penjualan();
					get_total_penjualan();
					$('#id_produk').val('');
					$('#id_produk').focus();
				}
			}
	});
});
</script>
