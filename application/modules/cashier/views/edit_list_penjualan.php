<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button class="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
            <h4 class="modal-title">Edit Pembelian</h4>
        </div>
        <div class="modal-body">
        	<form id = "form_edit_penjualan">
        		<div class="row" style="margin-bottom: 10px;">
					<div class="form-horizontal">
						<div class="form-group">
							<div class="col-md-12">
								<input id="quantity" name="quantity" class="form-control" placeholder="Quantity" type="text" style="height:45px;">
								<input id="id_jual" name="id_jual" class="form-control" placeholder="ID Penjualan" type="text" style="height:45px;">
								<input id="code" name="code" class="form-control" placeholder="ID Penjualan" type="text" style="height:45px;">
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-sm-12">
						<!-- <button onclick="alert('Sukses, Kasir Berhasil Dibuka!');$('#modal-regular').modal('hide');return false;" class="btn btn-block btn-lg btn-primary">Buka Kasir</button> -->
						<a id = "but_edit_produk" class="btn btn-block btn-lg btn-primary">Edit Produk</a>
					</div>
				</div>
        	</form>
		</div>
        <div class="modal-footer">
            <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
        </div>
    </div>
</div>