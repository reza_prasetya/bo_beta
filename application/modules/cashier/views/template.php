<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> 
<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title><?php echo $title; ?></title>

		<?php
			$url = $this->uri->segment(2);
			$this->load->view('template/script');
		?>

	</head>
	<!-- <body class="header-fixed-top" onload="bg();" onkeydown="processKeyDown(event);" > -->
	<body class="header-fixed-top" onkeydown="processKeyDown(event);" >
		<?php
			$this->load->view('template/sidebar');
		?>

		<div id="page-container">

			<?php
				$this->load->view('template/header');
				if($url==NULL || $url=='dashboard'){
					// do nothing
				} else {
					echo $content;
				}
			?>

		</div>

		<?php
			if($url==NULL || $url=='dashboard'){
				$this->load->view('template/dashboard-js-bottom');
			} else {
				$this->load->view('template/js-bottom');
			}
		?>

	</body>
</html>
<script type="text/javascript">
	   function open_window(url, width, height) {
		   var my_window;

		   my_window = window.open(url, "Print Invoice", "scrollbars=1, width="+width+", height="+height+", left=0, top=0");
		   my_window.focus();
		}
</script>