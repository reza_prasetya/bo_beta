<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_selling extends CI_Model {

	//function request cashier code
	public function request_cashdraw(){
		$sql = "SELECT COUNT(id) as total FROM atombizz_selling_cashdraw";
		$result = $this->db->query($sql);
		$fetch = $result->result();
		$total = $fetch[0]->total;
		$koder=$total + 1;
		$kodeAkhir = date('dmY');
		$bulan = substr($kodeAkhir, 2, 2);
		$hari = substr($kodeAkhir, 0, 2);
		$tahun = substr($kodeAkhir, -2);
		$str = strlen($koder);
		$kode = '';
		if($str > 1){
			$kode='CASH'.$koder.'/'.$bulan.$tahun;
		}else{
			$kode='CASH0'.$koder.'/'.$bulan.$tahun;
		}
		echo $kode.'|'.$total;
	}

	public function cek_stok($barcode)
	{
		$sql = "SELECT * FROM view_warehouse_stok WHERE `code`='$barcode' AND status='rak'";
		$result = $this->db->query($sql);
		foreach ($result->result() as $das) {
			# code...
		}
		return $das;
	}


	public function request_invoice($param=''){
		$user = $this->session->userdata('basmalahsession');
		$nota=$param['kas'];
		$sql = "SELECT COUNT(id) as total FROM atombizz_selling WHERE cashdraw_no='$nota'";
		$result = $this->db->query($sql);
		$fetch = $result->result();
		$total = $fetch[0]->total;
		$koder=$total + 1;
		$kodeAkhir = date('dmY');
		$bulan = substr($kodeAkhir, 2, 2);
		$hari = substr($kodeAkhir, 0, 2);
		$tahun = substr($kodeAkhir, -2);
		$str = strlen($koder);
		$kode = '';
		if($str > 1){
			$kode='TR'.$koder.'/'.$hari.$bulan.$tahun.'/'.$user[0]->nama;
		}else{
			$kode='TR0'.$koder.'/'.$hari.$bulan.$tahun.'/'.$user[0]->nama;
		}
		$sql2 = "SELECT COUNT(id) as total FROM view_total_tmp_trans_jual WHERE cashdraw='$nota'";
		$result2 = $this->db->query($sql2);
		$fetch2 = $result2->result();
		$total2=$fetch2[0]->total;
		if($total2 >= 1 ){
			$koder=$koder+$total2;
			$kodeAkhir = date('dmY');
			$bulan = substr($kodeAkhir, 2, 2);
			$hari = substr($kodeAkhir, 0, 2);
			$tahun = substr($kodeAkhir, -2);
			$str = strlen($koder);
			$kode = '';
			if($str > 1){
				$kode='TR'.$koder.'/'.$bulan.$tahun.'/'.$user[0]->nama;
			}else{
				$kode='TR0'.$koder.'/'.$bulan.$tahun.'/'.$user[0]->nama;
			}
			echo $kode.'|'.$total;
		}else{
			echo $kode.'|'.$total;
		}
	}

	//function open cashier
	public function open_cash($param=''){
		$user = $this->session->userdata('basmalahsession');
		$nota=$param['inv'];
		$kas_awal=$param['awal'];
		$userid=$user[0]->id;
		$tgl=date('Y-m-d');
		$cekin=date('h:i:s');

		$sql = "insert into atombizz_selling_cashdraw (cashdraw_no,date,check_in,user_id,start_cash,status) 
				values(?,now(),now(),?,?,'temporary')";
		$hasil = $this->db->query($sql,array($nota,$userid,$kas_awal));
		
		if($hasil){
			echo "1";
		}else{
			echo "0";
		}
	}

	public function get_category_product($value='')
	{
		$hasil = $this->db->get('atombizz_product_categories');

		return $hasil->result();
	}

	public function temp_cashdraw(){
	 	$tgl = $param['tgl'];
	  	$karyawan = $_SESSION['incKaryawanKeyCode'];
	  	$field = "*";
	  	$condition = "`tgl`='$tgl' AND `userid`='$karyawan'";
	  	$table = "`e-inventory_trans_kas`";
	  	$result = $as->selectWhere($field,$table,$condition);
	  	$row = mysql_fetch_array($result);
			$id = $row['id'];
			$invoice = $row['nota'];
			echo $id.'|'.$invoice;
	}

	public function temp_transaction($param=''){
	  	$inv = $param['inv'];
	  	$kas = $param['kas'];

	  	$sql = "select * from atombizz_tmp_detail_jual where cashdraw='$kas' AND invoice='$inv' AND status='temporary'";
	  	$hasil = $this->db->query($sql);

	  	//echo $kas;
	  	$tableData='';
	  	$totalPembelian=0;
	  	$totalDiskon=0;
	  	$i=0;
	  	foreach ($hasil->result_array() as $row) {
	  		$i++;
	  		$id = $row['id'];
	  		$invoice = $row['invoice'];
			$code = $row['code'];
			$brg = $row['item'];
			$jml = $row['qty'];
			$harga = $row['price'];
			$subtotal = $row['subtotal'];
			$diskon = $row['discount'];
			$subdiskon = $row['subdiscount'];
			$nom_diskon = $row['discount_nominal'];
			$operator = $row['operator'];
			$totalPembelian = $totalPembelian + $subdiskon;
			$totalDiskon = $totalDiskon + $nom_diskon;

			$tableData=$tableData.'<tr style="border:1px solid #afafaf;padding:10px 0;">
				<!--<td><label class="checkbox"><input type="checkbox" name="option'.$id.'" id="'.$id.'">'.$i.'</label></td>-->
				<td align="center">'.$i.'</td>
				<td>'.$brg.'</td>
				<td>'.$jml.'</td>
				<td>'.$harga.'</td>
				<td>'.$diskon.'%</td>
				<td>'.$subtotal.'</td>
	            <td id="r'.$i.'c1" data-id="'.$id.'" data-aksi="edit" data-nama="'.$brg.'" align="center">
	            	<div class="btn-group btn-group-xs">
						<a class="btn btn-default" title="" data-toggle="tooltip" onclick="editqty('.$id.');" href="javascript:void(0)" data-original-title="Edit">
							<i class="icon-edit"></i>
							Ubah Qty
						</a>
					</div>
				</td>
	            <td id="r'.$i.'c2" data-id="'.$id.'" data-aksi="del" data-nama="'.$brg.'" align="center">
					<div class="btn-group btn-group-xs">
						<a class="btn btn-default" title="" data-toggle="tooltip" onclick="del('.$id.');" href="javascript:void(0)" data-original-title="Delete">
							<i class="icon-remove"></i>
							Hapus
						</a>
					</div>
	            </td>
	        </tr>';
	  	}
		
		echo $tableData.'|'.$totalPembelian.'|'.$totalDiskon.'|'.$i;
	}

	public function temp_suspended($param=''){
	  	$kas = $param['kas'];
	  	
	  	$sql = "select `invoice` from atombizz_tmp_detail_jual where cashdraw = ? and status='suspended' group by `invoice` ";
	  	$invois = $this->db->query($sql,$kas);
	  	$invois = $invois->result();
	  	
	  	$produk = array();
	  	$a = 0;
	  	foreach ($invois as $key => $value) {
	  		$sql = "select item from atombizz_tmp_detail_jual where cashdraw = ? and status='suspended' and `invoice` = ? ";
		  	$item = $this->db->query($sql,array($kas,$value->invoice));
		  	$item = $item->result();
		  	$produknya = "";
		  	foreach ($item as $key => $itm) {
		  		$produknya .= $itm->item.",";
		  	}
		  	$produk[$a] = $produknya;
		  	$a++;
	  	}
	  	

	  	$sql = "select * from atombizz_tmp_detail_jual where cashdraw = ? and status='suspended' GROUP BY `invoice`";
	  	$hasil = $this->db->query($sql,$kas);
	  	
	  	$tableData='';
	  	$totalPembelian=0;
	  	$totalDiskon=0;
	  	$i=0;
	  	$j=0;
	  	foreach ($hasil->result_array() as $row) {
	  		$i++;
	  		$id = $row['id'];
	  		$invoice = $row['invoice'];
			$code = $row['code'];
			$brg = $row['item'];
			$jml = $row['qty'];
			$harga = $row['price'];
			$subtotal = $row['subtotal'];
			$diskon = $row['discount'];
			$subdiskon = $row['subdiscount'];
			$nom_diskon = $row['discount_nominal'];
			$operator = $row['operator'];
			

			$tableData=$tableData.'<tr style="border:1px solid #afafaf;padding:10px 0;">
				<!--<td><label class="checkbox"><input type="checkbox" name="option'.$id.'" id="'.$id.'">'.$i.'</label></td>-->
				<td align="center">'.$i.'</td>
				<td >'.$invoice.'</td>
				<td>'.$produk[$j].'</td>
				<td align="center">
					<div class="btn-group btn-group-xs">
						<a onClick="proses_suspend_back(this.id)" id = "proses_suspend_back'.$i.'" invoice = "'.$invoice.'" class="btn btn-default" title="" data-toggle="tooltip" data-original-title="Process to payment">
							<i class="icon-edit"></i>
							Process to payment
						</a>
					</div>
	            </td>
			</tr>';
			$j++;
	  	}
		
		echo $tableData;
	}

	public function ceksaldo_produk($param='')
	{
		$hasil = $this->db->get_where('view_warehouse_stok', array('code' => $param['barcode'], 'status' => 'rak'));
		$hasil = $hasil->result();

		if ($param['qty'] > $hasil[0]->saldo) {
			return 'tidak cukup';
		}else{
			return 'ada';
		}
	}

	public function suspen_back($invoice='')
	{
		$sql = "update atombizz_tmp_detail_jual set status = 'temporary' where status = 'suspended' and invoice = ?";
	  	$hasil = $this->db->query($sql,$invoice);

	  	$exec =  $this->db->affected_rows();

	  	if ($exec >= 0) {
	  		return "Data pembelian untuk invoice ".$invoice." BERHASIL dikembalikan ke proses pembelian";
	  	}else{
	  		return "Data pembelian untuk invoice ".$invoice." GAGAL dikembalikan ke proses pembelian";
	  	}
	}

	public function view_detail_temp($param=''){
	  	$ident = $param['ident'];

	  	$sql = "SELECT * from atombizz_tmp_detail_jual where id='$ident'";
	  	$hasil = $this->db->query($sql);

	  	foreach ($hasil->result_array() as $row) {
	  		$id = $row['id'];
			$jml = $row['qty'];
			$harga = $row['price'];
			$diskon = $row['discount'];
	  	}
		
		echo $jml.'|'.$harga.'|'.$diskon;
	}

	public function temp_count_check($param=''){
	  	$ident = $param['invoice'];

	  	$sql = "SELECT COUNT(id) as total from atombizz_tmp_detail_jual where invoice='$ident'";
	  	$result = $this->db->query($sql);
		$fetch = $result->result();
		$total = $fetch[0]->total;
		
		if(@$total == null || @$total <= 0){
	  		echo "0";
	  	}else{
	  		echo $total;
	  	}
	}

	//function open cashier
	public function add_transaction($param=''){
		$user = $this->session->userdata('basmalahsession');
		$nota=$param['inv'];
		$cash=$param['cash'];
		$barcode=$param['barcode'];
		$qty=$param['qty'];
		$userid=$user[0]->id;
		$operator=$user[0]->nama;

		$sqlCek = "SELECT COUNT(cashdraw) as total from atombizz_tmp_detail_jual where code='$barcode' AND invoice='$nota' AND cashdraw='$cash'";
	  	$hasilCek = $this->db->query($sqlCek);
	  	foreach ($hasilCek->result_array() as $row1) {
	  		$total = $row1['total'];
	  	}

	  	if($total >= 1){
	  		echo "2";
	  	}else if($total == 0){
	  		$sql = "SELECT * from atombizz_product where code='$barcode'";
		  	$hasil = $this->db->query($sql);

		  	foreach ($hasil->result_array() as $row) {
		  		$id = $row['id'];
		  		$name = $row['name'];
				$cost = $row['price'];
				$price1 = $row['price1'];
				$price2 = $row['price2'];
				$price3 = $row['price3'];
				$disc = $row['discount'];
				$data_qty = json_decode($row['qty']);
		  	}
		  	
		  	$member =  $this->session->userdata('membersession');
		  	if ($member == 'reguler') {
		  		if ($disc > 0 ) {
			  		$price = $price1;
			  	}else{
			  		if($qty >= 1 && $qty <= $data_qty[0]){
				  		$price = $price1;
				  	}else if($qty >= $data_qty[0] + 1 && $qty <= $data_qty[1]){
				  		$price = $price2;
				  	}else if($qty >= $data_qty[2]){
				  		$price = $price3;
				  	}	
				}
		  	}else{
		  		if ($disc > 0 ) {
			  		$price = $price2;
			  	}else{
			  		if($qty >= 1 && $qty <= $data_qty[0]){
				  		$price = $price2;
				  	}else if($qty >= $data_qty[0] + 1 && $qty <= $data_qty[1]){
				  		$price = $price2;
				  	}else if($qty >= $data_qty[2]){
				  		$price = $price3;
				  	}	
				}
		  	}
		  	
		  	$subtotal = $qty * $price;

		  	if($disc > 0){
		  		$diskon = ($disc * $subtotal) / 100;
		  		$subdiscount = $subtotal - $diskon;
		  	}else if($disc <= 0){
		  		$subdiscount = $subtotal;
		  		$diskon = 0;
		  	}

			$sql1 = "insert into atombizz_tmp_detail_jual (cashdraw,invoice,orderdate,status,code,item,qty,cost,price,subtotal,userid,operator,discount,discount_nominal,subdiscount) 
					values(?,?,now(),'temporary',?,?,?,?,?,?,?,?,?,?,?)";
			$hasil1 = $this->db->query($sql1,array($cash,$nota,$barcode,$name,$qty,$cost,$price,$subtotal,$userid,$operator,$disc,$diskon,$subdiscount));
			
			if($hasil1){
				echo "1";
			}else{
				echo "0";
			}
	  	}else{
	  		echo "3";
	  	}
	}

	public function delete_temp($param=''){
		$ident=$param['ident'];

		$sql = "delete from atombizz_tmp_detail_jual where id='$ident'";
	  	$hasil = $this->db->query($sql);

		if($hasil){
			echo "1";
		}else{
			echo "0";
		}
	  	
	}

	public function edit_transaction($param=''){
		$ident=$param['ident'];
		$qty=$param['qty'];
		$diskon=$param['diskon'];
		$subtotal=$param['subtotal'];
		$subdiskon=$param['subdiskon'];

		$sql = "UPDATE atombizz_tmp_detail_jual set qty = ?, subtotal = ?, subdiscount = ?, discount_nominal = ? WHERE id = ?";
	  	$hasil = $this->db->query($sql,array($qty,$subtotal,$subdiskon,$diskon,$ident));

		if($hasil){
			echo "1";
		}else{
			echo "0";
		}
	}

	public function suspend_transaction($param=''){
		$invoice=$param['invoice'];

		$sql = "UPDATE atombizz_tmp_detail_jual set status = 'suspended' WHERE invoice = ?";
	  	$hasil = $this->db->query($sql,array($invoice));

		if($hasil){
			echo "1";
			$this->session->unset_userdata('membersession');
		}else{
			echo "0";
		}
	}

	public function cancel_transaction($param=''){
		$invoice=$param['invoice'];

		$sql = "UPDATE atombizz_tmp_detail_jual set status = 'void' WHERE invoice = ?";
	  	$hasil = $this->db->query($sql,array($invoice));

		if($hasil){
			echo "1";
		}else{
			echo "0";
		}
	}

	public function payment_transaction($param=''){
		$user = $this->session->userdata('basmalahsession');
		$kas = $param['kas'];
		$inv = $param['invoice'];
		$tgl = date('Y-m-d');
		$idcustomer = $param['id_cust'];
		$namacustomer = $param['name_cust'];
		$bayar = $param['cara'];
		$total = $param['total'];
		$diskon = $param['diskon'];
		$dibayar = $param['bayar'];
		$kembali = $param['kembali'];
		$userlog=date('Y-m-d h:i:s');
		$userid=$user[0]->id;
		$operator=$user[0]->nama;

		$sqlcek = "SELECT COUNT(id) as total from atombizz_selling where date='$tgl' AND `invoice_no`='$inv' AND `cashdraw_no`='$kas'";
		
		$hasilcek = $this->db->query($sqlcek);
		foreach ($hasilcek->result_array() as $rowcek) {
	  		$numinv = $rowcek['total'];
	  	}
		
		if($numinv >= 1){
			echo '3';
		}else{
			$sqlSelling = "insert into atombizz_selling (cashdraw_no,invoice_no,customer_id,customer_name,date,total,pay,pay_back,inv_discount,user_id,user_name) 
					values(?,?,?,?,now(),?,?,?,?,?,?)";
			$hasilSelling = $this->db->query($sqlSelling,array($kas,$inv,$idcustomer,$namacustomer,$total,$dibayar,$kembali,$diskon,$userid,$operator));
			
			if($hasilSelling){

				$sqlcek1 = "SELECT COUNT(id) as total from atombizz_tmp_detail_jual where orderdate='$tgl' AND `invoice`='$inv' AND `cashdraw`='$kas'";
				$hasilcek1 = $this->db->query($sqlcek1);
				foreach ($hasilcek1->result_array() as $rowcek1) {
			  		$num = $rowcek1['total'];
			  	}
				
			  	$sql = "SELECT * from atombizz_tmp_detail_jual where orderdate='$tgl' AND `invoice`='$inv' AND `cashdraw`='$kas'";
				$hasil = $this->db->query($sql);
				
				$i=0;
				$insert = '';
				foreach ($hasil->result_array() as $row) {
					$kas2 = $param['kas'];
					$invoice2 = $param['invoice'];
					$barcode2=$row['code'];
					$item2=$row['item'];
					$qty2=$row['qty'];
					$price2=$row['price'];
					$subtotal2=$row['subtotal'];
					$subdiskon2=$row['subdiscount'];
					$diskon2=$row['discount'];
					$diskon_nominal2=$row['discount_nominal'];

					$H_param['date'] = date('Y-m-d');
					$H_param['product_code'] = $barcode2;
					$hpp=$this->get_hpp($H_param);
					
					$i=$i+1;
					if($i == $num){
						$insert= $insert."('".$kas2."','".$invoice2."','".$barcode2."','".$item2."','".$qty2."','".$price2."','".$subtotal2."','".$diskon_nominal2."','".$diskon2."','".$subdiskon2."','".$hpp."')";
					}else{
						$insert= $insert."('".$kas2."','".$invoice2."','".$barcode2."','".$item2."','".$qty2."','".$price2."','".$subtotal2."','".$diskon_nominal2."','".$diskon2."','".$subdiskon2."','".$hpp."'),";
					}
				}

				$sqlinsert = "insert into atombizz_selling_items (cashdraw,invoice,product_code,product_name,qty, price, subtotal, discount_val, discount, discount_sub, hpp) 
					values ".$insert."";
				$hasilinsert = $this->db->query($sqlinsert);
				
				if($hasilinsert){
					$this->session->unset_userdata('membersession');
					echo "1|".$invoice2;

					// proses pengurangan stok
					$kasir = $this->session->userdata('basmalahsession');

					$sql = "SELECT * from atombizz_tmp_detail_jual where orderdate='$tgl' AND `invoice`='$inv' AND `cashdraw`='$kas'";
					$hasil = $this->db->query($sql);
					$hasil = $hasil->result();

					foreach ($hasil as $key => $value) {
						$idproduk = $this->db->get_where('atombizz_product', array('code' => $value->code));
						$idproduk = $idproduk->result();
						$idproduk = $idproduk[0]->id;

						$rak = $this->db->get_where('view_warehouse_stok', array('produk' => $idproduk, 'status' => 'rak'));
						$rak = $rak->result();
						$rak = $rak[0]->warehouse;

						
						$keterangan = "penjualan dengan invoice ".$value->invoice;
						$sqlupdate = "insert into atombizz_warehouses_stok 
										(date,status,invoice,masuk,keluar,keterangan,userlog,
											operator,warehouse,produk) 
										values(now(),'penjualan',?,0,?,?,now(),?,?,?)";
						$this->db->query($sqlupdate,array(
												$value->invoice,
												$value->qty,
												$keterangan,
												$value->operator,
												$rak,
												$idproduk
												));
					}
				}else{
					echo "0|".$invoice2;
				}
			}else{
				echo "2";
			}
		}
	}

	public function get_hpp($param='')
	{
		$sql = "SELECT * FROM view_product_price WHERE `date` <= ? AND `code` = ? ORDER BY `date` DESC LIMIT 0,1";
		// echo $this->db->last_query();exit;
		$hasil = $this->db->query($sql,array($param['date'],$param['product_code']));
		foreach ($hasil->result() as $key => $value) {
			$hpp = $value->harga;
		}
		return $hpp;
	}

	public function tutupkasir($param='')
	{
		$kas = $param['kas'];
		$end = $param['end'];
		$sql1 = "SELECT * from view_omset_workstation where cashdraw_no='$kas'";
	  	$result1 = $this->db->query($sql1);
		$fetch1 = $result1->result();
		$omset = $fetch1[0]->omset;
		$total_cash = $fetch1[0]->total_cash;
		$check_out = date('H:i:s');

		$sql = "UPDATE atombizz_selling_cashdraw set check_out = ?, end_cash = ?, omset = ?, total_cash = ?, status = ? WHERE cashdraw_no = ?";
	  	$hasil = $this->db->query($sql,array($check_out,$end,$omset,$total_cash,'closed',$kas));

		if($hasil){
			echo "1";
		}else{
			echo "0";
		}
	}

	public function print_invoice($param='')
	{
		$sql = "select s.*, i.* from atombizz_selling s, atombizz_selling_items i
				where s.invoice_no = i.invoice and s.cashdraw_no = i.cashdraw 
				and i.invoice = ? and i.cashdraw = ? ";
		$hasil = $this->db->query($sql,array($param['invoice'],$param['kas']));

		return $hasil->result();
	}

	public function print_kas($param='')
	{
		$sql = "SELECT * FROM view_selling_cashdraw WHERE cashdraw_no = ?";
		$hasil = $this->db->query($sql,array($param['kas']));

		return $hasil->result();
	}

	public function move_transaction($param=''){
		$invoice = $param['invoice'];
		
		$sql = "delete from atombizz_tmp_detail_jual where invoice='$invoice'";
	  	$hasil = $this->db->query($sql);
			
		if($hasil){
			echo "1";
		}else{
			echo "0";
		}
	}

	public function cashdraw_transaction($param=''){
		$ident=$param['nota'];

		$sql = "UPDATE atombizz_selling_cashdraw set status = ? WHERE cashdraw_no = ?";
	  	$hasil = $this->db->query($sql,array('submited',$ident));

		if($hasil){
			echo "1";
		}else{
			echo "0";
		}
	}

	//function open cashier
	public function add_pelanggan($param=''){
		$pelanggan=$param['pelanggan'];

		$sqlcek = "SELECT COUNT(id) as total from atombizz_customers where code='$pelanggan'";
		$hasilcek = $this->db->query($sqlcek);
		foreach ($hasilcek->result_array() as $rowcek) {
	  		$total = $rowcek['total'];
	  	}

	  	if($total > 0){
	  		$sql = "SELECT * from atombizz_customers where code='$pelanggan'";
		  	$hasil = $this->db->query($sql);
	  		foreach ($hasil->result_array() as $row) {
		  		$ident = $row['id'];
		  		$nama = $row['name'];
		  	}
			echo '1|'.$ident.'|'.$nama;
	  	}else{
	  		echo '0|';
	  	}
	}

	public function total_transaksi($param){
		$sql = "select COUNT(id) as total from atombizz_selling where cashdraw_no = ?";
	  	$hasil = $this->db->query($sql,$param['kas']);
	  	foreach ($hasil->result_array() as $row) {
	  		$total = $row['total'];
	  	}
	  	if($total == null || $total <= 0){
	  		echo "0";
	  	}else{
	  		echo $total;
	  	}
	}

	public function total_suspend($param){
		$sql = "SELECT
					count(*) AS total
				FROM
					atombizz_tmp_detail_jual
				WHERE
					cashdraw = ?
				AND STATUS = 'suspended'";

	  	$hasil = $this->db->query($sql,$param['kas']);
	  	
	  	foreach ($hasil->result_array() as $row) {
	  		$total = @$row['total'];
	  	}
	  	if($hasil == null || @$total <= 0){
	  		echo "0";
	  	}else{
	  		echo $total;
	  	}
	}

	public function total_cancel(){
		$sql = "SELECT COUNT(id) AS total FROM atombizz_tmp_detail_jual WHERE status='void' GROUP BY invoice";
	  	$hasil = $this->db->query($sql);
	  	foreach ($hasil->result_array() as $row) {
	  		$total = @$row['total'];
	  	}
	  	if($hasil == null || @$total <= 0){
	  		echo "0";
	  	}else{
	  		echo $total;
	  	}
	}

	public function get_barang($tipe='',$value=''){
		if ($tipe == 'name') {
			if ($value) {
				$this->db->like('name', $value); 
			}
		}
		if ($tipe == 'code') {
			if ($value) {
				$this->db->like('code', $value); 
			}
		}
		$query = $this->db->get('atombizz_product');

		return $query->result();
	}

	public function get_product($param=''){

		$sql = "select * from atombizz_product where (name like '%".$this->db->escape_str($param['keyword'])."%' OR code like '%".$this->db->escape_str($param['keyword'])."%') ";
		
		if ($param['category'] != 'all') {
			$sql .= " and category = '".$this->db->escape_str($param['category'])."' ";
		}
		
		$hasil = $this->db->query($sql);

		return $hasil->result();
	}

	public function get_product_scan($param=''){

		$hasil = $this->db->get_where('view_warehouse_stok', array('code' => $param['barcode_produk'], 'status' => 'rak'));

		if ($hasil->result() != null) {
			return $hasil->result();
		}else{
			return null;
		}
		
	}

	public function getstok($code='')
	{
		$hasil = $this->db->get_where('view_warehouse_stok', array('code' => $code, 'status' => 'rak'));
		return $hasil->result();
	}
	

}

/* End of file mdl_selling.php */
/* Location: ./application/modules/cashier/models/mdl_selling.php */