<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mdl_checking extends CI_Model {

	public function kas(){
		$user = $this->session->userdata('basmalahsession');
		$tgl=date('Y-m-d');
		$user_id=$user[0]->id;
		$sql = "SELECT COUNT(id) as total FROM atombizz_selling_cashdraw WHERE date='$tgl' AND user_id='$user_id'";
		$result = $this->db->query($sql);
		$fetch=$result->result();
		$total = $fetch[0]->total;

		if($total >= 1){
			$sql1 = "SELECT status, cashdraw_no FROM atombizz_selling_cashdraw WHERE date='$tgl' AND user_id='$user_id'";
			$result1 = $this->db->query($sql1);
			$fetch1=$result1->result();
			$status = $fetch1[0]->status;
			$cash = $fetch1[0]->cashdraw_no;
			return '1|'.$status.'|'.$cash;
		}else{
			return '0|';
		}
	}

	public function cek_member($param='')
	{
		$today = date("Y-m-d");

		$member = $this->db->get_where('atombizz_customers', array('code' => $param['code_member']));
		$member = $member->result();

		if ($member != null) {
			$expired = @$member[0]->expired;
			if ($today > $expired) {
				return 'Member has been expired';
			}else{
				$this->session->set_userdata( 'membersession', $member );
				return 'Member Success';
			}
		}else{
			return "Member is not exist";
		}
		
	}

	public function transaction($param=''){
		$kas = $param['kas'];
		$tgl = date('Y-m-d');
		$sql = "select COUNT(id) as total from atombizz_tmp_detail_jual where cashdraw='$kas' AND orderdate='$tgl'";
		$result = $this->db->query($sql);
		$fetch=$result->result();
		$total = $fetch[0]->total;
		
		if($total >= 1){
			echo '1';
		}else{
			echo '0';
		}
	}

	public function temporary($param=''){
		$user = $this->session->userdata('basmalahsession');
  	  	$user_id=$user[0]->id;
  	  	$kas = $param['cash'];
  	  	$tgl = date('Y-m-d');

  	  	$sql = "SELECT COUNT(id) as total FROM atombizz_tmp_detail_jual WHERE orderdate='$tgl' AND userid='$user_id' AND cashdraw='$kas' AND status='temporary'";
		$result = $this->db->query($sql);
		$fetch=$result->result();
		$total = $fetch[0]->total;
		if($total >= 1){
			$sql2 = "SELECT * FROM atombizz_tmp_detail_jual WHERE orderdate='$tgl' AND userid='$user_id' AND cashdraw='$kas' AND status='temporary'";
		  	$result2 = $this->db->query($sql2);
		  	$fetch2=$result2->result();
		  	echo '1|'.$fetch2[0]->invoice;
		}else{
			echo '0|';
		}

	  	
  	}
}

/* End of file mdl_checking.php */
/* Location: ./application/modules/cashier/models/mdl_checking.php */