<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_karyawan extends MY_Model {

    function opt_jabatan()
    {
    	$sql = "SELECT * FROM atombizz_employee_position";
    	$res = $this->db->query($sql);

    	if($res->num_rows() > 0) {
    		$hasil[''] = "Pilih Jabatan";
    		foreach($res->result_array() as $val) {
    			$hasil[trim($val['id'])] = trim($val['group']);
    		}
    	} else {
    		$hasil[''] = "No Category Found";
    	}

    	return $hasil;
    }

    function mencari($cat,$key){
		$sql = "SELECT * FROM view_employee WHERE ".$cat." LIKE '%$key%'";
		$data = $this->db->query($sql);
		return $data;
	}

	function detail($key){
		$sql = "SELECT * FROM view_employee WHERE id=$key";
		$data = $this->db->query($sql);
		foreach ($data->result_array() as $data) {
            $data['upass'] = paramDecrypt($data['upass']);
            $data['confirm-upass'] = $data['upass'];
			$result = $data;
		}
		return $result;
	}
}