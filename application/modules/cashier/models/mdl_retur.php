<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mdl_retur extends CI_Model {

	
	public function checking($getter,$tgl,$user)
	{
		
		  if($getter=="transaction"){
				$tgl = $_REQUEST['tgl'];
				$meja = $_REQUEST['meja'];
				$kas = $_REQUEST['kas'];
				$sql = "select COUNT(id) as total from e-inventory_tmp_detail_jual where kas='$kas' AND tgl='$tgl' AND meja='$meja'";
				$result = $this->db->query($sql);
				
				$fetch=$result->result();
				
				$total = $fetch[0]->total;
				
				if($total >= 1){
					echo '1';
				}else{
					echo '0';
				}
		  }
		   else if($getter=="kas"){
		   		$user_id=$user[0]->id;
		   		
				$sql = "SELECT COUNT(id) as total FROM atombizz_selling_cashdraw WHERE dater='$tgl' AND user_id='$user_id'";
				$result = $this->db->query($sql);
				$fetch=$result->result();
				$total = $fetch[0]->total;

				if($total >= 1){
					$sql1 = "SELECT status FROM atombizz_selling_cashdraw WHERE dater='$tgl' AND user_id='$user_id'";
					$result1 = $this->db->query($sql1);
					$fetch1=$result1->result();
					$status = $fetch1[0]->status;
					return '1|'.$status;
				}else{
					return '0|';
				}
		  }

		  else if($getter == 'temporary'){
			  	  $user_id=$user[0]->id;

				  $sql = "SELECT * FROM atombizz_selling_cashdraw WHERE dater='$tgl' AND user_id='$user_id' ";
				  $result = $this->db->query($sql);
				  $fetch=$result->result();
				  echo $fetch[0]->id.'|'.$fetch[0]->cashdraw_no;
		  }

		  else if($getter=="pembelian"){
				$tgl = $_REQUEST['tgl'];
				$op = $_REQUEST['user'];
				$tempat = $_REQUEST['tempat'];
				$field = "COUNT(`id`) as total";
				$table = "`e-inventory_tmp_detail_beli`";
				$condition = "`tgl`='$tgl' AND `operator`='$op' AND `tempat`='$tempat'";
				$result = $as->selectWhere($field, $table, $condition);
				$fetch=mysql_fetch_array($result);
				$total = $fetch['total'];
				if($total >= 1){
					echo '1';
				}else{
					echo '0';
				}
		  }
		   else if($getter=="spoil"){
				$tgl = $_REQUEST['tgl'];
				$work = $_SESSION['incKaryawanWorkstation'];
				$karyawan = $_SESSION['incKaryawanUser'];
				$field = "COUNT(`id`) as total, `status`";
				$table = "`e-inventory_spoil`";
				$condition = "`tgl`='$tgl' AND `outlet`='$work' AND `operator`='$karyawan'";
				$result = $as->selectWhere($field, $table, $condition);
				$fetch=mysql_fetch_array($result);
				$total = $fetch['total'];
				$status = $fetch['status'];
				if($total >= 1){
					echo '1|'.$status;
				}else{
					echo '0|'.$status;
				}
		  }
		  else if($getter=="charge"){
				$tgl = $_REQUEST['tgl'];
				$work = $_SESSION['incKaryawanWorkstation'];
				$karyawan = $_SESSION['incKaryawanUser'];
				$field = "COUNT(`id`) as total, `status`";
				$table = "`e-inventory_charge`";
				$condition = "`tgl`='$tgl' AND `outlet`='$work' AND `operator`='$karyawan'";
				$result = $as->selectWhere($field, $table, $condition);
				$fetch=mysql_fetch_array($result);
				$total = $fetch['total'];
				$status = $fetch['status'];
				if($total >= 1){
					echo '1|'.$status;
				}else{
					echo '0|'.$status;
				}
		  }
		  else if($getter=="servent"){
				$tgl = $_REQUEST['tgl'];
				$work = $_SESSION['incKaryawanWorkstation'];
				$karyawan = $_SESSION['incKaryawanUser'];
				$field = "COUNT(`id`) as total, `status`";
				$table = "`e-inventory_servent`";
				$condition = "`tgl`='$tgl' AND `outlet`='$work' AND `operator`='$karyawan'";
				$result = $as->selectWhere($field, $table, $condition);
				$fetch=mysql_fetch_array($result);
				$total = $fetch['total'];
				$status = $fetch['status'];
				if($total >= 1){
					echo '1|'.$status;
				}else{
					echo '0|'.$status;
				}
		  }
		  else if($getter=="mutasi"){
				$tgl = $_REQUEST['tgl'];
				$op = $_REQUEST['user'];
				$field = "COUNT(`id`) as total";
				$table = "`e-inventory_tmp_mutasi`";
				$condition = "`tgl`='$tgl' AND `operator`='$op'";
				$result = $as->selectWhere($field, $table, $condition);
				$fetch=mysql_fetch_array($result);
				$total = $fetch['total'];
				if($total >= 1){
					echo '1';
				}else{
					echo '0';
				}
		  }
		   else if($getter=="audit"){
				$bln = $_REQUEST['bulan'];
				$thn = $_REQUEST['tahun'];
				$field = "COUNT(`uid`) as total";
				$table = "`e-inventory_audit`";
				$condition = "`bulan`='$bln' AND `tahun`='$thn'";
				$result = $as->selectWhere($field, $table, $condition);
				$fetch=mysql_fetch_array($result);
				$total = $fetch['total'];
				if($total >= 1){
					echo '1';
				}else{
					echo '0';
				}
		  }
		  else if($getter=="auditCek"){
				$tgl = $_REQUEST['tgl'];
				$user = $_REQUEST['user'];
				$field = "COUNT(`uid`) as total";
				$table = "`e-inventory_audit_tmp`";
				$condition = "`tgl`='$tgl' AND `operator`='$user'";
				$result = $as->selectWhere($field, $table, $condition);
				$fetch=mysql_fetch_array($result);
				$total = $fetch['total'];
				if($total >= 1){
					echo '1';
				}else{
					echo '0';
				}
		  }
		  
		  else{
			  echo 'You dont have authorization to access this page!!!';
		  }
	}

	public function get_nota($nota='')
	{
		$hasil = $this->db->get_where('atombizz_selling', array('invoice_no' => $nota));

		return $hasil->result();
	}

	public function insert_retur_tmp($data='')
	{
		$user = $this->session->userdata('basmalahsession');
		$gudang = $this->db->get_where('view_warehouse_stok', array('code' => $data[0]->product_code, 'status' => 'gudang'));
		$gudang = $gudang->result();

		$produk = $this->db->get_where('atombizz_product', array('code' => $data[0]->product_code));
		$produk = $produk->result();

		$cek = $this->db->get_where('atombizz_retur_jual_tmp', array('nota' => $data[0]->invoice, 'kdbrg' => $produk[0]->id));
		$cek = $cek->result();

		if ($cek == null) {
			$sql = "insert into atombizz_retur_jual_tmp (nota,tgl,kdgud,jml,kdbrg,namabrg,harga,
				userlog,username,subtotal) 
				values (?,now(),?,?,?,?,?,now(),?,?)";
			$datanya = array(
			   $data[0]->invoice ,
			   $gudang[0]->warehouse,
			   $data[0]->qty,
			   $produk[0]->id,
			   $data[0]->product_name,
			   $data[0]->price,
			   $user[0]->nama,
			   $data[0]->subtotal,
			);

			$this->db->query($sql, $datanya); 
		}

	}

	public function cekbarangretur($id='')
	{
		$nota = $this->session->userdata('notasession');
		$hasil = $this->db->get_where('atombizz_selling_items', array('product_code' => $id, 'invoice' => $nota[0]->invoice_no));

		return $hasil->result();
	}

	public function get_list_retur()
	{
		$nota = $this->session->userdata('notasession');
		
		$hasil = $this->db->get_where('atombizz_retur_jual_tmp', array('nota' => @$nota[0]->invoice_no));	
		
		return $hasil->result();
	}

	public function proses_retur($nota='')
	{
		$user = $this->session->userdata('basmalahsession');

		$data = $this->db->get_where('atombizz_retur_jual_tmp', array('nota' => $nota));
		$data = $data->result();

		if ($data != null) {
			$user = $this->session->userdata('basmalahsession');

			$totalnota = 0;
			$totaljmlbarang = 0;
			
			foreach ($data as $key => $value) {
				$totalnota += $value->subtotal;	
				$totaljmlbarang += $value->jml;
			}

			$sql = "insert into atombizz_retur_jual (nota,tgl,totalnota,username,userlog)
					values(?,now(),?,?,now())";
			$hasil = $this->db->query($sql, array($nota,$totalnota,$user[0]->nama));
			
			foreach ($data as $key => $value) {
				
				$produk = $this->db->get_where('atombizz_product', array('id' => $value->kdbrg));
				$produk = $produk->result();

				$gudang = $this->db->get_where('view_warehouse_stok', array('code' => $produk[0]->code, 'status' => 'gudang'));
				$gudang = $gudang->result();

				$sql = "insert into atombizz_retur_jual_detail 
						(nota,tgl,kdgud,jml,kdbrg,namabrg,harga,userlog,username)
						values(?,?,?,?,?,?,?,?,?)";
				$hasil = $this->db->query($sql, 
							array($nota,$value->tgl,@$gudang[0]->warehouse,$value->jml,
									$value->kdbrg,$value->namabrg,$value->harga,$value->userlog,$user[0]->nama));
			
				// insert data ke warehouse_stok

				$message_masuk = "Retur Barang ".$value->namabrg." dengan nomor invoice ".$nota;
				$message_keluar = "Penggantian Retur Barang ".$value->namabrg." untuk nomor invoice ".$nota;
				
				// untuk Retur masuk
				$sql = "insert into atombizz_warehouses_stok 
							(date,status,invoice,masuk,keluar,keterangan,userlog,operator,warehouse,produk)
							values(now(),'retur_jual',?,?,0,?,now(),?,?,?)";
				$hasil = $this->db->query($sql, 
									array($nota,$value->jml,$message_masuk,$user[0]->id,
											$gudang[0]->warehouse,$value->kdbrg));

				// untuk Retur keluar
				$sql = "insert into atombizz_warehouses_stok 
							(date,status,invoice,masuk,keluar,keterangan,userlog,operator,warehouse,produk)
							values(now(),'retur_jual',?,0,?,?,now(),?,?,?)";
				$hasil = $this->db->query($sql, 
									array($nota,$value->jml,$message_keluar,$user[0]->id,
											$gudang[0]->warehouse,$value->kdbrg));
				
				// hapus data di table atombizz_retur_jual_tmp
				$this->db->where('nota', $nota);
				$this->db->delete('atombizz_retur_jual_tmp');
			}
		}

		return $this->db->affected_rows();
	}

	public function get_list_penjualan($id='')
	{
		$user = $this->session->userdata('basmalahsession');
		$today = date("Y-m-d"); 
		$sql = "select j.*, p.name, p.price1 from atombizz_retur_jual_tmp j, atombizz_product p 
				where j.kdbrg = p.code and j.username = ? ";

		$hasil = $this->db->query($sql,array($user[0]->id));

		return $hasil->result();
	}

	public function add_penjualan($param='')
	{
		$user = $this->session->userdata('basmalahsession');
		$barang = $this->get_detail_barang($param['id_produk']);
		$cek_barang_temp = $this->get_barang_temp($param['id_produk']);
		$today = date("Y-m-d"); 

		if ($barang != null) {
			if ($cek_barang_temp != null) {
				$jumlah = intval($cek_barang_temp[0]->jml) + 1;
				$subtotal = intval($barang[0]->price1) * intval($jumlah);
				$sql = "update atombizz_retur_jual_tmp set jml = ?, harga = ?, userlog = now() where tgl = ? and userlog = ? and kdbrg = ? ";

				$hasil = $this->db->query($sql,array($jumlah,$subtotal,$today,$user[0]->id,$param['id_produk']));	
			}else{
				$sql = "insert into atombizz_retur_jual_tmp (nota,tgl,jml,kdbrg,namabrg,harga,username,userlog)
				values('1',now(),1,?,?,?,?,now())";

				$hasil = $this->db->query($sql,array($param['id_produk'],$barang[0]->name,$barang[0]->price1,$user[0]->id));	
			}
			
		}

		return $this->db->affected_rows();		

	}

	public function edit_penjualan($param='')
	{
		$barang = $this->get_detail_barang($param['code']);
		$subtotal = intval($barang[0]->price1) * intval($param['quantity']);
		$today = date("Y-m-d"); 
		$user = $this->session->userdata('basmalahsession');

		$sql = "update atombizz_retur_jual_tmp set jml = ?, harga = ? where username = ? and kdbrg = ? and id = ? ";

		$hasil = $this->db->query($sql,array($param['quantity'],$subtotal,$user[0]->id,$param['code'],$param['id_jual']));
	}

	public function delete_penjualan($param='')
	{
		$this->db->where('id', $param['delid_jual']);
		$this->db->delete('atombizz_retur_jual_tmp'); 

		return $this->db->affected_rows();
	}


	public function get_detail_barang($id='')
	{
		$query = $this->db->get_where('atombizz_product', array('code' => $id));

		return $query->result();
	}

	public function get_barang($tipe='',$value='')
	{
		if ($tipe == 'name') {
			if ($value) {
				$this->db->like('name', $value); 
			}
		}
		if ($tipe == 'code') {
			if ($value) {
				$this->db->like('code', $value); 
			}
		}
		$this->db->where('status', 'rak'); 
		$query = $this->db->get('view_warehouse_stok');



		return $query->result();
	}

	public function get_barang_temp_by_id($id='')
	{
		$query = $this->db->get_where('atombizz_retur_jual_tmp', array('id' => $id));

		return $query->result();
	}

	public function get_barang_temp($id='')
	{
		$user = $this->session->userdata('basmalahsession');
		$today = date("Y-m-d"); 

		$query = $this->db->get_where('atombizz_retur_jual_tmp', array('kdbrg' => $id, 'tgl' =>$today,'username'=>$user[0]->id));

		return $query->result();
	}

	public function proses_pembayaran($param='')
	{
		$user = $this->session->userdata('basmalahsession');

		$data = array(
               'status' => '2'
            );

		$this->db->where('tgl', date('Y-m-d'));
		$this->db->where('userid', $user[0]->id);
		$this->db->where('status', '1');
		$this->db->update('atombizz_tmp_detail_jual', $data); 

		return $this->db->affected_rows();
		
	}

	public function proses_suspend()
	{
		$user = $this->session->userdata('basmalahsession');

		$data = array(
               'status' => '3'
            );

		$this->db->where('tgl', date('Y-m-d'));
		$this->db->where('userid', $user[0]->id);
		$this->db->where('status', '1');
		$this->db->update('atombizz_tmp_detail_jual', $data); 

		return $this->db->affected_rows();
	}

	public function proses_void()
	{
		$user = $this->session->userdata('basmalahsession');

		$this->db->where('tgl', date('Y-m-d'));
		$this->db->where('userid', $user[0]->id);
		$this->db->where('status', '1');
		$this->db->delete('atombizz_tmp_detail_jual'); 

		return $this->db->affected_rows();
	}

}

/* End of file mdl_penjualan.php */
/* Location: ./application/modules/cashier/models/mdl_penjualan.php */