<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		// $this->module = 'master';
		
		// $access = strtolower($this->module);
		// $this->permission->check_module_permission($access);
	} 

	public function index()
	{
		// echo "tes admin";
		redirect('/admin/dashboard');
		// $data['content'] = $this->load->view('/dashboard',$data,TRUE);
		// $this->load->view('/template', $data);
	}

	public function dashboard()
	{
		// $data['data'] = $this->accounting_model->get_data();
		$data['title'] = "Dashboard | Admin";
		$data['content'] = $this->load->view('/dashboard',$data,TRUE);
		$this->load->view('/template', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
