<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stakeholder extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		// $this->module = 'stakeholder';
		$this->load->model('admin_model');
		$this->load->model('paket_model');
		
		// $access = strtolower($this->module);
		// $this->permission->check_module_permission($access);
	} 

	public function index()
	{
		$data['title'] = "Admin | Staheholder";
		$data['content'] = $this->load->view('/stakeholder',$data,TRUE);
		$this->load->view('/template', $data);
	}
	// data frontliner //
	function tambah_fl()
	{
		$n = $this->input->post('val_nama');
		$u = $this->input->post('val_uname');
		$p = sha1($this->input->post('val_pass'));
		$tgl=date('d-m-Y');
		$data = array(
			   'id' => '' ,
			   'position_id' => '3' ,
			   'nama' => $n ,
			   'uname' => $u ,
			   'upass' => $p ,
			   'date_reg' => $tgl 
			);
		$this->db->insert('vti_employee', $data); 
		redirect('admin/stakeholder');
	}
	function edit_fl($id)
	{
		
		$data['title'] = "Admin | Edit Frontliner";
		$data['id'] = $id;
		$data['content'] = $this->load->view('/part/edit_fl',$data,TRUE);
		$this->load->view('/template', $data);
	}
	function proses_edit_fl($id)
	{
		$n = $this->input->post('val_nama');
		$u = $this->input->post('val_uname');
		$new = $this->input->post('val_pass_baru');
		$new2 = $this->input->post('val_pass_baru2');
		if($new == $new2 && $new !=='' && $new2 !=='')
		{
			$data = array(
				'nama' => $n,
				'uname' => $u,
				'upass' => sha1($new),
			);
		}
		else
		{			
			$data = array(
				'nama' => $n,
				'uname' => $u,
			);
		}
		$this->db->where('id', $id);
		$this->db->update('vti_employee',$data);
		redirect('admin/stakeholder');
	}
	function hapus_Fl()
	{
		$i = $this->input->post('id_hapus');
		$this->db->delete('vti_employee', array('id' => $i));
		redirect('admin/stakeholder');
	}
	// end data frontliner //

	// data tim verifikasi //
	function tambah_ver()
	{
		$n = $this->input->post('val_nama');
		$u = $this->input->post('val_uname');
		$p = sha1($this->input->post('val_pass'));
		$tgl=date('d-m-Y');
		$data = array(
			   'id' => '' ,
			   'position_id' => '4' ,
			   'nama' => $n ,
			   'uname' => $u ,
			   'upass' => $p ,
			   'date_reg' => $tgl 
			);
		$this->db->insert('vti_employee', $data); 
		redirect('admin/stakeholder');
	}
	function edit_ver($id)
	{
		
		$data['title'] = "Admin | Edit Tim Verifikasi";
		$data['id'] = $id;
		$data['content'] = $this->load->view('/part/edit_ver',$data,TRUE);
		$this->load->view('/template', $data);
	}
	function proses_edit_ver($id)
	{
		$n = $this->input->post('val_nama');
		$u = $this->input->post('val_uname');
		$new = $this->input->post('val_pass_baru');
		$new2 = $this->input->post('val_pass_baru2');
		if($new == $new2 && $new !=='' && $new2 !=='')
		{
			$data = array(
				'nama' => $n,
				'uname' => $u,
				'upass' => sha1($new),
			);
		}
		else
		{			
			$data = array(
				'nama' => $n,
				'uname' => $u,
			);
		}
		$this->db->where('id', $id);
		$this->db->update('vti_employee',$data);
		redirect('admin/stakeholder');
	}
	function hapus_ver()
	{
		$i = $this->input->post('id_hapus');
		$this->db->delete('vti_employee', array('id' => $i));
		redirect('admin/stakeholder');
	}
	// end data tim verifikasi //

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
