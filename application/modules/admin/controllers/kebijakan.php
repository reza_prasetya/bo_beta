<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kebijakan extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		// $this->module = 'master';
		$this->load->model('paket_model');
		// $access = strtolower($this->module);
		// $this->permission->check_module_permission($access);
	} 

	public function index()
	{
		$data['title'] = "Admin | Kebijakan";
		$data['content'] = $this->load->view('/kebijakan',$data,TRUE);
		// $data['modal'] = $this->load->view('/stakeholder_modal');
		$this->load->view('/template', $data);
	}
	// data paket //
	function tambah_paket()
	{
		$n = $this->input->post('val_nama');
		$h = $this->input->post('val_harga');
		$d = $this->input->post('val_day');
		$t = $this->input->post('val_tol');
		$data = array(
				'name' => $n,
				'price' => $h,
				'cashback_time' => $d,
				'additional_time' => $t
			);
		$this->db->insert('vti_corp_package', $data); 
		redirect('admin/kebijakan');
	}
	function edit_paket($id)
	{
		$data['title'] = "Admin | Edit Paket";
		$data['id'] = $id;
		$data['content'] = $this->load->view('/part/edit_paket',$data,TRUE);
		$this->load->view('/template', $data);
	}
	function proses_edit_paket($id)
	{
		$n = $this->input->post('val_nama');
		$h = $this->input->post('val_harga');
		$d = $this->input->post('val_day');
		$t = $this->input->post('val_tol');
		$s = $this->input->post('val_status');
		$data = array(
				'name' => $n,
				'price' => $h,
				'cashback_time' => $d,
				'additional_time' => $t,
				'status' => $s
			);
		$this->db->where('package_id', $id);
		$this->db->update('vti_corp_package',$data);
		redirect('admin/kebijakan');
	}
	function hapus_paket()
	{
		$i = $this->input->post('id_hapus');
		$this->db->delete('vti_corp_package', array('package_id' => $i));
		redirect('admin/kebijakan');
	}
	// end data paket //
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
