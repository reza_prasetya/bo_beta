<div id="fx-container" class="fx-opacity">
				<div id="page-content" class="block" style="min-height:500px;">
					<div class="row">
						<div class="col-sm-12">																		
							<div class="block full" style="margin-top:10px;" id="kebijakan-div">
								<div class="row">
									<div class="pull-left" style="">
										<div class="clock">
											<div id="Date" style="font-size: 20px; float: left;">Monday 14 January 2013</div>
											<ul style="list-style: none outside none; font-size: 40px;">
												<li id="hours" style="display: inline;">10</li>
											    <li id="point" style="display: inline;">:</li>
											    <li id="min" style="display: inline;">13</li>
											    <li id="point" style="display: inline;">:</li>
											    <li id="sec" style="display: inline;">03</li>
											</ul>
										</div>
									</div>
								</div>
								<center>
									<a href="#modalTmbhFl" class="btn btn-block btn-lg btn-success" data-toggle="modal">
										<i class="icon-plus"></i> TAMBAH PAKET
									</a>
									<?php $this->load->view('/part/kebijakan_modal');; ?>
									<br />
								</center>
								<div class="table-responsive">
							        <table id="example-datatable" class="table table-bordered table-hover">
							          <thead>
							            <tr>
							              <th class="text-center">Nama</th>
							              <th class="text-center">Harga (dalam juta)</th>
							              <th class="text-center">Batas Pengembalian</th>
							              <th class="text-center">Toleransi</th>
							              <th class="text-center">Status</th>
							              <th class="text-center">Action</th>
							            </tr>
							          </thead>
							          <tbody>
							            <?php echo $this->paket_model->list_paket();?>
							          </tbody>
							        </table>
								</div>
							</div>
						</div>
					</div>
					<!-- End Content -->
				</div>
			<footer class="clearfix">
		        <div class="pull-right">
		          <!-- Design and Developed by <a target="_blank" href="#" target="_blank">Team</a> -->
		        </div>
		        <div class="pull-left">
		          <!-- <span id="year-copy"></span> &copy; <a target="_blank" href="#" target="_blank">Bisnis Online</a> -->
		        </div>
		    </footer>
		</div>
<script type="text/javascript">

	$(document).ready(function(){
		begin();
	});
	function e(e, t) {
		return Math.floor(Math.random() * (t - e + 1)) + e
	}
	function toggleButton(){
		if($('.hide-key').is(':visible')){
			$('.hide-key').hide();
		} else {
			$('.hide-key').show();
		}
	}
	function generate(){
		var t = 0;
		$(".progress-bar", ".bars-container").each(function () {
			t = e(10, 100) + "%",
			$(this).css("width", t).html(t)
		});
	}
	function flButton(){
		$('#head-div').slideDown();
		$('#kebijakan-div').slideDown();
		$('#ver-div').slideUp();
	}
	function verButton(){
		$('#head-div').slideDown();
		$('#kebijakan-div').slideUp();
		$('#ver-div').slideDown();
	}
	function begin(){
		// $('#kebijakan-div').slideDown();
		// $('#ver-div').slideUp();
		$('#kebijakan-div').show();
		$('#ver-div').hide();

		$('.hide-key').slideUp();
	}
	$(function(){
		  webApp.datatables(),$("#example-datatable").dataTable({
		    aoColumnDefs:[{bSortable:!1,aTargets:[4]}],iDisplayLength:15,aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]]}),
		      $(".dataTables_filter input").addClass("form-control").attr("placeholder","Cari")
		});
</script>