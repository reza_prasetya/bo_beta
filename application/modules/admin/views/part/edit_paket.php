<div id="fx-container" class="fx-opacity"><div id="page-content" class="block">
<div class="row" style='margin-bottom:10px;margin-left:35%;'>
    <!-- <div class="col-sm-4">
      <button class="btn btn-block btn-lg btn-primary" onClick="javascript:window.location.href=('dashboard.html');">
      <i class="icon-reply"></i> DASHBOARD</button>
    </div> -->
  </div>
<div class="block">
    <div class="col-md-11" style="margin-left:5%;">
      <center>
        <h3 style="font-weight:bold;">EDIT DATA PAKET</h3>
        <br />
        <a href="<?php echo base_url();?>admin/kebijakan" class="btn btn-block btn-lg btn-warning">
          <i class="icon-reply"></i> KEMBALI
        </a>
        <br />
      </center>
    </div>
		<?php echo $this->paket_model->edit_paket($id)?>
</div>
</div>
<footer class="clearfix">
        <div class="pull-right">
          <!-- Design and Developed by <a target="_blank" href="#" target="_blank">Team</a> -->
        </div>
        <div class="pull-left">
          <!-- <span id="year-copy"></span> &copy; <a target="_blank" href="#" target="_blank">Bisnis Online</a> -->
        </div>
      </footer>
</div>

<script>
  $(function(){$("#form-validation").
    validate({errorClass:"help-block",errorElement:"div",errorPlacement:function(e,t)
      {t.parents(".form-group > div").append(e)},
        highlight:function(e){$(e).closest(".form-group").removeClass("has-success has-error").addClass("has-error"),
        $(e).closest(".help-block").remove()},
        success:function(e){e.closest(".form-group").removeClass("has-success has-error").
        addClass("has-success"),e.closest(".help-block").remove()},
        rules:{
          // val_asal:{required:!0},
          // val_pass_lama:{equalTo:"#val_pass"},
          // val_pass_baru2:{equalTo:"#val_pass_baru"},
          val_terms:{required:!0}
        },
        messages:{
          // val_asal:"Masukkan alamat asal",
          // val_pass_lama:"Password tidak sama - Kosongi bila tidak perlu",
          // val_pass_baru2:"Password tidak sama - Kosongi bila tidak perlu",
          val_terms:"Anda harus memilih persetujuan"
            }
          }
        )
      }
    );
  </script>