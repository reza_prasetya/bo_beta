<div id="modalTmbhVer" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tambah Tim Verifikasi</h4>
			</div>
			<div class="modal-body">
				<div class="block">
					<form id="form-validation" action="stakeholder/tambah_ver" method="post" class="form-horizontal" style="margin-left:10%;">
						<div class="form-group">
							<label class="col-md-2 control-label">Nama</label>
								<div class="col-md-8">
									<div class="input-group">
										<input type="text" id="val_nama" name="val_nama" class="form-control">
										<span class="input-group-addon"><i class="icon-edit icon-fixed-width"></i></span>
									</div>
								</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Username</label>
								<div class="col-md-8">
									<div class="input-group">
										<input type="text" id="val_uname" name="val_uname" class="form-control">
										<span class="input-group-addon"><i class="icon-user icon-fixed-width"></i></span>
									</div>
								</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Password</label>
								<div class="col-md-8">
									<div class="input-group">
										<input type="password" id="val_pass" name="val_pass" class="form-control">
										<span class="input-group-addon"><i class="icon-user icon-unlock-alt"></i></span>
									</div>
								</div>
						</div>
						<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
						<br /><br />
					</form>
				</div>
			</div>
		</div>
	</div>
</div>