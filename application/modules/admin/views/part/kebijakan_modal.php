<div id="modalTmbhFl" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tambah Paket</h4>
			</div>
			<div class="modal-body">
				<div class="block">
					<form id="form-validation" action="kebijakan/tambah_paket" method="post" class="form-horizontal" style="margin-left:10%;">
						<div class="form-group">
							<label class="col-md-4 control-label">Nama</label>
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_nama" name="val_nama" class="form-control">
										<span class="input-group-addon"><i class="icon-edit icon-fixed-width"></i></span>
									</div>
								</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Harga</label>
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_harga" name="val_harga" class="form-control">
										<span class="input-group-addon">Juta Rupiah</span>
									</div>
								</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Pengembalian</label>
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_day" name="val_day" class="form-control">
										<span class="input-group-addon">Hari</span>
									</div>
								</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Toleransi</label>
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_tol" name="val_tol" class="form-control">
										<span class="input-group-addon">Hari</span>
									</div>
								</div>
						</div>
						<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
						<br /><br />
					</form>
				</div>
			</div>
		</div>
	</div>
</div>