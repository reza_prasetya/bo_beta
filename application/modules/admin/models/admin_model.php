<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends MY_Model {

	public function list_fl()
	{
		$query = $this->db->where('position_id','3')->get('vti_employee');
		$c = 1;
			foreach ($query->result() as $row)
			{
				$id = $row->id;
				$nama = $row->nama;
				$uname = $row->uname;
				$upass = $row->upass;
				$tgl = $row->date_reg;
				?>
					<tr>
						<td class="text-center"><?php echo $c;?></td>
						<td class="text-center"><center><?php echo $nama;?></center></a></td>
						<td class="text-center"><?php echo $uname;?></td>
						<td class="text-center">Aktif</td>
						<td class="text-center"><?php echo $tgl;?></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="stakeholder/edit_fl/<?php echo $id;?>" title="Edit" class="btn btn-xs btn-default" style="margin-left:-1%;">
									<i class="icon-pencil"></i> EDIT</a>
								<a href="#hapusFl<?php echo $id;?>" data-toggle="modal" title="Delete" class="btn btn-xs btn-default">
									<i class="icon-remove"></i> HAPUS</a>
								<div id="hapusFl<?php echo $id;?>" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title">Hapus Frontliner</h4>
											</div>
											<div class="modal-body">
												<div class="block">
													<form id="form-validation" action="stakeholder/hapus_fl" method="post" class="form-horizontal">
														<h3>Anda Yakin ?</h3>
														<input type="hidden" value="<?php echo $id;?>" name="id_hapus">
														<br />
														<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
														<button type="submit" class="btn btn-danger">Hapus</button>
														<br /><br />
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>
				<?php
				$c ++;
			}
	}
	public function edit_fl($id)
	{
		$query = $this->db->where('position_id','3')->where('id',$id)->get('vti_employee');
			foreach ($query->result() as $row)
			{
				$id = $row->id;
				$nama = $row->nama;
						$uname = $row->uname;
						$upass = $row->upass;
						$tgl = $row->date_reg;
			?>
				<form id="form-validation" action="<?php echo base_url().$this->uri->segment(1).'/stakeholder/proses_edit_fl/'.$id;?>" method="post" class="form-horizontal" style="margin-left:10%;">
						<div class="form-group">
				      <label class="col-md-2 control-label" for="val_nama">Nama</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="text" id="val_nama" name="val_nama" class="form-control text-center" value="<?php echo $nama;?>">
				          <span class="input-group-addon"><i class="icon-pencil icon-fixed-width"></i></span>
				        </div>
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="col-md-2 control-label" for="val_uname">Username</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="text" id="val_uname" name="val_uname" class="form-control text-center" value="<?php echo $uname;?>">
				          <span class="input-group-addon"><i class="icon-globe icon-fixed-width"></i></span>
				        </div>
				      </div>
				    </div>
				    <!-- <div class="form-group">
				      <label class="col-md-2 control-label" for="val_pass_lama">Password Lama</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="password" id="val_pass_lama" name="val_pass_lama" class="form-control text-center" placeholder="Kosongi bila tidak perlu">
				          <input type="hidden" id="val_pass" name="val_pass_lama" class="form-control text-center" value="<?php echo $upass;?>">
				          <span class="input-group-addon"><i class="icon-phone icon-fixed-width"></i></span>
				        </div>
				      </div>
				    </div> -->
				    <div class="form-group">
				      <label class="col-md-2 control-label" for="val_pass_baru">Password Baru</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="password" id="val_pass_baru" name="val_pass_baru" class="form-control text-center" placeholder="Kosongi bila tidak perlu">
				          <span class="input-group-addon"><i class="icon-phone icon-fixed-width"></i></span>
				        </div>
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="col-md-2 control-label" for="val_pass_baru2">Konfirmasi Password Baru</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="password" id="val_pass_baru2" name="val_pass_baru2" class="form-control text-center" placeholder="Kosongi bila tidak perlu">
				          <span class="input-group-addon"><i class="icon-phone icon-fixed-width"></i></span>
				        </div>
				      </div>
				    </div>
						<div class="form-group">
							<label class="col-md-2 control-label">Yakin ?</label>
							<div class="col-md-8">
								<div class="checkbox">
									<label for="val_terms">
										<input type="checkbox" id="val_terms" name="val_terms" value="1"> Saya yakin
									</label>
								</div>
							</div>
						</div>
						<br />
						<div class="form-group">
							<div class="col-md-6" style="margin-left:42%;">
								<!-- <button type="reset" class="btn btn-default"><i class="icon-remove"></i> Reset</button> -->
								<button type="submit" class="btn btn-success"><i class="icon-asterisk icon-spin-fast"></i> Simpan</button>
				      </div>
						</div>
						</form>
			<?php
		}
	}
	public function list_ver()
	{
		$query = $this->db->where('position_id','4')->get('vti_employee');
		$c = 1;
			foreach ($query->result() as $row)
			{
				$id = $row->id;
				$nama = $row->nama;
				$uname = $row->uname;
				$upass = $row->upass;
				$tgl = $row->date_reg;
				?>
					<tr>
						<td class="text-center"><?php echo $c;?></td>
						<td class="text-center"><center><?php echo $nama;?></center></a></td>
						<td class="text-center"><?php echo $uname;?></td>
						<td class="text-center">Aktif</td>
						<td class="text-center"><?php echo $tgl;?></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="stakeholder/edit_ver/<?php echo $id;?>" title="Edit" class="btn btn-xs btn-default" style="margin-left:-1%;">
									<i class="icon-pencil"></i> EDIT</a>
								<a href="#hapusFl<?php echo $id;?>" data-toggle="modal" title="Delete" class="btn btn-xs btn-default">
									<i class="icon-remove"></i> HAPUS</a>
								<div id="hapusFl<?php echo $id;?>" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title">Hapus Tim Verifikasi</h4>
											</div>
											<div class="modal-body">
												<div class="block">
													<form id="form-validation" action="stakeholder/hapus_ver" method="post" class="form-horizontal">
														<h3>Anda Yakin ?</h3>
														<input type="hidden" value="<?php echo $id;?>" name="id_hapus">
														<br />
														<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
														<button type="submit" class="btn btn-danger">Hapus</button>
														<br /><br />
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>
				<?php
				$c ++;
			}
	}
	public function edit_ver($id)
	{
		$query = $this->db->where('position_id','4')->where('id',$id)->get('vti_employee');
			foreach ($query->result() as $row)
			{
				$id = $row->id;
				$nama = $row->nama;
						$uname = $row->uname;
						$upass = $row->upass;
						$tgl = $row->date_reg;
			?>
				<form id="form-validation" action="<?php echo base_url().$this->uri->segment(1).'/stakeholder/proses_edit_ver/'.$id;?>" method="post" class="form-horizontal" style="margin-left:10%;">
						<div class="form-group">
				      <label class="col-md-2 control-label" for="val_nama">Nama</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="text" id="val_nama" name="val_nama" class="form-control text-center" value="<?php echo $nama;?>">
				          <span class="input-group-addon"><i class="icon-pencil icon-fixed-width"></i></span>
				        </div>
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="col-md-2 control-label" for="val_uname">Username</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="text" id="val_uname" name="val_uname" class="form-control text-center" value="<?php echo $uname;?>">
				          <span class="input-group-addon"><i class="icon-globe icon-fixed-width"></i></span>
				        </div>
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="col-md-2 control-label" for="val_pass_baru">Password Baru</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="password" id="val_pass_baru" name="val_pass_baru" class="form-control text-center" placeholder="Kosongi bila tidak perlu">
				          <span class="input-group-addon"><i class="icon-phone icon-fixed-width"></i></span>
				        </div>
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="col-md-2 control-label" for="val_pass_baru2">Konfirmasi Password Baru</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="password" id="val_pass_baru2" name="val_pass_baru2" class="form-control text-center" placeholder="Kosongi bila tidak perlu">
				          <span class="input-group-addon"><i class="icon-phone icon-fixed-width"></i></span>
				        </div>
				      </div>
				    </div>
						<div class="form-group">
							<label class="col-md-2 control-label">Yakin ?</label>
							<div class="col-md-8">
								<div class="checkbox">
									<label for="val_terms">
										<input type="checkbox" id="val_terms" name="val_terms" value="1"> Saya yakin
									</label>
								</div>
							</div>
						</div>
						<br />
						<div class="form-group">
							<div class="col-md-6" style="margin-left:42%;">
								<button type="submit" class="btn btn-success"><i class="icon-asterisk icon-spin-fast"></i> Simpan</button>
				      		</div>
						</div>
				</form>
			<?php
		}
	}
}
