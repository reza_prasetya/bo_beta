<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paket_model extends MY_Model {
	public function list_paket()
	{
		$query = $this->db->get('vti_corp_package');
		foreach ($query->result() as $row)
			{
				$id = $row->package_id;
				$nama = $row->name;
				$hrg = $row->price;
				$p = $row->percentage;
				$c = $row->cashback_time;
				$a = $row->additional_time;
				$s = $row->status;
			?>
				<tr>
					<td class="text-center"><center><?php echo $nama;?></center></a></td>
					<td class="text-center"><?php echo $hrg;?></td>
					<td class="text-center"><?php echo $c;?> Hari</td>
					<td class="text-center"><?php echo $a;?> Hari</td>
					<td class="text-center"><?php echo $s;?></td>
					<td class="text-center">
						<div class="btn-group">
							<a href="kebijakan/edit_paket/<?php echo $id;?>" title="Edit" class="btn btn-xs btn-default" style="margin-left:-1%;">
								<i class="icon-pencil"></i> EDIT</a>
							<a href="#hapusFl<?php echo $id;?>" data-toggle="modal" title="Delete" class="btn btn-xs btn-default">
								<i class="icon-remove"></i> HAPUS</a>
							<div id="hapusFl<?php echo $id;?>" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title">Hapus Paket</h4>
										</div>
										<div class="modal-body">
											<div class="block">
												<form id="form-validation" action="kebijakan/hapus_paket" method="post" class="form-horizontal">
													<h3>Anda Yakin ?</h3>
													<input type="hidden" value="<?php echo $id;?>" name="id_hapus">
													<br />
													<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
													<button type="submit" class="btn btn-danger">Hapus</button>
													<br /><br />
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			<?php
		}
	}
	public function edit_paket($id)
	{
		$query = $this->db->where('package_id',$id)->get('vti_corp_package');
			foreach ($query->result() as $row)
			{
				$id = $row->package_id;
				$nama = $row->name;
				$hrg = $row->price;
				$p = $row->percentage;
				$c = $row->cashback_time;
				$a = $row->additional_time;
				$s = $row->status;
			?>
				<form id="form-validation" action="<?php echo base_url().$this->uri->segment(1).'/kebijakan/proses_edit_paket/'.$id;?>" method="post" class="form-horizontal" style="margin-left:10%;">
						<div class="form-group">
				      <label class="col-md-2 control-label" for="val_nama">Nama</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="text" id="val_nama" name="val_nama" class="form-control text-center" value="<?php echo $nama;?>">
				          <span class="input-group-addon"><i class="icon-pencil icon-fixed-width"></i></span>
				        </div>
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="col-md-2 control-label" for="val_harga">Harga (dalam juta)</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="text" id="val_harga" name="val_harga" class="form-control text-center" value="<?php echo $hrg;?>">
				          <span class="input-group-addon">Rupiah</span>
				        </div>
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="col-md-2 control-label" for="val_day">Batas Pengembalian</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="text" id="val_day" name="val_day" class="form-control text-center" value="<?php echo $c;?>">
				          <span class="input-group-addon">Hari</span>
				        </div>
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="col-md-2 control-label" for="val_tol">Toleransi</label>
				      <div class="col-md-8">
				        <div class="input-group">
				          <input type="text" id="val_tol" name="val_tol" class="form-control text-center" value="<?php echo $a;?>">
				          <span class="input-group-addon">Hari</span>
				        </div>
				      </div>
				    </div>
				    <div class="form-group">
					<label class="col-md-2 control-label">Status</label>
						<div class="col-md-10">
							<div class="radio">
								<label for="example-radio1">
									<input type="radio" id="example-radio1" name="val_status" value="active" <?php if($s=='active'){echo 'checked="checked"';}else{}?>> Aktif
								</label>
							</div>
							<div class="radio">
								<label for="example-radio2">
									<input type="radio" id="example-radio2" name="val_status" value="nonactive" <?php if($s=='nonactive'){echo 'checked="checked"';}else{}?>> Non Aktif
								</label>
							</div>
						</div>
					</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Yakin ?</label>
							<div class="col-md-8">
								<div class="checkbox">
									<label for="val_terms">
										<input type="checkbox" id="val_terms" name="val_terms" value="1"> Saya yakin
									</label>
								</div>
							</div>
						</div>
						<br />
						<div class="form-group">
							<div class="col-md-6" style="margin-left:42%;">
								<button type="submit" class="btn btn-success"><i class="icon-asterisk icon-spin-fast"></i> Simpan</button>
				      </div>
						</div>
						</form>
			<?php
		}
	}
}