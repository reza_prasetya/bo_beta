<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup extends BASMALAH_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->module = 'master';
		$this->cname = 'master/setup';
		$this->load->model('mdl_setup_area', 'mas');
		$this->load->model('mdl_setup_produk', 'mak');
	}

	public function index()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "Menu Setup | Master Rak & Gudang";
		$data['content'] = $this->load->view('/menu_setup',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function area()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "Menu Setup | Master Rak & Gudang";
		$data['content'] = $this->load->view('/setup_area',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function get_opt_area($value='')
	{
		$query = $this->mas->_opt_area();
		$opt = '<option value="">Pilih Area</option>';
		foreach ($query->result() as $key => $value) {
			$opt .= '<option value="'.$value->code.'">'.$value->area_name.'</option>';
		}
		echo $opt;
	}

	public function get_opt_branch($value='')
	{
		$query = $this->mas->_opt_branch();
		$opt = '<option value="">Pilih Cabang</option>';
		foreach ($query->result() as $key => $value) {
			$opt .= '<option value="'.$value->code.'">'.$value->name.'</option>';
		}
		echo $opt;
	}

	public function setup_area()
	{
		$param = $this->input->post();
		$update = $this->mas->setup_area($param);
		if($update==1){
			echo '1|'.succ_msg('Setup Area berhasil dilakukan.');
		} else {
			echo '0|'.err_msg('Setup Area gagal dilakukan.');
		}
	}

	public function delete_area()
	{
		$param = $this->input->post();
		$data = array('area_code'=>NULL);
		$delete = $this->mas->replace('atombizz_branch',$data,$param);
		if($delete==1){
			echo "1|".succ_msg("Setup Area berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, menghapus setup area. -delete");
		}
	}

	public function produk($value='')
	{
		$data['cname'] = $this->cname;
		$data['title'] = "Menu Setup | Master Rak & Gudang";
		$data['content'] = $this->load->view('/setup_product',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function get_opt_product($value='')
	{
		$query = $this->mak->_opt_product();
		$opt = '<option value="">Pilih Produk</option>';
		foreach ($query->result() as $key => $value) {
			$opt .= '<option value="'.$value->code.'">'.$value->code.' '.$value->name.'</option>';
		}
		echo $opt;
	}

	public function get_opt_supplier($value='')
	{
		$query = $this->mak->_opt_supplier();
		$opt = '<option value="">Pilih Suplier</option>';
		foreach ($query->result() as $key => $value) {
			$opt .= '<option value="'.$value->code.'">'.$value->name.'</option>';
		}
		echo $opt;
	}

	public function setup_product()
	{
		$param = $this->input->post();
		$update = $this->mak->setup_product($param);
		if($update==1){
			echo '1|'.succ_msg('Setup Produk berhasil dilakukan.');
		} else {
			echo '0|'.err_msg('Setup Produk gagal dilakukan.');
		}
	}

	public function delete_supplier()
	{
		$param = $this->input->post();
		$data = array('supplier_code'=>NULL);
		$delete = $this->mas->replace('atombizz_product',$data,$param);
		if($delete==1){
			echo "1|".succ_msg("Setup Suplier berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, menghapus setup suplier. -delete");
		}
	}
}

/* End of file setup.php */
/* Location: ./application/modules/master/controllers/setup.php */