<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kabupaten extends BASMALAH_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->module = 'master';
		$this->cname = 'master/kabupaten';
		$this->load->model('mdl_kabupaten', 'mp');
	} 

	public function index()
	{
		// echo "hello pspell_save_wordlist(dictionary_link)";
		redirect($this->cname.'/data');
	}

	public function data()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "List Data kabupaten | Master kabupaten";
		$data['content'] = $this->load->view('/list_kabupaten',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function detail(){
		$uri = $this->uri->segment(4);
		$data['val'] = $this->mp->detail($uri);
		$data['cname'] = $this->cname;
		$data['title'] = "Detail kabupaten | Master kabupaten";
		$data['content'] = $this->load->view('/detail_kabupaten',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function tambah(){
		// $data['kabupaten'] = $this->mp->get_opt_kabupaten();
		// print_r($data['kabupaten']);exit;
		$data['cname'] = $this->cname;
		$data['title'] = "Tambah Data kabupaten | Master kabupaten";
		$data['content'] = $this->load->view('/tambah_kabupaten',$data,TRUE);
		$this->load->view('/template', $data);
	}

	// //fungsi logic
	public function tambah_kabupaten()
	{
		// $access = strtolower($this->module.'.'.__class__.'.'.__function__);
		// $this->permission->check_permission($access);

        $param = $this->input->post();

        $this->load->library('form_validation');
		$this->form_validation->set_rules('code', 'Kode kabupaten', 'trim|required|xss_clean');
		$this->form_validation->set_rules('regency_name', 'Nama kabupaten', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            echo "0|".warn_msg(validation_errors());
        } else {
            //save data
            $id = $this->input->post('id');
            unset($param['id']);

            //strtolower
            // $param['name'] = strtolower($param['name']);
            // $param['address'] = strtolower($param['address']);

	        if(is_numeric($id)){
	        	$where = array('id'=>$id);
	        	$save = $this->mp->replace('atombizz_regency',$param,$where);
	        } else {
	        	$where = array('code'=>$param['code']);
	        	$exist = $this->mp->total('atombizz_regency',$where,$like=null,$field=null);
	        	if($exist<=0){
	        		$save = $this->mp->write('atombizz_regency', $param);
	        	} else {
	        		$save = FALSE;
	        	}
	        }
            if ($save == TRUE) {
            	echo "1|".succ_msg("Data kabupaten berhasil disimpan");
            } else {
            	echo "0|".err_msg("Gagal, coba ulangi sekali lagi.");
            }
        }
	}

	public function detail_kabupaten(){
		$uri = $this->input->post('id');
		$data = $this->mp->detail($uri);
		echo json_encode($data);
		// $data['code']."|".$data['name']."|".$data['company']."|".$data['address']."|".$data['city']."|".$data['state']."|".$data['phone']."|".$data['email']."|".date('Y-m-d',strtotime($data['expired']));
	}

	public function delete_kabupaten()
	{
		// $access = strtolower($this->module.'.'.__class__.'.'.__function__);
		// $this->permission->check_permission($access);
		
		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_regency',$param);
		if($delete>0){
			echo "1|".succ_msg("Data kabupaten berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data kabupaten belum dihapus. -delete");
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
