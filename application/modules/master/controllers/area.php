<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area extends BASMALAH_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->module = 'master';
		$this->cname = 'master/area';
		$this->load->model('mdl_area', 'mp');
	} 

	public function index()
	{
		// echo "hello pspell_save_wordlist(dictionary_link)";
		redirect($this->cname.'/data');
	}

	public function data()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "List Data area | Master area";
		$data['content'] = $this->load->view('/list_area',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function detail(){
		$uri = $this->uri->segment(4);
		$data['val'] = $this->mp->detail($uri);
		$data['cname'] = $this->cname;
		$data['title'] = "Detail area | Master area";
		$data['content'] = $this->load->view('/detail_area',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function tambah(){
		// $data['area'] = $this->mp->get_opt_area();
		// print_r($data['area']);exit;
		$data['cname'] = $this->cname;
		$data['title'] = "Tambah Data area | Master area";
		$data['content'] = $this->load->view('/tambah_area',$data,TRUE);
		$this->load->view('/template', $data);
	}

	// //fungsi logic
	public function tambah_area()
	{
		// $access = strtolower($this->module.'.'.__class__.'.'.__function__);
		// $this->permission->check_permission($access);

        $param = $this->input->post();

        $this->load->library('form_validation');
		$this->form_validation->set_rules('code', 'Kode area', 'trim|required|xss_clean');
		$this->form_validation->set_rules('area_name', 'Nama area', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            echo "0|".warn_msg(validation_errors());
        } else {
            //save data
            $id = $this->input->post('id');
            unset($param['id']);

            //strtolower
            // $param['name'] = strtolower($param['name']);
            // $param['address'] = strtolower($param['address']);

	        if(is_numeric($id)){
	        	$where = array('id'=>$id);
	        	$save = $this->mp->replace('atombizz_area',$param,$where);
	        } else {
	        	$where = array('code'=>$param['code']);
	        	$exist = $this->mp->total('atombizz_area',$where,$like=null,$field=null);
	        	if($exist<=0){
	        		$save = $this->mp->write('atombizz_area', $param);
	        	} else {
	        		$save = FALSE;
	        	}
	        }
            if ($save == TRUE) {
            	echo "1|".succ_msg("Data area berhasil disimpan");
            } else {
            	echo "0|".err_msg("Gagal, coba ulangi sekali lagi.");
            }
        }
	}

	public function detail_area(){
		$uri = $this->input->post('id');
		$data = $this->mp->detail($uri);
		echo json_encode($data);
		// $data['code']."|".$data['name']."|".$data['company']."|".$data['address']."|".$data['city']."|".$data['state']."|".$data['phone']."|".$data['email']."|".date('Y-m-d',strtotime($data['expired']));
	}

	public function delete_area()
	{
		// $access = strtolower($this->module.'.'.__class__.'.'.__function__);
		// $this->permission->check_permission($access);
		
		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_area',$param);
		if($delete>0){
			echo "1|".succ_msg("Data area berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data area belum dihapus. -delete");
		}
	}
	public function gen_code(){
		$no = $this->mp->gen_kode();
		if($no<10)$no = '00'.$no;
        else if($no<100)$no = '0'.$no;
        else if($no<1000)$no = $no;
        $code = 'AR-'.$no;
        echo $code.'|'.$no;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
