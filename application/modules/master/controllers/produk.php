<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends BASMALAH_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->cname = 'master/produk';
		$this->module= "master";
		$this->load->model('mdl_produk', 'mp');
	} 

	public function index()
	{
		// echo "hello pspell_save_wordlist(dictionary_link)";
		redirect($this->cname.'/menu');
	}

	public function menu()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "Menu | Master Produk";
		$data['content'] = $this->load->view('/menu_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	//list_data
	public function data()
	{
		// $access = strtolower($this->module.'_'.__class__.'_'.__function__);
		// $this->permission->check_permission($access);

		$data['cname'] = $this->cname;
		$data['title'] = "Data Produk | Master Produk";
		$data['content'] = $this->load->view('/list_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	//cari produk
	public function cari()
	{
		// $access = strtolower($this->module.'_'.__class__.'_'.__function__);
		// $this->permission->check_permission($access);

		$data['cname'] = $this->cname;
		$data['title'] = "Data Produk | Master Produk";
		$data['content'] = $this->load->view('/cari_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function mencari(){
		$param = $this->input->post();

		$this->load->library('form_validation');
		// $this->form_validation->set_rules('category', 'Filter', 'trim|required|xss_clean');
		$this->form_validation->set_rules('keyword', 'Kata Kunci', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
            //tidak memenuhi validasi
            echo "0|".warn_msg(validation_errors());
        } else {
            //save data
            // $save = $this->mp->find('atombizz_customers',NULL,NULL,NULL,NULL,FALSE, $like = FALSE);
            $save = $this->mp->mencari($param['keyword']);
            if($save->num_rows() > 0){
            	$list = '';
	            foreach ($save->result() as $data) {
	            	$list .= '
	            		<a href="'.base_url().'master/produk/detail/'.$data->id.'" class="list-group-item" >
							<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
							<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">'.$data->name.'</h4>
							<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">'.$data->category.' - '.$data->sub_category.'</h5>
						</a>
	            	';
	            }

	            echo '1|'.$list;
            } else {
            	$list = '
	            	<a href="javascript:void(0)" class="list-group-item" >
						<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
						<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">Tidak Ada Hasil Pencarian</h4>
						<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">Silahkan Anda masukkankan kata kunci pada kolom search.</h5>
					</a>
	            ';
	            echo '1|'.$list;
            }
            
        }
	}
	//rak produk
	public function rak()
	{
		$where = array('status'=>'toko');
		$rak = $this->mp->find('atombizz_rack',$where);
		$where = array('status'=>'gudang');
		$gudang = $this->mp->find('atombizz_rack',$where);
		// print_r($data['toko']);exit;
		$data['toko'] = @$rak->result();
		$data['gudang'] = @$gudang->result();
		$data['cname'] = $this->cname;
		$data['title'] = "Data Rak Produk | Master Produk";
		$data['content'] = $this->load->view('/list_rak_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function set_rak_produk()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$param = $this->input->post();

		$this->load->library('form_validation');
		$this->form_validation->set_rules('code', 'Produk', 'required|xss_clean');
		$this->form_validation->set_rules('rak_code', 'Rak Toko', 'trim|xss_clean');
		$this->form_validation->set_rules('gudang_code', 'Rak Gudang', 'trim|xss_clean');

		if($this->form_validation->run() == FALSE){
			echo "0|".warn_msg(validation_errors());
		} else {
			// $where = array('produk'=>$param['produk'],'status'=>'rak');
			// $exist = $this->mp->find('atombizz_warehouses_stok', $where, $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
			// // foreach ($exist->result_array() as $data) {
			// // 	echo $data['saldo'];exit;
			// // }
			$produk = $param['code'];
			unset($param['code']);
			$update = $this->mp->update_where_in('atombizz_product_specification',$param,$produk);
			if($update == TRUE){
				echo "1|".succ_msg("Konfig Rak Produk berhasil ditambahkan");
			} else {
				echo "0|".err_msg("Gagal, coba periksa lagi inputan anda.");
			}
			// echo $produk;
		}
	}

	//diskon produk
	public function diskon()
	{

		$data['cname'] = $this->cname;
		$data['title'] = "Data Diskon Produk | Master Produk";
		$data['content'] = $this->load->view('/list_diskon_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function diskon_multi()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$param = $this->input->post();

		$this->load->library('form_validation');
		$this->form_validation->set_rules('produk', 'Produk', 'required|xss_clean');
		$this->form_validation->set_rules('discount', 'Diskon', 'trim|required|xss_clean');

		if($this->form_validation->run() == FALSE){
			echo "0|".warn_msg(validation_errors());
		} else {

			$produk = $this->input->post('produk');
			// print_r($produk);exit;
			// $where = array_map("intval",explode(",", $produk));
			// var_dump($where);
			unset($param['produk']);
			$update = $this->mp->update_where_in('atombizz_product_specification',$param,$produk);
			// echo $update;exit;
			if($update == TRUE){
				echo "1|".succ_msg("Update Diskon berhasil ditambahkan");
			} else {
				echo "0|".err_msg("Gagal, coba periksa lagi inputan anda.");
			}
			// echo $produk;
		}
	}

	public function mencari_diskon(){
		$param = $this->input->post();

		$this->load->library('form_validation');
		$this->form_validation->set_rules('category', 'Filter', 'trim|required|xss_clean');
		$this->form_validation->set_rules('keyword', 'Kata Kunci', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
            //tidak memenuhi validasi
            echo "0|".warn_msg(validation_errors());
        } else {
            //save data
            // $save = $this->mp->find('atombizz_customers',NULL,NULL,NULL,NULL,FALSE, $like = FALSE);
            $save = $this->mp->mencari($param['category'],$param['keyword']);
            if($save->num_rows() > 0){
            	$list = '';
	            foreach ($save->result() as $data) {
	            	$list .= '
	            		<a href="'.base_url().'master/produk/diskon/single/'.$data->id.'" class="list-group-item" >
							<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
							<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">'.$data->name.'</h4>
							<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">'.$data->category.' - '.$data->sub_category.'</h5>
						</a>
	            	';
	            }

	            echo '1|'.$list;
            } else {
            	$list = '
	            	<a href="javascript:void(0)" class="list-group-item" >
						<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
						<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">Tidak Ada Hasil Pencarian</h4>
						<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">Silahkan Anda masukkankan kata kunci pada kolom search.</h5>
					</a>
	            ';
	            echo '1|'.$list;
            }
            
        }
	}

	//kategori
	public function kategori()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$data['cat_dept'] = $this->mp->opt_department();
		$data['cname'] = $this->cname;
		$data['title'] = "Kategori Produk | Master Produk";
		$data['content'] = $this->load->view('/kategori_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function department()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$data['cname'] = $this->cname;
		$data['title'] = "Department Produk | Master Produk";
		$data['content'] = $this->load->view('/department',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function tambah_kategori()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$param = $this->input->post();
		
		$this->load->library('form_validation');
        $this->form_validation->set_rules('code', 'Kode Kategori', 'trim|required|xss_clean');
		$this->form_validation->set_rules('name', 'Nama Kategori', 'trim|required|xss_clean');
		$this->form_validation->set_rules('id_dept', 'Departemen', 'trim|required|xss_clean');

		if($this->form_validation->run() == FALSE){
			echo "0|".warn_msg(validation_errors());
		} else {
			$id = $param['id'];
			unset($param['id']);
			if($id == NULL){
				$where = array('code'=>$param['code'],'id_dept'=>$param['id_dept']);
				$exist = $this->mp->total('atombizz_product_categories',$where,$like=null,$field=null);
				if($exist<=0){
					$save = $this->mp->write('atombizz_product_categories',$param);
					if($save == TRUE){
						echo "1|".succ_msg("Kategori Produk berhasil ditambahkan");
					} else {
						echo "0|".err_msg("Gagal, coba periksa lagi inputan anda.");
					}
				} else {
					echo "0|".err_msg("Data sudah ada.");
				}	
			} else {
				$where = array('id' => $id);
				$update = $this->mp->replace('atombizz_product_categories',$param,$where);
				if($update == TRUE){
					echo "1|".succ_msg("Kategori Produk berhasil dirubah. -update");
				} else {
					echo "0|".err_msg("Gagal, coba periksa lagi inputan anda. -update");
				}
			}
		}
	}

	public function tambah_department()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$param = $this->input->post();
		
		$this->load->library('form_validation');
        $this->form_validation->set_rules('code', 'Kode Kategori', 'trim|required|xss_clean');
		$this->form_validation->set_rules('name', 'Nama Kategori', 'trim|required|xss_clean');

		if($this->form_validation->run() == FALSE){
			echo "0|".warn_msg(validation_errors());
		} else {
			$id = $param['id'];
			unset($param['id']);
			if($id == NULL){
				$where = array('code'=>$param['code']);
				$exist = $this->mp->total('atombizz_product_department',$where,$like=null,$field=null);
				if($exist<=0){
					$save = $this->mp->write('atombizz_product_department',$param);
					if($save == TRUE){
						echo "1|".succ_msg("Department Produk berhasil ditambahkan");
					} else {
						echo "0|".err_msg("Gagal, coba periksa lagi inputan anda.");
					}
				} else {
					echo "0|".err_msg("Data sudah ada.");
				}	
			} else {
				$where = array('id' => $id);
				$update = $this->mp->replace('atombizz_product_department',$param,$where);
				if($update == TRUE){
					echo "1|".succ_msg("Department Produk berhasil dirubah. -update");
				} else {
					echo "0|".err_msg("Gagal, coba periksa lagi inputan anda. -update");
				}
			}
		}
	}

	public function detail_kategori()
	{
		$param = $this->input->post();
		$data = $this->mp->find('atombizz_product_categories', $param , $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		foreach ($data->result_array() as $das) {
			$id = $das['id'];
			$code = $das['code'];
			$name = $das['name'];
			$id_dept = $das['id_dept'];
		}
		echo $id.'|'.$code.'|'.$name.'|'.$id_dept;
	}

	public function detail_department()
	{
		$param = $this->input->post();
		$data = $this->mp->find('atombizz_product_department', $param , $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		foreach ($data->result_array() as $das) {
			$das = $das;
		}
		echo json_encode($das);
	}

	public function delete_kategori()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_product_categories',$param);
		if($delete>0){
			echo "1|".succ_msg("Data kategori berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data kategori belum dihapus. -delete");
		}
	}

	public function delete_sub_kategori()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_product_subcategories',$param);
		if($delete>0){
			echo "1|".succ_msg("Data sub kategori berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data sub kategori belum dihapus. -delete");
		}
	}

	public function delete_department()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_product_department',$param);
		if($delete>0){
			echo "1|".succ_msg("Data department berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data department belum dihapus. -delete");
		}
	}

	public function delete_detail_blended()
	{
		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_blended_product',$param);
		if($delete>0){
			echo "1|".succ_msg("Data blended berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data blended belum dihapus. -delete");
		}
	}

	//sub kategori
	public function sub_kategori()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$data['cat_dept'] = $this->mp->opt_department();
		$data['cname'] = $this->cname;
		$data['title'] = "Sub Kategori Produk | Master Produk";
		$data['content'] = $this->load->view('/sub_kategori_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function tambah_sub_kategori()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);
		
		$param = $this->input->post();
		
		$this->load->library('form_validation');
        $this->form_validation->set_rules('code', 'Kode Sub Kategori', 'trim|required|xss_clean');
		$this->form_validation->set_rules('name', 'Nama Sub Kategori', 'trim|required|xss_clean');
		$this->form_validation->set_rules('category_id', 'Kategori', 'trim|required|xss_clean');

		if($this->form_validation->run() == FALSE){
			echo "0|".warn_msg(validation_errors());
		} else {
			$id = $param['id'];
			unset($param['id']);
			if($id == NULL){
				$where = array('category_id'=>$param['category_id'], 'code'=>$param['code']);
				$exist = $this->mp->total('atombizz_product_subcategories',$where,$like=null,$field=null);
				if($exist<=0){
					$save = $this->mp->write('atombizz_product_subcategories',$param);
					if($save == TRUE){
						echo "1|".succ_msg("Kategori Sub Produk berhasil ditambahkan");
					} else {
						echo "0|".err_msg("Gagal, coba periksa lagi inputan anda.");
					}
				} else {
					echo "0|".err_msg("Data sudah ada.");
				}
			} else {
				$where = array('id' => $id);
				$update = $this->mp->replace('atombizz_product_subcategories',$param,$where);
				if($update == TRUE){
					echo "1|".succ_msg("Kategori Sub Produk berhasil dirubah. -update");
				} else {
					echo "0|".err_msg("Gagal, coba periksa lagi inputan anda. -update");
				}
			}
		}
	}

	public function detail_sub_kategori()
	{
		$param = $this->input->post();
		$data = $this->mp->find('view_sub_category', $param , $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		foreach ($data->result_array() as $das) {
			$id = $das['id'];
			$code = $das['code'];
			$name = $das['name'];
			$category_id = $das['category_id'];
			$id_dept = $das['id_dept'];
		}
		echo $id.'|'.$category_id.'|'.$code.'|'.$name.'|'.$id_dept;
	}

	public function opt_kategori()
	{
		$param = $this->input->post();
		$data = $this->mp->find('atombizz_product_categories', $param , $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		$opt = '<option>Pilih Kategori</option>';
		foreach ($data->result_array() as $das) {
			$opt .= '
				<option value="'.$das['id'].'" data-id="'.$das['code'].'">'.$das['name'].'</option>
			';
		}
		echo $opt;
	}

	public function opt_sub_kategori()
	{
		$param = $this->input->post();
		$data = $this->mp->find('view_sub_category', $param , $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		$opt = '<option>Pilih Sub Kategori</option>';
		foreach ($data->result_array() as $das) {
			$opt .= '
				<option value="'.$das['id'].'" data-id="'.$das['code'].'">'.$das['name'].'</option>
			';
		}
		echo $opt;
	}

	public function harga_produk()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "Set Harga Produk | Master Produk";
		$data['content'] = $this->load->view('/set_harga_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function set_harga_produk()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$param = $this->input->post();

		$this->load->library('form_validation');
		$this->form_validation->set_rules('code', 'Kode Produk', 'trim|required|xss_clean');
		$this->form_validation->set_rules('price1', 'Harga Jual 1', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('price2', 'Harga Jual 2', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('price3', 'Harga Jual 3', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('qty1', 'Jumlah Maksimal Harga 1', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('qty2', 'Jumlah Maksimal Harga 2', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('qty3', 'Jumlah Maksimal Harga 3', 'trim|required|numeric|xss_clean');

		if($this->form_validation->run() == FALSE){
			echo "0|".warn_msg(validation_errors());
		} else {
			$code = $param['code'];
			unset($param['code']);
			unset($param['name']);
			// unset($param['search']);
			unset($param['harga_pokok']);

			$qty = array(
	            $param['qty1'] , 
	            $param['qty2'] ,
	            $param['qty3'] ,
	        );
	        $qty =  json_encode($qty); 
	        $param['qty'] = $qty;

	        unset($param['qty1']);
	        unset($param['qty2']);
	        unset($param['qty3']);

	        $where = array('code'=>$code);
	        $update = $this->mp->replace('atombizz_product',$param,$where);
	        if ($update == TRUE) {
	        	// print_r($where);exit;
	        	echo "1|".succ_msg("Produk berhasil ditambahkan.");
	        } else {
	        	echo "0|".err_msg("Gagal, coba periksa lagi inputan anda.");
	        }
		}
	}

	//produk
	public function tambah()
	{
		// $id = $this->uri->segment(4);
		// if(is_numeric($id)){
  //       	$data['val'] = $this->mp->detail($id);
  //       }
		$data['rak'] = $this->mp->get_rak();
        $data['cat_dept'] = $this->mp->opt_department();
        $data['cname'] = $this->cname;
		$data['title'] = "Tambah Data Produk | Master Produk";
		$data['content'] = $this->load->view('/tambah_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function detail()
	{
		$uri = $this->uri->segment(4);
		$data['val'] = $this->mp->detail($uri);
		$data['cname'] = $this->cname;
		$data['title'] = "Detail Produk | Master Produk";
		$data['content'] = $this->load->view('/detail_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function tambah_produk()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$param = $this->input->post();
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('department', 'Departemen', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('category', 'Kategori', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('subcategory', 'Sub Kategori', 'trim|xss_clean'); 
		$this->form_validation->set_rules('code', 'Kode Produk', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('name', 'Nama Produk', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('unit', 'Satuan Kemasan', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('discount', 'Diskon Produk', 'trim|xss_clean'); 
		$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean'); 

		if($this->form_validation->run() == FALSE){
			echo "0|".warn_msg(validation_errors());
		} else {
			$id = $param['id'];
			$discount = $param['discount'];
			unset($param['id'],$param['discount']);

	        if(!isset($param['subcategory'])){
	            $param['subcategory']=0;
	        }

			$param['name'] = strtolower($param['name']);
			if($id == NULL){
				$where = array('code'=>$param['code']);
				$exist = $this->mp->total('atombizz_product',$where);
				if($exist<=0){
					$save = $this->mp->write('atombizz_product',$param);
					if($save >= 0){
						$data = array('code'=>$param['code'],'discount'=>$discount);
						$save_spec = $this->mp->write('atombizz_product_specification',$data);
						if($save_spec >= 0){
							echo "1|".succ_msg("Produk berhasil ditambahkan");	
						} else {
							echo "0|".err_msg("Gagal, coba periksa lagi inputan anda.");	
						}
					} else {
						echo "0|".err_msg("Gagal, coba periksa lagi inputan anda.");
					}
				} else {
					echo "0|".err_msg("Gagal, data sudah ada.");
				}
			} else {
				$where = array('id' => $id);
				$update = $this->mp->replace('atombizz_product',$param,$where);
				if($update >= 0){
					$where = array('code'=>$param['code']);
					$data = array('discount'=>$discount);
					$update_spec = $this->mp->replace('atombizz_product_specification',$data,$where);
					if($update_spec >= 0){
						echo "1|".succ_msg("Produk berhasil dirubah. -update");
					} else {
						echo "0|".err_msg("Gagal, coba periksa lagi inputan anda. -update");
					}
				} else {
					echo "0|".err_msg("Gagal, coba periksa lagi inputan anda. -update");
				}
			}
		}
	}
	public function detail_produk()
	{
		//have used in EDIT_PRODUK,
		$uri = $this->input->post('id');
		$data = $this->mp->detail($uri);
		$data['hpp'] = $this->mp->get_hpp($data['code']);
		
		echo json_encode($data);
	}

	public function detail_search_produk()
	{
		$uri = $this->input->post('key');
		$data = $this->mp->detail_search($uri);
		$val = $this->mp->hpp($uri);
		if($val==null){
			$data['hpp'] = 0;
		} else {
			$data['hpp'] = $val['harga'];
		}
		// print_r($data);exit;
		echo json_encode($data);
	}
	
	public function detail_rak_produk()
	{
		$id = $this->input->post('id');
		$where = array('id'=>$id);
		$data = $this->mp->find('view_products', $where, $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		foreach ($data->result_array() as $das) {
			# code...
		}
		// print_r($das);exit;
		echo json_encode($das);
	}

	//blended produk
	public function blended()
	{
		// $access = strtolower($this->module.'_'.__class__.'_'.__function__);
		// $this->permission->check_permission($access);
		
		if($this->uri->segment(4)==NULL){
			redirect($this->cname.'/blended/data');
		}
		$data['rak'] = $this->mp->get_rak();
		$data['cat_dept'] = $this->mp->opt_department();
        $data['cname'] = $this->cname;
		$data['title'] = "Tambah Blended Produk | Master Produk";
		$data['content'] = $this->load->view('/tambah_blended_produk',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function blended_detail()
	{
		$param = $this->input->post();
		$where = array('id'=>$param['id']);
		$data = $this->mp->find('view_blended', $where, $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		// $data = $data->result_array();
		foreach ($data->result_array() as $das) {
			$data = $das;
		}
		// print_r($data);exit;
		echo json_encode($data);
	}

	public function edit_detail_blended()
	{
		$param = $this->input->post();
		// print_r($param);exit;
		$where = array('id'=>$param['id']);
		unset($param['id']);
		$update = $this->mp->replace('atombizz_blended_product',$param,$where);
		if($update == TRUE){
			echo "1|".succ_msg("Quantity berhasil dirubah.");
		} else {
			echo "0|".err_msg("Gagal, coba periksa lagi inputan anda.");
		}
	}

	public function tambah_detail_blended()
	{
		$param = $this->input->post();

		$this->load->library('form_validation');
		$this->form_validation->set_rules('barcode_product', 'Produk', 'trim|required|xss_clean');
		$this->form_validation->set_rules('qty_product', 'Jumlah Produk', 'trim|required|xss_clean');

		if($this->form_validation->run() == FALSE){
			echo "0|".warn_msg(validation_errors());
		} else {
			$where = array('barcode_product'=>$param['barcode_product'], 'barcode_blended'=>$param['barcode_blended']);
			$exist = $this->mp->total('atombizz_blended_product',$where,$like=null,$field=null);
			if($exist<=0){
				$save = $this->mp->write('atombizz_blended_product',$param);
				if($save == TRUE){
					echo "1|".succ_msg("Produk Blended berhasil ditambahkan");
				} else {
					echo "0|".err_msg("Gagal, coba periksa lagi inputan anda.");
				}
			} else {
				echo "0|".err_msg("Data sudah ada.");
			}
		}
	}

	public function cek_blended()
	{
		$param = $this->input->post();

		$where = array('barcode_blended'=>$param['barcode_blended']);
		$exist = $this->mp->total('atombizz_blended_product',$where,$like=null,$field=null);
		if($exist>0){
			$data = $this->mp->find('view_blended', $where, $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
			$tbl = '';
			foreach ($data->result() as $value) {
				$tbl .= '
					<tr>
						<td class="text-center">'.$value->barcode_product.'</td>
						<td>'.$value->name_list.'</td>
						<td>'.$value->qty_product.'</td>
						<td class="text-center">
							<div class="btn-group">
								<a onclick="actDetail('.$value->id.')" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="icon-pencil"></i></a>
								<a onclick="actDelete('.$value->id.')" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-default"><i class="icon-remove"></i></a>
							</div>
						</td>
					</tr>
				';
			}
		} else {
			$tbl = '
				<tr>
					<td colspan="4">Data blended tidak ada.</td>
				</tr>
			';
		}
		echo $tbl;
	}

	public function opt_products()
	{
		$data = $this->mp->find('view_products', $param = NULL , $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		$opt = '';
		foreach ($data->result_array() as $das) {
			$opt .= '
				<option value="'.$das['id'].'">'.$das['code'].' '.$das['name'].'</option>
			';
		}
		echo $opt;
	}

	public function opt_products_code()
	{
		$data = $this->mp->find('view_products', $param = NULL , $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		$opt = '';
		foreach ($data->result_array() as $das) {
			$opt .= '
				<option value="'.$das['code'].'">'.$das['code'].' '.$das['name'].'</option>
			';
		}
		echo $opt;
	}

	public function opt_for_blended()
	{
		$param = $this->input->post();
		$data = $this->mp->opt_blended('view_product_except_blended', $param['barcode']);
		$opt = '<option>Pilih Products</option>';
		foreach ($data->result_array() as $das) {
			$opt .= '
				<option value="'.$das['id'].'">'.$das['code'].' '.$das['name'].'</option>
			';
		}
		echo $opt;
	}

	public function opt_for_rak()
	{
		$data = $this->mp->find('view_products', $param = NULL , $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		$opt = '<option>Pilih Products</option>';
		foreach ($data->result_array() as $das) {
			$opt .= '
				<option value="'.$das['code'].'">'.$das['code'].' '.$das['name'].'</option>
			';
		}
		echo $opt;
	}

	public function un_blended()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);
		
		$date = date('Y-m-d');
		$log = date('Y-m-d H:i:s');
		$ket_unblend = "Unblended produk dengan kode";

		$param = $this->input->post();
		$id = $this->input->post('id');

		$where = array('id'=>$id);
		$data = $this->mp->find('view_blended_products', $where, $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
		foreach ($data->result_array() as $das) {
			$barcode = $das['barcode_blended'];
		}
		$where = array('code'=>$barcode);
		$total = $this->mp->total('view_warehouse_stok',$where,$like=null,$field=null);
		if($total>0){
			$where = array('code'=>$barcode);
			$data = $this->mp->find('view_warehouse_stok', $where, $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
			foreach ($data->result_array() as $das) {
				$saldo_stok = $das['saldo'];
				$arr_stok[] = array('warehouse'=>$param['warehouse'],'produk'=>$das['produk'],'date'=>$date,'status'=>'unblended','masuk'=>0,'keluar'=>$das['saldo'],'keterangan'=>$ket_unblend.' '.$barcode,'operator'=>$param['operator'],'userlog'=>$log);
			}
			$save_stok = $this->mp->insert_batch('atombizz_warehouses_stok',$arr_stok);
			if($save_stok==TRUE){
				$where = array('barcode_blended'=>$barcode);
				$data = $this->mp->find('view_blended_detail', $where, $field = NULL, $limit = NULL, $orderby = NULL, $join = FALSE, $like = FALSE);
				foreach ($data->result_array() as $das) {
					$stok = $das['qty_product']*$saldo_stok;
					$arr_all[] = array('warehouse'=>$param['warehouse'],'produk'=>$das['id_product'],'date'=>$date,'status'=>'unblended','masuk'=>$stok,'keluar'=>0,'keterangan'=>$ket_unblend.' '.$barcode,'operator'=>$param['operator'],'userlog'=>$log);
				}
				$save = $this->mp->insert_batch('atombizz_warehouses_stok',$arr_all);
				if($save==TRUE){
					$where = array('code'=>$barcode);
					$delete_product = $this->mp->delete('atombizz_product',$where);
					if($delete_product>0){
						$where = array('barcode_blended'=>$barcode);
						$delete = $this->mp->delete('atombizz_blended_product',$where);
						if($delete>0){
							$where = array('produk'=>$id);
							$delete = $this->mp->delete('atombizz_warehouses_stok',$where);
							if($delete){
								echo "1|".succ_msg("Berhasil, data di unblended.");
							} else {
								echo "0|".err_msg("Gagal, data gagal di unblended.");
							}
						} else {
							echo "0|".err_msg("Gagal, data gagal di unblended.");
						}
					} else {
						echo "0|".err_msg("Gagal, data gagal di unblended.");
					}
				} else {
					echo "0|".err_msg("Gagal, data gagal di unblended.");
				}
			} else {
				echo "0|".err_msg("Gagal, data gagal di unblended.");
			}
		} else {
			echo "2|".err_msg("Data masih belum di stok.");
		}
	}

	public function delete_produk()
	{
		// $access = strtolower($this->module.'.'.__class__.'.'.__function__);
		// $this->permission->check_permission($access);

		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_product',$param);
		if($delete>0){
			echo "1|".succ_msg("Data produk berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data produk belum dihapus. -delete");
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
