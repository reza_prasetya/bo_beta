<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rekening extends BASMALAH_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->cname = 'master/rekening';
		$this->load->model('mdl_master_akun', 'mma');
	} 

	public function index()
	{
		// echo "string";
		redirect($this->cname.'/data');
	}

	public function data()
	{
		// $data['akun'] = $this->mma->opt_akun();
		$data['cname'] = $this->cname;
		$data['title'] = "Master Akun | Master Data";
		$data['content'] = $this->load->view('/data_master_akun',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function tambah()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "Master Akun | Master Data";
		$data['content'] = $this->load->view('/tambah_master_akun',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function detail()
	{
		$where = array('id'=>$this->uri->segment(4));
		$val = $this->mma->find('view_master_akun',$where);
		$result = $val->result_array();
		$data['val'] = $result[0];
		$data['cname'] = $this->cname;
		$data['title'] = "Master Akun | Master Data";
		$data['content'] = $this->load->view('/detail_master_akun',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function ins_rekening($value='')
	{
		$param = $this->input->post();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('parent', 'Parent Rekening', 'trim|xss_clean'); 
        $this->form_validation->set_rules('position', 'Jenis', 'trim|required|xss_clean'); 
        $this->form_validation->set_rules('code', 'Kode Rekening', 'trim|required|xss_clean'); 
        $this->form_validation->set_rules('name', 'Nama Rekening', 'trim|required|xss_clean'); 
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required|xss_clean'); 

        if ($this->form_validation->run() == FALSE) {
            //tidak memenuhi validasi
            echo '0|'.warn_msg(validation_errors());
        } else {
        	$id = $param['id'];
        	unset($param['id']);
        	if(is_numeric($id)){
        		$where = array('id'=>$id);
        		$save = $this->mma->replace('atombizz_accounting_master_akun',$param,$where);
        	} else {
        		$save = $this->mma->write('atombizz_accounting_master_akun', $param);
        	}
			
            if ($save == TRUE) {
            	echo '1|'.succ_msg('Rekening berhasil disimpan.');
            } else {
                echo '0|'.err_msg('Rekening gagal disimpan.');
            }
        }
	}

	public function get_data()
	{
		$param = $this->input->post();
		$data = $this->mma->detail($param['id']);
		echo json_encode($data);
	}

	public function get_opt_rekening()
	{
		$query = $this->mma->_opt_rekening();
		$opt = '<option value="">Pilih Rekening</option>';
		foreach ($query->result() as $key => $value) {

			$opt .= '<option value="'.$value->code.'">'.space($value->space).$value->name.'</option>';
		}
		echo $opt;
	}

	public function gen_code()
	{
		$param = $this->input->post();
		$query = $this->mma->find('atombizz_accounting_master_akun', $param);
		$parent = $query->result();

		$max = $this->mma->max('atombizz_accounting_master_akun','urut',array('parent'=>$param['code']));
		$total = (!empty($max[0]->urut)? $max[0]->urut+1 : 0+1);
		$code = complete_zero_after($parent[0]->code_ref.$total,6);
		$urut = $total;
		$code_ref = $parent[0]->code_ref.$total;
		$space = (!empty($parent[0]->space)? $parent[0]->space+1 : 0+1);
		$result = array('code'=>$code,'urut'=>$urut,'code_ref'=>$code_ref,'space'=>$space);
		echo json_encode($result);
	}

	public function head_code()
	{
		$where = "parent = '' OR ISNULL(parent)";
		$orderby = "id DESC";
		$limit = "1";
		// $max = $this->mma->max('atombizz_accounting_master_akun','urut',$where);
		$max = $this->mma->find('atombizz_accounting_master_akun', $where, $field = NULL, $limit, $orderby, $join = FALSE, $like = FALSE);
		$res = @$max->result();
		$total = (!empty($res[0]->code_ref)? $res[0]->code_ref+1 : 10+1);
		$code = complete_zero_after($total,6);
		$urut = @$res[0]->urut+1;
		$code_ref = $total;
		$space = 0;
		$result = array('code'=>$code,'urut'=>$urut,'code_ref'=>$code_ref,'space'=>$space);
		echo json_encode($result);
	}

	public function custom_code()
	{
		$param = $this->input->post();
		$code = $param['code'];
		$code_ref  = str_replace('0', '', $code);
		$urut = substr($code_ref, -1, 1);
		$space = 0;
		$result = array('code'=>$code,'urut'=>$urut,'code_ref'=>$code_ref,'space'=>$space);
		echo json_encode($result);
	}

	public function delete_rekening()
	{
		// $access = strtolower($this->module.'.'.__class__.'.'.__function__);
		// $this->permission->check_permission($access);

		$param = $this->input->post();
		$delete = $this->mma->delete('atombizz_accounting_master_akun',$param);
		if($delete>0){
			echo "1|".succ_msg("Data rekekning berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data rekekning belum dihapus. -delete");
		}
	}

	// public function konfig()
	// {
	// 	if($this->input->post() == NULL)
 //        {
 //            if(@$this->session->flashdata('post_item')) {
 //                $data['val'] = @$this->session->flashdata('post_item');
 //            }
	// 		$data['cat_akun'] = $this->mma->opt_akun();
	// 		$data['cname'] = $this->cname;
	// 		$data['title'] = "Master Akun | Accounting";
	// 		$data['content'] = $this->load->view('/konfig_master_akun',$data,TRUE);
	// 		$this->load->view('/template', $data);
 //        } else {
 //            $param = $this->input->post();

 //            $this->load->library('form_validation');
 //            $this->form_validation->set_rules('penjualan', 'Penjualan', 'trim|required|xss_clean');
	// 		$this->form_validation->set_rules('hpp', 'Hpp', 'trim|required|xss_clean');
	// 		$this->form_validation->set_rules('piutang', 'Piutang', 'trim|required|xss_clean');
	// 		$this->form_validation->set_rules('kas', 'Kas', 'trim|required|xss_clean');
	// 		$this->form_validation->set_rules('persediaan', 'Persediaan', 'trim|required|xss_clean');
	// 		$this->form_validation->set_rules('hutang', 'Hutang', 'trim|required|xss_clean');
	// 		$this->form_validation->set_rules('denda', 'Denda', 'trim|required|xss_clean');
	// 		$this->form_validation->set_rules('pajak', 'Pajak', 'trim|required|xss_clean');
	// 		$this->form_validation->set_rules('diskon', 'Diskon', 'trim|required|xss_clean');

 //            if ($this->form_validation->run() == FALSE) {
 //                //tidak memenuhi validasi
 //                $this->session->set_flashdata('flash_message', warn_msg(validation_errors()));
 //                $this->session->set_flashdata('post_item', $param);
 //                redirect($_SERVER['HTTP_REFERER']);
 //            } else {
 //                //save data
 //                $penting = array('penjualan','hpp','piutang','kas','persediaan','hutang','denda','pajak','diskon');
 //                $akun = array($param['penjualan'],$param['hpp'],$param['piutang'],$param['kas'],$param['persediaan'],$param['hutang'],$param['denda'],$param['pajak'],$param['diskon']);
	// 			$save = $this->mma->set_akun_penting('atombizz_accounting_master_akun',$penting,$akun);
 //                if ($save == TRUE) {
 //                	$this->session->set_flashdata('flash_message', succ_msg('Konfig Akun berhasil ditambahkan.'));
 //                } else {
 //                    $this->session->set_flashdata('flash_message', err_msg('Gagal, coba ulangi sekali lagi.[-akun_kas]'));
 //                    // echo "string1";
 //                }
 //                // redirect($_SERVER['HTTP_REFERER']);
 //                redirect($this->cname.'/konfig');
 //            }
 //        }
	// }

	// public function opt_sub_klasifikasi()
	// {
	// 	$param = $this->input->post('klasifikasi');
	// 	$opt="<option value=''>Pilih Sub Klasifikasi</option>";
	// 	$data = $this->mma->opt_sub_klasifikasi($param);
	// 	foreach ($data->result() as $das) {
	// 		$opt .= "<option value='".$das->id."'>".$das->nama."</option>";
	// 	}
	// 	echo $opt;
	// }

	// public function kode_akun()
	// {
	// 	$klas = $this->input->post('klasifikasi');
	// 	$subKlas = $this->input->post('sub_klasifikasi');
	// 	$code = $klas.'.'.$subKlas;
	// 	$data = $this->mma->kode_akun($code);
	// 	foreach ($data->result() as $das) {
	// 		$urut = $das->total+1;
	// 		if($urut<10){
	// 			$urut = '00'.$urut;
	// 		} else if($urut<100){
	// 			$urut = '0'.$urut;
	// 		}
	// 	}

	// 	$code = $klas.'.'.$subKlas.'.'.$urut;
	// 	echo $code;
	// }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
