<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signin extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->module = 'master';
		$this->cname = 'master/signin';
		$this->load->model('Mdl_login');
	}

	public function index()
	{
		$data['cname'] = $this->module;
		$data['title'] = "Login";
		$data['content'] = $this->load->view($this->module.'/login',$data,TRUE);
		$this->load->view('/template', $data);
		
	}

	public function login()
	{
		$param = $this->input->post();
		
		$hasil = $this->Mdl_login->login($param);
		// echo $this->db->last_query();
		// EXIT;
		if ($hasil != null) {
			$this->session->set_userdata( 'basmalahsession', $hasil );
		}
		
		redirect($this->module);
	}

	public function logout()
	{
		$this->session->unset_userdata('basmalahsession');
		
		redirect(base_url());
	}

	public function genpass($value='')
	{
		echo paramEncrypt($value);
	}

}

/* End of file signin.php */
/* Location: ./application/modules/cashier/controllers/signin.php */