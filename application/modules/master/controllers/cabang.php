<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cabang extends BASMALAH_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->module = 'master';
		$this->cname = 'master/cabang';
		$this->load->model('mdl_cabang', 'mp');
		$this->load->model('mdl_kabupaten', 'mk');
	} 

	public function index()
	{
		// echo "hello pspell_save_wordlist(dictionary_link)";
		redirect($this->cname.'/data');
	}

	public function data()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "List Data Cabang | Master Cabang";
		$data['content'] = $this->load->view('/list_cabang',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function detail(){
		$uri = $this->uri->segment(4);
		$data['val'] = $this->mp->detail($uri);
		$data['cname'] = $this->cname;
		$data['title'] = "Detail Cabang | Master Cabang";
		$data['content'] = $this->load->view('/detail_cabang',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function tambah(){
		// $data['business'] = $this->mp->get_opt_business();
		$data['kabupaten'] = $this->mk->get_all_kab();
		// print_r($data['area']);exit;
		$data['cname'] = $this->cname;
		$data['title'] = "Tambah Data Cabang | Master Cabang";
		$data['content'] = $this->load->view('/tambah_cabang',$data,TRUE);
		$this->load->view('/template', $data);
	}

	// //fungsi logic
	public function tambah_cabang()
	{
		// $access = strtolower($this->module.'.'.__class__.'.'.__function__);
		// $this->permission->check_permission($access);

        $param = $this->input->post();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('type', 'Klasifikasi', 'trim|required|xss_clean');
		$this->form_validation->set_rules('regency_code', 'Kabupaten', 'trim|required|xss_clean');
		$this->form_validation->set_rules('name', 'Nama Cabang', 'trim|required|xss_clean');
		$this->form_validation->set_rules('address', 'Alamat', 'trim|required|xss_clean');
		$this->form_validation->set_rules('code', 'Kode Cabang', 'trim|required|xss_clean');
		$this->form_validation->set_rules('telp', 'No Telp', 'trim|required|xss_clean');
		$this->form_validation->set_rules('registered', 'Tanggal Registrasi', 'trim|required|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_emails');

        if ($this->form_validation->run() == FALSE) {
            echo "0|".warn_msg(validation_errors());
        } else {
            //save data
            $id = $this->input->post('id');
            unset($param['id']);

	        if(is_numeric($id)){
	        	$where = array('id'=>$id);
	        	$save = $this->mp->replace('atombizz_branch',$param,$where);
	        } else {
	        	
	        	$where = array('code'=>$param['code']);
	        	$exist = $this->mp->total('atombizz_branch',$where);
	        	if($exist <= 0){
	        		while ($this->mp->cek_kode($param['code'])) {
		        		$param['code'] = $this->kode_cabang($param['regency_code']);	
		        	}
	        		$save = $this->mp->write('atombizz_branch', $param);
	        	} else {
	        		$save = FALSE;
	        	}
	        }
            if ($save == TRUE) {
            	echo "1|".succ_msg("Data Cabang berhasil disimpan");
            } else {
            	echo "0|".err_msg("Gagal, coba ulangi sekali lagi.");
            }
        }
	}

	public function detail_cabang(){
		$uri = $this->input->post('id');
		$data = $this->mp->detail($uri);
		echo json_encode($data);
		// $data['code']."|".$data['name']."|".$data['company']."|".$data['address']."|".$data['city']."|".$data['state']."|".$data['phone']."|".$data['email']."|".date('Y-m-d',strtotime($data['expired']));
	}

	public function delete_cabang()
	{
		// $access = strtolower($this->module.'.'.__class__.'.'.__function__);
		// $this->permission->check_permission($access);
		
		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_branch',$param);
		if($delete>0){
			echo "1|".succ_msg("Data cabang berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data cabang belum dihapus. -delete");
		}
	}

	public function kode_cabang($kabupaten_code='')
	{
		if(empty($kabupaten_code))$param = $this->input->post();
		else $param['regency_code'] = $kabupaten_code;
		$no = $this->mp->nomor_urut($param['regency_code']);
		if($no<10)$new_no = '00'.$no;
        else if($no<100)$new_no = '0'.$no;
        else if($no<1000)$new_no = $no;
		$code = $param['regency_code'].'-'.$new_no;
		echo $code.'|'.$no;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
