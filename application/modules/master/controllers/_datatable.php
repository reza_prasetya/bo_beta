<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class _datatable extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
	} 

	public function index()
	{
		redirect('master');
	}
	
	public function pelanggan()
	{	
		$this->datatables->select('id,code,name,address,phone,email')
		->add_column('Actions', get_detail_edit_delete('$1'),'id')
        ->from('atombizz_customers');
        
        echo $this->datatables->generate();
	}

	public function cabang()
	{	
		$this->datatables->select('id,code,name,address,telp')
		->add_column('Actions', get_detail_edit_delete('$1'),'id')
        ->from('view_branch');
        
        echo $this->datatables->generate();
	}

	public function kabupaten()
	{	
		$this->datatables->select('id,code,regency_name,keterangan')
		->add_column('Actions', get_detail_edit_delete('$1'),'id')
        ->from('atombizz_regency');
        
        echo $this->datatables->generate();
	}
	public function area()
	{	
		$this->datatables->select('id,code,area_name,dc_name,registered')
		// ->edit_date('Tanggal', '$1','registered')
		->add_column('Actions', get_detail_edit_delete('$1'),'id')
		->edit_date('registered', '$1', 'registered')
        ->from('atombizz_area');
        
        echo $this->datatables->generate();
	}

	public function pemasok()
	{	
		$this->datatables->select('id,code,name,address,phone,pic_name')
		->add_column('Actions', get_detail_edit_delete('$1'),'id')
        ->from('atombizz_suppliers');
        
        echo $this->datatables->generate();
	}

	public function karyawan()
	{	
		$this->datatables->select('id,nik,nama,jabatan,telp,alamat')
		->add_column('Actions', get_detail_edit_delete('$1'),'id')
        ->from('view_employee');
        
        echo $this->datatables->generate();
	}

	public function rak_gudang()
	{	
		$this->datatables->select('id,kode,nama,keterangan')
		->add_column('Actions', get_detail_edit_delete('$1'),'id')
        ->from('atombizz_rack');
        
        echo $this->datatables->generate();
	}

	public function kategori()
	{	
		//show category with edit and delete button use ajax
		$this->datatables->select('id,code,name')
		->add_column('Actions', get_edit_delete_ajax('$1'),'id')
        ->from('atombizz_product_categories');
        
        echo $this->datatables->generate();
	}

	public function department()
	{	
		//show category with edit and delete button use ajax
		$this->datatables->select('id,code,name')
		->add_column('Actions', get_edit_delete_ajax('$1'),'id')
        ->from('atombizz_product_department');
        
        echo $this->datatables->generate();
	}

	public function sub_kategori()
	{	
		$this->datatables->select('id,category,code,name')
		->add_column('Actions', get_edit_delete_ajax('$1'),'id')
        ->from('view_sub_category');
        
        echo $this->datatables->generate();
	}

	public function product()
	{	
		$this->datatables->select('id,category_name,subcategory_name,code,name')
		->add_column('Actions', get_detail_edit_delete('$1'),'id')
        ->from('view_products');
        
        echo $this->datatables->generate();
	}
	
	public function setup_branch($value='')
	{
		$this->datatables->select('id,area_name,regency_name,name')
		->add_column('Actions', get_delete_ajax('$1'),'id')
        ->from('view_branch');
        
        echo $this->datatables->generate();
	}

	public function setup_product($value='')
	{
		$this->datatables->select('id,supplier_code,supplier_name,code,name')
		->add_column('Actions', get_delete_ajax('$1'),'id')
        ->from('view_products');
        
        echo $this->datatables->generate();
	}

	public function posisi()
	{	
		$this->datatables->select('id,group,information')
		->add_column('Actions', get_edit('$1'),'id')
        ->from('atombizz_employee_position');
        
        echo $this->datatables->generate();
	}

	public function blended()
	{	
		$this->datatables->select('id,barcode_blended,name,price1,price2,price3,rak_name')
		->edit_currency('price1', '$1', 'price1')
		->edit_currency('price2', '$1', 'price2')
		->edit_currency('price3', '$1', 'price3')
		->add_column('Actions', get_button_blended('$1'),'id')
        ->from('view_blended_products');
        
        echo $this->datatables->generate();
	}

	public function product_discount()
	{	
		$this->datatables->select('id,code,name,discount')
		->edit_column('discount', '$1%', 'discount')
		->add_column('Actions', get_edit('$1'),'id')
        ->from('view_products_discount');
        
        echo $this->datatables->generate();
	}

	public function product_rak()
	{	
		$this->datatables->select('id,code,name,rak_name,gudang_name')
		->add_column('Actions', get_edit('$1'),'id')
        ->from('view_products');
        
        echo $this->datatables->generate();
	}

	public function product_price()
	{	
		$this->datatables->select('id,code,name,price1,price2,price3')
		->unset_column('id')
		->edit_currency('price1', '$1', 'price1')
		->edit_currency('price2', '$1', 'price2')
		->edit_currency('price3', '$1', 'price3')
		->add_column('Actions', get_edit('$1'),'id')
        ->from('view_products');

        echo $this->datatables->generate();
	}

	public function master_akun()
	{	
		$this->datatables->select('id,code,name,position,keterangan')
		// ->edit_column('name', space_data('$1,$2'),'name,space')
		// ->unset_column('space')
		->add_column('Actions', get_detail_edit_delete('$1'),'id')
        ->from('atombizz_accounting_master_akun');

        echo $this->datatables->generate();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
