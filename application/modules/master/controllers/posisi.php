<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posisi extends BASMALAH_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->cname = 'master/posisi';
		$this->load->model('mdl_posisi', 'mp');
	} 

	public function index()
	{
		// echo "hello pspell_save_wordlist(dictionary_link)";
		redirect($this->cname.'/data');
		// show_error('Gak duwe akses we....');
		// $str = __class__.'_'.__function__;
		// $str = strtolower($str);
		// echo $str;
	}

	public function data()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "List Data Posisi | Master Posisi";
		$data['content'] = $this->load->view('/list_posisi',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function tambah()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$id = $this->uri->segment(4);
		if($this->input->post() == NULL)
        {
            if(@$this->session->flashdata('post_item')) {
                $data['val'] = @$this->session->flashdata('post_item');
            } else if(is_numeric($id)){
            	$data['val'] = $this->mp->detail($id);
            	$access = $this->mp->get_access($id);
            	$data['access'] = json_decode($access[0]->access);
            }
            $data['module'] = $this->mp->get_module();
            $data['cname'] = $this->cname;
			$data['title'] = "Tambah Data Posisi | Master Posisi";
			$data['content'] = $this->load->view('/tambah_posisi',$data,TRUE);
			$this->load->view('/template', $data);
        } else {
            $param = $this->input->post();

            $this->load->library('form_validation');
            $this->form_validation->set_rules('group', 'Group', 'trim|required|xss_clean');
			$this->form_validation->set_rules('information', 'Information', 'trim|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                //tidak memenuhi validasi
                $this->session->set_flashdata('flash_message', warn_msg(validation_errors()));
                $this->session->set_flashdata('post_item', $param);
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                //save data
                $id = $this->input->post('id');
                //strtolower
                $checkbox = $this->input->post('produk');
                // var_dump($checkbox);
                unset($param['produk']);
                $perm = json_encode($checkbox);
	            // $num = count($checkbox);
	            // for($i=0;$i<$num;$i++){
	            // 	if($i==$num-1){
	            // 		$perm .= $checkbox[$i];
	            // 	} else {
	            // 		$perm .= $checkbox[$i].',';
	            // 	}
	            // }
	            // echo $perm;
	            if(is_numeric($id)){
	            	unset($param['id']);
	            	$where = array('id'=>$id);
	            	$save = $this->mp->replace('atombizz_employee_position',$param,$where);
	            	if ($save == TRUE) {
	            		$where = array('pos_id'=>$id);
	            		$exist = $this->mp->total('atombizz_employee_groups',$where,$like=null,$field=null);
	            		if($exist>0){
	            			$ins = array('access'=>$perm);
	            			$save = $this->mp->replace('atombizz_employee_groups',$ins,$where);
	            		} else {
	            			$ins = array('pos_id'=>$id,'access'=>$perm);
		                   	$save = $this->mp->write('atombizz_employee_groups',$ins); 
	            		}
	                   
	                   	if($save==TRUE){
	                   		$this->session->set_flashdata('flash_message', succ_msg('Pemasok berhasil ditambahkan.'));
	                   	} else {
	                   		$this->session->set_flashdata('flash_message', err_msg('Gagal, coba ulangi sekali lagi.'));
	                   	}
	                } else {
	                    $this->session->set_flashdata('flash_message', err_msg('Gagal, coba ulangi sekali lagi.'));
	                    // echo "string1";
	                }
	            } else {
	            	$save = $this->mp->write_id('atombizz_employee_position', $param);
	            	if (is_numeric($save)) {
	            		$ins = array('pos_id'=>$save,'access'=>$perm);
	                   	$save = $this->mp->write('atombizz_employee_groups',$ins); 
	                   	if($save==TRUE){
	                   		$this->session->set_flashdata('flash_message', succ_msg('Pemasok berhasil ditambahkan.'));
	                   	} else {
	                   		$this->session->set_flashdata('flash_message', err_msg('Gagal, coba ulangi sekali lagi.'));
	                   	}
	                } else {
	                    $this->session->set_flashdata('flash_message', err_msg('Gagal, coba ulangi sekali lagi.'));
	                    // echo "string1";
	                }
	            }
                redirect($this->cname.'/tambah');
            }
        }
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
