<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Karyawan extends BASMALAH_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->module = 'master';
		$this->cname = 'master/karyawan';
		$this->load->model('mdl_karyawan', 'mp');
	} 

	public function index()
	{
		// echo "hello pspell_save_wordlist(dictionary_link)";
		redirect($this->cname.'/data');
	}

	public function data()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "List Data Karyawan | Master Karyawan";
		$data['content'] = $this->load->view('/list_karyawan',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function cari(){
		$data['cname'] = $this->cname;
		$data['title'] = "Cari Data Karyawan | Master Karyawan";
		$data['content'] = $this->load->view('/cari_karyawan',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function detail(){
		$uri = $this->uri->segment(4);
		$data['val'] = $this->mp->detail($uri);
		$data['cname'] = $this->cname;
		$data['title'] = "Cari Data Karyawan | Master Karyawan";
		$data['content'] = $this->load->view('/detail_karyawan',$data,TRUE);
		$this->load->view('/template', $data);
	}

	//fungsi logic
	public function tambah()
	{
		// print_r($this->module);exit;
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$data['cname'] = $this->cname;
        $data['cat'] = $this->mp->opt_jabatan();
		$data['title'] = "Tambah Data Karyawan | Master Karyawan";
		$data['content'] = $this->load->view('/tambah_karyawan',$data,TRUE);
		$this->load->view('/template', $data);
	}
	public function ins_karyawan()
	{
		$param = $this->input->post();

        $this->load->library('form_validation');
  //      	$this->form_validation->set_rules('tahun', 'Tahun Masuk', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('status_alumni', 'Status Alumni', 'trim|required|xss_clean');
		// $this->form_validation->set_rules('tgl_lahir', 'Tgl Lahir', 'trim|required|xss_clean');
		$this->form_validation->set_rules('nik', 'NIK', 'trim|required|xss_clean');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|xss_clean');
		$this->form_validation->set_rules('no_ktp', 'No KTP', 'trim|required|xss_clean');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|xss_clean');
		$this->form_validation->set_rules('telp', 'No. Telp', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('id', 'fieldlabel', 'trim|xss_clean');
		$this->form_validation->set_rules('uname', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('upass', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('confirm-upass', 'Password Confirm', 'trim|required|xss_clean');
		$this->form_validation->set_rules('group', 'Jabatan', 'trim|required|xss_clean');


        if ($this->form_validation->run() == FALSE) {
            //tidak memenuhi validasi
            echo '0|'.warn_msg(validation_errors());
        } else {
        	if($param['upass']==$param['confirm-upass']){
        		//save data
                unset($param['confirm-upass']);
                $param['upass'] = paramEncrypt($param['upass']);
                $id = $this->input->post('id');

            	if(is_numeric($id)){
                	unset($param['id']);
                	$where = array('id'=>$id);
                	$save = $this->mp->replace('atombizz_employee',$param,$where);
                } else {
                	$exist = $this->mp->cek_data('atombizz_employee', $param);
                	if($exist<=0){
                		$max = $this->mp->max('atombizz_employee','urut',array('tahun'=>$param['tahun']));
						$total = (!empty($max[0]->urut)? $max[0]->urut+1 : 0+1);
						$param['urut'] = $total;
		            	$save = $this->mp->write('atombizz_employee', $param);
                	} else {
                		$save = FALSE;
                	}
                }
                if ($save == TRUE) {
	                echo '1|'.succ_msg('Karyawan berhasil ditambahkan.');
	            } else {
	                echo '0|'.err_msg('Gagal menambah karyawan');
	            }
        	} else {
        		echo '0|'.warn_msg('Password yang dimasukkan tidak sama.');
        	} 
        }
	}
	public function mencari(){
		$param = $this->input->post();

		$this->load->library('form_validation');
		// $this->form_validation->set_rules('category', 'Filter', 'trim|required|xss_clean');
		$this->form_validation->set_rules('keyword', 'Kata Kunci', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
            //tidak memenuhi validasi
            echo "0|".warn_msg(validation_errors());
        } else {
            //save data
            // $save = $this->mp->find('atombizz_customers',NULL,NULL,NULL,NULL,FALSE, $like = FALSE);
            $save = $this->mp->mencari($param['keyword']);
            if($save->num_rows() > 0){
            	$list = '';
	            foreach ($save->result() as $data) {
	            	$list .= '
	            		<a href="'.base_url().'master/Karyawan/detail/'.$data->id.'" class="list-group-item" >
							<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
							<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">'.$data->nama.'</h4>
							<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">'.$data->alamat.'</h5>
						</a>
	            	';
	            }

	            echo '1|'.$list;
            } else {
            	$list = '
	            	<a href="javascript:void(0)" class="list-group-item" >
						<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
						<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">Tidak Ada Hasil Pencarian</h4>
						<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">Silahkan Anda masukkankan kata kunci pada kolom search.</h5>
					</a>
	            ';
	            echo '1|'.$list;
            }
            
        }
	}

	public function delete_karyawan()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);
		
		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_employee',$param);
		if($delete>0){
			echo "1|".succ_msg("Data karyawan berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data karyawan belum dihapus. -delete");
		}
	}

	public function get_code()
	{
		$param = $this->input->post();
		// if(!empty($param['tahun']) && !empty($param['status_alumni']) && !empty($param['tgl_lahir'])){
		$max = $this->mp->max('atombizz_employee','urut',array('tahun'=>$param['tahun']));
		// echo $this->db->last_query();exit;
		$total = (!empty($max[0]->urut)? $max[0]->urut+1 : 0+1);
		$tahun_masuk = date('y', strtotime($param['tahun'].'-01-01'));
		echo $tahun_masuk.$param['status_alumni'].date('ymd',strtotime($param['tgl_lahir'])).complete_zero($total,3);
	}

	public function get_data($value='')
	{
		$param = $this->input->post();
		$data = $this->mp->detail($param['id']);
		echo json_encode($data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
