<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pemasok extends BASMALAH_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->module = 'master';
		$this->cname = 'master/pemasok';
		$this->load->model('mdl_pemasok', 'mp');
	} 

	public function index()
	{
		// echo "hello pspell_save_wordlist(dictionary_link)";
		redirect($this->cname.'/data');
	}

	public function data()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "List Data Pemasok | Master Pemasok";
		$data['content'] = $this->load->view('/list_pemasok',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function cari(){
		$data['cname'] = $this->cname;
		$data['title'] = "Cari Data Pemasok | Master Pemasok";
		$data['content'] = $this->load->view('/cari_pemasok',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function detail(){
		$uri = $this->uri->segment(4);
		$data['val'] = $this->mp->detail($uri);
		$data['cname'] = $this->cname;
		$data['title'] = "Cari Data Pemasok | Master Pemasok";
		$data['content'] = $this->load->view('/detail_pemasok',$data,TRUE);
		$this->load->view('/template', $data);
	}

	//fungsi logic
	public function tambah()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$data['cname'] = $this->cname;
		$data['title'] = "Tambah Data Pemasok | Master Pemasok";
		$data['content'] = $this->load->view('/tambah_pemasok',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function ins_pemasok($value='')
	{
		$param = $this->input->post();

        $this->load->library('form_validation');
  //      	$this->form_validation->set_rules('type', 'Jenis', 'trim|required|xss_clean'); 
		// $this->form_validation->set_rules('classification', 'Klasifikasi', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('code', 'Kode Suplier', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('name', 'Nama', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('address', 'Alamat', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('phone', 'Telepon', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('bank_name', 'Nama Bank', 'trim|xss_clean'); 
		$this->form_validation->set_rules('bank_account', 'No Rekening', 'trim|xss_clean'); 
		$this->form_validation->set_rules('payment', 'Pembayaran', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('pic_name', 'Nama PIC', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('pic_position', 'Jabatan PIC', 'trim|xss_clean'); 
		$this->form_validation->set_rules('pic_phone', 'Telepon PIC', 'trim|required|xss_clean'); 
		$this->form_validation->set_rules('pic_email', 'Email PIC', 'trim|xss_clean'); 
		$this->form_validation->set_rules('account_code', 'Kode Akun', 'trim|xss_clean'); 
		$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean'); 

        if ($this->form_validation->run() == FALSE) {
            //tidak memenuhi validasi
            echo '0|'.warn_msg(validation_errors());
            // $this->session->set_flashdata('post_item', $param);
            // redirect($_SERVER['HTTP_REFERER']);
        } else {
            //save data
            $id = $this->input->post('id');
            
            if(is_numeric($id)){
            	unset($param['id']);
            	$where = array('id'=>$id);
            	$save = $this->mp->replace('atombizz_suppliers',$param,$where);
            } else {
            	$where = array('code'=>$param['code']);
            	$exist = $this->mp->total('atombizz_suppliers',$where);
            	if($exist<=0){
            		$max = $this->mp->max('atombizz_suppliers','urut',array('type'=>$param['type'], 'classification'=>$param['classification']));
					$total = (!empty($max[0]->urut)? $max+1 : 0+1);
					$param['urut'] = $total;
            		$param['registered'] = date('Y-m-d');
	            	$save = $this->mp->write('atombizz_suppliers', $param);
            	} else {
            		$save = FALSE;
            	}
            }
            if ($save == TRUE) {
                echo '1|'.succ_msg('Pemasok berhasil ditambahkan.');
            } else {
                echo '0|'.err_msg('Gagal, coba ulangi sekali lagi.');
            }
        }
	}

	public function mencari(){
		$param = $this->input->post();

		$this->load->library('form_validation');
		// $this->form_validation->set_rules('category', 'Filter', 'trim|required|xss_clean');
		$this->form_validation->set_rules('keyword', 'Kata Kunci', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
            //tidak memenuhi validasi
            echo "0|".warn_msg(validation_errors());
        } else {
            //save data
            $save = $this->mp->mencari($param['keyword']);
            if($save->num_rows() > 0){
            	$list = '';
	            foreach ($save->result() as $data) {
	            	$list .= '
	            		<a href="'.base_url().'master/pemasok/detail/'.$data->id.'" class="list-group-item" >
							<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
							<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">'.$data->name.'</h4>
							<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">'.$data->address.'</h5>
						</a>
	            	';
	            }

	            echo '1|'.$list;
            } else {
            	$list = '
	            	<a href="javascript:void(0)" class="list-group-item" >
						<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
						<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">Tidak Ada Hasil Pencarian</h4>
						<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">Silahkan Anda masukkankan kata kunci pada kolom search.</h5>
					</a>
	            ';
	            echo '1|'.$list;
            }
            
        }
	}

	public function delete_pemasok()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_suppliers',$param);
		if($delete>0){
			echo "1|".succ_msg("Data supplier berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data supplier belum dihapus. -delete");
		}
	}

	public function get_code()
	{
		$param = $this->input->post();
		$max = $this->mp->max('atombizz_suppliers','urut',array('type'=>$param['type'], 'classification'=>$param['classification']));
		// echo $this->db->last_query();exit;
		$total = (!empty($max[0]->urut)? $max[0]->urut+1 : 0+1);
		echo 'S'.$param['type'].$param['classification'].'-'.complete_zero($total,4);
		// print_r($total);exit;
	}

	public function get_data($value='')
	{
		$param = $this->input->post();
		$data = $this->mp->detail($param['id']);
		echo json_encode($data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
