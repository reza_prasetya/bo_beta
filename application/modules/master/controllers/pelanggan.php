<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pelanggan extends BASMALAH_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->module = 'master';
		$this->cname = 'master/pelanggan';
		$this->load->model('mdl_pelanggan', 'mp');
	} 

	public function index()
	{
		// echo "hello pspell_save_wordlist(dictionary_link)";
		redirect($this->cname.'/data');
	}

	public function data()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "List Data Pelanggan | Master Pelanggan";
		$data['content'] = $this->load->view('/list_pelanggan',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function cari(){
		$data['cname'] = $this->cname;
		$data['title'] = "Cari Data Pelanggan | Master Pelanggan";
		$data['content'] = $this->load->view('/cari_pelanggan',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function detail(){
		$uri = $this->uri->segment(4);
		$data['val'] = $this->mp->detail($uri);
		$data['cname'] = $this->cname;
		$data['title'] = "Cari Data Pelanggan | Master Pelanggan";
		$data['content'] = $this->load->view('/detail_pelanggan',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function tambah(){
		$data['cname'] = $this->cname;
		$data['title'] = "Cari Data Pelanggan | Head Pelanggan";
		$data['content'] = $this->load->view('/tambah_pelanggan',$data,TRUE);
		$this->load->view('/template', $data);
	}
	//fungsi logic
	public function tambah_pelanggan()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);

        $param = $this->input->post();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('code', 'Kode', 'trim|required|xss_clean');
        $this->form_validation->set_rules('name', 'Nama', 'trim|required|xss_clean');
        $this->form_validation->set_rules('registered', 'Tanggal Registrasi', 'trim|required|xss_clean');
		$this->form_validation->set_rules('identification_number', 'Nomor KTP', 'trim|xss_clean');
		$this->form_validation->set_rules('no_urut', 'Nomor urut', 'trim|xss_clean');
		$this->form_validation->set_rules('address', 'Alamat', 'trim|required|xss_clean');
		$this->form_validation->set_rules('born_date', 'Tanggal Lahir', 'trim|required|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'trim|xss_clean');
		$this->form_validation->set_rules('phone', 'Telepon', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|valid_email');
		$this->form_validation->set_rules('expired', 'Tanggal Expired', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            echo "0|".warn_msg(validation_errors());
        } else {
            //save data
            $id = $this->input->post('id');
            unset($param['id']);

            
	        if(is_numeric($id)){
	        	$where = array('id'=>$id);
	        	$save = $this->mp->replace('atombizz_customers',$param,$where);
	        } else {
	        	$where = array('code'=>$param['code']);
	        	$exist = $this->mp->total('atombizz_customers',$where,$like=null,$field=null);
	        	if($exist<=0){
	        		$save = $this->mp->write('atombizz_customers', $param);
	        	} else {
	        		$save = FALSE;
	        	}
	        }
            if ($save == TRUE) {
            	echo "1|".succ_msg("Data pelanggan berhasil disimpan");
            } else {
            	echo "0|".err_msg("Gagal, coba ulangi sekali lagi.");
            }
        }
	}
	public function detail_pelanggan(){
		$uri = $this->input->post('id');
		$data = $this->mp->detail($uri);
		echo 
		$data['code']."|".$data['name']."|".$data['registered']."|".$data['address']."|".$data['identification_number']."|".date('Y-m-d',strtotime($data['born_date']))."|".$data['phone']."|".$data['email']."|".date('Y-m-d',strtotime($data['expired']))."|".$data['status']."|".$data['no_urut'];
	}
	public function mencari(){
		$param = $this->input->post();

		$this->load->library('form_validation');
		// $this->form_validation->set_rules('category', 'Filter', 'trim|required|xss_clean');
		$this->form_validation->set_rules('keyword', 'Kata Kunci', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
            //tidak memenuhi validasi
            echo "0|".warn_msg(validation_errors());
        } else {
            //save data
            // $save = $this->mp->find('atombizz_customers',NULL,NULL,NULL,NULL,FALSE, $like = FALSE);
            $save = $this->mp->mencari($param['keyword']);
            if($save->num_rows() > 0){
            	$list = '';
	            foreach ($save->result() as $data) {
	            	$list .= '
	            		<a href="'.base_url().'master/pelanggan/detail/'.$data->id.'" class="list-group-item" >
							<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
							<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">'.$data->name.'</h4>
							<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">'.$data->address.'</h5>
						</a>
	            	';
	            }

	            echo '1|'.$list;
            } else {
            	$list = '
	            	<a href="javascript:void(0)" class="list-group-item" >
						<img src="'.base_url().'public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
						<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">Tidak Ada Hasil Pencarian</h4>
						<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">Silahkan Anda masukkankan kata kunci pada kolom search.</h5>
					</a>
	            ';
	            echo '1|'.$list;
            }
            
        }
	}
	public function delete_pelanggan()
	{
		$access = strtolower($this->module.'.'.__class__.'.'.__function__);
		$this->permission->check_permission($access);
		
		$param = $this->input->post();
		$delete = $this->mp->delete('atombizz_customers',$param);
		if($delete>0){
			echo "1|".succ_msg("Data pelanggan berhasil dihapus. -delete");
		} else {
			echo "0|".err_msg("Gagal, data pelanggan belum dihapus. -delete");
		}
	}
	public function gen_code(){
		$no = $this->mp->gen_kode();
		if($no<10)$no = '0000'.$no;
        else if($no<100)$no = '000'.$no;
        else if($no<1000)$no = '00'.$no;
        else if($no<10000)$no = '0'.$no;
        else if($no<100000)$no = $no;
        $code = 'C-'.date('Y').$no;
        echo $code.'|'.$no;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
