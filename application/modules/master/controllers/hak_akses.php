<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hak_akses extends BASMALAH_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->cname = 'master/hak_akses';
		$this->load->model('mdl_posisi', 'mp');
	} 

	public function index()
	{
		redirect($this->cname.'/data');
	}

	public function data()
	{
		$data['cname'] = $this->cname;
		$data['title'] = "List Data Hak Akses | Master Hak Akses";
		$data['content'] = $this->load->view('/list_posisi',$data,TRUE);
		$this->load->view('/template', $data);
	}

	public function tambah()
	{
		$id = $this->uri->segment(4);
		if($this->input->post() == NULL)
        {
            if(@$this->session->flashdata('post_item')) {
                $data['val'] = @$this->session->flashdata('post_item');
            } else if(is_numeric($id)){
            	$data['val'] = $this->mp->detail($id);
            	$access = $this->mp->get_access($id);
            	$data['access'] = json_decode(@$access[0]->access);
            	$data['module_access'] = json_decode(@$access[0]->module);
            }
            // $data['opt_module'] = $this->mp->opt_module();
            $data['module'] = $this->mp->get_module();
            $data['module_list'] = $this->mp->get_module_list();
            $data['cname'] = $this->cname;
			$data['title'] = "Tambah Data Hak Akses | Master Hak Akses";
			$data['content'] = $this->load->view('/tambah_posisi',$data,TRUE);
			$this->load->view('/template', $data);
        } else {
            $param = $this->input->post();

            $this->load->library('form_validation');
            $this->form_validation->set_rules('group', 'Group', 'trim|required|xss_clean');
			$this->form_validation->set_rules('information', 'Information', 'trim|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                //tidak memenuhi validasi
                $this->session->set_flashdata('flash_message', warn_msg(validation_errors()));
                $this->session->set_flashdata('post_item', $param);
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                //save data
                $id = $this->input->post('id');
                //strtolower
                $checkbox = $this->input->post('produk');
                $check_module = $this->input->post('module');

                // var_dump($checkbox);
                unset($param['produk']);
                unset($param['module']);
                $perm = json_encode($checkbox);
                $mod = json_encode($check_module);
                unset($param['id']);

	            if(is_numeric($id)){
	            	$where = array('id'=>$id);
	            	$save = $this->mp->replace('atombizz_employee_position',$param,$where);
	            	if ($save == TRUE) {
	            		$where = array('position_id'=>$id);
	            		$exist = $this->mp->total('atombizz_employee_access',$where,$like=null,$field=null);
	            		if($exist>0){
	            			$ins = array('access'=>$perm,'module'=>$mod);
	            			 // print_r($check_module);exit;
	            			$save = $this->mp->replace('atombizz_employee_access',$ins,$where);
	            		} else {
	            			$ins = array('position_id'=>$id,'access'=>$perm,'module'=>$mod);
		                   	$save = $this->mp->write('atombizz_employee_access',$ins); 
	            		}
	                   	if($save==TRUE){
	                   		$this->session->set_flashdata('flash_message', succ_msg('Pemasok berhasil ditambahkan.'));
	                   	} else {
	                   		$this->session->set_flashdata('flash_message', err_msg('Gagal, coba ulangi sekali lagi.'));
	                   	}
	                } else {
	                    $this->session->set_flashdata('flash_message', err_msg('Gagal, coba ulangi sekali lagi.'));
	                    // echo "string1";
	                }
	            } else {
	            	$where = array('group'=>$param['group']);
	            	$exist = $this->mp->total('atombizz_employee_position',$where,$like=null,$field=null);
	            	// print_r($param);print_r($exist);exit;
	            	if($exist<=0){
	            		$save = $this->mp->write_id('atombizz_employee_position', $param);
		            	// print_r($param);print_r($save);exit;
		            	if (is_numeric($save)) {
		            		$ins = array('position_id'=>$save,'access'=>$perm, 'module'=>$mod);
		                   	$save = $this->mp->write('atombizz_employee_access',$ins); 
		                   	if($save==TRUE){
		                   		$this->session->set_flashdata('flash_message', succ_msg('Pemasok berhasil ditambahkan.'));
		                   	} else {
		                   		$this->session->set_flashdata('flash_message', err_msg('Gagal, coba ulangi sekali lagi.'));
		                   	}
		                } else {
		                    $this->session->set_flashdata('flash_message', err_msg('Gagal, coba ulangi sekali lagi.'));
		                    // echo "string1";
		                }
	            	} else {
	            		$this->session->set_flashdata('flash_message', err_msg('Gagal, data sudah ada.'));
	            	}
	            }
                redirect($this->cname.'/tambah');
            }
        }
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
