<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
				<ul style="white-space:normal;">
						<li>
							<a onclick="tambah()"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a onclick="daftar()"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Daftar</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().$cname; ?>/menu"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Menu</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<!-- Start Content -->
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="daftar-div">
					<div class="row">
						<div class="col-sm-12">
							<blockquote>
								<p><i class="icon-file-text"></i> Data Departemen</p>
							</blockquote>
						</div>
					</div>
					<div class="table-responsive">
						<table id="example-datatable" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="75px" class="text-center">No</th>
									<th width="150px">Kode</th>
									<th width="">Departemen</th>
									<th width="100px"></th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<span id="flash_message"></span>
					<form id="addDepartment" action="" method="post" class="form-horizontal" onSubmit="return false;">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode Department</span>
								<input type="hidden" id="id_department" name="id_department" class="form-control" placeholder="Id Department">
								<input type="text" id="kode_department" name="kode_department" class="form-control" placeholder="Kode Department">
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama Department</span>
								<input type="text" id="nama_department" name="nama_department" class="form-control" placeholder="Nama Department">
								<span class="input-group-addon"><i class="icon-user"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- End Content -->
	</div>
</div>
<script type="text/javascript">
	$(function(){
		webApp.datatables(),
		$("#example-datatable").dataTable({
			// aoColumnDefs:[{
			// 	bSortable:!1,
			// 	aTargets:[0]
			// }],
			fnDrawCallback: function ( oSettings ) {
				/* Need to redo the counters if filtered or sorted */
				if ( oSettings.bSorted || oSettings.bFiltered )
				{
					for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
					{
						$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
					}
				}
			},
			aoColumnDefs: [
				{ "bSortable": false, "aTargets": [ 0,3 ] }
			],
			bProcessing:true,
			bServerSide:true,
			sAjaxSource: "<?php echo base_url(); ?>master/_datatable/department/",
			iDisplayLength:15,
			aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
			"fnServerData": function(sSource, aoData, fnCallback){
			    $.ajax(
			       	{
			       	  'dataType': 'json',
			       	  'type'  : 'POST',
			       	  'url'    : sSource,
			       	  'data'  : aoData,
			       	  'success' : fnCallback
			       	}
			    );
			} 
		}),
		$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
	});
</script>
<script type="text/javascript">
function tambah(){
	$("#tambah-div").slideDown();
	$("#daftar-div").slideUp();
}
function daftar(){
	$("#daftar-div").slideDown();
	$("#tambah-div").slideUp();
}
function begin(){
	$("#daftar-div").show();
	$("#tambah-div").hide();
}
function actDelete(Object){
	// alert(Object);
	alertify.confirm("Apakah anda yakin untuk menghapus item ini?", function (e) {
		if (e) {
			var url = "<?php echo base_url(); ?>master/produk/delete_department";
			var form_data = {
				id: Object
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					data = msg.split("|");
					if(data[0]==1){
						$("#example-datatable").dataTable().fnDraw();
					}
					$("#flash_message").show();
					$("#flash_message").html(data[1]);
					setTimeout(function() {location.reload();}, 5000);
				}
			});
			return false;
		} else {
			
		}
	});
	return false;
}
function actEdit(Object){
	var id = Object;
	var url = "<?php echo base_url(); ?>master/produk/detail_department";
	var form_data = {
		id: id
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(msg)
		{
			data = JSON.parse(msg);
			$("#id_department").val(data.id);
			$("#kode_department").val(data.code);
			$("#nama_department").val(data.name);
			tambah();
		}
	});
	return false;
}
$(document).ready(function(){
	begin();
	$("#addDepartment").submit(function(){
		var id = $("#id_department").val();
		var kode = $("#kode_department").val();
		var nama = $("#nama_department").val();
		var url = "<?php echo base_url(); ?>master/produk/tambah_department";
		var form_data = {
			id:id,
			code: kode,
			name: nama
		};
		// alert('as');
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = msg.split("|");
				if(data[0]==1){
					clear();
					$("#example-datatable").dataTable().fnDraw();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
				// alert(msg);
			}
		});
		return false;
	});
	function clear(){
		$("#kode_department").val('');
		$("#nama_department").val('');
		$("#id_department").val('');
	}
});
</script>