<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/produk/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/produk/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().$cname; ?>/"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Menu Setup</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<!-- Start Content -->
		<div class="row">
			<div class="col-sm-12">
				<span id="flash_message" style="display:none"></span>
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<span id="flash_message"></span>
					<form id="addProduct" method="post" class="form-horizontal" onSubmit="return false;">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Produk</span>
								<select id="product" name="product[]" class="select-chosen form-control" data-placeholder="Pilih Produk" multiple>
									
								</select>
								<span class="input-group-addon"><i class="icon-gift"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Suplier</span>
								<select id="supplier" name="supplier" class="form-control select-chosen" data-placeholder="Pilih Area">
								</select>
								<span class="input-group-addon"><i class="icon-truck"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
				<div class="block full" style="margin-top:10px;" id="data-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Data Kelompok Suplier</p>
							</blockquote>
						</div>
					</div>
					<div class="table-responsive">
						<table id="example-datatable" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="75px">No</th>
									<th width="175px">Kode Suplier</th>
									<th width="">Nama Suplier</th>
									<th width="175px">kode Produk</th>
									<th width="">Nama Produk</th>
									<th width="50px"></th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- End Content -->
	</div>
</div>
<script type="text/javascript">
$(function(){
	webApp.datatables(),
	$("#example-datatable").dataTable({
		// aoColumnDefs:[{
		// 	bSortable:!1,
		// 	aTargets:[0]
		// }],
		fnDrawCallback: function ( oSettings ) {
			/* Need to redo the counters if filtered or sorted */
			if ( oSettings.bSorted || oSettings.bFiltered )
			{
				for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
				{
					$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
				}
			}
		},
		aoColumnDefs: [
			{ "bSortable": false, "aTargets": [ 0,5 ] }
		],
		aaSorting: [[ 1, "desc" ]],
		bProcessing:true,
		bServerSide:true,
		sAjaxSource: "<?php echo base_url(); ?>master/_datatable/setup_product",
		iDisplayLength:15,
		aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
		"fnServerData": function(sSource, aoData, fnCallback){
		    $.ajax(
		       	{
		       	  'dataType': 'json',
		       	  'type'  : 'POST',
		       	  'url'    : sSource,
		       	  'data'  : aoData,
		       	  'success' : fnCallback
		       	}
		    );
		} 
	}),
	$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
});
function begin () {
	val = '<?php echo $this->uri->segment(4); ?>';
	if(val=='data'){
		$('#data-div').show();
		$('#tambah-div').hide();
	} else if(val=='tambah'){
		$('#data-div').hide();
		$('#tambah-div').show();
	} else {
		window.location = "<?php echo base_url().$cname ?>/produk/data";
	}
	optProduct();
	optSupplier();
}
function optProduct () {
	var url = "<?php echo base_url($this->cname); ?>/get_opt_product";
	$.ajax({
		type: "POST",
		url: url,
		data: {},
		success: function(msg)
		{
			// alert(msg);
			$('#product').html(msg);
			$('#product').trigger('chosen:updated');
		}
	});
	return false;
}
function optSupplier (argument) {
	var url = "<?php echo base_url($this->cname); ?>/get_opt_supplier";
	$.ajax({
		type: "POST",
		url: url,
		data: {},
		success: function(msg)
		{
			$('#supplier').html(msg);
			$('#supplier').trigger('chosen:updated');
		}
	});
	return false;
}
function actDelete(Object){
	// alert(Object);
	alertify.confirm("Apakah anda yakin akan menghapus setup suplier untuk data ini?", function (e) {
		if (e) {
			var url = "<?php echo base_url(); ?>master/setup/delete_supplier";
			var form_data = {
				id: Object
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					data = msg.split("|");
					if(data[0]==1){
						$("#example-datatable").dataTable().fnDraw();
					}
					$("#flash_message").show();
					$("#flash_message").html(data[1]);
					setTimeout(function() {location.reload();}, 5000);
				}
			});
			return false;
		} else {
			
		}
	});
	return false;
}
$(document).ready(function(){
	begin();
	$('#addProduct').submit(function(){
		var url = "<?php echo base_url($this->cname); ?>/setup_product";
		$.ajax({
			type: "POST",
			url: url,
			data: $('#addProduct').serialize(),
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				if(data[0]==1){
					optProduct();
					optSupplier();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 3000);
			}
		});
		return false;
	});
});
</script>