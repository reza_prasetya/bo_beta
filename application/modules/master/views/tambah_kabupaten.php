<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Pendaftaran kabupaten</p>
							</blockquote>
						</div>
					</div>
					<span id="flash_message"></span>
					<form id="addkabupaten" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode kabupaten</span>
								<input type="hidden" id="id" name="id" class="form-control" placeholder="ID kabupaten">
								<input type="text" id="code" name="code" class="form-control" placeholder="Kode kabupaten">
								<span class="input-group-addon"><i class="icon-home"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama kabupaten</span>
								<input type="text" id="regency_name" name="regency_name" class="form-control" placeholder="Nama kabupaten">
								<span class="input-group-addon"><i class="icon-home"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Keterangan</span>
								<input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan kabupaten">
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function begin(){
	var val = "<?php echo $this->uri->segment(3); ?>";
	var id = "<?php echo $this->uri->segment(4); ?>";
	// alert(id);
	if(val=='tambah' && id){
		$("#id").val(id);
		var url = "<?php echo base_url(); ?>master/kabupaten/detail_kabupaten";
		// alert('asas');
		var form_data = {
			id: id
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = JSON.parse(msg);
				// $('#id').val(data.id);
				$('#code').val(data.code);
				$('#regency_name').val(data.regency_name);
				$('#keterangan').val(data.keterangan);
			}
		});
		return false;
	}
}
$(document).ready(function(){
	begin();
	$("#addkabupaten").submit(function(){
		var url = "<?php echo base_url(); ?>master/kabupaten/tambah_kabupaten";
		$.ajax({
			type: "POST",
			url: url,
			data: $('#addkabupaten').serialize(),
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				if(data[0]==1){
					clear();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	function clear(){
		$('#id').val('');
		$('#code').val('');
		$('#regency_name').val('');
		$('#keterangan').val('');
	}
});
</script>