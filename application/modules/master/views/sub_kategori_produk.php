<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
				<ul style="white-space:normal;">
						<li>
							<a onclick="tambah()"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a onclick="daftar()"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Daftar</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().$cname; ?>/menu"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Menu</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<!-- Start Content -->
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="daftar-div">
					<div class="row">
						<div class="col-sm-12">
							<blockquote>
								<p><i class="icon-file-text"></i> Data Sub Kategori</p>
							</blockquote>
						</div>
					</div>
					<div class="table-responsive">
						<table id="example-datatable" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="75px" class="text-center">No</th>
									<th width="300px">Kategori</th>
									<th width="75px">Kode</th>
									<th width="">Sub Kategori</th>
									<th width="100px"></th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<span id="flash_message"></span>
					<form id="addSubKategori" method="post" class="form-horizontal" onSubmit="return false;">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Departemen</span>
								<?php echo form_dropdown('department', @$cat_dept, @$val['department'], 'class="form-control size="1" id="department"'); ?>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kategori</span>
								<select id="kategori" name="kategori" class="form-control" data-placeholder="Pilih Sub Kategori">
									<option value="">Pilih Kategori</option>
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode Sub Kategori</span>
								<input type="hidden" id="id_kategori" name="id_kategori" class="form-control" placeholder="Kode Sub Kategori">
								<input type="text" id="kode" name="kode" class="form-control" placeholder="Kode Sub Kategori">
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama Sub Kategori</span>
								<input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Sub Kategori">
								<span class="input-group-addon"><i class="icon-user"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- End Content -->
	</div>
</div>
<script type="text/javascript">
	$(function(){
		webApp.datatables(),
		$("#example-datatable").dataTable({
			// aoColumnDefs:[{
			// 	bSortable:!1,
			// 	aTargets:[4]
			// }],
			fnDrawCallback: function ( oSettings ) {
			/* Need to redo the counters if filtered or sorted */
			if ( oSettings.bSorted || oSettings.bFiltered )
			{
				for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
				{
					$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
				}
			}
		},
		aoColumnDefs: [
			{ "bSortable": false, "aTargets": [ 0,4 ] }
		],
			bProcessing:true,
			bServerSide:true,
			sAjaxSource: "<?php echo base_url(); ?>master/_datatable/sub_kategori/",
			iDisplayLength:15,
			aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
			"fnServerData": function(sSource, aoData, fnCallback){
			    $.ajax(
			       	{
			       	  'dataType': 'json',
			       	  'type'  : 'POST',
			       	  'url'    : sSource,
			       	  'data'  : aoData,
			       	  'success' : fnCallback
			       	}
			    );
			} 
		}),
		$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
	});
</script>
<script type="text/javascript">
function tambah(){
	$("#tambah-div").slideDown();
	$("#daftar-div").slideUp();
}
function daftar(){
	$("#daftar-div").slideDown();
	$("#tambah-div").slideUp();
}
function begin(){
	$("#daftar-div").show();
	$("#tambah-div").hide();
}
function actEdit(Object){
	var id = Object;
	var url = "<?php echo base_url(); ?>master/produk/detail_sub_kategori";
	var form_data = {
		id: id
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(msg)
		{
			// alert(msg);
			data = msg.split('|');
			$("#id_kategori").val(data[0]);
			// $("#kategori").val(data[1]);
			$("#kode").val(data[2]);
			$("#nama").val(data[3]);
			$("#department").val(data[4]);
			kategori(data[4]);
			tambah();
		}
	});
	return false;
}
function actDelete(Object){
	// alert(Object);
	alertify.confirm("Apakah anda yakin untuk menghapus item ini?", function (e) {
		if (e) {
			var url = "<?php echo base_url(); ?>master/produk/delete_sub_kategori";
			var form_data = {
				id: Object
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					data = msg.split("|");
					if(data[0]==1){
						$("#example-datatable").dataTable().fnDraw();
					}
					$("#flash_message").show();
					$("#flash_message").html(data[1]);
					setTimeout(function() {location.reload();}, 5000);
				}
			});
			return false;
		} else {
			
		}
	});
	return false;
}
function kategori(Object){
	var department = Object;
	var url = "<?php echo base_url(); ?>master/produk/opt_kategori";
	var form_data = {
		id_dept: department
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(msg)
		{
			// alert(msg);
			$("#kategori").html(msg);
			$("#kategori").val(data[1]);
		}
	});
	return false;
}
$(document).ready(function(){
	begin();
	$("#addSubKategori").submit(function(){
		var id = $("#id_kategori").val();
		var kategori = $("#kategori").val();
		var kode = $("#kode").val();
		var nama = $("#nama").val();

		var url = "<?php echo base_url(); ?>master/produk/tambah_sub_kategori";
		var form_data = {
			id:id,
			category_id:kategori,
			code: kode,
			name: nama
		};
		// alert(kategori);
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = msg.split("|");
				if(data[0]==1){
					clear();
					$("#example-datatable").dataTable().fnDraw();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	$("#department").change(function(evt){
		evt.preventDefault()
		var department = $('#department').val();
		var url = "<?php echo base_url(); ?>master/produk/opt_kategori";
		var form_data = {
			id_dept: department
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$("#kategori").html(msg);
			}
		});
		return false;
	});
	function clear(){
		$("#id_kategori").val('');
		$("#kategori").val('');
		$("#department").val('');
		$("#kode").val('');
		$("#nama").val('');
	}
});
</script>