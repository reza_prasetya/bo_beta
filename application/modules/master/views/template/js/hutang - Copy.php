<a href="javascript:void(0)" id="to-top"><i class="icon-angle-up"></i></a>
<script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main-1.03.js"></script>
<script>
$(function () {
	$("#modal-compose").on("shown.bs.modal", function () {
		$(".modal-select-chosen").chosen({
			width : "100%"
		})
	}),
	$(".ms-message-list").slimScroll({
		height : 610,
		color : "#000000",
		size : "3px",
		touchScrollStep : 750
	});
	var e = $(".ms-categories");
	$("a", e).click(function () {
		$("li", e).removeClass("active"),
		$(this).parent().addClass("active"),
		$(this).data("cat")
	});
	begin();

	webApp.datatables(),
	$("#example-datatable").dataTable({
		aoColumnDefs:[{
			bSortable:!1,
			aTargets:[4]
		}],
		bAutoWidth:true,
		bProcessing:true,
		bServerSide:true,
		sAjaxSource: "<?php echo base_url(); ?>accounting/_datatable/list_hutang/",
		iDisplayLength:15,
		aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
		"fnServerData": function(sSource, aoData, fnCallback){
		    $.ajax(
		       	{
		       	  'dataType': 'json',
		       	  'type'  : 'POST',
		       	  'url'    : sSource,
		       	  'data'  : aoData,
		       	  'success' : fnCallback
		       	}
		    );
		} 
	}),
	$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
});
</script>
<script type="text/javascript">
	var col;
	function ChangeCol(){
		if(col == 1){
			colors = new Array('#4D3D98', '#0C8B44', '#88288F', '#D24827', '#00A0B1'); /* Colors of the Menu Items */
			body_color = "#1C1651"; /* Background Color of the whole Navigation */
			hover_color = "#352869"; /* Color of the border of the menu items for HOVER*/
			scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
			scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
			scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
			$("div.metro_nav").show();
			$("div.metro_na").hide();
		}else if(col == 2){
			colors = new Array('#4472B8', '#56439B', '#069547', '#B41E43', '#D74F2B');
			body_color = "#1D1D1D"; /* Background Color of the whole Navigation */
			hover_color = "#2D2D2D"; /* Color of the border of the menu items for HOVER*/
			scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
			scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
			scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
			$("div.metro_nav").show();
			$("div.metro_na").hide();
		}else if(col == 3){
			$("div.metro_nav").hide();
			$("div.metro_na").show();
		}
		setColors();
	}
</script>
<script type="text/javascript">
	function e(e, t) {
		return Math.floor(Math.random() * (t - e + 1)) + e
	}
	function toggleButton(){
		if($('.hide-key').is(':visible')){
			$('.hide-key').hide();
		} else {
			$('.hide-key').show();
		}
	}
	function generate(){
		var t = 0;
		$(".progress-bar", ".bars-container").each(function () {
			t = e(10, 100) + "%",
			$(this).css("width", t).html(t)
		});
	}
	function bayar(){
		$('#bayar-hutang').slideDown();
		$('#transaksi-hutang').slideUp();
		$('#daftar-hutang').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function data_transaksi(){
		$('#bayar-hutang').slideUp();
		$('#transaksi-hutang').slideDown();
		$('#daftar-hutang').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function data_hutang(){
		$('#bayar-hutang').slideUp();
		$('#transaksi-hutang').slideUp();
		$('#daftar-hutang').slideDown();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function importButton(){
		$('#bayar-hutang').slideUp();
		$('#transaksi-hutang').slideUp();
		$('#daftar-hutang').slideUp();
		$('#import-div').slideDown();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function exportButton(){
		$('#bayar-hutang').slideUp();
		$('#transaksi-hutang').slideUp();
		$('#daftar-hutang').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideDown();
		$('#sync-div').slideUp();
	}
	function syncButton(){
		$('#bayar-hutang').slideUp();
		$('#transaksi-hutang').slideUp();
		$('#daftar-hutang').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideDown();
		generate();
	}
	function begin(){
		$('#bayar-hutang').slideUp();
		$('#transaksi-hutang').slideUp();
		$('#daftar-hutang').slideDown();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function no_transfer(){
	    var url = "<?php echo base_url(); ?>accounting/_nomor_urut/no_hutang";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split('|');
				$("#kode").val(data[0]);
				$("#no-bukti").val(data[1]);
			}
		});
		return false;
	}
	function optKas(){
		var url = "<?php echo base_url(); ?>accounting/_option/opt_akun_kas";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$(".akun-kas").html(msg);
				$('.akun-kas').trigger("chosen:updated");
			}
		});
		return false;
	}
	function edit(Object){
		alert(Object);
	}
	$(document).ready(function() {
		no_transfer();
		optKas();
		$("#faktur").blur(function() {
			var url = "<?php echo base_url(); ?>accounting/hutang/faktur";
			var faktur = $("#faktur").val();
			var form_data = {
				faktur:faktur
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					data = msg.split('|');
					$("#tanggal-faktur").val(data[0]);
					$("#penerima").val(data[1]);
				}
			});
			return false;
		});
		$("#form-hutang").submit(function() {
			/* Act on the event */
			var url = "<?php echo base_url(); ?>accounting/hutang/insert";
			var kode = $("#kode").val();
			var referensi = $("#no-bukti").val();
			var akun_kas = $("#akun-kas").val();
			var tanggal = $("#tanggal").val();
			var keterangan = $("#keterangan").val();
			var faktur = $("#faktur").val();
			var nominal = $("#nominal").val();
			var denda = $("#nominal-denda").val();
			var form_data = {
				kode:kode,
				referensi:referensi,
				akun_kas:akun_kas,
				tanggal:tanggal,
				keterangan:keterangan,
				faktur:faktur,
				nominal:nominal,
				denda:denda
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					if(msg==0){
						alert('gagal');
					} else if(msg==1){
						alert('berhasil');
						clear();
					} else {
						alert(msg);
					}
				}
			});
			return false;
		});
		function clear(){
			no_transfer();
			optKas();
			$("#tanggal").val('');
			$("#keterangan").val('');
			$("#faktur").val('');
			$("#nominal").val('0');
			$("#nominal-denda").val('0');
			$("#tanggal-faktur").val('');
			$("#penerima").val('');
		}
	});
</script>