<a href="javascript:void(0)" id="to-top"><i class="icon-angle-up"></i></a>
<script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main-1.03.js"></script>
<script>
$(function () {
	$("#modal-compose").on("shown.bs.modal", function () {
		$(".modal-select-chosen").chosen({
			width : "100%"
		})
	}),
	$(".ms-message-list").slimScroll({
		height : 610,
		color : "#000000",
		size : "3px",
		touchScrollStep : 750
	});
	var e = $(".ms-categories");
	$("a", e).click(function () {
		$("li", e).removeClass("active"),
		$(this).parent().addClass("active"),
		$(this).data("cat")
	});
	begin();
});
</script>
<script type="text/javascript">
	var col;
	function ChangeCol(){
		if(col == 1){
	colors = new Array('#4D3D98', '#0C8B44', '#88288F', '#D24827', '#00A0B1'); /* Colors of the Menu Items */
	body_color = "#1C1651"; /* Background Color of the whole Navigation */
	hover_color = "#352869"; /* Color of the border of the menu items for HOVER*/
	scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
	scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
	scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
	$("div.metro_nav").show();
	$("div.metro_na").hide();
		}else if(col == 2){
	colors = new Array('#4472B8', '#56439B', '#069547', '#B41E43', '#D74F2B');
	body_color = "#1D1D1D"; /* Background Color of the whole Navigation */
	hover_color = "#2D2D2D"; /* Color of the border of the menu items for HOVER*/
	scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
	scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
	scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
	$("div.metro_nav").show();
	$("div.metro_na").hide();
		}else if(col == 3){
			$("div.metro_nav").hide();
			$("div.metro_na").show();
		}
		setColors();
	}
</script>
<script type="text/javascript">
	function e(e, t) {
		return Math.floor(Math.random() * (t - e + 1)) + e
	}
	function toggleButton(){
		if($('.hide-key').is(':visible')){
			$('.hide-key').hide();
		} else {
			$('.hide-key').show();
		}
	}
	function generate(){
		var t = 0;
		$(".progress-bar", ".bars-container").each(function () {
			t = e(10, 100) + "%",
			$(this).css("width", t).html(t)
		});
	}
	function transfer(){
		$('#transfer-kas').slideDown();
		$('#kas-masuk').slideUp();
		$('#list-masuk').slideUp();
		$('#kas-keluar').slideUp();
		$('#list-keluar').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
		no_transfer();
	}
	function kas_masuk(){
		$('#transfer-kas').slideUp();
		$('#kas-masuk').slideDown();
		$('#list-masuk').slideDown();
		$('#kas-keluar').slideUp();
		$('#list-keluar').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
		no_kas_masuk();
	}
	function kas_keluar(){
		$('#transfer-kas').slideUp();
		$('#kas-masuk').slideUp();
		$('#list-masuk').slideUp();
		$('#kas-keluar').slideDown();
		$('#list-keluar').slideDown();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
		no_kas_keluar();
	}
	function importButton(){
		$('#transfer-kas').slideUp();
		$('#kas-masuk').slideUp();
		$('#list-masuk').slideUp();
		$('#kas-keluar').slideUp();
		$('#list-keluar').slideUp();
		$('#import-div').slideDown();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function exportButton(){
		$('#transfer-kas').slideUp();
		$('#kas-masuk').slideUp();
		$('#list-masuk').slideUp();
		$('#kas-keluar').slideUp();
		$('#list-keluar').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideDown();
		$('#sync-div').slideUp();
	}
	function syncButton(){
		$('#transfer-kas').slideUp();
		$('#kas-masuk').slideUp();
		$('#list-masuk').slideUp();
		$('#kas-keluar').slideUp();
		$('#list-keluar').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideDown();
		generate();
	}
	function begin(){
		$('#transfer-kas').slideUp();
		$('#kas-masuk').slideUp();
		$('#list-masuk').slideDown();
		$('#kas-keluar').slideUp();
		$('#list-keluar').slideDown();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function no_transfer(){
	    var url = "<?php echo base_url(); ?>accounting/_nomor_urut/no_transfer_kas";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split('|');
				$("#tr-kode-bukti").val(data[0]);
				$("#tr-no-bukti").val(data[1]);
			}
		});
		return false;
	}
	function no_kas_masuk(){
	    var url = "<?php echo base_url(); ?>accounting/_nomor_urut/no_kas_masuk";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split('|');
				$("#km-kode-bukti").val(data[0]);
				$("#km-no-bukti").val(data[1]);
				kas_masuk_list();
			}
		});
		return false;
	}
	function no_kas_keluar(){
	    var url = "<?php echo base_url(); ?>accounting/_nomor_urut/no_kas_keluar";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split('|');
				$("#kk-kode-bukti").val(data[0]);
				$("#kk-no-bukti").val(data[1]);
				kas_keluar_list();
			}
		});
		return false;
	}
	function optAkun(){
		var url = "<?php echo base_url(); ?>accounting/_option/opt_akun_list";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$(".akun-list").html(msg);
				$('.akun-list').trigger("chosen:updated");
			}
		});
		return false;
	}
	function optKas(){
		var url = "<?php echo base_url(); ?>accounting/_option/opt_akun_kas";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$(".akun-kas").html(msg);
				$('.akun-kas').trigger("chosen:updated");
			}
		});
		return false;
	}
	function kas_masuk_list(){
		var kode = $('#km-no-bukti').val();
		var url = "<?php echo base_url(); ?>accounting/kas/kas_masuk_list/";
		var form_data = {
			kode:kode
		};
		// alert(kode);
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$('#kas-masuk-list').html(msg);
			}
		});
		return false;
	}
	function batalkan_kas_masuk(){
		var kode = $('#km-no-bukti').val();
		var url = "<?php echo base_url(); ?>accounting/kas/batalkan/";
		var form_data = {
			kode:kode
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				if(msg==0){
					alert('Delete gagal');
				} else if(msg==1){
					no_kas_masuk();
					optKas();
					optAkun();
					$("#km-tanggal").val('');
					$("#km-nominal").val('');
					$("#km-keterangan").val('');
					kas_masuk_list();
				} else {
					alert(msg);
				}
			}
		});
		return false;
	}
	function simpan_kas_masuk(){
		var kode = $('#km-no-bukti').val();
		var akun_sumber = $('#km-akun-sumber').val();
		var url = "<?php echo base_url(); ?>accounting/kas/simpan/";
		var form_data = {
			kode:kode,
			akun_kas:akun_sumber,
			form:'Kas Masuk'
		};
		// this bit needs to be loaded on every page where an ajax POST may happen
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				if(msg==0){
					alert('Delete gagal');
				} else if(msg==1){
					no_kas_masuk();
					optKas();
					optAkun();
					$("#km-tanggal").val('');
					$("#km-nominal").val('');
					$("#km-keterangan").val('');
					kas_masuk_list();
				} else {
					alert(msg)
				}
			}
		});
		return false;
	}
	function kas_keluar_list(){
		var kode = $('#kk-no-bukti').val();
		var url = "<?php echo base_url(); ?>accounting/kas/kas_keluar_list/";
		var form_data = {
			kode:kode
		};
		// alert(kode);
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$('#kas-Keluar-list').html(msg);
			}
		});
		return false;
	}
	function batalkan_kas_keluar(){
		var kode = $('#kk-no-bukti').val();
		var url = "<?php echo base_url(); ?>accounting/kas/batalkan/";
		var form_data = {
			kode:kode
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				if(msg==0){
					alert('Delete gagal');
				} else if(msg==1){
					no_kas_keluar();
					optKas();
					optAkun();
					$("#kk-tanggal").val('');
					$("#kk-nominal").val('');
					$("#kk-keterangan").val('');
					kas_keluar_list();
				} else {
					alert(msg);
				}
			}
		});
		return false;
	}
	function simpan_kas_keluar(){
		var kode = $('#kk-no-bukti').val();
		var akun_sumber = $('#kk-akun-sumber').val();
		var url = "<?php echo base_url(); ?>accounting/kas/simpan/";
		var form_data = {
			kode:kode,
			akun_kas:akun_sumber,
			form:'Kas Keluar'
		};
		alert(akun_sumber);
		// this bit needs to be loaded on every page where an ajax POST may happen
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				if(msg==0){
					alert('Delete gagal');
				} else if(msg==1){
					no_kas_keluar();
					optKas();
					optAkun();
					$("#kk-tanggal").val('');
					$("#kk-nominal").val('');
					$("#kk-keterangan").val('');
					kas_keluar_list();
				} else {
					alert(msg)
				}
			}
		});
		return false;
	}
	$(document).ready(function() {
		optAkun();
		optKas();
		$("#form-transfer-kas").submit(function(event) {
			var kode = $("#tr-kode-bukti").val();
			var referensi = $("#tr-no-bukti").val();
			var akun_sumber = $("#tr-akun-sumber").val();
			var tanggal = $("#tr-tanggal").val();
			var akun_tujuan = $("#tr-akun-tujuan").val();
			var nominal = $("#tr-nominal").val();
			var keterangan = $("#tr-keterangan").val();

			var url = "<?php echo base_url(); ?>accounting/kas/transfer_kas/";
			var form_data = {
				kode: kode,
				referensi: referensi,
				akun_sumber: akun_sumber,
				tanggal: tanggal,
				akun_tujuan: akun_tujuan,
				nominal: nominal,
				keterangan: keterangan,

				form:'Transfer Kas'
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					if(msg==0){
						alert('gagal');
					} else if(msg==1){
						alert('berhasil');
						$("#tr-kode-bukti").val('');
						$("#tr-no-bukti").val('');
						$("#tr-tanggal").val('');
						$("#tr-nominal").val('');
						$("#tr-keterangan").val('');
						optKas();
						no_transfer();
					} else {
						alert(msg);
					}
				}
			});
			return false;
		});
		$('#form-kas-masuk').submit(function(){
			var kode = $("#km-kode-bukti").val();
			var referensi = $("#km-no-bukti").val();
			var tanggal = $("#km-tanggal").val();
			var akun_tujuan = $("#km-akun-tujuan").val();
			var nominal = $("#km-nominal").val();
			var keterangan = $("#km-keterangan").val();
			var url = "<?php echo base_url(); ?>accounting/kas/insert/";
			var form_data = {
				kode:kode,
				referensi:referensi,
				tanggal:tanggal,
				akun_tujuan:akun_tujuan,
				nominal:nominal,
				keterangan:keterangan,

				form:'kas masuk'
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					if(msg==0){
						alert('gagal');
					} else if(msg==1){
						kas_masuk_list();
						optAkun();
						$("#km-nominal").val('');
					} else {
						alert(msg);
					}
				}
			});
			return false;
		});
		$('#form-kas-keluar').submit(function(){
			var kode = $("#kk-kode-bukti").val();
			var referensi = $("#kk-no-bukti").val();
			var tanggal = $("#kk-tanggal").val();
			var akun_tujuan = $("#kk-akun-tujuan").val();
			var nominal = $("#kk-nominal").val();
			var keterangan = $("#kk-keterangan").val();
			var url = "<?php echo base_url(); ?>accounting/kas/insert/";
			var form_data = {
				kode:kode,
				referensi:referensi,
				tanggal:tanggal,
				akun_tujuan:akun_tujuan,
				nominal:nominal,
				keterangan:keterangan,

				form:'kas keluar'
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					if(msg==0){
						alert('gagal');
					} else if(msg==1){
						kas_keluar_list();
						optAkun();
						$("#kk-nominal").val('');
					} else {
						alert(msg);
					}
				}
			});
			return false;
		});	
	});
</script>