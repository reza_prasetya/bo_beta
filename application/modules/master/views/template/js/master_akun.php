<a href="javascript:void(0)" id="to-top"><i class="icon-angle-up"></i></a>
<script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main-1.03.js"></script>
<script>
$(function () {
	$("#modal-compose").on("shown.bs.modal", function () {
		$(".modal-select-chosen").chosen({
			width : "100%"
		})
	}),
	$(".ms-message-list").slimScroll({
		height : 610,
		color : "#000000",
		size : "3px",
		touchScrollStep : 750
	});
	var e = $(".ms-categories");
	$("a", e).click(function () {
		$("li", e).removeClass("active"),
		$(this).parent().addClass("active"),
		$(this).data("cat")
	});
	begin();

	webApp.datatables(),
	$("#example-datatable").dataTable({
		aoColumnDefs:[{
			bSortable:!1,
			aTargets:[4]
		}],
		bProcessing:true,
		bServerSide:true,
		sAjaxSource: "<?php echo base_url(); ?>accounting/_datatable/master_akun/",
		iDisplayLength:15,
		aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
		"fnServerData": function(sSource, aoData, fnCallback){
		    $.ajax(
		       	{
		       	  'dataType': 'json',
		       	  'type'  : 'POST',
		       	  'url'    : sSource,
		       	  'data'  : aoData,
		       	  'success' : fnCallback
		       	}
		    );
		} 
	}),
	$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
});
</script>
<script type="text/javascript">
	var col;
	function ChangeCol(){
		if(col == 1){
			colors = new Array('#4D3D98', '#0C8B44', '#88288F', '#D24827', '#00A0B1'); /* Colors of the Menu Items */
			body_color = "#1C1651"; /* Background Color of the whole Navigation */
			hover_color = "#352869"; /* Color of the border of the menu items for HOVER*/
			scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
			scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
			scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
			$("div.metro_nav").show();
			$("div.metro_na").hide();
		}else if(col == 2){
			colors = new Array('#4472B8', '#56439B', '#069547', '#B41E43', '#D74F2B');
			body_color = "#1D1D1D"; /* Background Color of the whole Navigation */
			hover_color = "#2D2D2D"; /* Color of the border of the menu items for HOVER*/
			scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
			scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
			scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
			$("div.metro_nav").show();
			$("div.metro_na").hide();
		}else if(col == 3){
			$("div.metro_nav").hide();
			$("div.metro_na").show();
		}
		setColors();
	}
</script>
<script type="text/javascript">
	function e(e, t) {
		return Math.floor(Math.random() * (t - e + 1)) + e
	}
	function toggleButton(){
		if($('.hide-key').is(':visible')){
			$('.hide-key').hide();
		} else {
			$('.hide-key').show();
		}
	}
	function generate(){
		var t = 0;
		$(".progress-bar", ".bars-container").each(function () {
			t = e(10, 100) + "%",
			$(this).css("width", t).html(t)
		});
	}
	function tambah(){
		$('#master-akun').slideUp();
		$('#tambah-akun').slideDown();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
		$('#konfig-akun').slideUp();
	}
	function konfig(){
		$('#master-akun').slideUp();
		$('#tambah-akun').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
		$('#konfig-akun').slideDown();
		optAkun();
	}
	function importButton(){
		$('#master-akun').slideUp();
		$('#tambah-akun').slideUp();
		$('#import-div').slideDown();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
		$('#konfig-akun').slideUp();
	}
	function exportButton(){
		$('#master-akun').slideUp();
		$('#tambah-akun').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideDown();
		$('#sync-div').slideUp();
		$('#konfig-akun').slideUp();
	}
	function syncButton(){
		$('#master-akun').slideUp();
		$('#tambah-akun').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideDown();
		$('#konfig-akun').slideUp();
		generate();
	}
	function master(){
		$('#master-akun').slideDown();
		$('#tambah-akun').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
		$('#konfig-akun').slideUp();
	}
	function begin(){
		$('#master-akun').slideDown();
		$('#tambah-akun').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
		$('#konfig-akun').slideUp();
	}
	function optAkun(){
		var url = "<?php echo base_url(); ?>accounting/_option/opt_akun_list";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$(".akun-list").html(msg);
				$('.akun-list').trigger("chosen:updated");
			}
		});
		return false;
	}
	function optKlas(){
		// var rekening =  $("#no_rekening").val();
	    var url = "<?php echo base_url(); ?>accounting/_option/opt_klasifikasi";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$("#id_klasifikasi").html(msg);
				$('#id_klasifikasi').trigger("chosen:updated");
			}
		});
		return false;
	}
	function edit(Object){
		alert(Object);
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		optKlas();
	    $("#id_klasifikasi").change(function() {
	    	var klasifikasi =  $("#id_klasifikasi").val();
	    	var url = "<?php echo base_url(); ?>accounting/_option/opt_sub_klasifikasi";
			var form_data = {
				klasifikasi: klasifikasi
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					$("#sub_klasifikasi").html(msg);
					$('#sub_klasifikasi').trigger("chosen:updated");
				}
			});
			return false;
	    });
	    $("#sub_klasifikasi").change(function() {
	    	var klasifikasi =  $("#id_klasifikasi").val();
	    	var sub_klasifikasi =  $("#sub_klasifikasi").val();

	    	var url = "<?php echo base_url(); ?>accounting/master_akun/kode_akun";
			var form_data = {
				klasifikasi: klasifikasi,
				sub_klasifikasi: sub_klasifikasi
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					$("#kode").val(msg);
				}
			});
			return false;
	    });
	} );
</script>