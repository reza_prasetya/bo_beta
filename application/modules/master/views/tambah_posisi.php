<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>/setup"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Menu Setup</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Posisi</p>
							</blockquote>
						</div>
					</div>
					<?php echo $this->session->flashdata('flash_message'); ?>
					<form action="<?php echo base_url().$cname; ?>/tambah" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama</span>
								<input type="hidden" id="id" name="id" value="<?php echo @$val['id']; ?>" class="form-control">
								<input type="text" id="group" name="group" value="<?php echo @$val['group']; ?>" class="form-control">
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Informasi</span>
								<input type="text" id="information" name="information" value="<?php echo @$val['information']; ?>" class="form-control">
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>

						<div class="form-group">
							<!-- <div class="input-group"> -->
							<div class="row">
								<div class="col-sm-2">
									<label style="font-size:14px; font-weight:bold; padding-bottom:0px; margin-bottom:0px;">
										Module
									</label>
									<?php
										foreach ($module->result() as $key => $value) {
										?>
										<div class="checkbox">
											<label for="example-checkbox1">
												<input type="checkbox" name="module[]" onchange="moduleSelect(this.id)" id="<?php echo @$value->module; ?>" value="<?php echo @$value->module; ?>" <?php echo @in_array($value->module, $module_access) ? 'checked' : ''  ?> > <?php echo $value->module; ?>
											</label>
										</div>
										<?php
										}
									?>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<label style="font-size:14px; font-weight:bold; padding-bottom:0px; margin-bottom:0px;">
										Komponen Module
									</label>
								</div>
								<br>
								<div class="col-sm-12">
									<?php
										foreach ($module_list->result() as $key => $value) {
										?>
										<div class="<?php echo $value->module; ?> komponen col-sm-3">
											<label for="example-checkbox1">
												<input type="checkbox" name="produk[]" value="<?php echo @$value->kode; ?>" <?php echo @in_array($value->kode, $access) ? 'checked' : ''  ?> > <?php echo $value->alias; ?>
											</label>
										</div>
										<?php
										}
									?>
								</div>
							</div>
						</div>

						<?php
						if(isset($val['id'])){
							echo form_hidden('id', $val['id']);
						?>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Edit</button>
						</div>
						<?php
						} else {
						?>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Register</button>
						</div>
						<?php
						}
						?>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function begin(){
	var uri = "<?php echo $this->uri->segment(4); ?>";
	if(uri){
		$('.komponen').show();
	} else {
		$('.komponen').hide();
	}
}
function moduleSelect(Object){
	var status = document.getElementById(Object);
	if(status.checked){
		// alertify.alert(status);	
		$('.'+Object).show();
	} else {
		// alertify.alert('unchecked');	
		$('.'+Object).hide();
	}
	
}
$(document).ready(function(){
	begin();
});
</script>