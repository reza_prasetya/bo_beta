<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Pendaftaran Cabang</p>
							</blockquote>
						</div>
					</div>
					<span id="flash_message"></span>

					<form id="addCabang" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Klasifikasi</span>
								<select id="type" name="type" class="form-control select-chosen" data-placeholder="Pilih Klasifikasi">
									<option value=""></option>
									<option value="P">Pusat</option>
									<option value="A">Area</option>
									<option value="C">Cabang</option>
								</select>
								<span class="input-group-addon"><i class="icon-home"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kabupaten</span>
								<select id="regency_code" name="regency_code" class="form-control select-chosen" data-placeholder="Pilih Kabupaten">
									<option value=""></option>
									<?php foreach ($kabupaten as $key => $value): ?>
										<option value="<?php echo $value->code; ?>"><?php echo $value->regency_name; ?></option>
									<?php endforeach ?>
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode Cabang</span>
								<input type="hidden" id="id" name="id" class="form-control" placeholder="ID Cabang">
								<input type="hidden" id="no_urut" name="no_urut" class="form-control">
								<input type="text" id="code" name="code" class="form-control" placeholder="Kode Cabang" readonly>
								<span class="input-group-addon"><i class="icon-home"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama Cabang</span>
								<input type="text" id="name" name="name" class="form-control" placeholder="Nama Cabang">
								<span class="input-group-addon"><i class="icon-home"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Alamat</span>
								<input type="text" id="address" name="address" class="form-control" placeholder="Alamat Cabang">
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">No Telp</span>
								<input type="text" id="telp" name="telp" class="form-control" placeholder="Nomor Telepon Cabang">
								<span class="input-group-addon"><i class="halfling-phone"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Email</span>
								<input type="text" id="email" name="email" class="form-control" placeholder="Email">
								<span class="input-group-addon">@</span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Tanggal Registrasi</span>
								<input type="text" id="registered" name="registered" class="form-control" placeholder="Kode Cabang" readonly value="<?php echo date('Y-m-d'); ?>">
								<span class="input-group-addon"><i class="icon-calendar"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Status</span>
								<select id="status" name="status" class="form-control select-chosen" data-placeholder="Pilih Klasifikasi">
									<option value="1">Aktif</option>
									<option value="0">Tidak Aktif</option>
								</select>
								<span class="input-group-addon"><i class="icon-eye-open"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function begin(){
	var val = "<?php echo $this->uri->segment(3); ?>";
	var id = "<?php echo $this->uri->segment(4); ?>";
	// alert(id);
	if(val=='tambah' && id){
		$("#id").val(id);
		var url = "<?php echo base_url(); ?>master/cabang/detail_cabang";
		// alert('asas');
		var form_data = {
			id: id
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = JSON.parse(msg);
				// $('#id').val(data.id);
				$('#code').val(data.code);
				$('#name').val(data.name);
				$('#address').val(data.address);
				$('#email').val(data.email);
				$('#telp').val(data.telp);
				$('#no_urut').val(data.no_urut);
				$('#regency_code').val(data.regency_code);
				$('#regency_code').trigger('chosen:updated');
				$('#status').val(data.status);
				$('#status').trigger('chosen:updated');
				$('#type').val(data.type);
				$('#type').trigger('chosen:updated');
				$('#registered').val(data.registered);
			}
		});
		return false;
	}
}
function genCode(){
	var url = "<?php echo base_url(); ?>master/cabang/kode_cabang";
	$.ajax({
		type: "POST",
		url: url,
		data: $('#addCabang').serialize(),
		success: function(msg)
		{
			// alert(msg);
			var data = msg.split('|');
			var code = data[0];
			var no = data[1];
			$('#code').val(code);
			$('#no_urut').val(no);
		}
	});
	return false;
}
$(document).ready(function(){
	begin();
	$("#addCabang").submit(function(){
		var url = "<?php echo base_url(); ?>master/cabang/tambah_cabang";
		$.ajax({
			type: "POST",
			url: url,
			data: $('#addCabang').serialize(),
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				if(data[0]==1){
					clear();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	$('#type').on('change',function(){
		var type = $('#type').val();
		if(type=='C'){
			genCode();	
			$('#code').Attr('readonly','readonly');		
		} else if(type=='A'){
			$('#code').removeAttr('readonly');
			$('#code').val('');
		} else if(type=='P'){
			$('#code').val('PUSAT');
		}
	});
	$('#regency_code').on('change',function(){
		var type = $('#type').val();
		if(type=='C'){
			genCode();
			$('#code').Attr('readonly','readonly');
		} else if(type=='A'){
			$('#code').removeAttr('readonly');
			$('#code').val('');
		} else if(type=='P'){
			$('#code').val('PUSAT');
		}
	});
	function clear(){
		$('#id').val('');
		$('#code').val('');
		$('#name').val('');
		$('#address').val('');
		$('#email').val('');
		$('#telp').val('');
		$('#regency_code').val('');
		$('#status').val('');
		$('#type').val('');
		$('#registered').val('');
		$('#no_urut').val('');
	}
});
</script>