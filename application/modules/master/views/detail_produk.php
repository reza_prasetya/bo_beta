<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/cari"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Cari</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="scan-div">
					<div class="row">
						<div class="col-sm-12">
							<blockquote>
								<p><i class="icon-file-text"></i> Detail Data Produk</p>
							</blockquote>
						</div>
					</div>
					<div class="row" style="border-top:1px dotted black;">
						<div class="list-group">
							<a href="javascript:void(0)" class="list-group-item" style="">
							<h4 class="list-group-item-heading">
							<strong><?php echo $val['name']; ?></strong>
							</h4>
							<img src="<?php echo base_url(); ?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-right" width="150px" height="150px" style="margin-right:30px;">
							<p class="list-group-item-text">
							<table id="general-table" class="table table-stripped" style="max-width:80%;">
								<tbody>
									<tr>
										<td>Kode Produk</td>
										<td><?php echo $val['code']; ?></td>
									</tr>
									<tr>
										<td>Kategori</td>
										<td><?php echo $val['category_name']; ?></td>
									</tr>
									<tr>
										<td>Sub Kategori</td>
										<td><?php echo $val['subcategory_name']; ?></td>
									</tr>
									<?php $hpp = $this->mp->get_hpp($val['code']); ?>
									<tr>
										<td>Harga Pokok</td>
										<td><?php echo @format_rupiah($hpp); ?></td>
									</tr>
									<tr>
										<td>Diskon</td>
										<td><?php echo $val['discount']; ?></td>
									</tr>
									<tr>
										<td>Satuan Kemasan</td>
										<td><?php echo $val['unit']; ?></td>
									</tr>
									<?php $qty = json_decode($val['qty']); ?>
									<tr>
										<td>Harga Jual 1</td>
										<td><?php echo format_rupiah($val['price1']); ?> pembelian sampai <?php echo $qty[0]; ?> barang</td>
									</tr>
									<tr>
										<td>Harga Jual 2</td>
										<td><?php echo format_rupiah($val['price2']); ?> pembelian sampai <?php echo $qty[1]; ?> barang</td>
									</tr>
									<tr>
										<td>Harga Jual 3</td>
										<td><?php echo format_rupiah($val['price3']); ?> pembelian diatas <?php echo $qty[2]; ?> barang</td>
									</tr>
								</tbody>
							</table>
							</p>
							<a href="<?php echo base_url().'master/produk/tambah/'.$val['id']; ?>" type="button" class="btn btn-success" style="margin-top:20px;">Edit</a>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>