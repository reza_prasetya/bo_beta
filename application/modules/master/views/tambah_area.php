<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Pendaftaran area</p>
							</blockquote>
						</div>
					</div>
					<span id="flash_message"></span>
					<form id="addarea" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode area</span>
								<input type="hidden" id="id" name="id" class="form-control" placeholder="ID area">
								<input type="hidden" id="no_urut" name="no_urut" class="form-control" placeholder="nomor urut">
								<input readonly type="text" id="code" name="code" class="form-control" placeholder="Kode area">
								<span class="input-group-addon"><i class="icon-user"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama area</span>
								<input type="text" id="area_name" name="area_name" class="form-control" placeholder="Nama area">
								<span class="input-group-addon"><i class="icon-user"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama DC</span>
								<input readonly type="text" id="dc_name" name="dc_name" class="form-control" placeholder="Nama area">
								<span class="input-group-addon"><i class="icon-user"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Registered</span>
								<input readonly type="text" id="registered" name="registered" class="form-control" placeholder="Tanggal Registrasi Area" value="<?php echo date('Y-m-d'); ?>">
								<span class="input-group-addon"><i class="icon-user"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Status</span>
								<select id="status" name="status" class="form-control" data-placeholder="Pilih Klasifikasi">
									<option value="1">Aktif</option>
									<option value="0">Tidak Aktif</option>
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function begin(){
	var val = "<?php echo $this->uri->segment(3); ?>";
	var id = "<?php echo $this->uri->segment(4); ?>";
	// alert(id);
	if(val=='tambah' && id){
		$("#id").val(id);
		var url = "<?php echo base_url(); ?>master/area/detail_area";
		// alert('asas');
		var form_data = {
			id: id
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = JSON.parse(msg);
				// $('#id').val(data.id);
				$('#code').val(data.code);
				$('#no_urut').val(data.no_urut);
				$('#area_name').val(data.area_name);
				$('#dc_name').val(data.dc_name);
				$('#registered').val(data.registered);
				$('#status').val(data.status);
				$('#status').trigger('chosen:updated');
			}
		});
		return false;
	}
	else {
		var url = "<?php echo base_url(); ?>master/area/gen_code";
		// alert('asas');
		var form_data = {
			id: id
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = msg.split('|');
				$("#code").val(data[0]);
				$("#no_urut").val(data[1]);
				code = data[0];
				code = code.split('-');
				$("#dc_name").val('DC-'+code[0]+code[1]);
				$("#area_name").focus();
			}
		});
		return false;
	}
}
$(document).ready(function(){
	begin();
	$("#addarea").submit(function(){
		var url = "<?php echo base_url(); ?>master/area/tambah_area";
		$.ajax({
			type: "POST",
			url: url,
			data: $('#addarea').serialize(),
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				if(data[0]==1){
					clear();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	function clear(){
		$('#id').val('');
		$('#code').val('');
		$('#area_name').val('');
	}
});
</script>