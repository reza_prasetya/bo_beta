<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname.'/data'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Master akun</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname.'/tambah'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Tambah Akun</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Tambah Rekening</p>
							</blockquote>
						</div>
					</div>
					<span id="flash_message" style="display:none"></span>
					<form id="formRekening" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Parent Rekening</span>
								<select id="parent" name="parent" class="select-chosen form-control" data-placeholder="Pilih Rekening">
									
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Jenis</span>
								<select id="position" name="position" class="select-chosen form-control" data-placeholder="Pilih Jenis">
									<option value="">Pilih Jenis</option>
									<option value="header">Header</option>
									<option value="detail">Detail</option>
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode Akun</span>
								<input type="text" id="code" name="code" class="form-control" placeholder="Kode Akun" readonly>
								<input type="hidden" id="id" name="id" class="form-control">
								<input type="hidden" id="code_ref" name="code_ref" class="form-control">
								<input type="hidden" id="urut" name="urut" class="form-control">
								<input type="hidden" id="space" name="space" class="form-control">
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama Akun</span>
								<input type="text" id="name" name="name" class="form-control" placeholder="Nama Akun">
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Keterangan</span>
								<select id="keterangan" name="keterangan" class="select-chosen form-control" data-placeholder="Pilih Keterangan">
									<option value="">Pilih Keterangan</option>
									<option value="neraca">Neraca</option>
									<option value="L/R">L/R</option>
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function begin(){
	optRekening();
	var val = '<?php echo $this->uri->segment(4); ?>';
	if(val){
		// alert(val);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url($this->cname).'/get_data'; ?>",
			data: {id:val},
			success: function(msg)
			{
				// alert(msg);
				data = JSON.parse(msg);
				$('#id').val(data.id);
				$('#parent').val(data.parent);
				$('#parent').trigger('chosen:updated');

				$('#position').val(data.position);
				$('#position').trigger('chosen:updated');

				$('#keterangan').val(data.keterangan);
				$('#keterangan').trigger('chosen:updated');

				$('#code').val(data.code);
				$('#name').val(data.name);
				$('#code_ref').val(data.code_ref);
				$('#urut').val(data.urut);
				$('#space').val(data.space);
			}
		});
		return false;
	}
}
function optRekening(){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url($this->cname).'/get_opt_rekening'; ?>",
		data: {},
		success: function(msg)
		{
			// alert(msg);
			$('#parent').html(msg);
			$('#parent').trigger('chosen:updated');
		}
	});
	return false;
}
function genCode(){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url($this->cname).'/gen_code'; ?>",
		data: {code:$('#parent').val()},
		success: function(msg)
		{
			// alert(msg);
			data = JSON.parse(msg);
			$('#code').val(data.code);
			$('#space').val(data.space);
			$('#urut').val(data.urut);
			$('#code_ref').val(data.code_ref);
		}
	});
	return false;
}
function headCode(){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url($this->cname).'/head_code'; ?>",
		data: {},
		success: function(msg)
		{
			// alert(msg);
			data = JSON.parse(msg);
			$('#code').val(data.code);
			$('#space').val(data.space);
			$('#urut').val(data.urut);
			$('#code_ref').val(data.code_ref);
		}
	});
	return false;
}
function customCode(){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url($this->cname).'/custom_code'; ?>",
		data: {code:$('#code').val()},
		success: function(msg)
		{
			// alert(msg);
			data = JSON.parse(msg);
			$('#code').val(data.code);
			$('#space').val(data.space);
			$('#urut').val(data.urut);
			$('#code_ref').val(data.code_ref);
		}
	});
	return false;
}
$(document).ready(function(){
	begin();
	$('#parent').change(function(){
		var parent = $('#parent').val();
		if(parent == ''){
			$('#code').removeAttr('readonly');
			// $('#code').val('');
			headCode();			
		} else {
			genCode();
			$('#code').attr('readonly','readonly');
		}
	});
	$('#position').change(function(){
		var parent = $('#parent').val();
		if(parent == ''){
			$('#code').removeAttr('readonly');
			// $('#code').val('');
			headCode();
		} else {
			genCode();
			$('#code').attr('readonly','readonly');
		}
	});
	$('#code').on('blur', function(){
		var parent = $('#parent').val();
		if(parent == ''){
			customCode();
		}
	});
	$('#formRekening').submit(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url($this->cname).'/ins_rekening'; ?>",
			data: $('#formRekening').serialize(),
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				if(data[0]==1){
					clear();
					optRekening();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	function clear(){
		$('#parent').val('');
		$('#parent').trigger('chosen:updated');

		$('#position').val('');
		$('#position').trigger('chosen:updated');

		$('#keterangan').val('');
		$('#keterangan').trigger('chosen:updated');

		$('#code').val('');
		$('#name').val('');
		$('#code_ref').val('');
		$('#urut').val('');
		$('#space').val('');
	}
});
</script>