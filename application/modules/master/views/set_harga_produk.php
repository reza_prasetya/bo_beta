<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/harga_produk/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/harga_produk/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<!-- <li>
							<a href="<?php echo base_url().$cname; ?>/cari"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Cari</span></a>
						</li> -->
						<li class="pull-right">
							<a href="<?php echo base_url().$cname; ?>/menu"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Menu Produk</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<!-- Start Content -->
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="list-div">
					<div class="row">
						<div class="col-sm-12">
							<blockquote>
								<p><i class="icon-file-text"></i> Data Harga Produk</p>
							</blockquote>
						</div>
					</div>
					<div class="table-responsive">
						<table id="example-datatable" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>Kode Produk</th>
									<th>Nama Produk</th>
									<th>Harga Jual 1</th>
									<th>Harga Jual 2</th>
									<th>Harga Jual 3</th>
									<th width="100px"></th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<span id="flash_message"></span>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">Search</span>
							<input type="text" id="search" name="search" class="form-control" placeholder="Search">
							<span class="input-group-addon"><i class="icon-key"></i></span>
						</div>
					</div>
					<form id="addProduk" method="post" class="form-horizontal" onSubmit="return false;">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode Produk</span>
								<input type="text" id="kode_produk" name="code" class="form-control" placeholder="Kode Produk" readonly>
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama Produk</span>
								<input type="text" id="nama_produk" name="name" class="form-control" placeholder="Nama Produk" readonly>
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Harga Pokok</span>
								<input type="text" id="harga_pokok" name="harga_pokok" class="form-control" placeholder="Harga Pokok" readonly>
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Harga Jual 1</span>
								<input type="text" id="price1" name="price1" class="form-control" placeholder="Harga Jual 1">
								<input type="text" id="qty1" name="qty1" class="form-control span3" placeholder="Jumlah Maksimal Harga 1">
								<span class="input-group-addon"><i class="icon-money"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Harga Jual 2</span>
								<input type="text" id="price2" name="price2" class="form-control" placeholder="Harga Jual 2">
								<input type="text" id="qty2" name="qty2" class="form-control span3" placeholder="Jumlah Maksimal Harga 2">
								<span class="input-group-addon"><i class="icon-money"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Harga Jual 3</span>
								<input type="text" id="price3" name="price3" class="form-control" placeholder="Harga Jual 3">
								<input type="text" id="qty3" name="qty3" class="form-control span3" placeholder="Jumlah Maksimal Harga 3" readonly>
								<span class="input-group-addon"><i class="icon-money"></i></span>
							</div>
						</div>
						<div class="form-group">
							<!-- <input type="hidden" name="edit" id="edit"/> -->
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- End Content -->
	</div>
</div>
<script type="text/javascript">
$(function(){
	// $('.coba').html(':p');
	webApp.datatables(),
	$("#example-datatable").dataTable({
		aoColumnDefs:[{
			bSortable:!1,
			aTargets:[4]
		}],
		bProcessing:true,
		bServerSide:true,
		sAjaxSource: "<?php echo base_url(); ?>master/_datatable/product_price/",
		iDisplayLength:15,
		aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
		"fnServerData": function(sSource, aoData, fnCallback){
		    $.ajax(
		       	{
		       	  'dataType': 'json',
		       	  'type'  : 'POST',
		       	  'url'    : sSource,
		       	  'data'  : aoData,
		       	  'success' : fnCallback
		       	}
		    );
		} 
	}),
	$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
});
</script>
<script type="text/javascript">
function begin(){
	var one = "<?php echo $this->uri->segment(4); ?>";
	var two = "<?php echo $this->uri->segment(5); ?>";
	if(one == 'tambah' && two){
		$("#tambah-div").show();
		$("#list-div").hide();
		var url = "<?php echo base_url(); ?>master/produk/detail_produk";
		var data_post = {id:two};
		$.ajax({
			type: "POST",
			url: url,
			data: data_post,
			success: function(msg)
			{
				data = JSON.parse(msg);
				$('#kode_produk').val(data.code);
				$('#nama_produk').val(data.name);
				$('#harga_pokok').val(data.hpp);
				qty = JSON.parse(data.qty);
				$('#price1').val(data.price1);
				$('#qty1').val(qty[0]);
				$('#price2').val(data.price2);
				$('#qty2').val(qty[1]);
				$('#price3').val(data.price3);
				$('#qty3').val(qty[2]);
			}
		});
		return false;
	} else if(one == 'tambah'){
		$("#tambah-div").show();
		$("#list-div").hide();
	} else if(one == 'data'){
		$("#tambah-div").hide();
		$("#list-div").show();
	} else {
		window.location = "<?php echo base_url().$cname ?>/harga_produk/data";
	}
}
function IsNumeric(input)
{
    return (input - 0) == input && (''+input).replace(/^\s+|\s+$/g, "").length > 0;
}
$(document).ready(function(){
	begin();
	$("#search").on('keyup',function(){
		var search = $("#search").val();
		var url = "<?php echo base_url(); ?>master/produk/detail_search_produk";
		var form_data = {
			key: search
		};
		// alert(search);
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				var data = JSON.parse(msg);
				var qty = ""+JSON.parse(data.qty);
				var dataqty = qty.split(",");

				$("#kode_produk").val(data.code);
				$("#nama_produk").val(data.name);
				
				$("#price1").val(data.price1);
				$("#price2").val(data.price2);
				$("#price3").val(data.price3);
				$("#harga_pokok").val(data.hpp);
				$("#qty1").val(dataqty[0]);
				$("#qty2").val(dataqty[1]);
				$("#qty3").val(dataqty[2]);
				$("#price1").focus();
			}
		});
		return false;
		// alert(search);
	});
	$("#addProduk").submit(function(){
		var url = "<?php echo base_url(); ?>master/produk/set_harga_produk";
		var data_post = $('#addProduk').serializeArray();
		$.ajax({
			type: "POST",
			url: url,
			data: data_post,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				if(data[0]==1){
					clear();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				// $("#flash_message").html(msg);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	function clear(){
		$("#search").val('');
		$("#kode_produk").val('');
		$("#nama_produk").val('');
		$("#price1").val('');
		$("#price2").val('');
		$("#price3").val('');
		$("#qty1").val('');
		$("#qty2").val('');
		$("#qty3").val('');
		$("#harga_pokok").val('');
	}

	$('#qty2').change(function(){
		var qty1 = parseInt($('#qty1').val());
		var qty2 = parseInt($('#qty2').val());
		if (qty2 <= qty1) {
			alert('Jumlah maksimal harga 2 harus lebih besar dari jumlah maksimal harga 1');
			$('#qty2').val('');
			$('#qty3').val('');
		}else{
			var qty3 = qty2 + 1;
			$('#qty3').val(qty3);
		}
	});
});
</script>