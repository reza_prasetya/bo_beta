<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block" style="height: 150px; border-bottom-left-radius: 3px; border-bottom-right-radius: 3px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/area"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Setup area</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/produk"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Setup Barang</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().'master'; ?>/hak_akses"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Setup Akses</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().'master/produk'; ?>/blended"><img src="<?php echo base_url(); ?>/public/images/icon/blended.png" /><span>Blended</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>