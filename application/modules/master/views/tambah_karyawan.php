<?php $user = $this->session->userdata('basmalahsession'); ?>
<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/cari"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Cari</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Pendaftaran Karyawan</p>
							</blockquote>
						</div>
					</div>
					<span id="flash_message"></span>
					<form id="formKaryawan" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Tahun Masuk</span>
								<input type="text" id="tahun" name="tahun" value="<?php echo @$val['nama']; ?>" class="form-control">
								<span class="input-group-addon"><i class="icon-calendar"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Status Alumni</span>
								<select id="status_alumni" name="status_alumni" class="form-control">
									<option value="1">Alumni (PP Sidogiri)</option>
									<option value="0">Non Alumni</option>
								</select>
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Tgl Lahir</span>
								<input id="tgl_lahir" name="tgl_lahir" class="form-control input-datepicker text-center" data-date-format="yyyy-mm-dd" placeholder="Tgl Lahir" type="text">
								<span class="input-group-addon"><i class="icon-calendar"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">NIK</span>
								<input type="text" id="nik" name="nik" class="form-control" readonly>
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Jabatan</span>
									<?php echo form_dropdown('group', @$cat, @$val['group'], 'id="group" class="form-control" size="1"'); ?>
									<span class="input-group-addon"><i class="icon-star"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Nama</span>
									<input type="text" id="nama" name="nama" class="form-control">
									<span class="input-group-addon"><i class="icon-user"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">No KTP</span>
									<input type="text" id="no_ktp" name="no_ktp" class="form-control">
									<span class="input-group-addon"><i class="icon-user"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Alamat</span>
									<input type="text" id="alamat" name="alamat" class="form-control">
									<span class="input-group-addon"><i class="icon-credit-card"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">No Telp</span>
									<input type="text" id="telp" name="telp" class="form-control">
									<span class="input-group-addon"><i class="icon-phone"></i></span>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Email</span>
									<input type="text" id="email" name="email" class="form-control">
									<span class="input-group-addon"><i class="icon-envelope"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Username</span>
									<input type="text" id="uname" name="uname" class="form-control">
									<span class="input-group-addon"><i class="icon-key"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Password</span>
									<input type="password" id="upass" name="upass" class="form-control">
									<span class="input-group-addon"><i class="icon-key"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Confirm Password</span>
									<input type="password" id="confirm-upass" name="confirm-upass" class="form-control">
									<span class="input-group-addon"><i class="icon-key"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Status</span>
									<select id="status" name="status" class="form-control">
										<option value="1">Aktif</option>
										<option value="0">Non Aktif</option>
									</select>
									<span class="input-group-addon"><i class="icon-eye-open"></i></span>
								</div>
							</div>
							<input type="hidden" name="id" id="id"/>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function begin () {
	var id = '<?php echo $this->uri->segment(4); ?>';
	if(id){
		// alert(id);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url($this->cname).'/get_data'; ?>",
			data: {id:id},
			success: function(msg)
			{
				// alert(msg);
				data = JSON.parse(msg);
				$('#tahun').val(data.tahun);
				$('#status_alumni').val(data.status_alumni);
				$('#tgl_lahir').val(data.tgl_lahir);
				$('#nik').val(data.nik);
				$('#nama').val(data.nama);
				$('#no_ktp').val(data.no_ktp);
				$('#alamat').val(data.alamat);
				$('#telp').val(data.telp);
				$('#email').val(data.email);
				$('#status').val(data.status);
				$('#id').val(data.id);
				$('#uname').val(data.uname);
				$('#upass').val(data.upass);
				$('#confirm-upass').val(data.upass);
				$('#group').val(data.group);

			}
		});
		return false;
	}
}
$(document).ready(function(){
	begin();
	$('#tgl_lahir').change(function(){
		// alert('sip');
		var id = $('#id').val();
		if(id==''){
			// alert('sip');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url($this->cname).'/get_code'; ?>",
				data: {tahun: $('#tahun').val(), status_alumni: $('#status_alumni').val(), tgl_lahir: $('#tgl_lahir').val()},
				success: function(msg)
				{
					// alert(msg); 
					$('#nik').val(msg);
				}
			});
			return false;
		}
	});
	$('#tahun').change(function(){
		// alert('sip');
		var id = $('#id').val();
		if(id==''){
			// alert('sip');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url($this->cname).'/get_code'; ?>",
				data: {tahun: $('#tahun').val(), status_alumni: $('#status_alumni').val(), tgl_lahir: $('#tgl_lahir').val()},
				success: function(msg)
				{
					// alert(msg); 
					$('#nik').val(msg);
				}
			});
			return false;
		}
	});
	$('#status_alumni').change(function(){
		// alert('sip');
		var id = $('#id').val();
		if(id==''){
			// alert('sip');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url($this->cname).'/get_code'; ?>",
				data: {tahun: $('#tahun').val(), status_alumni: $('#status_alumni').val(), tgl_lahir: $('#tgl_lahir').val()},
				success: function(msg)
				{
					// alert(msg); 
					$('#nik').val(msg);
				}
			});
			return false;
		}
	});

	$('#formKaryawan').submit(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url($this->cname).'/ins_karyawan'; ?>",
			data: $('#formKaryawan').serialize(),
			success: function(msg)
			{
				data = msg.split("|");
				if(data[0]==1){
					clear();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});

	function clear () {
		$('#tahun').val('');
		$('#status_alumni').val('');
		$('#tgl_lahir').val('');
		$('#nik').val('');
		$('#nama').val('');
		$('#no_ktp').val('');
		$('#alamat').val('');
		$('#telp').val('');
		$('#email').val('');
		$('#status').val('');
		$('#id').val('');
		$('#uname').val('');
		$('#upass').val('');
		$('#confirm-upass').val('');
		$('#group').val('');
	}
});
</script>