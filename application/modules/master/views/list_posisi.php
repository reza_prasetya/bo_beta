<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>/setup"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Menu Setup</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="master-akun">
					<div class="row">
						<div class="col-sm-12">
							<blockquote>
								<p><i class="icon-file-text"></i> Data Karyawan</p>
							</blockquote>
						</div>
					</div>
					<div class="table-responsive">
						<table id="example-datatable" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">ID</th>
									<th>Nama</th>
									<th>Keterangan</th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		webApp.datatables(),
		$("#example-datatable").dataTable({
			aoColumnDefs:[{
				bSortable:!1,
				aTargets:[0]
			}],
			bProcessing:true,
			bServerSide:true,
			sAjaxSource: "<?php echo base_url(); ?>master/_datatable/posisi/",
			iDisplayLength:15,
			aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
			"fnServerData": function(sSource, aoData, fnCallback){
			    $.ajax(
			       	{
			       	  'dataType': 'json',
			       	  'type'  : 'POST',
			       	  'url'    : sSource,
			       	  'data'  : aoData,
			       	  'success' : fnCallback
			       	}
			    );
			} 
		}),
		$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
	});
</script>