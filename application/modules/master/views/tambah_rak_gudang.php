<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/cari"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Cari</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Pendaftaran Rak</p>
							</blockquote>
						</div>
					</div>
					<span id="flash_message" style="display:none"></span>
					<form id="formRak" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Jenis</span>
								<select id="jenis" name="jenis" class="form-control">
									<option value="">Pilih Rak</option>
									<option value="T">Toko</option>
									<option value="G">Gudang</option>
								</select>
								<input type="hidden" id="status" name="status">
								<span class="input-group-addon"><i class="icon-home"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode</span>
								<input type="text" id="kode" name="kode" class="form-control" readonly>
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama</span>
								<input type="text" id="nama" name="nama" class="form-control">
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Keterangan</span>
								<input type="text" id="keterangan" name="keterangan" class="form-control">
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<input type="hidden" id="id" name="id">
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function begin (argument) {
	var id = '<?php echo $this->uri->segment(4); ?>';
	if(id){
		$('#jenis').attr("disabled", true);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url($this->cname).'/get_data'; ?>",
			data: {id:id},
			success: function(msg)
			{
				// alert(msg);
				data = JSON.parse(msg);
				$('#id').val(data.id);
				$('#jenis').val(data.jenis);
				$('#kode').val(data.kode);
				$('#nama').val(data.nama);
				$('#keterangan').val(data.keterangan);
			}
		});
		return false;
	}
}
$(document).ready(function(){
	begin();
	$('#jenis').change(function(){
		// alert('sip');
		var id = $('#id').val();
		if(id==''){
			// alert('sip');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url($this->cname).'/get_code'; ?>",
				data: {jenis: $('#jenis').val()},
				success: function(msg)
				{
					// alert(msg);
					if($('#jenis').val()=='T'){
						$('#status').val('toko');
					} else if($('#jenis').val()=='R'){
						$('#status').val('gudang');
					}
					$('#kode').val(msg);
				}
			});
			return false;
		}
	});
	$('#formRak').submit(function(){
		// alert(sip);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url($this->cname).'/ins_rak'; ?>",
			data: $('#formRak').serialize(),
			success: function(msg)
			{
				data = msg.split("|");
				if(data[0]==1){
					clear();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	function clear() {
		$('#id').val('');
		$('#jenis').val('');
		$('#kode').val('');
		$('#nama').val('');
		$('#keterangan').val('');
	}
});
</script>