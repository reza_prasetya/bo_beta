<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname ?>/diskon/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Multi Diskon</span></a>
						</li>
						<!-- <li>
							<a href="<?php echo base_url().$cname ?>/diskon/cari"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Single Diskon</span></a>
						</li> -->
						<li>
							<a href="<?php echo base_url().$cname ?>/diskon/daftar"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar Diskon</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().$cname; ?>/menu"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Menu Produk</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="multi-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Diskon Produk</p>
							</blockquote>
						</div>
					</div>
					<span id="flash_message_multi"></span>
					<form id="addMultiDiskon" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Pilih Produk</span>
								<select id="products" name="products[]" class="select-chosen form-control" data-placeholder="Pilih Produk" multiple>
									
								</select>
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Diskon</span>
								<input type="text" id="diskon" name="diskon" class="form-control" placeholder="Diskon Produk">
								<span class="input-group-addon">%</span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
				<div class="block full" style="margin-top:10px;" id="search-div">
					<div class="row">
						<div class="block-section">
							<span id="notif"></span>
							<form id="form-search" method="post">
								<div class="input-group input-group-lg">
									<div class="input-group-btn">
										<select id="category" name="category" class="form-control" size="1" style="width: 200px; height: 45px; font-size: 17px; background-color: #D8D8D8;">
											<option value="">Pilih Filter</option>
											<option value="name">Nama</option>
											<option value="code">Kode</option>
										</select>
									</div>
									<input id="keyword" name="keyword" class="form-control" placeholder="Search Keyword.." type="text">
									<div class="input-group-btn">
										<button type="submit" class="btn btn-default"><i class="icon-search icon-fixed-width"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-top: -30px;">
							<h4 class="sub-header">SEARCH RESULT</h4>
							<div class="ms-message-list list-group" id="result">
								<a href="javascript:void(0)" class="list-group-item" >
									<img src="<?php echo base_url(); ?>public/img/template/avatar2.jpg" alt="Avatar" class="media-object pull-left" width="50" height="50">
									<h4 class="list-group-item-heading" style="padding-left:60px; max-width:70%;">Tidak Ada Hasil Pencarian</h4>
									<h5 class="list-group-item-heading" style="padding-left:60px; min-height: 22px;">Silahkan Anda masukkankan kata kunci pada kolom search.</h5>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="block full" style="margin-top:10px;" id="list-div">
					<div class="row">
						<div class="col-sm-12">
							<blockquote>
								<p><i class="icon-file-text"></i> Data Produk Diskon</p>
							</blockquote>
						</div>
					</div>
					<div class="table-responsive">
						<table id="example-datatable" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px" class="text-center">No</th>
									<th width="200px">Kode</th>
									<th>Nama Produk</th>
									<th width="100px">Diskon</th>
									<th width="70px" class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		webApp.datatables(),
		$("#example-datatable").dataTable({
			// aoColumnDefs:[{
			// 	bSortable:!1,
			// 	aTargets:[4]
			// }],
			fnDrawCallback: function ( oSettings ) {
			/* Need to redo the counters if filtered or sorted */
			if ( oSettings.bSorted || oSettings.bFiltered )
			{
				for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
				{
					$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
				}
			}
		},
		aoColumnDefs: [
			{ "bSortable": false, "aTargets": [ 0,4] }
		],
			bProcessing:true,
			bServerSide:true,
			sAjaxSource: "<?php echo base_url(); ?>master/_datatable/product_discount/",
			iDisplayLength:15,
			aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
			"fnServerData": function(sSource, aoData, fnCallback){
			    $.ajax(
			       	{
			       	  'dataType': 'json',
			       	  'type'  : 'POST',
			       	  'url'    : sSource,
			       	  'data'  : aoData,
			       	  'success' : fnCallback
			       	}
			    );
			} 
		}),
		$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
	});
</script>
<script type="text/javascript">
function begin(){
	var one = "<?php echo $this->uri->segment(4); ?>";
	var two = "<?php echo $this->uri->segment(5); ?>";
	if(one == 'tambah' && !two){
		$("#multi-div").show();
		$("#search-div").hide();
		$("#list-div").hide();
		optProduct();
	} else if(one == 'tambah' && two){
		$("#multi-div").show();
		$("#search-div").hide();
		$("#list-div").hide();
		optProduct();

		var url = "<?php echo base_url(); ?>master/produk/detail_produk";
		var form_data = {
			id:two
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = JSON.parse(msg);
				$("#products").val(data.code);
				$("#products").trigger("chosen:updated");
				$("#diskon").val(data.discount);
			}
		});
		return false;
	} else if(one == 'daftar'){
		$("#multi-div").hide();
		$("#search-div").hide();
		$("#list-div").show();
	} else if(one == "cari") {
		$("#multi-div").hide();
		$("#search-div").show();
		$("#list-div").hide();
	} else {
		window.location = "<?php echo base_url().$cname ?>/diskon/daftar";
	}
}
function IsNumeric(input)
{
    return (input - 0) == input && (''+input).replace(/^\s+|\s+$/g, "").length > 0;
}
function optProduct(){
	// var obj = Object;
	var url = "<?php echo base_url(); ?>master/produk/opt_products_code";
	$.ajax({
		type: "POST",
		url: url,
		data: {},
		success: function(msg)
		{
			// alert(msg);
			$("#products").html(msg);
			$("#products").trigger("chosen:updated");
		}
	});
	return false;
}
$(document).ready(function(){
	begin();
	$("#form-search").submit(function(){
		var category = $('#category').val();
		var keyword = $('#keyword').val();
		var url = "<?php echo base_url(); ?>master/produk/mencari_diskon";
		var form_data = {
			category: category,
			keyword: keyword
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split('|');
				if(data[0]==1) {
					$("#notif").hide()
					$("#result").html(data[1]);
				} else {
					$("#notif").show()
					$("#result").html(data[1])
				}
			}
		});
		return false;
	});
	$("#keyword").keyup(function(){
		var category = $('#category').val();
		var keyword = $('#keyword').val();
		var url = "<?php echo base_url(); ?>master/produk/mencari_diskon";
		var form_data = {
			category: category,
			keyword: keyword
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split('|');
				if(data[0]==1) {
					$("#notif").hide()
					$("#result").html(data[1]);
				} else {
					$("#notif").show()
					$("#result").html(data[1])
				}
			}
		});
		return false;
	});
	$("#addMultiDiskon").submit(function(){
		var produk = $("#products").val();
		var diskon = $("#diskon").val();
		var url = "<?php echo base_url(); ?>master/produk/diskon_multi";
		// alert(produk);
		var form_data = {
			produk:produk,
			discount:diskon
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				$("#flash_message_multi").show();
				$("#flash_message_multi").html(data[1]);
				if(data[0]==1){
					clear("multi");
				}
				setTimeout(function() {$("#flash_message_multi").hide();}, 1500);
				
			}
		});
		return false;
	});
	function clear(Object){
		if(Object=="multi"){
			$("#products").val('');
			$("#products").trigger('chosen:updated');
			$("#diskon").val('');
		} else if(Object=="single"){
			$("#code_product").val('');
			$("#discount").val('');
		}
	}
});
</script>