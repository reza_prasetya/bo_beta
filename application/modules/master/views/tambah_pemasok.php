<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/cari"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Cari</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Pendaftaran Pemasok</p>
							</blockquote>
						</div>
					</div>
					<span id="flash_message" style="display:none"></span>
					<form id="formPemasok" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Jenis</span>
								<select id="type" name="type" class="form-control" placeholder="Pilih Jenis">
									<!-- <option value="">Pilih Jenis</option> -->
									<option value="R">Regular</option>
									<option value="K">Konsinyasi</option>
								</select>
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Klasifikasi</span>
								<select id="classification" name="classification" class="form-control" placeholder="Pilih Klasifikasi" >
									<!-- <option value="">Pilih Klasifikasi</option> -->
									<option value="1">PT</option>
									<option value="2">CV / UD</option>
									<option value="3">Perorangan</option>
								</select>
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Code</span>
								<input type="text" id="code" name="code" class="form-control" readonly>
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Nama</span>
									<input type="text" id="name" name="name" class="form-control">
									<span class="input-group-addon"><i class="icon-truck"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Alamat</span>
									<input type="text" id="address" name="address" class="form-control">
									<span class="input-group-addon"><i class="icon-credit-card"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">No Telp</span>
									<input type="text" id="phone" name="phone" class="form-control">
									<span class="input-group-addon"><i class="halfling-phone"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Nama Bank</span>
									<input type="text" id="bank_name" name="bank_name" class="form-control">
									<span class="input-group-addon"><i class="icon-credit-card"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">No. Rekening</span>
									<input type="text" id="bank_account" name="bank_account" class="form-control">
									<span class="input-group-addon"><i class="icon-credit-card"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Tipe Pembayaran</span>
									<input type="text" id="payment" name="payment" class="form-control">
									<span class="input-group-addon"><i class="icon-money"></i></span>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Nama PIC</span>
									<input type="text" id="pic_name" name="pic_name" class="form-control">
									<span class="input-group-addon"><i class="icon-user"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Jabatan PIC</span>
									<input type="text" id="pic_position" name="pic_position" class="form-control">
									<span class="input-group-addon"><i class="icon-user"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">No. Telp PIC</span>
									<input type="text" id="pic_phone" name="pic_phone" class="form-control">
									<span class="input-group-addon"><i class="halfling-phone"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Email</span>
									<input type="text" id="pic_email" name="pic_email" class="form-control">
									<span class="input-group-addon"><i class="icon-envelope-alt"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Kode Akun</span>
									<input type="text" id="account_code" name="account_code" class="form-control">
									<span class="input-group-addon"><i class="icon-credit-card"></i></span>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Status</span>
									<select id="status" name="status" class="form-control" placeholder="Status">
										<!-- <option value=""></option> -->
										<option value="1">Aktif</option>
										<option value="0">Non Aktif</option>
									</select>
									<span class="input-group-addon"><i class="icon-eye-open"></i></span>
								</div>
							</div>
						</div>
						<input type="hidden" id="id" name="id" class="form-control">
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function begin (argument) {
	$('#type').focus();
	var id = '<?php echo $this->uri->segment(4); ?>';
	if(id){
		// alert(id);
		$('#type').attr("disabled", true);
		$('#classification').attr("disabled", true);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url($this->cname).'/get_data'; ?>",
			data: {id:id},
			success: function(msg)
			{
				// alert(msg);
				data = JSON.parse(msg);
				$('#id').val(data.id);
				$('#type').val(data.type);
				$('#classification').val(data.classification);
				$('#code').val(data.code);
				$('#name').val(data.name);
				$('#address').val(data.address);
				$('#phone').val(data.phone);
				$('#bank_name').val(data.bank_name);
				$('#bank_account').val(data.bank_account);
				$('#payment').val(data.payment);
				$('#pic_name').val(data.pic_name);
				$('#pic_position').val(data.pic_position);
				$('#pic_phone').val(data.pic_phone);
				$('#pic_email').val(data.pic_email);
				$('#account_code').val(data.account_code);
				$('#status').val(data.status);
			}
		});
		return false;
	}
}
$(document).ready(function(){
	begin();
	$('#type').change(function(){
		// alert('sip');
		var id = $('#id').val();
		if(id==''){
			// alert('sip');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url($this->cname).'/get_code'; ?>",
				data: {type: $('#type').val(), classification: $('#classification').val()},
				success: function(msg)
				{
					// alert(msg); 
					$('#code').val(msg);
				}
			});
			return false;
		}
	});
	$('#classification').change(function(){
		var id = $('#id').val();
		if(id==''){
			$.ajax({
				type: "POST",
				url: "<?php echo base_url($this->cname).'/get_code'; ?>",
				data: {type: $('#type').val(), classification: $('#classification').val()},
				success: function(msg)
				{
					// alert(msg); 
					$('#code').val(msg);
				}
			});
			return false;
		}
	});
	$('#formPemasok').submit(function(){
		// alert(sip);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url($this->cname).'/ins_pemasok'; ?>",
			data: $('#formPemasok').serialize(),
			success: function(msg)
			{
				data = msg.split("|");
				if(data[0]==1){
					clear();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	function clear() {
		$('#type').val('');
		$('#classification').val('');
		$('#code').val('');
		$('#name').val('');
		$('#address').val('');
		$('#phone').val('');
		$('#bank_name').val('');
		$('#bank_account').val('');
		$('#payment').val('');
		$('#pic_name').val('');
		$('#pic_position').val('');
		$('#pic_phone').val('');
		$('#pic_email').val('');
		$('#account_code').val('');
		$('#status').val('');
		$('#id').val('');
	}
});
</script>