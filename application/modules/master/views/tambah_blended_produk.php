<?php $user = $this->session->userdata('basmalahsession'); ?>
<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/blended/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/blended/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master/setup'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Menu Setup</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<!-- Start Content -->
		<div class="row">
			<div class="col-sm-12">
				<span id="warehouse" style="display:none">1</span>
				<span id="user" style="display:none"><?php echo $user[0]->id; ?></span>
				<div class="block full" style="margin-top:10px;" id="begin-div">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">Kode Produk</span>
							<input type="text" id="kode_produk_begin" name="kode_produk_begin" class="form-control">
							<span class="input-group-addon"><i class="icon-key"></i></span>
						</div>
					</div>
				</div>
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<span id="flash_message"></span>
					<form id="addProduk" method="post" class="form-horizontal" onSubmit="return false;">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Departemen</span>
								<?php echo form_dropdown('department', @$cat_dept, @$val['id_dept'], 'class="form-control size="1" id="department"'); ?>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kategori</span>
								<input type="hidden" id="id" name="id" value="<?php echo @$val['id']; ?>" class="form-control" placeholder="Kode Produk">
								<select id="kategori" name="category" value="<?php echo @$val['category']; ?>" class="form-control" data-placeholder="Pilih Sub Kategori">
									<option value="">Pilih Kategori</option>
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Sub Kategori</span>
								<select id="sub_kategori" name="subcategory" value="<?php echo @$val['subcategory']; ?>" class="form-control" data-placeholder="Pilih Sub Kategori">
									<option value="">Pilih Sub Kategori</option>
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode Produk</span>
								<input type="text" id="kode_produk" name="code" value="<?php echo @$val['code']; ?>" class="form-control" placeholder="Kode Produk">
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama Produk</span>
								<input type="text" id="nama_produk" name="name" value="<?php echo @$val['name']; ?>" class="form-control" placeholder="Nama Produk">
								<span class="input-group-addon"><i class="icon-user"></i></span>
							</div>
						</div>
						<div class="form-group">
							<!-- <input type="hidden" name="edit" id="edit"  /> -->
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
				<div class="block full" style="margin-top:10px;" id="blended-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Tambah Produk Blended</p>
							</blockquote>
						</div>
					</div>
					<span id="flash_message_2"></span>
					<form id="addDetailBlended" method="post">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama Blended</span>
								<input type="hidden" id="barcode_blended" name="barcode_blended" class="form-control" placeholder="id paket">
								<select id="barcode_product" name="barcode_product" class="form-control" data-placeholder="Pilih Product">
									<option value="">Pilih Product</option>
								</select>
								<span class="input-group-addon"><i class="icon-gift"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Jumlah</span>
								<input type="text" id="qty_product" name="qty_product" class="form-control" placeholder="Jumlah Produk Blended">
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Detail Blended</p>
							</blockquote>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center">Barcode</th>
									<th>Nama</th>
									<th>Jumlah</th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody id="list_detail_blended">
								
							</tbody>
						</table>
					</div>
				</div>
				<div class="block full" style="margin-top:10px;" id="data-div">
					<div class="row">
						<div class="col-sm-4">
							<blockquote>
								<p><i class="icon-file-text"></i> Data Blended</p>
							</blockquote>
						</div>
					</div>
					<div class="table-responsive">
						<table id="example-datatable" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Barcode</th>
									<th>Nama</th>
									<th>Price1</th>
									<th>Price2</th>
									<th>Price3</th>
									<th>Rak</th>
									<th width="150px"></th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- End Content -->
	</div>
</div>
<div id="modal-add" class="modal" aria-hidden="true" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>
				<h4 class="modal-title">Edit Quantity</h4>
			</div>
			<div class="modal-body">
				<div class="row" style="margin-bottom: 10px;">
					<form id="formEdit" method="post">
						<div class="form-group" style="display:none">
							<div class="input-group">
								<span class="input-group-addon">Produk</span>
								<input type="text" id="id_edit" name="id_edit" class="form-control">
								<span class="input-group-addon"><i class="icon-gift"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Quantity</span>
								<input type="text" id="qty_edit" name="qty_edit" class="form-control">
								<span class="input-group-addon"><i class="icon-credit-card"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		webApp.datatables(),
		$("#example-datatable").dataTable({
			aoColumnDefs:[{
				bSortable:!1,
				aTargets:[4]
			}],
			bProcessing:true,
			bServerSide:true,
			sAjaxSource: "<?php echo base_url(); ?>master/_datatable/blended/",
			iDisplayLength:15,
			aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
			"fnServerData": function(sSource, aoData, fnCallback){
			    $.ajax(
			       	{
			       	  'dataType': 'json',
			       	  'type'  : 'POST',
			       	  'url'    : sSource,
			       	  'data'  : aoData,
			       	  'success' : fnCallback
			       	}
			    );
			} 
		}),
		$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
	});
</script>
<script type="text/javascript">
function begin(){
	var id = "<?php echo $this->uri->segment(5); ?>";
	var val = "<?php echo $this->uri->segment(4); ?>";
	// alert(id);
	$("#edit").val('tambah');
	if(id){
		$("#begin-div").hide();
		$("#blended-div").hide();
		$("#tambah-div").show();
		$("#data-div").hide();
		$("#id").val(id);
		var url = "<?php echo base_url(); ?>master/produk/detail_produk";
		// alert('asas');
		var form_data = {
			id: id
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				var obj = jQuery.parseJSON(msg);
				var qty = ""+jQuery.parseJSON(obj.qty);
				
				var dataqty = qty.split(",");
				// alert(msg);
				$("#department").val(obj.id_dept);
				$("#id").val(obj.id);
				$("#sub_kategori").val(obj.subcategory);
				$("#kode_produk").val(obj.code);
				$("#nama_produk").val(obj.name);
				// $("#price1").val(obj.price1);
				// $("#price2").val(obj.price2);
				// $("#price3").val(obj.price3);
				// $("#qty1").val(dataqty[0]);
				// $("#qty2").val(dataqty[1]);
				// $("#qty3").val(dataqty[2]);
				$("#edit").val('update');

				kategori(obj.id_dept,obj.category_id);
				sub_kategori(obj.category_id,obj.subcategory);
			}
		});
		return false;
	} else {
		if(val=='tambah'){
			$("#kode_produk_begin").focus();
			$("#tambah-div").hide();
			$("#data-div").hide();
		} else if(val=='data'){
			$("#begin-div").hide();
			$("#blended-div").hide();
			$("#tambah-div").hide();
			$("#data-div").show();
		}
	}
	$('#blended-div').hide();
	optProduct();
	cekBlended();
}
function optProduct(){
	var barcode = $('#barcode_blended').val();
	var url = "<?php echo base_url(); ?>master/produk/opt_for_blended";
	var form_data = {
		barcode: barcode
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(msg)
		{
			// alert(obj);
			$("#barcode_product").html(msg);
			$("#barcode_product").trigger("chosen:updated");
		}
	});
	return false;
}
function kategori(Object,id){

	var kategori = $('#kategori').val();
	var sub = Object;
	var url = "<?php echo base_url(); ?>master/produk/opt_kategori";
	var form_data = {
		id_dept: sub
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(msg)
		{
			// alert(msg);
			$("#kategori").html(msg);
			$("#kategori").val(id);
		}
	});
	return false;
}
function sub_kategori(Object,id){
	var kategori = $('#kategori').val();
	var sub = Object;
	var url = "<?php echo base_url(); ?>master/produk/opt_sub_kategori";
	var form_data = {
		category_id: sub
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(msg)
		{
			// alert(msg);
			$("#sub_kategori").html(msg);
			$("#sub_kategori").val(id);
		}
	});
	return false;
}
function cekBlended(){
	var id = $('#barcode_blended').val();
	var url = "<?php echo base_url(); ?>master/produk/cek_blended";
	var form_data = {
		barcode_blended: id
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(msg)
		{
			// alert(msg);
			$('#list_detail_blended').html(msg);
			optProduct();
		}
	});
	return false;
}
function actDetail(Object){
	var url = "<?php echo base_url(); ?>master/produk/blended_detail";
	var form_data = {
		id: Object
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(msg)
		{
			// alert(msg);
			obj = JSON.parse(msg);
			// alert(obj.id);
			$('#id_edit').val(obj.id);
			$('#qty_edit').val(obj.qty_product);
			$('#modal-add').modal('show');
		}
	});
	return false;
}
function actDelete(Object){
	// alert(Object);
	alertify.confirm("Apakah anda yakin untuk menghapus item ini?", function (e) {
		if (e) {
			var url = "<?php echo base_url(); ?>master/produk/delete_detail_blended";
			var form_data = {
				id: Object
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					data = msg.split("|");
					if(data[0]==1){
						cekBlended();
					}
					$("#flash_message").show();
					$("#flash_message").html(data[1]);
					setTimeout(function() {$("#flash_message").hide();}, 3000);
				}
			});
			return false;
		} else {
			
		}
	});
	return false;
}
function actUnblend(Object){
	alertify.confirm("Apakah anda yakin untuk melakukan proses unblended?", function (e) {
		if (e) {
			var url = "<?php echo base_url(); ?>master/produk/un_blended";
			var warehouse = $('#warehouse').html();
			var operator = $('#user').html();
			var form_data = {
				id: Object,
				warehouse: warehouse,
				operator: operator
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					data = msg.split("|");
					if(data[0]==1){
						$("#example-datatable").dataTable().fnDraw();
					} else if(data[0]==2){
						alertify.alert('Maaf, produk tidak ada dalam stok gudang. [belum di stok]');
					}
					$("#flash_message").show();
					$("#flash_message").html(data[1]);
					setTimeout(function() {$("#flash_message").hide();}, 3000);
				}
			});
			return false;
		} else {
			
		}
	});
	return false;
}
$(document).ready(function(){
	begin();
	$("#kode_produk_begin").change(function(){
		var kode_produk = $("#kode_produk_begin").val();
		$("#kode_produk").val(kode_produk);
		$("#begin-div").hide();
		$("#tambah-div").show();
	});
	$("#department").change(function(evt){
		evt.preventDefault()
		var department = $('#department').val();
		var url = "<?php echo base_url(); ?>master/produk/opt_kategori";
		var form_data = {
			id_dept: department
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$("#kategori").html(msg);
			}
		});
		return false;
	});
	$("#kategori").change(function(evt){
		evt.preventDefault()
		var kategori = $('#kategori').val();
		// alert(kategori);
		var url = "<?php echo base_url(); ?>master/produk/opt_sub_kategori";
		var form_data = {
			category_id: kategori
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$("#sub_kategori").html(msg);
			}
		});
		return false;
	});
	$('#formEdit').submit(function(){
		var id = $('#id_edit').val();
		var qty_product = $('#qty_edit').val();
		var url = "<?php echo base_url(); ?>master/produk/edit_detail_blended";
		var form_data = {
			id: id,
			qty_product: qty_product
		};
		$('#modal-add').modal('hide');
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				if(data[0]==1){
					cekBlended();
				}
				$("#flash_message_2").show();
				$("#flash_message_2").html(data[1]);
				$('#id_edit').val('');
				$('#qty_edit').val('');
				setTimeout(function() {$("#flash_message_2").hide();}, 5000);
				
			}
		});
		return false;
	});
	$('#addDetailBlended').submit(function() {
		var barcode_blended = $('#barcode_blended').val();
		var barcode_product = $('#barcode_product').val();
		var qty_product = $('#qty_product').val();
		var url = "<?php echo base_url(); ?>master/produk/tambah_detail_blended";
		var form_data = {
			barcode_blended: barcode_blended,
			barcode_product: barcode_product,
			qty_product: qty_product

		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				data = msg.split("|");
				if(data[0]==1){
					cekBlended();
				}
				$("#flash_message_2").show();
				$("#flash_message_2").html(data[1]);
				setTimeout(function() {$("#flash_message_2").hide();}, 5000);
				$('#barcode_product').val('');
				$('#qty_product').val('');
			}
		});
		return false;
	});
	$("#addProduk").submit(function(){
		var department = $("#department").val();
		var cd_kategori = $("#kategori option:selected").data('id');
		var cd_sub_kategori = $("#sub_kategori option:selected").data('id');
		var barcode = $('#kode_produk').val();

		if(!cd_sub_kategori){
			cd_sub_kategori = 0;
		}

		var url = "<?php echo base_url(); ?>master/produk/tambah_produk";
		var code_group = department+cd_kategori+cd_sub_kategori;
		var data_post = $('#addProduk').serializeArray();
		data_post.push({name: 'code_group', value: code_group});

		$.ajax({
			type: "POST",
			url: url,
			// data: form_data,
			data: data_post,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				if(data[0]==1){
					clear();
					$('#barcode_blended').val(barcode);
					$('#blended-div').show();
					$('#tambah-div').hide();
					cekBlended();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	function clear(){
		$("#id").val('');
		$("#kategori").val('');
		$("#sub_kategori").val('');
		$("#kode_produk").val('');
		$("#nama_produk").val('');
		$("#price1").val('');
		$("#price2").val('');
		$("#price3").val('');
		$("#qty1").val('');
		$("#qty2").val('');
		$("#qty3").val('');
	}

	$('#qty2').change(function(){
		var qty1 = parseInt($('#qty1').val());
		var qty2 = parseInt($('#qty2').val());
		if (qty2 <= qty1) {
			alertify.alert('Jumlah maksimal harga 2 harus lebih besar dari jumlah maksimal harga 1');
			$('#qty2').val('');
			$('#qty3').val('');
		}else{
			var qty3 = qty2 + 1;
			$('#qty3').val(qty3);
		}
	});
});
</script>