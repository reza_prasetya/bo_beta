<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/cari"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Cari</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="master-akun">
					<div class="row">
						<div class="col-sm-12">
							<blockquote>
								<p><i class="icon-file-text"></i> Data Pemasok</p>
							</blockquote>
						</div>
					</div>
					<div class="table-responsive">
						<table id="example-datatable" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th width="50px" class="text-center">No</th>
									<th width="120px">Kode</th>
									<th width="180px">Nama</th>
									<th>Alamat</th>
									<th width="140">No Telp</th>
									<th width="180px">PIC</th>
									<th class="text-center" width="110px">Aksi</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	webApp.datatables(),
	$("#example-datatable").dataTable({
		fnDrawCallback: function ( oSettings ) {
			/* Need to redo the counters if filtered or sorted */
			if ( oSettings.bSorted || oSettings.bFiltered )
			{
				for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
				{
					$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
				}
			}
		},
		aoColumnDefs: [
			{ "bSortable": false, "aTargets": [ 0,6 ] }
		],
		// aoColumnDefs:[{
		// 	bSortable:!1,
		// 	aTargets:[4]
		// }],
		bProcessing:true,
		bServerSide:true,
		sAjaxSource: "<?php echo base_url(); ?>master/_datatable/pemasok/",
		iDisplayLength:15,
		aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
		"fnServerData": function(sSource, aoData, fnCallback){
		    $.ajax(
		       	{
		       	  'dataType': 'json',
		       	  'type'  : 'POST',
		       	  'url'    : sSource,
		       	  'data'  : aoData,
		       	  'success' : fnCallback
		       	}
		    );
		} 
	}),
	$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
});
function actDelete(Object){
	// alert(Object);
	alertify.confirm("Apakah anda yakin untuk menghapus item ini?", function (e) {
		if (e) {
			var url = "<?php echo base_url(); ?>master/pemasok/delete_pemasok";
			var form_data = {
				id: Object
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					data = msg.split("|");
					if(data[0]==1){
						$("#example-datatable").dataTable().fnDraw();
					}
					$("#flash_message").show();
					$("#flash_message").html(data[1]);
					setTimeout(function() {location.reload();}, 5000);
				}
			});
			return false;
		} else {
			
		}
	});
	return false;
}
</script>