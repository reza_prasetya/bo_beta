<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/tambah"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Tambah</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Daftar List</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/cari"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Cari</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url().$cname; ?>/menu"><img src="<?php echo base_url(); ?>public/images/icon/kategori.png" /><span>Menu Produk</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="page-content" class="block" style="min-height:500px;">
		<!-- Start Content -->
		<div class="row">
			<div class="col-sm-12">
				<div class="block full" style="margin-top:10px;" id="begin-div">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">Kode Produk</span>
							<input type="text" id="kode_produk_begin" name="kode_produk_begin" class="form-control">
							<span class="input-group-addon"><i class="icon-key"></i></span>
						</div>
					</div>
				</div>
				<div class="block full" style="margin-top:10px;" id="tambah-div">
					<span id="flash_message"></span>
					<form id="addProduk" method="post" class="form-horizontal" onSubmit="return false;">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Departemen</span>
								<?php echo form_dropdown('department', @$cat_dept, @$val['id_dept'], 'class="form-control size="1" id="department" data-placeholder="Pilih Department"'); ?>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kategori</span>
								<input type="hidden" id="id" name="id" value="<?php echo @$val['id']; ?>" class="form-control" placeholder="Kode Produk">
								<select id="kategori" name="category" value="<?php echo @$val['category']; ?>" class="form-control" data-placeholder="Pilih Kategori">
									<option value="">Pilih Kategori</option>
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Sub Kategori</span>
								<select id="sub_kategori" name="subcategory" value="<?php echo @$val['subcategory']; ?>" class="form-control" data-placeholder="Pilih Sub Kategori">
									<option value="">Pilih Sub Kategori</option>
								</select>
								<span class="input-group-addon"><i class="icon-list"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Kode Produk</span>
								<input type="text" id="kode_produk" name="code" value="<?php echo @$val['code']; ?>" class="form-control" placeholder="Kode Produk">
								<span class="input-group-addon"><i class="icon-key"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Nama Produk</span>
								<input type="text" id="nama_produk" name="name" value="<?php echo @$val['name']; ?>" class="form-control" placeholder="Nama Produk">
								<span class="input-group-addon"><i class="icon-gift"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Satuan Kemasan</span>
								<input type="text" id="unit" name="unit" value="<?php echo @$val['unit']; ?>" class="form-control" placeholder="Satuan Produk">
								<span class="input-group-addon"><i class="icon-gift"></i></span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Diskon Produk (%)</span>
								<input type="text" id="discount" name="discount" value="<?php echo @$val['discount']; ?>" class="form-control numeric" placeholder="Diskon Produk">
								<span class="input-group-addon">%</span>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Status</span>
								<select id="status" name="status" class="form-control">
									<option value="1">Aktif</option>
									<option value="0">Tidak Aktif</option>
								</select>
								<span class="input-group-addon"><i class="icon-eye-open"></i></span>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- End Content -->
	</div>
</div>
<script type="text/javascript">
function numeric(){
	$(".numeric").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
}
function begin(){
	var id = "<?php echo $this->uri->segment(4); ?>";
	// alert(id);
	$("#edit").val('tambah');
	if(id){
		$("#begin-div").hide();
		$("#tambah-div").show();
		$("#id").val(id);
		var url = "<?php echo base_url(); ?>master/produk/detail_produk";
		// alert('asas');
		var form_data = {
			id: id
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				
				var obj = jQuery.parseJSON(msg);
				var qty = ""+jQuery.parseJSON(obj.qty);
				
				var dataqty = qty.split(",");
				$("#department").val(obj.department_id);
				$("#id").val(obj.id);
				$("#sub_kategori").val(obj.subcategory);
				$("#kode_produk").val(obj.code);
				$("#nama_produk").val(obj.name);
				$("#price1").val(obj.price1);
				$("#price2").val(obj.price2);
				$("#price3").val(obj.price3);
				$("#unit").val(obj.unit);
				$("#discount").val(obj.discount);
				$("#qty1").val(dataqty[0]);
				$("#qty2").val(dataqty[1]);
				$("#qty3").val(dataqty[2]);
				$("#rak").val(obj.rak);
				$("#edit").val('update');

				kategori(obj.department_id,obj.category_id);
				sub_kategori(obj.category_id,obj.subcategory_id);

			}
		});
		return false;
	} else {
		$("#kode_produk_begin").focus();
		$("#tambah-div").hide();
	}
}
function kategori(Object,id){

	var kategori = $('#kategori').val();
	var sub = Object;
	var url = "<?php echo base_url(); ?>master/produk/opt_kategori";
	var form_data = {
		id_dept: sub
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(msg)
		{
			// alert(msg);
			$("#kategori").html(msg);
			$("#kategori").val(id);
		}
	});
	return false;
}
function sub_kategori(Object,id){

	var kategori = $('#kategori').val();
	var sub = Object;
	var url = "<?php echo base_url(); ?>master/produk/opt_sub_kategori";
	var form_data = {
		category_id: sub
	};
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		success: function(msg)
		{
			// alert(msg);
			$("#sub_kategori").html(msg);
			$("#sub_kategori").val(id);
		}
	});
	return false;
}
$(document).ready(function(){
	begin();
	numeric();
	$("#kode_produk_begin").change(function(){
		var kode_produk = $("#kode_produk_begin").val();
		$("#kode_produk").val(kode_produk);
		$("#begin-div").hide();
		$("#tambah-div").show();
	});
	$("#department").change(function(evt){
		evt.preventDefault()
		var department = $('#department').val();
		var url = "<?php echo base_url(); ?>master/produk/opt_kategori";
		var form_data = {
			id_dept: department
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$("#kategori").html(msg);
			}
		});
		return false;
	});
	$("#kategori").change(function(evt){
		evt.preventDefault()
		var kategori = $('#kategori').val();
		// alert(kategori);
		var url = "<?php echo base_url(); ?>master/produk/opt_sub_kategori";
		var form_data = {
			category_id: kategori
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$("#sub_kategori").html(msg);
			}
		});
		return false;
	});
	$("#addProduk").submit(function(){
		var department = $("#department").val();
		var cd_kategori = $("#kategori option:selected").data('id');
		var cd_sub_kategori = $("#sub_kategori option:selected").data('id');
		var rak = $("#rak").val();

		if(!cd_sub_kategori){
			cd_sub_kategori = 0;
		}

		var url = "<?php echo base_url(); ?>master/produk/tambah_produk";
		var code_group = department+cd_kategori+cd_sub_kategori;
		var data_post = $('#addProduk').serializeArray();
		data_post.push({name: 'code_group', value: code_group});

		$.ajax({
			type: "POST",
			url: url,
			// data: form_data,
			data: data_post,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split("|");
				if(data[0]==1){
					clear();
				}
				$("#flash_message").show();
				$("#flash_message").html(data[1]);
				setTimeout(function() {$("#flash_message").hide();}, 5000);
			}
		});
		return false;
	});
	function clear(){
		$("#id").val('');
		$("#department").val('');
		$("#kategori").val('');
		$("#sub_kategori").val('');
		$("#kode_produk").val('');
		$("#nama_produk").val('');
		$("#price1").val('');
		$("#price2").val('');
		$("#price3").val('');
	}

	$('#qty2').change(function(){
		var qty1 = parseInt($('#qty1').val());
		var qty2 = parseInt($('#qty2').val());
		if (qty2 <= qty1) {
			alert('Jumlah maksimal harga 2 harus lebih besar dari jumlah maksimal harga 1');
			$('#qty2').val('');
			$('#qty3').val('');
		}else{
			var qty3 = qty2 + 1;
			$('#qty3').val(qty3);
		}
	});
});
</script>