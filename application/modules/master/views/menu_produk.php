<div id="fx-container" class="fx-opacity">
	<div id="page-content" class="block" style="height: 150px; border-bottom-left-radius: 3px; border-bottom-right-radius: 3px;">
		<div class="row">
			<div class="col-sm-12">
				<div class="metro_nav" style="padding-right:40px;">
					<ul style="white-space:normal;">
						<li>
							<a href="<?php echo base_url().$cname; ?>/department"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Department</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/kategori"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Kategori</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/sub_kategori"><img src="<?php echo base_url(); ?>/public/images/icon/kategori.png" /><span>Sub Kategori</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/data"><img src="<?php echo base_url(); ?>/public/images/icon/produk.png" /><span>Produk</span></a>
						</li>
						<!-- <li>
							<a href="<?php echo base_url().$cname; ?>/blended"><img src="<?php echo base_url(); ?>/public/images/icon/blended.png" /><span>Blended</span></a>
						</li> -->
						<li>
							<a href="<?php echo base_url().$cname; ?>/diskon"><img src="<?php echo base_url(); ?>/public/images/icon/discount.png" /><span>Discounted</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/rak"><img src="<?php echo base_url(); ?>/public/images/icon/gudang.png" /><span>Rak Produk</span></a>
						</li>
						<li>
							<a href="<?php echo base_url().$cname; ?>/harga_produk"><img src="<?php echo base_url(); ?>/public/images/icon/acc-piutang.png" /><span>Harga Produk</span></a>
						</li>
						<li class="pull-right">
							<a href="<?php echo base_url(); ?>/master"><img src="<?php echo base_url(); ?>/public/images/icon/acc-piutang.png" /><span>Home</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>