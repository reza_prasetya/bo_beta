<div id="page-content" class="block">
	<div class="row">
		<div class="col-sm-12">
			<div class="metro_nav" style="padding-right:40px;">
				<ul style="white-space:normal;">
					<li>
						<a href="<?php echo base_url().$cname.'/data'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Master akun</span></a>
					</li>
					<li>
						<a href="<?php echo base_url().$cname.'/tambah'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Tambah Akun</span></a>
					</li>
					<li class="pull-right">
						<a href="<?php echo base_url().'master'; ?>"><img src="<?php echo base_url(); ?>public/images/icon/master-akun.png" /><span>Home</span></a>
					</li>
				</ul>
			</div>
			
		</div>
	</div>
</div>
<div id="page-content" class="block" style="min-height:500px;">
	<!-- Start Content -->
	<div class="row">
		<div class="col-sm-12">
			<div class="block full" style="margin-top:10px;" id="list-masuk">
				<div class="row">
					<div class="col-sm-4">
						<blockquote>
							<p><i class="icon-file-text"></i> Data Master Akun</p>
						</blockquote>
					</div>
				</div>
				<div class="table-responsive">
					<table id="example-datatable" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th width="50px	" class="text-center">No</th>
								<th width="150px">Kode</th>
								<th>Nama Akun</th>
								<th width="150px">Posisi</th>
								<th width="150px">Keterangan</th>
								<th class="text-center" width="110px"></th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- End Content -->
</div>
<script type="text/javascript">
$(function(){
	webApp.datatables(),
	$("#example-datatable").dataTable({
		fnDrawCallback: function ( oSettings ) {
			/* Need to redo the counters if filtered or sorted */
			if ( oSettings.bSorted || oSettings.bFiltered )
			{
				for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
				{
					$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
				}
			}
		},
		aoColumnDefs: [
			{ "bSortable": false, "aTargets": [ 0,5 ] }
		],
		aaSorting: [[ 1, "asc" ]],
		bProcessing:true,
		bServerSide:true,
		sAjaxSource: "<?php echo base_url(); ?>master/_datatable/master_akun/",
		iDisplayLength:15,
		aLengthMenu:[[15,30,50,-1],[15,30,50,"All"]],
		"fnServerData": function(sSource, aoData, fnCallback){
		    $.ajax(
		       	{
		       	  'dataType': 'json',
		       	  'type'  : 'POST',
		       	  'url'    : sSource,
		       	  'data'  : aoData,
		       	  'success' : fnCallback
		       	}
		    );
		} 
	}),
	$(".dataTables_filter input").addClass("form-control").attr("placeholder","Search")
});
function actDelete(Object){
	// alert(Object);
	alertify.confirm("Apakah anda yakin untuk menghapus item ini?", function (e) {
		if (e) {
			var url = "<?php echo base_url(); ?>master/rekening/delete_rekening";
			var form_data = {
				id: Object
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					data = msg.split("|");
					if(data[0]==1){
						$("#example-datatable").dataTable().fnDraw();
					}
					$("#flash_message").show();
					$("#flash_message").html(data[1]);
					setTimeout(function() {location.reload();}, 5000);
				}
			});
			return false;
		} else {
			
		}
	});
	return false;
}
</script>