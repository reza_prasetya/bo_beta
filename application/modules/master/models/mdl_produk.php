<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_produk extends MY_Model {

     function opt_kategori()
    {
        $sql = "SELECT * FROM atombizz_product_categories";
        $res = $this->db->query($sql);

        if($res->num_rows() > 0) {
            $hasil[''] = "Pilih Kategori";
            foreach($res->result_array() as $val) {
                $hasil[trim($val['id'])] = trim($val['name']);
            }
        } else {
            $hasil[''] = "Data Tidak Ditemukan";
        }

        return $hasil;
    }
    function opt_department()
    {
    	$sql = "SELECT * FROM atombizz_product_department";
    	$res = $this->db->query($sql);

    	if($res->num_rows() > 0) {
    		$hasil[''] = "Pilih Department";
    		foreach($res->result_array() as $val) {
    			$hasil[trim($val['id'])] = trim($val['name']);
    		}
    	} else {
    		$hasil[''] = "Data Tidak Ditemukan";
    	}

    	return $hasil;
    }
    function opt_blended($table,$key)
    {
        $sql = "SELECT * FROM $table WHERE `code` <> '$key'";
        $res = $this->db->query($sql);

        return $res;
    }

    function get_rak()
    {
        $hasil = $this->db->get_where('atombizz_rack', array('status' => 'rak'));
        return $hasil->result();
    }

    function get_hpp($code){
        $sql = "SELECT * FROM atombizz_product_price WHERE code=? ORDER BY id DESC LIMIT 0,1";
        $data = $this->db->query($sql,array($code));
        foreach ($data->result() as $das) {
            $result = $das->harga;
        }
        return (isset($result)) ? $result : 0;
        // $result = $this->db->last_query();
        // return $result;
    } 

    function detail($key){
        $sql = "SELECT * FROM view_products WHERE id=$key";
        $data = $this->db->query($sql);
        foreach ($data->result_array() as $data) {
            $result = $data;
        }
        return $result;
    }  

    function mencari($key){
        $sql = "SELECT * FROM view_products WHERE code LIKE '%$key%' OR name LIKE '%$key%'";
        $data = $this->db->query($sql);
        return $data;
    }  

    function insert_blended($table,$data){
        $save = $this->db->insert($table,$data);
        $id = $this->db->insert_id();
        
        return $id;
    }

    function get_detail($id){
        $sql = "SELECT * FROM view_paket_detail WHERE paket=$id order by id asc";
        $data = $this->db->query($sql);
        $list = '';
        $debit = $kredit = 0;
        foreach ($data->result() as $das) {
            $list .= '
                <tr>
                    <td class="text-center">'.$das->id.'</td>
                    <td>'.$das->product_name.'</td>
                    <td>'.$das->quantity.'</td>
                    <td class="text-center">
                        <a onclick="delObj('.$das->id.')" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-default"><i class="icon-remove"></i></a>
                    </td>
                </tr>
            ';
        }
        return $list;
    }
    function update_where_in($table,$data,$where){
        $this->db->where_in('code',$where);
        $update = $this->db->update($table,$data);
        return $update;
    }

    public function simpan_produk($param='')
    {

        $qty = array(
            $param['qty1'] , 
            $param['qty2'] ,
            $param['qty3'] ,
        );
        $qty =  json_encode($qty); 

        if(!isset($param['subcategory'])){
            $param['subcategory']=0;
        }

        if ($param['edit'] == 'tambah') {
            $sql = "insert into atombizz_product 
                    (code,department,category,subcategory,name,qty,price1,price2,price3,code_group,rak,discount)
                    values(?,?,?,?,?,?,?,?,?,?,?,?)";
            $hasil = $this->db->query($sql,array($param['code'],$param['department'],$param['category'],$param['subcategory'],$param['name'],
                                            $qty,$param['price1'],$param['price2'],$param['price3'],$param['code_group'],$param['rak'],$param['discount']));
        }else if ($param['edit'] == 'update') {
            $sql = "update atombizz_product 
                    set code = ?,department = ?,category= ?,subcategory= ?,name= ?,qty= ?,price1= ?,price2= ?,price3= ?,code_group= ?,rak = ?,discount = ?
                    where id = ? ";
            $hasil = $this->db->query($sql,array($param['code'],$param['department'],$param['category'],$param['subcategory'],$param['name'],
                                            $qty,$param['price1'],$param['price2'],$param['price3'],$param['code_group'],$param['rak'],$param['discount'],$param['id']));
        }

        return $this->db->affected_rows();
    }

    function insert_batch($table,$data){
        if($table == FALSE || $data == FALSE)
        {
            return FALSE;
        }
        else
        {
            if(!is_array($data))
            {
                return FALSE;
            }
            else
            {
                $insert = $this->db->insert_batch($table, $data);
                return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
            }
        }
    }

    function detail_search($key){
        $sql = "SELECT * FROM view_products WHERE `code`='$key' OR `name`='$key' LIMIT 0,1";
        $data = $this->db->query($sql);
        foreach ($data->result_array() as $das) {   
        }

        return $das;
    }

    function hpp($key){
        $sql = "SELECT * FROM view_product_price WHERE `code`='$key' OR `name`='$key' ORDER BY id DESC LIMIT 0,1";
        $data = $this->db->query($sql);
        foreach ($data->result_array() as $das) {   
        }

        return @$das;
    }
}