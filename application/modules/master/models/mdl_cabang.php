<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_cabang extends MY_Model 
{
	function mencari($cat,$key){
		$sql = "SELECT * FROM atombizz_customers WHERE ".$cat." LIKE '%$key%'";
		$data = $this->db->query($sql);
		return $data;
	}

	function detail($key){
		$sql = "SELECT * FROM view_branch WHERE id=$key";
		$data = $this->db->query($sql);
		foreach ($data->result_array() as $data) {
			$result = $data;
		}
		return $result;
	}

	function get_opt_area()
	{
		$sql = "SELECT * FROM atombizz_area";
		$data = $this->db->query($sql);
		return $data;
	}

	function get_opt_business()
	{
		$sql = "SELECT * FROM atombizz_business";
		$data = $this->db->query($sql);
		return $data;
	}
	function cek_kode($code){
		$this->db->where('code', $code);
		$result = $this->db->get('atombizz_branch');
		return $result->result();
	}
	function nomor_urut($param){
        $this->db->where('regency_code',$param);
        $this->db->select_max('no_urut');
        $result = $this->db->get('atombizz_branch');
        $hasil = $result->result();
        if($result->num_rows()) $no = ($hasil[0]->no_urut)+1;
        else $no = 1;
        return $no;
    }
}