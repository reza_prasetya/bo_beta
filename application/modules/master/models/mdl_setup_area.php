<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_setup_area extends MY_Model {

	public function _opt_branch()
	{
		$sql = "SELECT * FROM atombizz_branch WHERE ((NOT area_code <> '') OR ISNULL(area_code)) AND `status` = '1'";
		$query = $this->db->query($sql);
		return $query;
	}

	public function _opt_area()
	{
		$sql = "SELECT * FROM atombizz_area WHERE status = '1'";
		$query = $this->db->query($sql);
		return $query;
	}

	public function setup_area($param='')
	{
		$data = array('area_code'=>$param['area']);
		$this->db->where_in('code',$param['branch']);
		$update = $this->db->update('atombizz_branch',$data);
		return $update;
	}
}

/* End of file mdl_setup_area.php */
/* Location: ./application/modules/master/models/mdl_setup_area.php */