<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_karyawan extends MY_Model {

    function opt_jabatan()
    {
    	$sql = "SELECT * FROM atombizz_employee_position";
    	$res = $this->db->query($sql);

    	if($res->num_rows() > 0) {
    		$hasil[''] = "Pilih Jabatan";
    		foreach($res->result_array() as $val) {
    			$hasil[trim($val['id'])] = trim($val['group']);
    		}
    	} else {
    		$hasil[''] = "No Category Found";
    	}

    	return $hasil;
    }

    function mencari($key){
		$sql = "SELECT * FROM view_employee WHERE nama LIKE '%$key%' OR alamat LIKE '%$key%' OR telp LIKE '%$key%'";
		$data = $this->db->query($sql);
		return $data;
	}

	function detail($key){
		$sql = "SELECT * FROM view_employee WHERE id=$key";
		$data = $this->db->query($sql);
		foreach ($data->result_array() as $data) {
            $data['upass'] = paramDecrypt($data['upass']);
            // $data['confirm-upass'] = $data['upass'];
			$result = $data;
		}
		return $result;
	}

    function cek_data($table, $param){
        $sql = "SELECT COUNT(`id`) AS total FROM atombizz_employee WHERE `nik`= ? OR `no_ktp`= ?";
        $data = $this->db->query($sql, array($param['nik'], $param['no_ktp']));
        $result = $data->result();
        return $result[0]->total;
    }
}