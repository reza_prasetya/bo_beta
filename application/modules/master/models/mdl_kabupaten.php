<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_kabupaten extends MY_Model 
{
	function get_all_kab(){
		$result = $this->db->get('atombizz_regency');
		return $result->result();
	}
	function detail($id){
		$this->db->where('id',$id);
		$result = $this->db->get('atombizz_regency')->result_array();
		return $result[0];
	}
	
}