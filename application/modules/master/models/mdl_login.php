<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_login extends CI_Model {

	public function login($param='')
	{
		$password = paramEncrypt($param['login-password']);
		
		$query = $this->db->get_where('view_employee', 
			array('uname' => $param['login-email'],'upass' => $password));

		return $query->result();
	}

}

/* End of file mdl_login.php */
/* Location: ./application/modules/cashier/models/mdl_login.php */