<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_setup_produk extends MY_Model {

	public function _opt_product()
	{
		$sql = "SELECT * FROM atombizz_product WHERE NOT supplier_code <> '' OR ISNULL(supplier_code)";
		$query = $this->db->query($sql);
		return $query;
	}

	public function _opt_supplier()
	{
		$sql = "SELECT * FROM atombizz_suppliers";
		$query = $this->db->query($sql);
		return $query;
	}

	public function setup_product($param='')
	{
		$data = array('supplier_code'=>$param['supplier']);
		$this->db->where_in('code',$param['product']);
		$update = $this->db->update('atombizz_product',$data);
		return $update;
	}
}

/* End of file mdl_setup_produk.php */
/* Location: ./application/modules/master/models/mdl_setup_produk.php */