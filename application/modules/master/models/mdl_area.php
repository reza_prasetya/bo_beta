<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_area extends MY_Model 
{
	function get_all_kab(){
		$result = $this->db->get('atombizz_area');
		return $result->result();
	}
	function detail($id){
		$this->db->where('id',$id);
		$result = $this->db->get('atombizz_area')->result_array();
		return $result[0];
	}
	function gen_kode(){
        $this->db->select_max('no_urut');
        $result = $this->db->get('atombizz_area');
        $hasil = $result->result();
        if($result->num_rows()) $no = ($hasil[0]->no_urut)+1;
        else $no = 1;
        
        return $no;
    }
}