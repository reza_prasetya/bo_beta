<?php

if(!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class VTI_Controller extends MX_Controller
{        
    function __construct()
    {
        parent::__construct();

        $vtisession = @$this->session->userdata('vtisession');

        if($vtisession == NULL ) {
            redirect('authenticate');
        }
    }
}

