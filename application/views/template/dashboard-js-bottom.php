<div id="metro">
	<div class="metro-layout">
		<div class="header">
			<h1 style="color:#fff;">APPLICATION</h1>
			<div class="controls">
				<span class="next" title="Scroll left"></span>
				<span class="prev" title="Scroll right"></span>
			</div>
		</div>
	</div>
</div>
<a href="javascript:void(0)" id="to-top"><i class="icon-angle-up"></i></a>
<script src="<?php echo base_url(); ?>public/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/metro.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/<?php echo $this->uri->segment(1); ?>.js"></script>
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>public/js/vendor/bootstrap.min-1.03.js"></script>
<script src="<?php echo base_url(); ?>public/js/plugins-1.03.js"></script>
<script src="<?php echo base_url(); ?>public/js/main-1.03.js"></script>