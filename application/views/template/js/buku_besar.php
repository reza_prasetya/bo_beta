<a href="javascript:void(0)" id="to-top"><i class="icon-angle-up"></i></a>
<script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main-1.03.js"></script>
<script>
$(function () {
	$("#modal-compose").on("shown.bs.modal", function () {
		$(".modal-select-chosen").chosen({
			width : "100%"
		})
	}),
	$(".ms-message-list").slimScroll({
		height : 610,
		color : "#000000",
		size : "3px",
		touchScrollStep : 750
	});
	var e = $(".ms-categories");
	$("a", e).click(function () {
		$("li", e).removeClass("active"),
		$(this).parent().addClass("active"),
		$(this).data("cat")
	});
	buku();
});
</script>
<script type="text/javascript">
	var col;
	function ChangeCol(){
		if(col == 1){
	colors = new Array('#4D3D98', '#0C8B44', '#88288F', '#D24827', '#00A0B1'); /* Colors of the Menu Items */
	body_color = "#1C1651"; /* Background Color of the whole Navigation */
	hover_color = "#352869"; /* Color of the border of the menu items for HOVER*/
	scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
	scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
	scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
	$("div.metro_nav").show();
	$("div.metro_na").hide();
		}else if(col == 2){
	colors = new Array('#4472B8', '#56439B', '#069547', '#B41E43', '#D74F2B');
	body_color = "#1D1D1D"; /* Background Color of the whole Navigation */
	hover_color = "#2D2D2D"; /* Color of the border of the menu items for HOVER*/
	scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
	scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
	scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
	$("div.metro_nav").show();
	$("div.metro_na").hide();
		}else if(col == 3){
			$("div.metro_nav").hide();
			$("div.metro_na").show();
		}
		setColors();
	}
</script>
<script type="text/javascript">
	function e(e, t) {
		return Math.floor(Math.random() * (t - e + 1)) + e
	}
	function toggleButton(){
		if($('.hide-key').is(':visible')){
			$('.hide-key').hide();
		} else {
			$('.hide-key').show();
		}
	}
	function generate(){
		var t = 0;
		$(".progress-bar", ".bars-container").each(function () {
			t = e(10, 100) + "%",
			$(this).css("width", t).html(t)
		});
	}
	function buku(){
		$('#pilih-akun').slideDown();
		$('#list-buku').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function importButton(){
		$('#pilih-akun').slideUp();
		$('#list-buku').slideUp();
		$('#import-div').slideDown();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function exportButton(){
		$('#pilih-akun').slideUp();
		$('#list-buku').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideDown();
		$('#sync-div').slideUp();
	}
	function syncButton(){
		$('#pilih-akun').slideUp();
		$('#list-buku').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideDown();
		generate();
	}
	function optAkun(){
		var url = "<?php echo base_url(); ?>accounting/_option/opt_akun_list";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$(".akun-list").html(msg);
				$('.akun-list').trigger("chosen:updated");
			}
		});
		return false;
	}
	$(document).ready(function() {
		optAkun();
		$("#akun").change(function() {
			var akun = $('#akun').val();
			var url = "<?php echo base_url(); ?>accounting/buku_besar/buku_besar_list";
			var form_data = {
				akun:akun
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					$('#buku-besar-list').html(msg);
					$('#list-buku').slideDown();
				}
			});
			return false;
		});
	});
</script>