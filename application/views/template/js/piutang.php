<a href="javascript:void(0)" id="to-top"><i class="icon-angle-up"></i></a>
<script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins-1.03.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main-1.03.js"></script>
<script>
$(function () {
	$("#modal-compose").on("shown.bs.modal", function () {
		$(".modal-select-chosen").chosen({
			width : "100%"
		})
	}),
	$(".ms-message-list").slimScroll({
		height : 610,
		color : "#000000",
		size : "3px",
		touchScrollStep : 750
	});
	var e = $(".ms-categories");
	$("a", e).click(function () {
		$("li", e).removeClass("active"),
		$(this).parent().addClass("active"),
		$(this).data("cat")
	});
	begin();
});
</script>
<script type="text/javascript">
	var col;
	function ChangeCol(){
		if(col == 1){
	colors = new Array('#4D3D98', '#0C8B44', '#88288F', '#D24827', '#00A0B1'); /* Colors of the Menu Items */
	body_color = "#1C1651"; /* Background Color of the whole Navigation */
	hover_color = "#352869"; /* Color of the border of the menu items for HOVER*/
	scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
	scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
	scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
	$("div.metro_nav").show();
	$("div.metro_na").hide();
		}else if(col == 2){
	colors = new Array('#4472B8', '#56439B', '#069547', '#B41E43', '#D74F2B');
	body_color = "#1D1D1D"; /* Background Color of the whole Navigation */
	hover_color = "#2D2D2D"; /* Color of the border of the menu items for HOVER*/
	scroll_color = "#5A4B80"; /* Default background color for the scroll arrow */
	scroll_hover = "#FFFFFF"; /* Hover background color for the scroll arrow */
	scroll_click = "#282828"; /* background color for the scroll arrow when clicked */
	$("div.metro_nav").show();
	$("div.metro_na").hide();
		}else if(col == 3){
			$("div.metro_nav").hide();
			$("div.metro_na").show();
		}
		setColors();
	}
</script>
<script type="text/javascript">
	function e(e, t) {
		return Math.floor(Math.random() * (t - e + 1)) + e
	}
	function toggleButton(){
		if($('.hide-key').is(':visible')){
			$('.hide-key').hide();
		} else {
			$('.hide-key').show();
		}
	}
	function generate(){
		var t = 0;
		$(".progress-bar", ".bars-container").each(function () {
			t = e(10, 100) + "%",
			$(this).css("width", t).html(t)
		});
	}
	function bayar(){
		$('#bayar-piutang').slideDown();
		$('#transaksi-piutang').slideUp();
		$('#data-piutang').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function data_piutang(){
		$('#bayar-piutang').slideUp();
		$('#transaksi-piutang').slideUp();
		$('#data-piutang').slideDown();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function data_transaksi(){
		$('#bayar-piutang').slideUp();
		$('#transaksi-piutang').slideDown();
		$('#data-piutang').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function importButton(){
		$('#bayar-piutang').slideUp();
		$('#transaksi-piutang').slideUp();
		$('#data-piutang').slideUp();
		$('#import-div').slideDown();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function exportButton(){
		$('#bayar-piutang').slideUp();
		$('#transaksi-piutang').slideUp();
		$('#data-piutang').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideDown();
		$('#sync-div').slideUp();
	}
	function syncButton(){
		$('#bayar-piutang').slideUp();
		$('#transaksi-piutang').slideUp();
		$('#data-piutang').slideUp();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideDown();
		generate();
	}
	function begin(){
		$('#bayar-piutang').slideUp();
		$('#transaksi-piutang').slideUp();
		$('#data-piutang').slideDown();
		$('#import-div').slideUp();
		$('#export-div').slideUp();
		$('#sync-div').slideUp();
	}
	function no_transfer(){
	    var url = "<?php echo base_url(); ?>accounting/_nomor_urut/no_piutang";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				data = msg.split('|');
				$("#kode").val(data[0]);
				$("#no-bukti").val(data[1]);
			}
		});
		return false;
	}
	function optKas(){
		var url = "<?php echo base_url(); ?>accounting/_option/opt_akun_kas";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$(".akun-kas").html(msg);
				$('.akun-kas').trigger("chosen:updated");
			}
		});
		return false;
	}
	function optEmployee(){
		var url = "<?php echo base_url(); ?>accounting/_option/opt_employee";
		var form_data = {
		};
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			success: function(msg)
			{
				// alert(msg);
				$(".employee").html(msg);
				$('.employee').trigger("chosen:updated");
			}
		});
		return false;
	}
	$(document).ready(function() {
		no_transfer();
		optKas();
		optEmployee();
		$("#dari").change(function() {
			var url = "<?php echo base_url(); ?>accounting/piutang/dari";
			var dari = $("#dari").val();
			var form_data = {
				dari:dari
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					data = msg.split('|');
					$("#nama").val(data[0]);
					$(".faktur").html(msg);
					$('.faktur').trigger("chosen:updated");
				}
			});
			return false;
		});
		$("#faktur").change(function() {
			var url = "<?php echo base_url(); ?>accounting/piutang/faktur";
			var faktur = $("#faktur").val();
			var form_data = {
				faktur:faktur
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					// alert(msg);
					$("#sisa").val(msg);
				}
			});
			return false;
		});
		$("#nominal").change(function() {
			total();
		});

		$("#form-piutang").submit(function() {
			/* Act on the event */
			var url = "<?php echo base_url(); ?>accounting/piutang/insert";
			var dari = $("#dari").val();
			var nama = $("#nama").val();
			var kode = $("#kode").val();
			var referensi = $("#no-bukti").val();
			var akun_kas = $("#akun-kas").val();
			var tanggal = $("#tanggal").val();
			var keterangan = $("#keterangan").val();
			var faktur = $("#faktur").val();
			var nominal = $("#nominal").val();

			var form_data = {
				dari: dari,
				nama: nama,
				kode: kode,
				referensi: referensi,
				akun_kas: akun_kas,
				tanggal: tanggal,
				keterangan: keterangan,
				faktur: faktur,
				nominal: nominal
			};
			$.ajax({
				type: "POST",
				url: url,
				data: form_data,
				success: function(msg)
				{
					if(msg==0){
						alert('gagal');
					} else if(msg==1){
						alert('berhasil');
						clear();
					} else {
						alert(msg);
					}
				}
			});
			return false;
		});

		function total(){
			var nominal = $("#nominal").val();
			var sisa = $("#sisa").val();
			var total = parseInt(sisa)-parseInt(nominal);
			$("#sisa").val(total);
		}
		function clear(){
			no_transfer();
			optKas();
			$("#tanggal").val('');
			$("#keterangan").val('');
			$("#faktur").val('');
			$("#nominal").val('0');
			$("#nominal-denda").val('0');
			$("#tanggal-faktur").val('');
			$("#dari").val('');
		}
	});
</script>