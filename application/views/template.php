<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> 
<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title><?php echo $title; ?></title>

		<?php
			$url = $this->uri->segment(2);
			$this->load->view('template/script');
		?>

	</head>
	<body class="header-fixed-top">

		<?php
			$this->load->view('template/sidebar');
		?>

		<div id="page-container">

			<?php
				$this->load->view('template/header');
				if($url=='dashboard'){
					// do nothing
				} else {
					echo $content;
				}
			?>

		</div>

		<?php
			if($url=='dashboard'){
				$this->load->view('template/dashboard-js-bottom');
			} else {
				$this->load->view('template/js-bottom');
			}
		?>

	</body>
</html>