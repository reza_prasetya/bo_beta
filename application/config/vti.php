<?php
if (!defined("BASEPATH")) exit("No direct script access allowed");
$config["vti"] = array (
  'bas_code_dept' => 'N1-003',
  'bas_regency_code' => 'N1',
  'bas_branch_code' => 'N1-003',
  'bas_branch_name' => 'Gondang Legi',
  'bas_branch_address' => 'Gondang Legi',
  'bas_branch_phone' => '-',
  'bas_branch_email' => 'admin@bo.com',
  'bas_apps_name' => 'Gondang Legi',
)
?>