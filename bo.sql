/*
Navicat MySQL Data Transfer

Source Server         : reza
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : bo

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2014-11-10 18:20:58
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `vti_admin_bank`
-- ----------------------------
DROP TABLE IF EXISTS `vti_admin_bank`;
CREATE TABLE `vti_admin_bank` (
  `admin_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `branch` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_holder_name` varchar(225) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` enum('nonactive','active') COLLATE utf8_unicode_ci DEFAULT 'nonactive',
  `adlog_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vti_admin_bank
-- ----------------------------
INSERT INTO vti_admin_bank VALUES ('1', 'Bank Central Asia ', 'BCA', 'Surabaya', '3880433368', 'Yuliani Tjahyana ', 'nonactive', null);

-- ----------------------------
-- Table structure for `vti_corp_adlog`
-- ----------------------------
DROP TABLE IF EXISTS `vti_corp_adlog`;
CREATE TABLE `vti_corp_adlog` (
  `adlog_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `uname` varchar(30) NOT NULL DEFAULT '',
  `upass` longtext NOT NULL,
  `registered` datetime NOT NULL,
  `privilege` enum('admin','frontliner','rootadmin','verifikasi') DEFAULT NULL,
  `status` enum('block','approved','registered') NOT NULL DEFAULT 'registered',
  `last_login` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `last_ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`adlog_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1309 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_corp_adlog
-- ----------------------------
INSERT INTO vti_corp_adlog VALUES ('1', 'owner', 'a1278bdc01d8113d6a3e71d90f6538f8', '2012-08-25 12:47:36', null, 'approved', '2013-12-12 21:02:52', '202.168.1.21');

-- ----------------------------
-- Table structure for `vti_corp_config`
-- ----------------------------
DROP TABLE IF EXISTS `vti_corp_config`;
CREATE TABLE `vti_corp_config` (
  `corp_name` varchar(255) NOT NULL DEFAULT '',
  `site_logo` varchar(255) NOT NULL DEFAULT '',
  `limit_date` int(10) DEFAULT NULL,
  `referral_percent` int(50) DEFAULT NULL,
  `pairing_percent` int(50) DEFAULT NULL,
  `administration_percent` int(50) DEFAULT NULL,
  PRIMARY KEY (`corp_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_corp_config
-- ----------------------------
INSERT INTO vti_corp_config VALUES ('Vegas Trader Inc', '', null, null, null, null);

-- ----------------------------
-- Table structure for `vti_corp_package`
-- ----------------------------
DROP TABLE IF EXISTS `vti_corp_package`;
CREATE TABLE `vti_corp_package` (
  `package_id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `price` float(50,1) DEFAULT NULL,
  `percentage` int(100) DEFAULT NULL,
  `cashback_time` varchar(50) DEFAULT NULL,
  `additional_time` varchar(50) DEFAULT NULL,
  `status` enum('active','nonactive') NOT NULL DEFAULT 'nonactive',
  `adlog_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_corp_package
-- ----------------------------
INSERT INTO vti_corp_package VALUES ('5', 'paket 1', '1.5', '30', '7', '1', 'active', null);
INSERT INTO vti_corp_package VALUES ('6', 'paket 2', '5.0', '30', '7', '1', 'active', null);
INSERT INTO vti_corp_package VALUES ('7', 'paket 3', '10.0', null, '7', '1', 'active', null);

-- ----------------------------
-- Table structure for `vti_employee`
-- ----------------------------
DROP TABLE IF EXISTS `vti_employee`;
CREATE TABLE `vti_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_id` int(11) DEFAULT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `uname` varchar(30) DEFAULT NULL,
  `upass` longtext,
  `tgl_lahir` date DEFAULT NULL,
  `telp` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `group` int(11) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `workstation` varchar(50) DEFAULT NULL,
  `gaji` bigint(20) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `date_reg` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_employee
-- ----------------------------
INSERT INTO vti_employee VALUES ('1', '3', 'cole', 'fl_cole', 'fb61fd0e3ef8a518fee988a633f955c082a27202', null, null, null, null, null, null, null, null, null, null, null, '10-11-2014');
INSERT INTO vti_employee VALUES ('4', '4', 'ver_cole', 'ver_cole', '44ca459ca3593b4684032f8c7d78ac777eab15ce', null, null, null, null, null, null, null, null, null, null, null, '10-11-2014');

-- ----------------------------
-- Table structure for `vti_employee_access`
-- ----------------------------
DROP TABLE IF EXISTS `vti_employee_access`;
CREATE TABLE `vti_employee_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_id` int(11) NOT NULL,
  `access` longtext NOT NULL,
  `module` longtext,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_employee_access
-- ----------------------------
INSERT INTO vti_employee_access VALUES ('1', '1', '[\"master.produk.department\",\"master.produk.tambah_department\",\"master.produk.delete_department\",\"master.produk.kategori\",\"master.produk.tambah_kategori\",\"master.produk.delete_kategori\",\"master.produk.sub_kategori\",\"master.produk.tambah_sub_kategori\",\"master.produk.delete_sub_kategori\",\"master.produk.tambah_produk\",\"\",\"master.produk.set_rak_produk\",\"master.produk.diskon_multi\",\"master.produk.set_harga_produk\",\"master.produk.tambah_product_blended\",\"master.produk.un_blended\",\"master.pelanggan.tambah_pelanggan\",\"master.pelanggan.delete_pelanggan\",\"master.pemasok.tambah\",\"master.pemasok.delete_pemasok\",\"master.posisi.tambah\",\"master.rak_gudang.tambah\",\"master.rak_gudang.delete_rak\",\"master.karyawan.tambah\",\"master.karyawan.delete_karyawan\"]', '[\"master\"]', null);
INSERT INTO vti_employee_access VALUES ('2', '2', '[\"gudang.mutasi.mutasi_produk\",\"gudang.pembelian.edit\",\"gudang.pembelian.transaksi\",\"gudang.retur.index\",\"gudang.stok.opname\",\"gudang.stok.blended\",\"gudang.stok.validasi_opname\",\"gudang.validasi_kasir.index\"]', '[\"gudang\"]', null);
INSERT INTO vti_employee_access VALUES ('3', '3', '[\"head.head.dashboard\"]', '[\"head\"]', null);
INSERT INTO vti_employee_access VALUES ('4', '4', '[\"cashier.cashier.dashboard\",\"cashier.selling.index\",\"cashier.retur.index\"]', '[\"cashier\"]', null);

-- ----------------------------
-- Table structure for `vti_employee_position`
-- ----------------------------
DROP TABLE IF EXISTS `vti_employee_position`;
CREATE TABLE `vti_employee_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(20) DEFAULT NULL,
  `information` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_employee_position
-- ----------------------------
INSERT INTO vti_employee_position VALUES ('1', 'rootadmin', '');
INSERT INTO vti_employee_position VALUES ('2', 'admin', '');
INSERT INTO vti_employee_position VALUES ('3', 'fl', '');
INSERT INTO vti_employee_position VALUES ('4', 'verifikasi', '');

-- ----------------------------
-- Table structure for `vti_module`
-- ----------------------------
DROP TABLE IF EXISTS `vti_module`;
CREATE TABLE `vti_module` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) DEFAULT NULL,
  `kode` longtext,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_module
-- ----------------------------
INSERT INTO vti_module VALUES ('1', 'master', 'master.produk.department', 'Halaman Department');
INSERT INTO vti_module VALUES ('2', 'master', 'master.produk.tambah_department', 'Tambah Department');
INSERT INTO vti_module VALUES ('3', 'master', 'master.produk.delete_department', 'Delete Department');
INSERT INTO vti_module VALUES ('4', 'master', 'master.produk.kategori', 'Halaman Kategori');
INSERT INTO vti_module VALUES ('5', 'master', 'master.produk.tambah_kategori', 'Tambah Kategori');
INSERT INTO vti_module VALUES ('6', 'master', 'master.produk.delete_kategori', 'Delete Kategori');
INSERT INTO vti_module VALUES ('7', 'master', 'master.produk.sub_kategori', 'Halaman Sub Kategori');
INSERT INTO vti_module VALUES ('8', 'master', 'master.produk.tambah_sub_kategori', 'Tambah Sub Kategori');
INSERT INTO vti_module VALUES ('9', 'master', 'master.produk.delete_sub_kategori', 'Delete Sub Kategori');
INSERT INTO vti_module VALUES ('10', 'master', 'master.produk.tambah_produk', 'Tambah Produk');
INSERT INTO vti_module VALUES ('11', 'master', null, 'Delete Produk');
INSERT INTO vti_module VALUES ('12', 'master', 'master.produk.set_rak_produk', 'Konfig Rak Produk');
INSERT INTO vti_module VALUES ('13', 'master', 'master.produk.diskon_multi', 'Konfig Diskon');
INSERT INTO vti_module VALUES ('14', 'master', 'master.produk.set_harga_produk', 'Konfig Harga Jual Produk');
INSERT INTO vti_module VALUES ('15', 'master', 'master.produk.tambah_product_blended', 'Tambah Produk Blended');
INSERT INTO vti_module VALUES ('16', 'master', 'master.produk.un_blended', 'Unblended Produk');
INSERT INTO vti_module VALUES ('17', 'master', 'master.pelanggan.tambah_pelanggan', 'Tambah Pelanggan');
INSERT INTO vti_module VALUES ('18', 'master', 'master.pelanggan.delete_pelanggan', 'Delete Pelanggan');
INSERT INTO vti_module VALUES ('19', 'master', 'master.pemasok.tambah', 'Tambah Pemasok');
INSERT INTO vti_module VALUES ('20', 'master', 'master.pemasok.delete_pemasok', 'Delete Pemasok');
INSERT INTO vti_module VALUES ('21', 'master', 'master.posisi.tambah', 'Tambah Hak Akses');
INSERT INTO vti_module VALUES ('22', 'master', 'master.rak_gudang.tambah', 'Tambah Rak');
INSERT INTO vti_module VALUES ('23', 'master', 'master.rak_gudang.delete_rak', 'Delete Rak');
INSERT INTO vti_module VALUES ('24', 'master', 'master.karyawan.tambah', 'Tambah Karyawan');
INSERT INTO vti_module VALUES ('25', 'master', 'master.karyawan.delete_karyawan', 'Delete Karyawan');
INSERT INTO vti_module VALUES ('26', 'gudang', 'gudang.mutasi.mutasi_produk_gudang', 'Mutasi Gudang ke Rak');
INSERT INTO vti_module VALUES ('27', 'gudang', 'gudang.pembelian.edit', 'Edit Pembelian Supplier');
INSERT INTO vti_module VALUES ('28', 'gudang', 'gudang.pembelian.transaksi', 'Transaksi Pembelian Supplier');
INSERT INTO vti_module VALUES ('29', 'gudang', 'gudang.retur.index', 'Retur Pembelian');
INSERT INTO vti_module VALUES ('30', 'gudang', 'gudang.stok.opname', 'Stok Opname');
INSERT INTO vti_module VALUES ('31', 'gudang', 'gudang.stok.blended', 'Tambah Stok Produk Blended');
INSERT INTO vti_module VALUES ('32', 'gudang', 'gudang.stok.validasi_opname', 'Validasi Opname');
INSERT INTO vti_module VALUES ('33', 'gudang', 'gudang.validasi_kasir.index', 'Validasi Tutup Kasir');
INSERT INTO vti_module VALUES ('34', 'head', 'head.head.dashboard', 'Dashboard Head');
INSERT INTO vti_module VALUES ('35', 'master', 'master.cabang.tambah', 'Tambah Cabang');
INSERT INTO vti_module VALUES ('36', 'master', 'master.cabang.delete_cabang', 'Delete Cabang');
INSERT INTO vti_module VALUES ('37', 'cashier', 'cashier.cashier.dashboard', 'Dashboard Cashier');
INSERT INTO vti_module VALUES ('38', 'cashier', 'cashier.selling.index', 'Penjualan Outlet');
INSERT INTO vti_module VALUES ('39', 'cashier', 'cashier.retur.index', 'Retur Penjualan Outlet');

-- ----------------------------
-- Table structure for `vti_partner`
-- ----------------------------
DROP TABLE IF EXISTS `vti_partner`;
CREATE TABLE `vti_partner` (
  `partner_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `nik` longtext NOT NULL,
  `referral` varchar(100) NOT NULL,
  `registered` datetime NOT NULL,
  `status` enum('block','approved','registered') NOT NULL DEFAULT 'registered',
  `adlog_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`partner_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1313 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_partner
-- ----------------------------
INSERT INTO vti_partner VALUES ('1', '123456789', '123456789', '2012-08-25 12:47:36', 'registered', null);
INSERT INTO vti_partner VALUES ('2', '321654987', '123456789', '2012-08-28 12:57:06', 'registered', null);
INSERT INTO vti_partner VALUES ('3', '654987321', '123456789', '2012-08-28 12:57:31', 'registered', null);
INSERT INTO vti_partner VALUES ('4', '789456123', '123456789', '2013-12-14 13:21:47', 'registered', null);
INSERT INTO vti_partner VALUES ('5', '456789123', 'demo', '2013-12-14 13:22:48', 'registered', null);

-- ----------------------------
-- Table structure for `vti_partner_bank`
-- ----------------------------
DROP TABLE IF EXISTS `vti_partner_bank`;
CREATE TABLE `vti_partner_bank` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(200) NOT NULL DEFAULT '0',
  `name` varchar(225) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `branch` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `account_holder_name` varchar(225) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `adlog_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vti_partner_bank
-- ----------------------------
INSERT INTO vti_partner_bank VALUES ('1', '1', 'Bank Central Asia ', 'BCA', 'Surabaya', '3880433368', 'Yuliani Tjahyana ', null);

-- ----------------------------
-- Table structure for `vti_partner_deposit`
-- ----------------------------
DROP TABLE IF EXISTS `vti_partner_deposit`;
CREATE TABLE `vti_partner_deposit` (
  `deposit_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `partner_id` bigint(200) NOT NULL DEFAULT '0',
  `package_id` int(11) DEFAULT NULL,
  `completion` int(100) DEFAULT NULL,
  `due_date` date NOT NULL,
  `status` enum('uncomplete','complete') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uncomplete',
  `adlog_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`deposit_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_partner_deposit
-- ----------------------------
INSERT INTO vti_partner_deposit VALUES ('1', '5', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('2', '12', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('3', '6', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('4', '19', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('5', '20', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('6', '21', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('7', '7', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('8', '9', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('9', '10', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('10', '8', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('11', '11', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('12', '14', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('13', '13', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('14', '16', null, null, '0000-00-00', 'uncomplete', null);
INSERT INTO vti_partner_deposit VALUES ('15', '17', null, null, '0000-00-00', 'uncomplete', null);

-- ----------------------------
-- Table structure for `vti_partner_deposit_completion`
-- ----------------------------
DROP TABLE IF EXISTS `vti_partner_deposit_completion`;
CREATE TABLE `vti_partner_deposit_completion` (
  `completion_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `deposit_id` int(255) DEFAULT NULL,
  `partner_id` bigint(200) NOT NULL DEFAULT '0',
  `amount` int(100) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `due_time` time DEFAULT NULL,
  `transfer_date` date DEFAULT NULL,
  `transfer_time` time DEFAULT NULL,
  `status` enum('transfered','pending') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `adlog_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`completion_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_partner_deposit_completion
-- ----------------------------
INSERT INTO vti_partner_deposit_completion VALUES ('1', null, '5', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('2', null, '12', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('3', null, '6', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('4', null, '19', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('5', null, '20', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('6', null, '21', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('7', null, '7', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('8', null, '9', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('9', null, '10', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('10', null, '8', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('11', null, '11', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('12', null, '14', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('13', null, '13', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('14', null, '16', null, null, null, null, null, 'transfered', null);
INSERT INTO vti_partner_deposit_completion VALUES ('15', null, '17', null, null, null, null, null, 'transfered', null);

-- ----------------------------
-- Table structure for `vti_partner_profile`
-- ----------------------------
DROP TABLE IF EXISTS `vti_partner_profile`;
CREATE TABLE `vti_partner_profile` (
  `profil_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `partner_id` varchar(25) NOT NULL DEFAULT '',
  `nickname` varchar(75) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(200) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `primary_mail` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `country` varchar(35) NOT NULL DEFAULT '',
  `city1` varchar(35) DEFAULT NULL,
  `city2` varchar(35) DEFAULT NULL,
  `address1` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `rt-rw` varchar(200) DEFAULT NULL,
  `kel-desa` varchar(200) DEFAULT NULL,
  `kec` varchar(200) DEFAULT NULL,
  `status` enum('BELUM KAWIN','KAWIN') DEFAULT NULL,
  `pekerjaan` varchar(200) DEFAULT NULL,
  `phone1` varchar(30) NOT NULL,
  `phone2` varchar(30) NOT NULL,
  `adlog_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`profil_id`),
  KEY `uid` (`profil_id`) USING BTREE,
  KEY `uname` (`partner_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1313 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_partner_profile
-- ----------------------------
INSERT INTO vti_partner_profile VALUES ('1', '1', 'Irwan', 'male', '1988-12-02', null, null, 'thoniscience@gmail.com', '65153', 'IDN', '947', null, 'THIRD FLOOR 207 REGENT STREET ', '', null, null, null, null, null, '+47543567', '', null);
INSERT INTO vti_partner_profile VALUES ('2', '2', 'Irwin', null, null, null, null, null, null, '65', null, null, 'Orchid Road', '', null, null, null, null, null, '+614356543', '', null);
INSERT INTO vti_partner_profile VALUES ('3', '3', 'Irwun', null, null, null, null, null, null, '62', null, null, '', '', null, null, null, null, null, '', '', null);
INSERT INTO vti_partner_profile VALUES ('4', '4', 'Irwen', 'male', '1970-02-01', null, null, 'asdf@gmail.com', '352345', 'AGO', '58', null, 'sdfgsdfgsdfg', '', null, null, null, null, null, '72634752345', '', null);
INSERT INTO vti_partner_profile VALUES ('5', '5', 'Irwon', 'male', '1970-02-01', null, null, 'rizalnoe.ads@gmail.com', '43434', 'BHS', '148', null, 'sdsdsd', '', null, null, null, null, null, '65676767', '', null);

-- ----------------------------
-- Table structure for `vti_partner_referral`
-- ----------------------------
DROP TABLE IF EXISTS `vti_partner_referral`;
CREATE TABLE `vti_partner_referral` (
  `pr_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `partner_id` bigint(200) NOT NULL,
  `referral_id` bigint(200) NOT NULL DEFAULT '0',
  `amount` decimal(50,0) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `memo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `adlog_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`pr_id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vti_partner_referral
-- ----------------------------
INSERT INTO vti_partner_referral VALUES ('1', '1', '2', '840', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('2', '1', '3', '360', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('3', '1', '4', '360', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('4', '1', '5', '120', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('5', '1', '6', '120', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('6', '1', '7', '120', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('7', '1', '8', '120', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('8', '1', '9', '120', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('9', '1', '10', '120', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('10', '1', '11', '120', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('11', '1', '12', '120', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('12', '1', '13', '120', '2013-11-21 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('16', '1', '14', '120', '2013-11-22 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('17', '2', '15', '120', '2013-11-22 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('18', '2', '16', '120', '2013-11-22 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('19', '2', '17', '12', '2013-11-22 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('20', '2', '18', '12', '2013-11-24 23:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('21', '3', '19', '120', '2013-11-25 01:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('22', '3', '20', '120', '2013-11-25 02:59:36', 'referral bonus', null);
INSERT INTO vti_partner_referral VALUES ('23', '3', '21', '120', '2013-11-25 03:12:07', 'referral bonus', null);

-- ----------------------------
-- Table structure for `vti_partner_wallet`
-- ----------------------------
DROP TABLE IF EXISTS `vti_partner_wallet`;
CREATE TABLE `vti_partner_wallet` (
  `wallet_id` bigint(255) NOT NULL AUTO_INCREMENT,
  `status` enum('partner','admin') DEFAULT 'partner',
  `partner_id` bigint(255) DEFAULT '0',
  `admin_id` bigint(255) DEFAULT NULL,
  `in` decimal(50,0) NOT NULL,
  `out` decimal(50,0) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` enum('WITHDRAW','ADMINISTRATION','REFFERAL') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `memo` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment` enum('transfer','cash') DEFAULT NULL,
  `adlog_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`wallet_id`)
) ENGINE=MyISAM AUTO_INCREMENT=317 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_partner_wallet
-- ----------------------------

-- ----------------------------
-- Table structure for `vti_places_area`
-- ----------------------------
DROP TABLE IF EXISTS `vti_places_area`;
CREATE TABLE `vti_places_area` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `area_name` varchar(200) DEFAULT NULL,
  `dc_name` varchar(200) DEFAULT NULL,
  `alias` varchar(200) DEFAULT NULL,
  `fs_result` varchar(200) DEFAULT NULL,
  `coverage_area` varchar(200) DEFAULT NULL,
  `registered` varchar(200) DEFAULT NULL,
  `status` enum('1','0') DEFAULT NULL,
  `no_urut` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_places_area
-- ----------------------------

-- ----------------------------
-- Table structure for `vti_places_branch`
-- ----------------------------
DROP TABLE IF EXISTS `vti_places_branch`;
CREATE TABLE `vti_places_branch` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `regency_code` varchar(100) NOT NULL,
  `no_urut` int(11) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `registered` date DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `business_code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`,`regency_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vti_places_branch
-- ----------------------------
